<?php
session_start();

if (!isset($_POST['tokenPage']) || !in_array($_POST['tokenPage'], $_SESSION['token']['pages'])) {
  header('Location: ../index.php');
}

error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/model_profile_company.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_profile_company.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");

$modelProfileCompany = new modelProfileCompany();
$controlProfileCompany = new controlProfileCompany();
$functions_sql = new functions_sql();
$messages = new messages_system();
$controlUtilities = new controlUtilities();

switch ($_POST["profileCompanyToken"]) {
	case 'createOfert':
		$values = $functions_sql->functions_sql_clear_dates($_POST);

    // COMENTANDO PAGO DE PSE Y SALDO DE LA CUENTA
    // if ($values['payment_type'] == 'pse') {
    //   // Validar si el psd envia el id de la oferta
    // }if ($values['payment_type'] == 'balance') {
      
    // }else {
    //   header('Location: /views/profile_company.php');
    // }

    $publish_free = $controlProfileCompany->control_profile_company_get_publish_free($_SESSION['user']['id_ci']);

    if ($publish_free['publish_free_ci']  == 0) {
      $values['payment_type'] = 'balance';

      $balances = $controlProfileCompany->control_profile_company_get_balances();
      $value_offer = $controlProfileCompany->control_profile_company_get_value_offer($values['selPriority'], $values['payment_type']);

      if ($balances['available'] < $value_offer) {
        $url = 'https://secureacceptance.allegraplatform.com/botondepago/code-connection-professional-sas/';
        $messege = $messages->messages_failed('insufficient balance failed', NULL, $url);
        $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Saldo Insuficiente!', $messege);
        header('Location: /views/profile_company.php');
        break;
      }
    }
     
		$insert_id_offer = $modelProfileCompany->model_profile_company_insert_ofert_company_prepare($values['itName'], $values['itaDescription'], $values['itakKowledge'], $values['selGenre'],  $values['itProfession'], $values['selValue'], $values['selTypeContract'],  $values['selPriority'], $values['selVacancies'], $values['selCity'], $publish_free['publish_free_ci']);

		if(!empty($insert_id_offer)) {
      if ($publish_free['publish_free_ci'] == 0) {
        $new_available_balance  = $balances['available'] - $value_offer;
        $new_balance_spent = $balances['spent'] + $value_offer;
        $iva_offer = ($value_offer / 100) * 19;
        $base_value_o = $value_offer - $iva_offer;
        $pay_method = 7; // metodo de pago
        // insert bill registre 
        $insert_id_billing = $modelProfileCompany->model_profile_company_insert_billing($insert_id_offer, $value_offer, $iva_offer, $pay_method, $base_value_o);

        // build bill pdf
        $response_bill_pdf = $controlProfileCompany->control_profile_company_create_pdf_bill($value_offer, $insert_id_billing, $insert_id_offer);

        $path = 'bill/';

        $messege = $messages->messages_success('insert ofert success', null, null, $response_bill_pdf, $_POST['tokenPage'], $path);
        $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Oferta Creada!', $messege);

        $response_update_balance = $modelProfileCompany->model_profile_company_update_balances($_SESSION['user']['id_ci'], $new_available_balance, $new_balance_spent);

        $email_support = 'soporte@mihv.com.co';

        if(empty($response_update_balance) || $response_update_balance != 1) {
         
          $from_name = 'MI HV - Fallo Urgente!!!';

          $subject = 'Fallo actualizando saldo disponible de la empresa';
          $content ='
            <div>
              <p>No se puedo actualizar el nuevo saldo disponible de la siguiente empresa:</p>
              <br>
              <p>Id Empresa: ' . $_SESSION['user']['id_ci'] . '</p>
              <p>Nombre Empresa: ' . $_SESSION['user']['name_ci'] . '</p>
              <p>Saldo Anterior: ' . $balances['available'] . '</p>
              <p>Precio Oferta: ' . $value_offer . '</p>
              <p>Nuevo Saldo Disponible: ' . $new_available_balance . ' (Actualizar registro)</p>
              <p>Anterior Saldo Gastado: ' . $balances['spent'] . '</p>
              <p>Nuevo Saldo Gastado: ' . $new_balance_spent . ' (Actualizar registro)</p>
              <br><br>
              <p><b>Nota:</b> si las variables "Nuevo Saldo Disponible" y "Nuevo Saldo Gastado" son false, quiere decir que la oferta se creo gratis</p>
            </div>
          ';

          $response_mail_support = $controlUtilities->control_utilities_send_mail_support($email_support, $content, $subject, $from_name);
        }
      }else {

        $update_publish_free = $modelProfileCompany->model_profile_company_update_publish_free($_SESSION['user']['id_ci']);

        $email_support = 'soporte@mihv.com.co';

        if(empty($update_publish_free) || $update_publish_free != 1) {
         
          $from_name = 'MI HV - Fallo Urgente!!!';

          $update_publish_free_new = $publish_free['publish_free_ci']--;

          $subject = 'Fallo actualizando saldo disponible de la empresa';
          $content ='
            <div>
              <p>No se pudo actualizar la oferta gratis utilizada de la siguiente empresa:</p>
              <br>
              <p>Id Empresa: ' . $_SESSION['user']['id_ci'] . '</p>
              <p>Nombre Empresa: ' . $_SESSION['user']['name_ci'] . '</p>
              <p>Publicar Gratis Anterior: ' . $publish_free['publish_free_ci'] . '</p>
              <p>Publicar Gratis Nuevo(actualizar): ' . $update_publish_free_new . '</p>
              <br><br>
              <p>Actualizar lo mas pronto posible!</p>
            </div>
          ';

          $response_mail_support = $controlUtilities->control_utilities_send_mail_support($email_support, $content, $subject, $from_name);
        }


        $messege = $messages->messages_success('insert offer free success');
        $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Oferta Creada!', $messege);

        
      }
		}else {
      $messege = $messages->messages_failed('insert ofert failed');
      $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Lo Sentimos,', $messege);	
		}

  	header('Location: /views/profile_company.php');	
	break;

 //  // COMENTANDO EDISION DE LA OFERTA
	// case 'editOfert':
	// 	$values = $functions_sql->functions_sql_clear_dates($_POST);
    
 //    $update_ofert = $modelProfileCompany->model_profile_company_update_ofert_company($values['itName'], $values['itaDescription'], $values['itakKowledge'], $values['selGenre'], $values['itProfession'], $values['selValue'], $values['selTypeContract'],  $values['selPriority'], $values['selVacancies'], $values['selCity'], $_POST['t0k3n_1d_0']);

 //    if(!empty($update_ofert) && $update_ofert == 1) {
 //      $messege = $messages->messages_success('update ofert success');
 //      $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Oferta Actualizada!', $messege);
 //    }else {
 //      $messege = $messages->messages_failed('update ofert failed');
 //      $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Lo Sentimos,', $messege);
 //    }
    
 //    header('Location: /views/profile_company.php');
	// break;

  
  case 'REMOVE_OFFER':
    $values = $functions_sql->functions_sql_clear_dates($_POST);

    $response = $modelProfileCompany->model_profile_company_validate_offer($_POST['t0k3n_1d_0']);

    if ($response == TRUE) {
      $response = $modelProfileCompany->model_profile_company_remove_offer($_POST['t0k3n_1d_0']);

      if(!empty($response) && $response == 1) {
        $messege = $messages->messages_success('end offer success');
        $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Oferta Finalizada!', $messege);
      }else {
        $messege = $messages->messages_failed('end offer failed');
        $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Lo Sentimos,', $messege);
      }
    }
    
    header('Location: /views/profile_company.php');
    
  break;

 //  // COMENTANDO DESPUBLICAR OFERTA
 //  // DESPUBLICAR OFERTA VIA AJAX
	// case 'unpublishOfert':
 //    $values = $functions_sql->functions_sql_clear_dates($_POST);
 //    $unpublish_ofert = $modelProfileCompany->model_profile_company_unpublish_ofert_company($_POST['id_ofert']);

 //    if(!empty($unpublish_ofert) && $unpublish_ofert == 1) {
 //      $messege = $messages->messages_success('unpublish ofert success');
 //      $message_system_ajax[] = $messages->messages_build('SUCCESS', 'Oferta Despublicada!', $messege);
 //    }else {
 //      $messege = $messages->messages_failed('unpublish ofert failed');
 //      $message_system_ajax[] = $messages->messages_build('FAILED', 'Lo Sentimos,', $messege);
 //    }

 //    $messeges = $messages->menssages_render($message_system_ajax);
 //    echo $messeges;
 //  break;

  // PUBLICAR OFERTA VIA AJAX
  case 'publishOfert':
    $values = $functions_sql->functions_sql_clear_dates($_POST);
    $publish_ofert = $modelProfileCompany->model_profile_company_publish_ofert_company($values['id_ofert']);

    if(!empty($publish_ofert) && $publish_ofert == 1) {
      $messege = $messages->messages_success('publish ofert success');
      $message_system_ajax[] = $messages->messages_build('SUCCESS', 'Oferta Publicada!', $messege);
    }else {
      $messege = $messages->messages_failed('publish ofert failed');
      $message_system_ajax[] = $messages->messages_build('FAILED', 'Lo Sentimos,', $messege);
    }

    $messeges = $messages->menssages_render($message_system_ajax);
    echo $messeges;
  break;

  // CARGAR LISTA DE APLICADOS A OFERTA SELECCIONADA VIA AJAX
  case 'LOAD_LIST_APPLY_OFERT':
    $values = $functions_sql->functions_sql_clear_dates($_POST);
    $rows_send = $modelProfileCompany->model_profile_company_load_apply_ofert_selected($values['id_ofert'], 'APPLIED');
    $rows_contact = $modelProfileCompany->model_profile_company_load_apply_ofert_selected($values['id_ofert'], 'SELECTED');
    $rows_discard = $modelProfileCompany->model_profile_company_load_apply_ofert_selected($values['id_ofert'], 'DISCARDED');

    $content_lists_apply = $controlProfileCompany->control_profile_company_load_list_ofert_apply($rows_send, $rows_contact, $rows_discard, $values['id_ofert']);

    echo $content_lists_apply;
  break;

  // CARGAR TODA LA INFORMACION DE LA PERSONA VIA AJAX FROM contactPersonForOfert()
  case 'LOAD_FULL_DATA_PERSON':
    $values = $functions_sql->functions_sql_clear_dates($_POST);
    $rows_query = $modelProfileCompany->model_profile_company_load_full_data_person($values['id_person']);
    $data_person = $controlProfileCompany->control_profile_company_load_full_data_person($rows_query, $values['id_tab']);

    echo $data_person;
  break;

  // CONTACTAR PERSONA PARA OFERTA - VIA AJAX  contactPersonForOfert();
  case 'SELECTED_PERSON_FOR_OFFER':
    $values = $functions_sql->functions_sql_clear_dates($_POST);
    
    // ---- ENVIAR CORREO A LA PERSONA
    $send_email = $controlProfileCompany->control_profile_company_selected_person_offer($values['id_ofert'], $values['id_person']);

    if ($send_email === true) {
      // Cambia el estado de la persona a la oferta que aplico
      $update_status = $modelProfileCompany->model_profile_company_update_status_person_for_ofert('SELECTED', $values['id_ofert'], $values['id_person']);

      if ($update_status === true) {    
        $messege = $messages->messages_success('selected person ofert success');
        $message_system_ajax[] = $messages->messages_build('SUCCESS', 'Persona Seleccionada!', $messege);
        
        $contact_person = array(
          'status_mail' => 'EMAIL_SEND',
        );
      }else {
        $messege = $messages->messages_failed('update selected person ofert failed');
        $message_system_ajax[] = $messages->messages_build('FAILED', 'Lo Sentimos,', $messege);
        
        $contact_person = array(
          'status_mail' => 'UPDATE_STATUS_FAILED',
        );
      }        
    }else {
      $messege = $messages->messages_failed('selected person ofert failed');
      $message_system_ajax[] = $messages->messages_build('FAILED', 'Lo Sentimos,', $messege);

      $contact_person = array(
          'status_mail' => 'EMAIL_FAILED',        
      );
    }

    $contact_person['message_system_ajax'] = $messages->menssages_render($message_system_ajax);

    echo json_encode($contact_person);
  break;


  // DESCARTAR PERSONA PARA OFERTA - VIA AJAX  discardPersonForOfert();
  case 'DISCARDED_PERSON_FOR_OFERT':
    $values = $functions_sql->functions_sql_clear_dates($_POST);
    
    // ---- ENVIAR CORREO A LA PERSONA
    $send_email = $controlProfileCompany->control_profile_company_discard_person_for_ofert($values['id_ofert'], $values['id_person']);

    if ($send_email === true) {
      // Cambia el estado de la persona a la oferta que aplico
      $update_status = $modelProfileCompany->model_profile_company_update_status_person_for_ofert('DISCARDED', $values['id_ofert'], $values['id_person']);

      if ($update_status === true) {
        $messege = $messages->messages_success('discard person ofert success');
        $message_system_ajax[] = $messages->messages_build('SUCCESS', 'Persona Descartada!', $messege);
        
        $contact_person = array(
          'status_mail' => 'EMAIL_SEND',
        );
      } else {
        $messege = $messages->messages_failed('update discard person ofert failed');
        $message_system_ajax[] = $messages->messages_build('FAILED', 'Lo Sentimos,', $messege);
        
        $contact_person = array(
          'status_mail' => 'UPDATE_STATUS_FAILED',
        );
      }        
    }else {
      $messege = $messages->messages_failed('discard person ofert failed');
      $message_system_ajax[] = $messages->messages_build('FAILED', 'Lo Sentimos,', $messege);

      $contact_person = array(
          'status_mail' => 'EMAIL_FAILED',        
      );
    }

    $contact_person['message_system_ajax'] = $messages->menssages_render($message_system_ajax);

    echo json_encode($contact_person);
  break;

    // EDITAR EMPRESA - ACTUALIZAR DATOS DE LA COMPANIAA
    case 'UPDATE_DATA_COMPANY':
    
    $values        = $functions_sql->functions_sql_clear_dates($_POST);
    $file          = $_FILES['fLogo'];
    $flag_logo     = FALSE;
    $dir_upload    = '';
    $new_name_logo = '';

    if (!empty($file['name']) && !empty($file['tmp_name'] )) {

      $new_name_logo = $_SESSION['user']['id_ci'] . '_' . $file['name'];
      $dir_upload = '../src/img/company/logo/';
      $max_size = 2000000;

      if(!empty($values['logo_current'])) {
        unlink($dir_upload . $values['logo_current']);
      }
            
      if ($file['size'] <= $max_size && $file['size'] > 0) {
        if (move_uploaded_file($file['tmp_name'], $dir_upload . $new_name_logo)) {
          $_SESSION['user']['image_ci'] = $new_name_logo;
          $flag_logo = TRUE;
        }else {
          $messege = $messages->messages_failed('upload logo company failed');
          $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Error,', $messege);
        }
      }else {
        //$message = 'Max file size 2mb!';
        $messege = $messages->messages_failed('max size logo company failed');
        $_SESSION["message_system"][] = $messages->messages_build('WARNING', 'Logo Pesado,', $messege);
      }
    }else {
      $new_name_logo = $values['logo_current'];
      $flag_logo = TRUE;
    }

    if (empty($values['itPassword'])) {
   
        if ($flag_logo == TRUE) {
          $update_data_company = $modelProfileCompany->model_profile_company_update_data_company($values['itName'], $new_name_logo, $values['itEconomicActivity'], $values['itPhone'], $values['itMovil'], $values['itaDescription'], NULL, $values['selCity'], $values['itAddress'], $values['itEmail']);

          if(!empty($update_data_company) && $update_data_company == 1) {

            // Actualizando variables de sesion
            $_SESSION["user"]['name_ci']    = $values['itName'];
            $_SESSION["user"]['phone_ci']   = $values['itPhone'];
            $_SESSION["user"]['city_ci']    = $values['selCity'];
            $_SESSION["user"]['address_ci'] = $values['itAddress'];
            $_SESSION["user"]['email_ci']   = $values['itEmail'];

            $messege = $messages->messages_success('update data company success');
            $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Datos actualizados!,', $messege);
          }else {
            if (!unlink($dir_upload . $new_name_logo)) {
              
              rename($dir_upload . $new_name_logo, $dir_upload . 'REMOVE_' . $new_name_logo);
            }

            $messege = $messages->messages_failed('update data company failed');
            $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Error al actualizar,', $messege);
          }
        }
          
    }else {
      
      if (md5($values['itCurrentPassword']) === $values['cl4w0rd']) {

        if ($flag_logo == TRUE) {

            $pass = md5($values['itPassword']);
      
            $update_data_company = $modelProfileCompany->model_profile_company_update_data_company($values['itName'], $new_name_logo, $values['itEconomicActivity'], $values['itPhone'], $values['itMovil'], $values['itaDescription'], $pass, $values['selCity'], $values['itAddress'], $values['itEmail']);

            if(!empty($update_data_company) && $update_data_company == 1) {
              
              // Actualizando variables de sesion
              $_SESSION["user"]['name_ci']    = $values['itName'];
              $_SESSION["user"]['phone_ci']   = $values['itPhone'];
              $_SESSION["user"]['city_ci']    = $values['selCity'];
              $_SESSION["user"]['address_ci'] = $values['itAddress'];
              $_SESSION["user"]['email_ci']   = $values['itEmail'];

              $messege = $messages->messages_success('update data company success');
              $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Datos actualizados!,', $messege);
            }else {
              if (!unlink($dir_upload . $new_name_logo)) {
                rename($dir_upload . $new_name_logo, $dir_upload . 'REMOVE_' . $new_name_logo);
              }
              $messege = $messages->messages_failed('update data company failed');
              $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Error al actualizar,', $messege);
            }
          }

      }else {
        $messege = $messages->messages_failed('different passwords failed');
        $_SESSION["message_system"][] = $messages->messages_build('WARNING', 'Clave actual incorrecta,', $messege); 
      }
    }
    
    header('Location: /views/edit_company.php');
  break;
}
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com