<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
if (!isset($_POST['tokenPage']) || !in_array($_POST['tokenPage'], $_SESSION['token']['pages'])) {
  header('Location: ../index.php');
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/models_utilities.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");

$modelUtilities = new modelUtilities();
$functions_sql = new functions_sql();
$messages = new messages_system();
$values = $functions_sql->functions_sql_clear_dates($_POST);
$pass = md5($values['newPassword']);

$response = $modelUtilities->models_utilities_recover_password($values['1dus3r'], $values['type'], $pass);
if ($response == 1) {
  $messege = $messages->messages_success('change password success');
  $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Contraseña Cambiada!', $messege); 
}else {
  $messege = $messages->messages_failed('change password failed');
  $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Cambio de Contraseña,', $messege); 
}

header('Location: /views/login.php');
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com