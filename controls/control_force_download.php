<?php
session_start();

if (!isset($_GET['tokenPage']) || !in_array($_GET['tokenPage'], $_SESSION['token']['pages'])) {
  exit();
}

if (isset($_SESSION['user']['rol_su']) && $_SESSION['user']['rol_su'] == 'FULL_ADMIN') {
	if (!isset($_SESSION['user']['id_su'])) {
    exit();
  }
}else {
	if (!isset($_SESSION['user']['id_ci'])) {
  	exit();
  }
}



if (!isset($_GET['file']) || empty($_GET['file']) || !isset($_GET['path']) || empty($_GET['file'])) {
 exit();
}
$root = '/src/' . $_GET['path'];
//$root = "src/bill/";
$file = basename($_GET['file']);
$path = $_SERVER['DOCUMENT_ROOT'] . $root . $file;
$type = '';

if (file_exists($path) && is_file($path)) {


 $size = filesize($path);
 if (function_exists('mime_content_type')) {
 $type = mime_content_type($path);
 } else if (function_exists('finfo_file')) {
 $info = finfo_open(FILEINFO_MIME);
 $type = finfo_file($info, $path);
 finfo_close($info);
 }
 if ($type == '') {
 $type = "application/force-download";
 }
 // Definir headers
 header("Content-Type: $type");
 //header('Content-type: application/pdf');
 header("Content-Disposition: attachment; filename=$file");
 header("Content-Transfer-Encoding: binary");
 header("Content-Length: " . $size);
 // Descargar archivo
 readfile($path);

 // $fp=fopen("$f", "r");
 // fpassthru($fp);
} else {
	require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");
	$messages = new messages_system();
	$messege = $messages->messages_failed('bill not found failed', 'factura@mihv.com.co');
    $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Lo Sentimos,', $messege);	
 	header('Location: /views/billing_company.php');
}