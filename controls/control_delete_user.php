<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
if (!isset($_POST['tokenPageModal']) && in_array(!$_POST['tokenPageModal'], $_SESSION['token']['pages'])) {
  header('Location: ../index.php');
}

$suffix = '';
if (isset($_SESSION['user']['rol_pi'])) {
	$suffix = 'pi';
}else if (isset($_SESSION['user']['rol_ci'])) {
	$suffix = 'ci';
}

if ($suffix == '') {
	header('Location: ../index.php');
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/model_user.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");

$modelUser = new modelUser();
$messages = new messages_system();

$removing = 'SUCCESS';

if ($suffix == 'pi') { // PERSONA
	$delete_registry = $modelUser->model_user_delete_registry_oferts_apply('tbl_person_info_id_pi', $_SESSION['user']['id_pi']);

	if ($delete_registry == 1) {
		$delete_user = $modelUser->model_user_delete_user('person', $suffix, $_SESSION['user']['id_pi']);

		if($delete_user == 1) {
			session_destroy();
			session_start();
			$messege = $messages->messages_success('delete user success');
      $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Cuenta Eliminada!,', $messege);

      header('Location: /index.php');
		}else {

			$messege = $messages->messages_failed('delete user failed');
      $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Error al Eliminar Cuenta,', $messege);

      header('Location: /views/edit_company.php');
		}
	}

}else if ($suffix == 'ci') { // EMPRESA
	$rows_offers = $modelUser->model_user_get_offers($_SESSION['user']['id_ci']);
	
	foreach ($rows_offers as $key_offer => $value_offer) {
		$del_offer_appy = $modelUser->model_user_delete_offer_appy($value_offer['id_o']);

		$removing = 'SUCCESS';
		if($del_offer_appy == 1) {
			$removing = 'SUCCESS';
			$del_offer = $modelUser->model_user_delete_offer($_SESSION['user']['id_ci']);
			if($del_offer == 1) {
				$removing = 'SUCCESS';
			}else {
				$removing = 'FAILED';
				break;
			}
		}else {
			$removing = 'FAILED';
			break;
		}
	}

	if ($removing == 'SUCCESS') {
		
		$dir_upload = $_SERVER['DOCUMENT_ROOT'] . '/src/img/company/logo/';

		if (!unlink($dir_upload . $_SESSION['user']['image_ci'])) {
      rename($dir_upload . $_SESSION['user']['image_ci'], $dir_upload . 'REMOVE_' . $_SESSION['user']['image_ci']);
    } 

		$delete_user = $modelUser->model_user_delete_user('company', $suffix, $_SESSION['user']['id_ci']);

		if($delete_user == 1) {
			session_destroy();
			session_start();
			$messege = $messages->messages_success('delete user success');
	    $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Cuenta Eliminada!,', $messege);

	    header('Location: /index.php');
		}else {
			$messege = $messages->messages_failed('delete user failed');
	    $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Error al Eliminar Cuenta,', $messege);

	    header('Location: /views/edit_company.php');
		}
	}else {
		$messege = $messages->messages_failed('delete user failed');
    $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Error al Eliminar Cuenta,', $messege);

    header('Location: /views/edit_company.php');
	}

}else {
	header('Location: ../index.php');
}
