<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();

if (!isset($_SESSION['user']['id_su'])) {
  header('Location: /index.php');
}

if (!isset($_POST['tokenPage']) || !in_array($_POST['tokenPage'], $_SESSION['token']['pages'])) {
  header('Location: ../index.php');
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/model_user.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");

$functions_sql = new functions_sql();
$modelUser = new modelUser();
$messages = new messages_system();

$values = $functions_sql->functions_sql_clear_dates($_POST);

// TOKEN BUSCAR USUARIO
switch ($values['tokenRol']) {

  // Buscar informacion de empresa
  case 'company':

    $nit = $values['itNit'] . '-' . $values['dv_hidden'];
    
    switch ($values['tokenAction']) {
      case 'recharge_balance':
      case 'recharge_history':  
        $data = $modelUser->model_user_get_data_user('tbl_company_info', 'ci', 'NIT', $nit);

        if ($data) {
          $params = '?tokenPage=' . urlencode($values['tokenPage']) . '&id_ci=' . urlencode($data['id_ci']) . '&name_ci=' . urlencode($data['name_ci']) .'&nit_ci=' . urlencode($data['nit_ci']) .'&email_ci=' . urlencode($data['email_ci']) . '&available_balance_ci=' . urlencode($data['available_balance_ci']);

          $redirect = '/views/system/' . $values['tokenAction'] . '.php' . $params;
          
          header('Location: ' . htmlspecialchars_decode($redirect));
          break;
        }
        
        $messege = $messages->messages_failed('search user failed', $nit);
        $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Usuario no encontrado,', $messege); 
        header('Location: ' . htmlspecialchars_decode($values['tokenBack']));
        
      break;
    }
  break;

  default:
    header('Location: ' . htmlspecialchars_decode($values['tokenBack']));
    break;
}

// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com