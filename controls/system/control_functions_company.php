<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();

if (!isset($_SESSION['user']['id_su'])) {
  header('Location: /index.php');
}

if (!isset($_POST['tokenPage']) || !in_array($_POST['tokenPage'], $_SESSION['token']['pages'])) {
  header('Location: ../index.php');
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/model_profile_company.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_profile_company.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");

$functions_sql = new functions_sql();
$modelProfileCompany = new modelProfileCompany();
$controlProfileCompany = new controlProfileCompany();
$messages = new messages_system();
$controlUtilities = new controlUtilities();


$values = $functions_sql->functions_sql_clear_dates($_POST);

switch ($values['tokenAction']) {
  
  case 'RECHARGE_BALANCE':

    $priority = 'na';
    $payment_type = 'balance';
    $value_offer = $controlProfileCompany->control_profile_company_get_value_offer($priority, $payment_type);

    if(!is_numeric($values['value_recharge']) || $values['value_recharge'] < $value_offer) { 
      $params = array('value_offer' => $value_offer);
      $messege = $messages->messages_failed('min value recharge failed', null, null, $params);
      $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Valor Insuficiente!', $messege);
      header('Location: /views/system/dashboard.php');
      break;
    }

    $response = $modelProfileCompany->model_profile_company_recharge_balance('SUM', $values['id_ci'], $values['nit_ci'], $values['value_recharge']);

    if ($response) {

      //Agregar registro en tabla del sistema: tbl_system_balance_recharge
      $id_recharge = $modelProfileCompany->model_profile_company_save_system_recharge_balance($values['id_transaction'], $values['id_ci'], $values['nit_ci'], $values['name_ci'], $values['email_ci'], $values['value_recharge']);
      
      if($id_recharge != 0) {
        //Crear PDF de Recarga
        $response_pdf = $controlProfileCompany->control_profile_company_create_pdf_recharge_balance($id_recharge, $values['id_ci'], $values['nit_ci'], $values['name_ci'], $values['email_ci'], $values['value_recharge']);
  
        $url_validate_pdf = '/src/recharge_balance/comprobante-recarga-saldo_' . $id_recharge . '.pdf';

        if ($response_pdf == $url_validate_pdf) {

          //Enviar a la empresa el correo con el PDF de Recarga
          $response_email = $controlProfileCompany->control_profile_company_send_email_recharge_balance($id_recharge, $values['name_ci'], $values['email_ci'], $values['value_recharge'], $response_pdf);

          if ($response_email) {
            $messege = $messages->messages_success('recharge balance success');
            $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Saldo Recargado!', $messege);
          }else {

            $email_support = 'soporte@mihv.com.co';
            $from_name = 'MI HV - Fallo Urgente!!!';

            $subject = 'Fallo al enviar pdf de comprobante de recarga del saldo a empresa';
            $content ='
              <div>
                <p>No se pudo enviar el correo con el pdf del comprobante de recarga del saldo a la siguiente empresa:</p>
                <br>
                <p>Id Empresa: ' . $values['id_ci'] . '</p>
                <p>Nombre Empresa: ' . $values['name_ci'] . '</p>
                <p>Correo Empresa: ' . $values['email_ci'] . '</p>
                <p>Valor de la Recarga: ' . $values['value_recharge'] . '</p>
                
                <p>Ruta del pdf: ' . $response_pdf . '</p>
                <br><br>
                <p><b>Nota:</b> Enviar manualmente el correo a la empresa con el pdf del comprobante de recarga del saldo <b>Adjuntado</b></p>
              </div>
            ';

            $attach = array($response_pdf);
            $response_mail_support = $controlUtilities->control_utilities_send_mail_support($email_support, $content, $subject, $from_name, $attach);

            $messege = $messages->messages_failed('send mail recharge balance failed');
            $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Correo Fallido,', $messege);
          }

        }else {

          $response = $modelProfileCompany->model_profile_company_delete_recharge_balance($id_recharge);

          if ($response) {
            
            $note = 'Se borro el registro de tbl_system_balance_recharge <br>';

            $response = $modelProfileCompany->model_profile_company_recharge_balance('SUBTRACT', $values['id_ci'], $values['nit_ci'], $values['value_recharge']);

            if ($response) {
              $note .= 'Se devolvio al saldo disponible anterior OK en tbl_company_info';
            }else {
              $note .= '<b>NO!</b> se pudo devolver al saldo anterior, realizar el proceso manualmente en tbl_company_info restando el valor de: ' . $values['value_recharge'];
            }
          }else {
            $note = '<b>NO!</b> se pudo borrar el registro de tbl_system_balance_recharge, borrarlo manual inmediatamente<br> tampoco restar el saldo disponible en la empresa, realizar el proceso manualmente en tbl_company_info restando el valor de: ' . $values['value_recharge'];
          }


          $email_support = 'soporte@mihv.com.co';
          $from_name = 'MI HV - Fallo Urgente!!!';

          $subject = 'Fallo al crear pdf de comprobante de recarga del saldo';
          $content ='
            <div>
              <p>No se pudo crear el pdf del comprobante de recarga del saldo y se dejaron los datos de la empresa intactos</p>
              <br>
              <p>Id Empresa: ' . $values['id_ci'] . '</p>
              <p>Nombre Empresa: ' . $values['name_ci'] . '</p>
              <p>Correo Empresa: ' . $values['email_ci'] . '</p>
              <p>Valor de la Recarga: ' . $values['value_recharge'] . '</p>
              
              <br><br>
              <p><b>Nota:</b> 
              <br>
              ' . $note . '
              </p>
            </div>
          ';

          $response_mail_support = $controlUtilities->control_utilities_send_mail_support($email_support, $content, $subject, $from_name);

          $params = array('note' => $note);

          $messege = $messages->messages_failed('create pdf recharge balance failed', null, null, $params);
          $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Saldo Recargado Fallido,', $messege);
        }

      }else {

        $response = $modelProfileCompany->model_profile_company_recharge_balance('SUBTRACT', $values['id_ci'], $values['nit_ci'], $values['value_recharge']);

        if ($response) {
          $status_balance_available = 'Se devolvio al saldo anterior OK';
        }else {
          $status_balance_available = 'NO se pudo devolver al saldo anterior, realizar el proceso manualmente en tbl_company_info';
        }

        $email_support = 'soporte@mihv.com.co';
        $from_name = 'MI HV - Fallo Urgente!!!';

        $subject = 'Fallo al actualizar tabla del comprobante de recarga del saldo';
        $content ='
          <div>
            <p>No se pudo crear el registro en la tabla del comprobante de recarga del saldo:</p>
            <br>
            <p>Id Empresa: ' . $values['id_ci'] . '</p>
            <p>Nombre Empresa: ' . $values['name_ci'] . '</p>
            <p>Correo Empresa: ' . $values['email_ci'] . '</p>
            <p>Valor de la Recarga: ' . $values['value_recharge'] . '</p>
            
            <br><br>
            <p><b>Nota:</b> 
            <br>
            1. ' . $status_balance_available . '
            </p>
          </div>
        ';

        $response_mail_support = $controlUtilities->control_utilities_send_mail_support($email_support, $content, $subject, $from_name);

        $messege = $messages->messages_failed('save system recharge balance failed');
        $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Saldo Recargado Fallido,', $messege);
      }
    }else {
      $messege = $messages->messages_failed('recharge balance failed');
      $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Saldo Recargado Fallido,', $messege); 
    }

    header('Location: /views/system/dashboard.php');
    
    break;

  default:
    header('Location: /index.php');
    break;
}

// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com