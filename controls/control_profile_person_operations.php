<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
if (!isset($_POST['tokenPage']) || !in_array($_POST['tokenPage'], $_SESSION['token']['pages'])) {
  header('Location: ../index.php');
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/model_profile_person.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_profile_person.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");

$modelProfilePerson = new modelProfilePerson();
$controlProfilePerson = new controlProfilePerson();
$functions_sql = new functions_sql();
$messages = new messages_system();

switch ($_POST["profilePersonToken"]) {
  // Editar persona
	case 'UPDATE_DATA_PERSON':
    $values = $functions_sql->functions_sql_clear_dates($_POST);

    if (empty($values['itNewPassword'])) {
      $update_data_person = $modelProfilePerson->model_profile_person_update_data_person($values['itName'], $values['itLastName'], $values['itPhone'], $values['itMovil'], $values['itExperience'], $values['itEmail'], NULL, $values['itaKnowledge'], $values['itaStudies'], $values['age'], $values['itProfession'], $values['selCity'], $values['itAddress']);

      if(!empty($update_data_person) && $update_data_person == 1) {

        $_SESSION["user"]['name_pi']       = $values['itName'];
        $_SESSION["user"]['last_name_pi']  = $values['itLastName'];
        $_SESSION["user"]['email_pi']      = $values['itEmail'];
        $_SESSION["user"]['profession_pi'] = $values['itProfession'];

        $messege = $messages->messages_success('update data person success');
        $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Datos actualizados!,', $messege);
      }else {
        $messege = $messages->messages_failed('update data person failed');
        $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Error al actualizar,', $messege);
      }
    }else {
      if (md5($values['itCurrentPassword']) === $values['cl4w0rd']) {
        $pass = md5($values['itNewPassword']);

        $update_data_person = $modelProfilePerson->model_profile_person_update_data_person($values['itName'], $values['itLastName'], $values['itPhone'], $values['itMovil'], $values['itExperience'], $values['itEmail'], $pass, $values['itaKnowledge'], $values['itaStudies'], $values['age'], $values['itProfession'], $values['selCity'], $values['itAddress']);

        if(!empty($update_data_person) && $update_data_person == 1) {

          $_SESSION["user"]['name_pi']       = $values['itName'];
          $_SESSION["user"]['last_name_pi']  = $values['itLastName'];
          $_SESSION["user"]['email_pi']      = $values['itEmail'];
          $_SESSION["user"]['profession_pi'] = $values['itProfession'];
        
          $messege = $messages->messages_success('update data person success');
          $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Datos actualizados!,', $messege);
        }else {
          $messege = $messages->messages_failed('update data person failed');
          $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Datos actualizados!,', $messege);
        }
      }else {
        $messege = $messages->messages_failed('different passwords failed');
        $_SESSION["message_system"][] = $messages->messages_build('WARNING', 'Clave actual incorrecta,', $messege); 
      }
    }
    
    header('Location: /views/profile_person.php');
	break;
}
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com