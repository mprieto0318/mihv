<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/models/model_profile_person.cls.php");

class controlProfilePerson  
{
	function control_profile_person_get_all_oferts_apply_by_user() {
		$modelProfilePerson = new modelProfilePerson();
		$response = $modelProfilePerson->model_profile_person_get_all_oferts_apply_by_user();
		return $response;
	}

  function control_profile_person_get_all_oferts_apply_by_user_rows_table($rows_query) {
    $rows = '';
    foreach ($rows_query as $key => $value) {
      $rows .= '<tr>';
      foreach ($value as $key2 => $value2) {

        switch ($value['status_oa']) {
          case 'APPLIED':
            $styles = '';
            $icon = 'glyphicon glyphicon-eye-close';
            break;

          case 'SELECTED':
            $styles = 'color: #26a112';
            $icon = 'glyphicon glyphicon-ok-circle';
            break;

          case 'DISCARDED':
            $styles = 'text-decoration:line-through; color: #333;';
            $icon = 'glyphicon glyphicon-remove-circle';
            break;
        }

        switch ($key2) {
          case 'name_o':
            $rows .= '<td>
              <input type="hidden" id="t0k3n_1d_0" name="t0k3n_1d_0" value=' .  $value['id_o'] . '>
              <button type="button" style="' . $styles . '" class="btn btn-link show-ofert-apply" data-toggle="modal" data-target="#ofert-selected-modal-person"><span class="' . $icon . '"></span> ' . utf8_encode($value2) . '
              </button>
              <div class="animation_image" style="display:none;">
                <img alt="Cargando" src="/src/img/mihv/site/ajax-loader.gif"> Cargando...
              </div>
            </td>';
            break;

          case 'name_ci':
            $rows .= '<td><p style="' . $styles . '">' . utf8_encode($value2) . '</p></td>';
            break;

          case 'date_applied_oa':
            $data_applied_oa = new DateTime($value2);
            $rows .= '<td><p style="' . $styles . '">' . $data_applied_oa->format('d/m/Y') . '</p></td>';
            break;
        } 
      }
      $rows .= '</tr>';
    }

    return $rows;
  }

  function control_profile_person_get_full_data_person() {
    $modelProfilePerson = new modelProfilePerson();
    $data_person = $modelProfilePerson->model_profile_person_get_full_data_person();
    return $data_person;
  }
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
}