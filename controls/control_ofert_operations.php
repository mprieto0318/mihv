<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
if (!isset($_POST['tokenPage']) || !in_array($_POST['tokenPage'], $_SESSION['token']['pages'])) {
  header('Location: ../index.php');
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/model_ofert_operations.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");

$modelOfertOperations = new modelOfertOperations();
$functions_sql = new functions_sql();
$messages = new messages_system();

switch ($_POST["id_operation"]) {

  // APLICAR OFERTA VIA AJAX
  case 'APPLY_OFERT':
    $values = $functions_sql->functions_sql_clear_dates($_POST);
    $apply_ofert = $modelOfertOperations->model_ofert_operations_apply_ofert($values['id_ofert']);

    if(!empty($apply_ofert) && $apply_ofert == 1) {
      $message_system = $messages->messages_success('apply ofert success');
    }else {
      $message_system = $messages->messages_failed('apply ofert failed');
    }
    
    echo $message_system;
  break;
  
  // DESAPLICAR OFERTA VIA AJAX
  case 'DISAPPLY_OFERT':
    $values = $functions_sql->functions_sql_clear_dates($_POST);
    $disapply_ofert = $modelOfertOperations->model_ofert_operations_disapply_ofert($values['id_ofert']);

    if(!empty($disapply_ofert) && $disapply_ofert == 1) {
      $message_system = $messages->messages_success('disapply ofert success');
    }else {
      $message_system = $messages->messages_failed('disapply ofert failed');
    }

    echo $message_system;
  break;
}
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com