<?php

class messages_system
{
	
	function messages_success($msg, $email = null, $user_name = null, $file = null, $tokenPage = null, $path = null) {
    switch ($msg) {
			case 'insert user success':
				$message = 'ya has creado tu usuario, ahora revisa tu email y activa tu cuenta, ten en cuenta de revisar la carpeta de spam o correo no deseado. "';
			break;

			case 'login success':
				$message = 'Hola ' . $user_name . ', buena suerte en tu busqueda';
			break;

			case 'insert ofert success':
				$message = 'Felicidades has creado tu oferta. <a href="/controls/control_force_download.php?file=' . $file . '&tokenPage=' . $tokenPage . '&path=' . $path . '">Descargar Factura</a>';
			break;

			case 'update ofert success':
				$message = 'Se actualizó tu oferta, no se te olvide revisar el listado de los aspirantes';
			break;

      case 'end offer success':
        $message = 'Se ha finalizado tu oferta, esperamos que pronto crees una nueva.';
      break;

      case 'publish ofert success':
        $message = 'Tu oferta está en línea, recuerda observar periodicamente los candidatos que apliquen';
      break;
      
      case 'apply ofert success':
        $message = 'APPLIED_OK';
      break;  

      case 'disapply ofert success':
        $message = 'DISAPPEARED_OK';
      break;

      case 'update data person success':
            $message = 'Se han actualizado tus datos satisfactoriamente.';
      break;

      case 'update data company success':
        $message = 'Se han actualizado los datos de tu compañía satisfactoriamente.';
      break;

      case 'user actived success':
        $message = 'QUE BIEN! se ha activado tu usuario correctamente, ahora puedes iniciar sesión.';
      break;

      case 'send token active user success':
        $message = 'Se te ha enviado un correo para que actives tu cuenta, "No olvides revisar la carpeta de spam o correos no deseados".';
      break;

      case 'selected person ofert success':
        $message = 'Se ha enviado un correo a la persona notificandole que lo haz seleccionado para que este pendiente a tu llamada.';
      break;

	    case 'discard person ofert success':
        $message = 'Se ha enviado un correo a la persona notificandole que lo haz descartado para la oferta.';
      break;

      case 'recover password success':
        $message = 'Se ha enviado un correo con el proceso para recuperar tu contraseña. No olvides revisar la carpeta spam o correo no deseado".';
      break;

      case 'change password success':
        $message = 'Tu contraseña se ha cambiado correctamente, ahora puedes iniciar sesión!';
      break;

      case 'recharge balance success':
        $message = 'Se ha recargado el saldo satisfactoriamente.';
      break;

      case 'insert offer free success':
        $message = 'Felicidades, se creo tu oferta <b>gratis</b>, enhorabuena por ello.';
      break;

      case 'delete user success':
        $message = 'Tu cuenta en <b>MI HV</b> se elimino, gracias por utilizar nuestro aplicativo!';
      break;

    }
		return $message;
	}

	function messages_failed($msg, $email = null, $url = null, $params = array()) {
    $message = '';
		switch ($msg) {
			case 'insert user failed':
				$message = 'Ha ocurrido un error al crear el usuario, por favor inténtalo mas tarde';
			break;

			case 'insert user exists':
				$message = 'Ya existe un usuario registrado con el email "' . $email . '" ó el nit ingresado, por favor intenta con otro email. ';
			break;
	
			case 'login failed':
				$message = 'Ups! email y/o contraseña incorrectos, por favor verifica e intenta de nuevo';
			break;

			case 'insert ofert failed':
				$message = 'Ha ocurrido un error al crear la oferta, hazlo nuevamente, si persiste el error inténtalo mas tarde.';
			break;

			case 'update ofert failed':
        $message = 'Ha ocurrido un error al actualizar la oferta, inténtalo otra vez.';
      break;

      case 'end offer failed':
        $message = 'Ha ocurrido un error al finalizar la oferta, por favor inténtalo mas tarde.';
      break;

      case 'publish ofert failed':
        $message = 'Ha ocurrido un error al publicar la oferta,  por favor inténtalo mas tarde.';
      break;

      case 'apply ofert failed':
        $message = 'APPLIED_FAILED';
      break; 

      case 'disapply ofert failed':
        $message = 'DISAPPEARED_FAILED';
      break;

      case 'different passwords failed':
        $message = 'No se pudo actualizar tu perfil, verifica la contraseña actual.';
      break;
      
      case 'update data person failed':
      case 'update data company failed':
        $message = 'Ha ocurrido un error al actualizar tus datos, por favor inténtalo mas tarde.';
      break;

      case 'user actived failed':
        $message = 'Ha ocurrido un error al activar tu cuenta. Para activarla debes iniciar sesión.';
      break;

      case 'insert user and email failed':
        $message = 'Te haz registrado satisfactoriamente, pero no se pudo enviar el correo de activacion de tu cuenta, tienes acceso a MIHV por 24 horas. pronto te enviaremos el correo de activacion de tu cuenta, no olvides revisar la carpera de spam o correo no deseado.';
      break;
      
      case 'send token active user failed':
        $message = 'No se pudo enviar el correo de activacion de tu cuenta, tienes acceso a MIHV por 24 horas. pronto te enviaremos el correo de activacion de tu cuenta, no olvides revisar la carpera de spam o correo no deseado.';
      break;

      case 'update selected person ofert failed':
        $message = 'No se pudo actualizar el usuario a "Seleccionados", pero si se le envio un correo a la persona que lo contactaste para la oferta.';
      break;

      case 'selected person ofert failed':
        $message = 'No se pudo seleccionar la persona, inténtalo mas tarde.';
      break;
      
      case 'update discard person ofert failed':
        $message = 'No se pudo actualizar el usuario a "Descartados", pero se le envio un correo a la persona notificandole que esta descartado.';
      break;

      case 'discard person ofert failed':
        $message = 'No se pudo actualizar la persona a "Descartados", intenta mas tarde.';
      break;
      
      case 'upload logo company failed':
        $message = 'Ha ocurrido un error inesperado al subir el logo, por favor intentalo de nuevo.';
      break;

      case 'max size logo company failed':
        $message = 'El tamaño maximo del LOGO no debe superar los 2MB, verificalo e intenta de nuevo.';
      break;
      
      case 'recover password failed':
      case 'recover password and email failed':
        $message = 'Ha ocurrido un error al intentar enviar el correo de recuperación de clave, por favor inténtalo de nuevo.';
      break;

      case 'recover password user no exists':
        $message = 'No hemos encontrado el email ingresado "' . $email . '", por favor verificalo e intenta de nuevo.';
      break;

      case 'change password failed':
        $message = 'Ha ocurrido un error al intentar cambiar tu contraseña, por favor intenta de nuevo.';
      break;
      
      case 'insert user - access provisional failed':
        $message = 'Ha ocurrido un problema al crear tu cuenta pronto nos comunicaremos contigo a tu email < ' . $email . ' >.';
      break;

      case 'insufficient balance failed':
        $message = 'No tienes el saldo suficiente para publicar esta oferta, por favor adquire mas saldo <a class="link-buy" href="#" data-toggle="modal" data-target="#modal_steps_reload_balance">Aqui!</a>';
      break;

      case 'recharge balance failed':
        $message = 'Ha ocurrido un error inesperado al recargar tu saldo, porfavor intenta mas tarde.';
      break;

      case 'url show offer failed':
        $message = 'La url de la oferta no es correcta, porfavor verificala.';
      break;

      case 'delete user failed':
        $message = 'Ha ocurrido un error al eliminar tu cuenta, porfavor intenta mas tarde.';
      break;

      case 'search user failed':
        $message = 'No se ha encontrado el usuario con el dato: ' . $email;
      break;

      case 'min value recharge failed':
        $message = 'El valor a recargar debe ser mayor a : ' . $params['value_offer'] . ' o el dato no es entero';
      break;

      case 'save system recharge balance failed':
        $message = 'Se recargo el saldo de la empresa y se devolvio al saldo anterior, porque no ingreso el registro en la db:tbl_system_balance_recharge';
      break;

      case 'send mail recharge balance failed':
        $message = 'No se pudo enviar el correo con el pdf del comprobante de recarga del saldo a la empresa, se envio un correo a soporte@mihv.com.co para que haga la gestion necesaria.';
      break;

      case 'create pdf recharge balance failed':
        $message = 'No se pudo crear el pdf del comprobante de recarga del saldo. ' . $params['note'] . '<br> NO realize ninguna operacion con este usuario hasta que soporte notifique proseguir.';
      break;

      case 'bill not found failed':
        $message = 'No se pudo encontrar la factura, puedes solicitarla enviando un correo a: ' . $email;
      break;      
		}
		return $message;
	}

	function messages_build($type = NULL, $pre_message, $message) {
		$type_class = '';

		switch ($type) {
			case 'SUCCESS':
				$type_class = 'alert-success';
				break;

			case 'FAILED':
				$type_class = 'alert-danger';
				break;

			case 'INFO':
				$type_class = 'alert-info';
				break;

			case 'WARNING':
				$type_class = 'alert-warning';
				break;
		}

		$message = '
			<div class="col-md-12">
				<div class="alert ' . $type_class . ' fade in">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			    <strong>' . $pre_message . '</strong> ' . $message . '
			  </div>
			 </div>
		';

		return $message;
	}

	function menssages_render($messages  = array()){
		if (empty($messages)) {
			return false;
		}

		$message_html = '';
		foreach ($messages as $key => $value) {
			$message_html .= $value;
		}

		return $message_html;
	}
}