<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/models/models_utilities.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/models/model_load_oferts.cls.php");

class controlUtilities  
{
  function control_utilities_check_plain($text) { 
    return htmlspecialchars($text, ENT_QUOTES, 'UTF-8'); 
  }

  function control_utilities_create_token_pages($page = 'anonymous'){
    
    if(!isset($_SESSION['token']['pages'][$page])) {
      $token = md5(mt_rand(11,999));
      $_SESSION['token']['pages'][$page] = $token;
    }else {
      $token = $_SESSION['token']['pages'][$page];
    }

    return $token;
  }

  function control_utilities_clip_text($text, $limit = 100){ 
    $text = trim($text);
    $text = strip_tags($text);
    $size = strlen($text);
    $result = '';
    if($size <= $limit){
      return $text;
    }else{
      $text = substr($text, 0, $limit);
      $words = explode(' ', $text);
      $result = implode(' ', $words);
      $result .= '...';
    } 
    return $result;
  }

	function control_utilities_get_types_profession() {
		$modelUtilities = new modelUtilities();
		$response = $modelUtilities->model_utilities_get_types_profession();
		
		return $response;
		
	}

	function _control_utilities_get_types_profession_select($getSelTypeProf = NULL) {
    $response = $this->control_utilities_get_types_profession();
    $options = '';

		foreach ($response as $key => $value) {
			$option_selected = ($key == $getSelTypeProf) ? 'selected="selected"' : '';
			$options .= '<option value="' . $key . '" ' . $option_selected . '>' . $value . '</option>';
  	}
		return $options;
	}

  function control_utilities_get_list_profession($type_profession = NULL) {
    $modelUtilities = new modelUtilities();
    $response = $modelUtilities->model_utilities_get_list_profession($type_profession);
    return $response;
  }

  function _control_utilities_get_list_profession($type_profession_pi = NULL, $list_profession_pi = NULL, $type = 'text') {
    
    $response = $this->control_utilities_get_list_profession($type_profession_pi);

    $list_profession_pi = explode(';', $list_profession_pi);
    
    if ($type == 'text') {
      $data = array();
      foreach ($response as $value) {
        if (in_array($value['id_lp'], $list_profession_pi)) {
          $data[] = $value['name_lp'];
        }
      }
    }else if ($type == 'select') {
      $data = '';
      $option_seleted = '';
      foreach ($response as $value) {
        $option_seleted = (in_array($value['id_lp'], $list_profession_pi)) ? 'selected="selected"' : '';
        $data .= '<option value="' . $value['id_lp'] . '" ' . $option_seleted . '>' . $value['name_lp'] . '</option>';
      }
    }
    return $data;
  }


	function control_utilities_get_citys() {
		$modelUtilities = new modelUtilities();
		$response = $modelUtilities->model_utilities_get_citys();
		
		return $response;
		
	}

	function _control_utilities_get_citys_select($getSelCity = NULL) {
		$response = $this->control_utilities_get_citys();
		//return $response;
		if ($response !== FALSE) {
			$select = '<option value="all">Ciudad</option>';
			foreach ($response as $key => $value) {
				$option_selected = ($key == $getSelCity) ? 'selected="selected"' : '';
				$select .= '<option value="' . $key . '" ' . $option_selected . '>' . utf8_encode($value) . '</option>';
	  	}
			return $select;
		}
	}

  function _control_utilities_get_citys_name($id_city = NULL) {
    $response = $this->control_utilities_get_citys();
    $name = 'No Especifica';
    if ($response !== FALSE) {
      foreach ($response as $key => $value) {
        if ($key == $id_city) {
          $name = utf8_encode($value);
          break;
        }
      }
    }
    
    return $name;
  }

	function _control_utilities_get_salary_select($getSelSalary = NULL) {
			
		$option_values = array(
			'all' => 'Todos',
			'400000' => 'Mayor a $400.000',
			'700000' => 'Mayor a $700.000',
			'1000000' => 'Mayor a $1.000.000',
			'1500000' => 'Mayor a $1.500.000',
			'2000000' => 'Mayor a $2.000.000',
			'2500000' => 'Mayor a $2.000.000',
			'3000000' => 'Mayor a $3.000.000',
			'4000000' => 'Mayor a $4.000.000',
			'6000000' => 'Mayor a $6.000.000',
		);

		$options = '';
		foreach ($option_values as $key => $value) {
			$option_selected = ($key == $getSelSalary) ? 'selected="selected"' : '';
			$options .= '<option value="' . $key . '" ' . $option_selected . '>' . $value . '</option>';
		}

		return $options;
	}

	function _control_utilities_get_genre_select($getSelGenre = NULL) {
			
		$option_values = array(
			'all' => 'Ambos',
			'male' => 'Hombre',
			'female' => 'Mujer',
		);

		$options = '';
		foreach ($option_values as $key => $value) {
			$option_selected = ($key == $getSelGenre) ? 'selected="selected"' : '';
			$options .= '<option value="' . $key . '" ' . $option_selected . '>' . $value . '</option>';
		}

		return $options;
	}




	function _control_utilities_filter_date_select($getSelDate = NULL) {
    $today = date('Y-m-d');

    $option_text = array(
      'Hoy', 
      'Ayer', 
      'Hace una Semana', 
      'Hace 15 Días',
      'Hace 1 Mes',
      'Hace mas de 2 Meses',
    );

    $option_select = '<option value="all">Todos</option>';

    for ($i=0 ; $i < 6 ; $i++) { 
      
      switch ($i) {
        case 0:
          $new_date = $today;
          break;
        case 1:
          $date_convert = strtotime ('-1 day', strtotime($today));
          $new_date = date('Y-m-d', $date_convert);
          break;
        case 2:
          $date_convert = strtotime ('-1 week', strtotime($today));
          $new_date = date('Y-m-d', $date_convert);
          break;
        case 3:
          $date_convert = strtotime ('-2 week', strtotime($today));
          $new_date = date('Y-m-d', $date_convert);
          break;
        case 4:
          $date_convert = strtotime ('-1 month', strtotime($today));
          $new_date = date('Y-m-d', $date_convert);
          break;
        case 5:
          $date_convert = strtotime ('-2 month', strtotime($today));
          $new_date = date('Y-m-d', $date_convert);
          break;
      }
      $option_selected = ($new_date == $getSelDate) ? 'selected="selected"' : '';
      $option_select .= '<option value="' . $new_date . '" ' . $option_selected . '>' . $option_text[$i] . '</option>';

    }
    
    return $option_select;
  }

  function control_utilities_get_oferts_priority() {

  	// TRAER CANTIDAD DE OFERTAS EN LAS CATEGORIAS - PENDIENTE
  	$modelLoadOferts = new modelLoadOferts();
		$oferts_priority = $modelLoadOferts->model_load_oferts_priority_blocks();

    $count_oferts = count($oferts_priority);
    
    if ($count_oferts == 4 ) {
      $rows_oferts_priority = $this->_control_utilities_build_oferts_priority($oferts_priority);
      return $rows_oferts_priority;
    }else if ($count_oferts < 4 ) {
      $limit = 4 - $count_oferts;

      $oferts_priority_complete = $modelLoadOferts->model_load_oferts_priority_blocks_complete($limit);

      $rows_oferts_priority = $this->_control_utilities_build_oferts_priority(array_merge($oferts_priority, $oferts_priority_complete));
      return $rows_oferts_priority;
    }else {
      return FALSE;  
    } 
  }


  function _control_utilities_build_oferts_priority($data_oferts) {
  	$count_oferts_html = '<div class="row">';
    
    $url_logo = $_SERVER['DOCUMENT_ROOT'] . '/src/img/company/logo/';

    foreach ($data_oferts as $key => $value) {
      $count_oferts_html .= '
        <div class="col-xs-12 col-sm-6 col-md-3">
          <div class="panel status panel-priority">
            <div class="panel-header">';

              if (file_exists($url_logo . $value['image_ci']) && is_file($url_logo . $value['image_ci'])) {
                $count_oferts_html .= '
                  <img alt="' . $value['image_ci'] .'" class="logo-company" src="../src/img/company/logo/' . $value['image_ci'] .'">';
                clearstatcache();
              }else {
                $count_oferts_html .= '<p class="logo-company-default">
                  '. strtoupper(substr($value['name_ci'], 0, 1)) . '
                </p>';
              }

              $count_oferts_html .= '<p class="panel-title text-center">' .  utf8_encode($value['name_ci']) . '</p>
            </div>
            <div class="panel-body text-center">                        
              <h5>' .  utf8_encode($value['name_o']) . '</h5 >
              <p>' .  utf8_encode($this->control_utilities_clip_text($value['description_o'], 100))  . '</p>
              <h6>' . utf8_encode($value['name_c']) . '</h6>
              <button class="btn btn-success btn-block show-ofert" data-ofert="' . $value['id_o'] . '" data-toggle="modal" data-target="#ofert-selected-modal-person"><span class="glyphicon glyphicon-eye-open"> </span> Ver</button>
              <div class="animation_image" style="display:none;">
                <img alt="Cargando" src="/src/img/mihv/site/ajax-loader.gif"> Cargando...
              </div>
            </div>
          </div>
        </div>
      ';
    }

    $count_oferts_html .= '</div>';

  	return $count_oferts_html;
  }


  function control_utilities_get_adverting_menu_side($page = 'default') {
    $adverting = $this->_control_utilities_build_list_adverting();

    return $adverting;
  }

  /*
   * Construir publicidad de menu lateral
   */
  function _control_utilities_build_list_adverting() {
    //print_r($_SESSION['advertising']['menu_side']['current']);
    $all_image = '';
    $dirname = '/src/img/mihv/4dv3rt1s1ng/menu-side/publicidad-';
    $flag_image = FALSE;
    $i = 1;
    $end = 3;

    while ($i <= $end) {
      $image = $_SESSION['advertising']['menu_side']['current'];
      if (file_exists($_SERVER['DOCUMENT_ROOT'] . $dirname . $image . '.jpg')) {
        $all_image .= '    
          <li>
            <a href="#">
              <img alt="pauta ' . $image . '" src="' . $dirname . $image . '.jpg' . '" />
            </a>
          </li>
        ';
      }else {
        $image = 1;
        $all_image .= '    
          <li>
            <a href="#">
              <img alt="pauta ' . $image . '" src="' . $dirname . $image . '.jpg' . '" />
            </a>
          </li>
        ';
        $flag_image = TRUE;
      }

      if($flag_image == TRUE) {
        $_SESSION['advertising']['menu_side']['current'] = 1;
        $flag_image = FALSE;
      }
      $_SESSION['advertising']['menu_side']['current']++;
      $i++;
    }
    
    return $all_image;
  }

  /*
   * Construir publicidad de resultado ofertas
   */
  function _control_utilities_build_result_oferts_adverting() {
    //print_r($_SESSION['advertising']['menu_side']['current']);
    $adverting_ofert = '';
    $dirname = '/src/img/mihv/4dv3rt1s1ng/result-oferts/publicidad-';
    $flag_image = FALSE;
    
    $image = $_SESSION['advertising']['result_oferts']['current'];
    if (file_exists($_SERVER['DOCUMENT_ROOT'] . $dirname . $image . '.jpg')) {
      $adverting_ofert = '
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <p class="text-center">Publicidad</p>
              <img alt="pauta ' . $image . '" class="img-adverting-oferts" src="' . $dirname . $image . '.jpg' . '" />  
            </div>
          </div>
          <hr><br>
        </div>
      ';
    }else {
      $image = 1;
      $adverting_ofert = '
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <p class="text-center">Publicidad</p>
              <img alt="pauta ' . $image . '" class="img-adverting-oferts" src="' . $dirname . $image . '.jpg' . '" />  
            </div>
          </div>
          <hr><br>
        </div>
      ';
      $flag_image = TRUE;
    }

    if($flag_image == TRUE) {
      $_SESSION['advertising']['result_oferts']['current'] = 1;
      $flag_image = FALSE;
    }
    $_SESSION['advertising']['result_oferts']['current']++;

      
    return $adverting_ofert;
  }



  function _control_utilities_render_oferts_priority($result_priority) {
    $list_oferts = '';
    $url_logo = $_SERVER['DOCUMENT_ROOT'] . '/src/img/company/logo/';
    foreach ($result_priority as $key => $value) {
     $validate_next_ofert = 0;
      foreach ($value as $key_data => $value_data) {
        if($validate_next_ofert == 0) {
          $created_o = new DateTime($value['created_o']);
          $list_oferts .= '
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 block-ofert-header">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                      <h4 class="list-group-item-heading">
                        <span class="glyphicon glyphicon-star-empty"> </span>  ' . utf8_encode($value['name_o']) . '
                      </h4>
                    </div> 
                    <div class="col-xs-4 hidden-lg hidden-md hidden-sm">';

                    if (file_exists($url_logo . $value['image_ci']) && is_file($url_logo . $value['image_ci'])) {
                      $list_oferts .= '
                        <img alt="' . $value['image_ci'] .'" class="logo-company" src="../src/img/company/logo/' . $value['image_ci'] .'">';
                      clearstatcache();
                    }else {
                      $list_oferts .= '<p class="logo-company-default-list">
                        '. strtoupper(substr($value['name_ci'], 0, 1)) . '
                      </p>';
                    }

                    
                    $list_oferts .= '</div>
                    <div class="col-xs-8 col-sm-4 col-md-3">
                      <p><span class="glyphicon glyphicon-briefcase"></span>' . utf8_encode($value['name_ci']) . '</p>
                      <p class="hidden-lg hidden-md hidden-sm">
                        <span class="glyphicon glyphicon-time"></span> ' . $created_o->format('d/m/Y') . '
                      </p>
                    </div> 
                    <div class="hidden-xs col-xs-12 col-sm-3 col-md-3">
                      <p>
                        <span class="glyphicon glyphicon-time"></span> ' . $created_o->format('d/m/Y') . '
                      </p>
                    </div>    
                  </div>
                </div>
                <div class="hidden-xs col-xs-2 col-sm-2 col-md-2">';


                    if (file_exists($url_logo . $value['image_ci']) && is_file($url_logo . $value['image_ci'])) {
                      $list_oferts .= '
                        <img alt="' . $value['image_ci'] .'" class="logo-company" src="../src/img/company/logo/' . $value['image_ci'] .'">';
                      clearstatcache();
                    }else {
                      $list_oferts .= '<p class="logo-company-default-list">
                        '. strtoupper(substr($value['name_ci'], 0, 1)) . ' 
                      </p>';
                    }

                $list_oferts .= '</div>
                <div class="col-xs-12 col-sm-8 col-md-8 block-ofert-body-description">
                  <p class="list-group-item-text">
                    ' . utf8_encode($this->control_utilities_clip_text($value['description_o'], 320)) . '
                  </p>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2">
                  <input type="hidden" id="t0k3n_1d_0" name="t0k3n_1d_0" value=' .  $value['id_o'] . '>
                  <button type="button" class="btn btn-success show-ofert btn-block" data-toggle="modal" data-target="#ofert-selected-modal-person">
                    <span class="glyphicon glyphicon-eye-open"> </span> Ver
                  </button>
                  <div class="animation_image" style="display:none;">
                      <img alt="Cargando" src="/src/img/mihv/site/ajax-loader.gif"> Cargando...
                  </div>
                </div>
              </div>
              <br><hr><br>
            </div>   
          ';
        }
        $validate_next_ofert++;
      }
    } 
    return $list_oferts;
  }

  function control_utilities_get_city($id_city) {
    $modelUtilities = new modelUtilities();
    $response = $modelUtilities->model_utilities_get_city($id_city);
    
    return utf8_encode($response['name_c']);
    
  }



  /*
   * Enviar correo a usuario registrado para activar la cuenta
   */
  function control_utilities_send_mail_user_register($type, $token_active_user, $name_user, $email, $pass = FALSE) {

    $protocol = '';
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") { 
      $protocol = "https://";
    } else { 
      $protocol = "http://";
    }

    $html = '';
    $html .='

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8"> <!-- utf-8 works for most cases -->
        <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldnt be necessary -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
        <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

        <style>

            /* What it does: Remove spaces around the email design added by some email clients. */
            /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
            html,
            body {
                margin: 0 auto !important;
                padding: 0 !important;
                height: 100% !important;
                width: 100% !important;
            }
            
            /* What it does: Stops email clients resizing small text. */
            * {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
            }
            
            /* What is does: Centers email on Android 4.4 */
            div[style*="margin: 16px 0"] {
                margin:0 !important;
            }
            
            /* What it does: Stops Outlook from adding extra spacing to tables. */
            table,
            td {
                mso-table-lspace: 0pt !important;
                mso-table-rspace: 0pt !important;
            }
                    
            /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
            table {
                border-spacing: 0 !important;
                border-collapse: collapse !important;
                table-layout: fixed !important;
                margin: 0 auto !important;
            }
            table table table {
                table-layout: auto; 
            }
            
            /* What it does: Uses a better rendering method when resizing images in IE. */
            img {
                -ms-interpolation-mode:bicubic;
            }
            
            /* What it does: A work-around for iOS meddling in triggered links. */
            .mobile-link--footer a,
            a[x-apple-data-detectors] {
                color:inherit !important;
                text-decoration: none !important;
            }
          
        </style>
        
        <!-- Progressive Enhancements -->
        <style>
            
            /* What it does: Hover styles for buttons */
            .button-td,
            .button-a {
                transition: all 100ms ease-in;
            }
            .button-td:hover,
            .button-a:hover {
                background: #555555 !important;
                border-color: #555555 !important;
            }


        </style>

    </head>
    <body width="100%" bgcolor="#ffffff" style="margin: 0; background: #ffffff;">
        <center style="width: 100%; background: #ffffff;">

            <!-- Visually Hidden Preheader Text : BEGIN -->
            <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
                Hola, has solicitado la inscripción en Mi HV. Actíva tu cuenta para acceder a nuestro portal de ofertas.
            </div>
            <!-- Visually Hidden Preheader Text : END -->

            <!--    
                Set the email width. Defined in two places:
                1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 600px.
                2. MSO tags for Desktop Windows Outlook enforce a 600px width.
            -->
            <div style="max-width: 600px; margin: auto;">
                <!--[if mso]>
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="600" align="center">
                <tr>
                <td>
                <![endif]-->

                <!-- Email Header : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
                    <tr>
                        <td style="padding: 20px 0; text-align: center;">
                          <img alt="Saludo email" src="https://imagenes.mihv.com.co/email/saludo.jpg" alt="alt_text" border="0" align="center" style="width: 100%; max-width: 600px; background: none; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding-bottom:10px;">

                            <img alt="Header mi hv" src="https://imagenes.mihv.com.co/email/header-logo.jpg" width="90%" height="50" alt="alt_text" border="0" style="background: none; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                        </td>
                    </tr>
                </table>
                <!-- Email Header : END -->
                
                <!-- Email Body : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
                    
                    

                    <!-- 1 Column Text + Button : BEGIN -->
                    <tr>
                        <td bgcolor="#ffffff">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td style="padding: 40px; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                        Hola ' . $name_user . ' como estas?:
                                        <br><br>
                                        Has tomado una excelente decisión al inscribirte en nuestro portal y por ello te damos la bienvenida.
                                        <br><br>
                                        Queremos decirte que ya te encuentras registrado y te queda un paso el cual es confirmar éste correo por cuestiones de seguridad, pero es fácil, dale click en el botón Confirmar Mi Correo
                                        <br><br>
                                        <!-- Button : Begin -->
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto;">
                                            <tr>
                                                <td style="border-radius: 3px; background: #222222; text-align: center;" class="button-td">
                                                    <a href="' . $protocol . $_SERVER['HTTP_HOST'] . '/controls/control_active_user.php?active_user=' . $token_active_user . '&type=' . $type . '" style="background: #337AB7; border: 15px solid #337AB7; font-family: sans-serif; font-size: 13px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" class="button-a" target="_blank">
                                                        &nbsp;&nbsp;&nbsp;&nbsp;<span style="color:#ffffff">Confirmar Mi Correo</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- Button : END -->
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto;" >
                                        <br>                                    
                                        Si presionaste el botón y aún no pasa nada, no hay lio; dale click en el siguiente link y si no funciona, cópialo:<br><br>
                                        <a href="' . $protocol . $_SERVER['HTTP_HOST'] . '/controls/control_active_user.php?active_user=' . $token_active_user . '&type=' . $type . '" style="word-wrap: break-word;">
                                        ' . $protocol . $_SERVER['HTTP_HOST'] . '/controls/control_active_user.php?active_user=' . $token_active_user . '&type=' . $type . '
                                        </a>
                                        </table>
                                        <br><br>
                                        Una vez copiado abre una nueva pestaña o ventana en tu navegador, pégalo ahí y presiona ENTER y así confirmas tu correo.
                                        <br><br>
                                        Una vez hecho lo del link te invitamos a que ingreses a <a href="https://www.mihv.com.co">www.mihv.com.co</a> con tus datos y realices tu búsqueda.
                                        <br><br>';
                                        if ($pass != FALSE) {
                                          $html .='Estos son tus datos de acceso:<br>
                                          <b>Usuario: </b> ' . $email . '<br>
                                          <b>Clave: </b> ' . $pass . '';
                                        }else {
                                          $html .='Este es tu usuario de acceso:<br>
                                          <b>Usuario: </b> ' . $email . '<br>';
                                        }

                                        $html .='<br><br>
                                        Éxitos en tu búsqueda y felíz resto de día.
                                        <br><br><br>
                                        El equipo de Mi HV.
                                        <br><br><br>
                                        PD: Posiblemente te interese...<br>
                                    </td>
                                    </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- Email Body : END -->

                <!-- 2 Even Columns : BEGIN -->
                    <tr>                    
                        <td bgcolor="#ffffff" align="center" height="100%" valign="top" width="100%" style="padding-bottom: 40px">
                            <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:560px;">
                                <tr>
                                    <td align="center" valign="top" width="50%">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                            <tr>
                                                <td style="text-align: center; padding: 0 10px;">
                                                    <a href="https:/www.google.com.co" target="_blank">
                                                    <img alt="pauta email" src="https://imagenes.mihv.com.co/email/200-200.png" width="200" height="200" alt="alt_text" class="center-on-narrow" style="border: 0;width: 100%;max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                                    </a>
                                                </td>
                                            </tr>                                        
                                        </table>
                                    </td>
                                    <td align="center" valign="top" width="50%">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                            <tr>
                                                <td style="text-align: center; padding: 0 10px;">
                                                    <a href="https:/www.google.com.co" target="_blank">
                                                    <img alt="pauta email" src="https://imagenes.mihv.com.co/email/200-200.png" width="200" height="200" alt="alt_text" class="center-on-narrow" style="border: 0;width: 100%;max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                                    </a>
                                                </td>
                                            </tr>
                                            
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Two Even Columns : END -->
              
                <!-- Email Footer : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;">
                    <tr>
                        <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;">                        
                            <span class="mobile-link--footer"><a href="https://www.codection.pro" target="_blank">Mi HV (CODECTION SAS)</a></span><br><span class="mobile-link--footer">Carrera 13 # 13 - 24 Oficina 725<br>Bogotá D.C., C/marca, 110311 CO</span><br><span class="mobile-link--footer"><a href="tel:+573165269362"><img alt="whatsapp" src="https://imagenes.mihv.com.co/email/whatsapp.jpg" width="100%" style="max-width:9px;"> (+57) 316 526 9362</a></span>
                            <!--<br><br>
                            <unsubscribe style="color:#888888; text-decoration:underline;">unsubscribe</unsubscribe>-->
                        </td>
                    </tr>
                </table>
                <!-- Email Footer : END -->

                <!--[if mso]>
                </td>
                </tr>
                </table>
                <![endif]-->
            </div>
        </center>
    </body>
    </html>
    ';

    $subject = 'Activa tu usario MI HV';

    //incluir libreria para correo
    require_once($_SERVER['DOCUMENT_ROOT'] . "/libraries/PHPMailer-master/class.phpmailer.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/libraries/PHPMailer-master/class.smtp.php");

    $mail = new PHPMailer;
    $mail->IsSMTP();
    //permite modo debug para ver mensajes de las cosas que van ocurriendo
    //$mail->SMTPDebug = 2;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = "starttls";
    $mail->Host = "mail.mihv.co";
    $mail->Port = 587;
    $mail->Username = "informacion";
    $mail->Password = "Info2016*..*";
    $mail->From = "informacion@envios.mihv.co";   
    $mail->FromName = "MI HV - Activa tu usuario";
    $mail->Subject = $subject;
    $mail->AddAddress($email);
    $mail->MsgHTML(utf8_decode($html));
    $mail->IsHTML(true);
    //$mail->AddAttachment('../uploadAlDia/mi_archivo.jpg');
    
    if ($mail->Send()) {
      return true;
    }else {
      return false;
    }
  }

  /*
   * Enviar correo Recuperar Contrasenia
   */
  function control_utilities_send_mail_recover_password($id_user, $name, $email, $type, $token_new_password) {

    $protocol = '';
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") { 
      $protocol = "https://";
    } else { 
      $protocol = "http://";
    }

    $html = '';
    $html .='
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"> <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

    <style>

        /* What it does: Remove spaces around the email design added by some email clients. */
        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }
        
        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        
        /* What is does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }
        
        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }
                
        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto; 
        }
        
        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }
        
        /* What it does: A work-around for iOS meddling in triggered links. */
        .mobile-link--footer a,
        a[x-apple-data-detectors] {
            color:inherit !important;
            text-decoration: none !important;
        }
      
    </style>
    
    <!-- Progressive Enhancements -->
    <style>
        
        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #555555 !important;
            border-color: #555555 !important;
        }

    </style>

</head>
<body width="100%" bgcolor="#ffffff" style="margin: 0;">
    <center style="width: 100%; background: #ffffff;">

        <!-- Visually Hidden Preheader Text : BEGIN -->
        <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
            Hola '.$name.' has solicitado la restauración de clave de tu usuario en Mi HV. Abre este mail para más información.
        </div>
        <!-- Visually Hidden Preheader Text : END -->

        <div style="margin: auto;">
            <!--[if mso]>
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="90%" align="center">
            <tr>
            <td>
            <![endif]-->

            <!-- Email Body : BEGIN ddddddddddeeeeeeeeeeeeeeesssssssssssddddddddeeeeeeee aqui -->
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="90%">
                
                <!-- Email Header : BEGIN -->
                
                <tr>
                    <td style="padding: 20px 0; text-align: center">
                      <img alt="login" src="https://imagenes.mihv.com.co/email/login-registro.jpg" width="100%" height="auto" alt="logo1" border="0" align="center" style="background: none; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding-bottom: 20px;">

                        <img alt="Header mi hv" src="https://imagenes.mihv.com.co/email/header-logo.jpg" width="100%" height="auto" alt="mihvlogo" border="0" style="background: none; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                    </td>
                </tr>
              
                <!-- Email Header : END -->

                <!-- 1 Column Text + Button : BEGIN -->
                <tr>
                    <td bgcolor="#ffffff">
                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" align="justify">
                            <tr>
                                <td style="padding: 40px; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                    Hola '.$name.' como vas:
                                    <br><br>
                                    Hemos recibido la solicitud de restauración de tu contraseña.
                                    <br><br>
                                    Si fuiste tú presiona el siguiente botón el cuál te llevará a ingresar tu nueva contraseña
                                    <br><br>
                                    <!-- Button : Begin -->
                                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto;">
                                        <tr>
                                            <td style="border-radius: 3px; background: #222222; text-align: center;" class="button-td">
                                                <a style="background: #337AB7; border: 15px solid #337AB7; font-family: sans-serif; font-size: 13px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" target="_blank" href="' . $protocol . $_SERVER['HTTP_HOST'] . '/views/recover_password.php?token_new_password=' . $token_new_password . '&type=' . $type . '&1dus3r='. $id_user .'" class="button-a">&nbsp;&nbsp;&nbsp;&nbsp;<span style="color:#ffffff">Cambia tu clave</span>&nbsp;&nbsp;&nbsp;&nbsp;</a>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- Button : END -->
                                    <br>                                    
                                    Si presionaste el botón y aún no pasa nada, tranquilo, copia o dale click el siguiente link:<br><br>
                                    <a href="' . $protocol . $_SERVER['HTTP_HOST'] . '/views/recover_password.php?token_new_password=' . $token_new_password . '&type=' . $type . '&1dus3r='. $id_user .'" style="word-wrap: break-word;">' . $protocol . $_SERVER['HTTP_HOST'] . '/views/recover_password.php?token_new_password=' . $token_new_password . '&type=' . $type . '&1dus3r='. $id_user .'</a>
                                    <br><br>
                                    Una vez copiado abre una nueva pestaña o ventana en tu navegador, pégalo ahí y presiona ENTER y podrás colocar tu nueva clave de acceso.
                                    <br><br>
                                    Recuerda tener una clave que sea fácil de recordar y en lo posible sea diferente de claves como cuentas bancarias o transacciones personales.
                                    <br><br>
                                    Si no quieres cambiar tu clave o ya te acordaste, fácil, haz caso omiso a este correo y te invitamos a que ingreses a <a href="https://www.mihv.com.co">www.mihv.com.co</a> y realices tu búsqueda.
                                    <br><br>
                                    Éxitos y que pases un excelente día.
                                    <br><br><br>
                                    El equipo de Mi HV.
                                    <br><br><br>
                                    PD: Tal vez te interese...<br>
                                </td>
                                </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- Email Body : END -->

            <!-- 2 Even Columns : BEGIN -->
                <tr>                    
                    <td bgcolor="#ffffff" align="center" height="100%" valign="top" width="100%" style="padding-bottom: 40px">
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:560px;">
                            <tr>
                                <td align="center" valign="top" width="50%">
                                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                        <tr>
                                            <td style="text-align: center; padding: 0 10px;">
                                                <a href="https://www.google.com.co" target="_blank">
                                                <img alt="pauta email" src="https://imagenes.mihv.com.co/email/200-200.png" width="200" height="200" alt="alt_text" class="center-on-narrow" style="border: 0;width: 100%;max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                                </a>
                                            </td>
                                        </tr>                                        
                                    </table>
                                </td>
                                <td align="center" valign="top" width="50%">
                                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                        <tr>
                                            <td style="text-align: center; padding: 0 10px;">
                                                <a href="https://www.google.com.co" target="_blank">
                                                <img alt="pauta email" src="https://imagenes.mihv.com.co/email/200-200.png" width="200" height="200" alt="alt_text" class="center-on-narrow" style="border: 0;width: 100%;max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                                </a>
                                            </td>
                                        </tr>
                                        
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- Two Even Columns : END -->
          
            <!-- Email Footer : BEGIN -->
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;">
                <tr>
                    <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;">                        
                        <span class="mobile-link--footer"><a href="https://www.codection.pro" target="_blank">Mi HV (CODECTION SAS)</a></span><br><span class="mobile-link--footer">Carrera 13 # 13 - 24 Oficina 725<br>Bogotá D.C., C/marca, 110311 CO</span><br><span class="mobile-link--footer"><a href="tel:+573165269362"><img alt="whatsapp" src="https://imagenes.mihv.com.co/email/whatsapp.jpg" width="100%" style="max-width:9px;"> (+57) 316 526 9362</a></span>
                        <!--<br><br>
                        <unsubscribe style="color:#888888; text-decoration:underline;">unsubscribe</unsubscribe>-->
                    </td>
                </tr>
            </table>
            <!-- Email Footer : END -->

            <!--[if mso]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </div>
    </center>
</body>
</html>                

                
    ';

    $subject = 'Recuperar Clave - MI HV';

    //incluir libreria para correo
    require_once($_SERVER['DOCUMENT_ROOT'] . "/libraries/PHPMailer-master/class.phpmailer.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/libraries/PHPMailer-master/class.smtp.php");
    
    $mail = new PHPMailer;
    $mail->IsSMTP();
    //permite modo debug para ver mensajes de las cosas que van ocurriendo
    //$mail->SMTPDebug = 2;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = "starttls";
    $mail->Host = "mail.mihv.co";
    $mail->Port = 587;
    $mail->Username = "informacion";
    $mail->Password = "Info2016*..*";
    $mail->From = "informacion@envios.mihv.co";    
    $mail->FromName = "MI HV - Recupera tu clave";
    $mail->Subject = $subject;
    $mail->AddAddress($email);
    $mail->MsgHTML(utf8_decode($html));
    $mail->IsHTML(true);
    //$mail->AddAttachment('../uploadAlDia/mi_archivo.jpg');
    
    if ($mail->Send()) {
      return true;
    }else {
      return false;
    }
  }

  /*
   * Enviar correo a soporte
   */
  function control_utilities_send_mail_support($email_support, $content, $subject, $from_name, $attach = array()) {

    $html = '';
    $html .='

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8"> <!-- utf-8 works for most cases -->
        <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldnt be necessary -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
        <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->
    </head>
    <body width="100%" bgcolor="#222222" style="margin: 0;">
        <center style="width: 100%; background: #ffffff;">
          ' . $content . '
        </center>
    </body>
    </html>
    ';

    //incluir libreria para correo
    require_once($_SERVER['DOCUMENT_ROOT'] . "/libraries/PHPMailer-master/class.phpmailer.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/libraries/PHPMailer-master/class.smtp.php");
    
    $mail = new PHPMailer;
    $mail->IsSMTP();
    //permite modo debug para ver mensajes de las cosas que van ocurriendo
    //$mail->SMTPDebug = 2;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = "starttls";
    $mail->Host = "mail.mihv.co";
    $mail->Port = 587;
    $mail->Username = "informacion";
    $mail->Password = "Info2016*..*";
    $mail->From = "informacion@envios.mihv.co";   
    $mail->FromName = $from_name;
    $mail->Subject = $subject;
    $mail->AddAddress($email_support);
    $mail->MsgHTML(utf8_decode($html));
    $mail->IsHTML(true);

    if (!empty($attach)) {
      foreach ($attach as $key => $value) {
        $mail->AddAttachment($_SERVER['DOCUMENT_ROOT'] . $value);  
      }
    }
    
    
    if ($mail->Send()) {
      return true;
    }else {
      return false;
    }
  }

  /*
   * Envia correo de notificacion de oferta que no se ha finalizado
   */
  function control_utilities_send_mail_user_offer_life_cycle($id_o, $name_o, $email_ci, $name_ci, $time) {

    $protocol = '';
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") { 
      $protocol = "https://";
    } else { 
      $protocol = "http://";
    }

    $html = '';
    $html .='

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8"> <!-- utf-8 works for most cases -->
        <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldnt be necessary -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
        <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

        <style>

            /* What it does: Remove spaces around the email design added by some email clients. */
            /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
            html,
            body {
                margin: 0 auto !important;
                padding: 0 !important;
                height: 100% !important;
                width: 100% !important;
            }
            
            /* What it does: Stops email clients resizing small text. */
            * {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
            }
            
            /* What is does: Centers email on Android 4.4 */
            div[style*="margin: 16px 0"] {
                margin:0 !important;
            }
            
            /* What it does: Stops Outlook from adding extra spacing to tables. */
            table,
            td {
                mso-table-lspace: 0pt !important;
                mso-table-rspace: 0pt !important;
            }
                    
            /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
            table {
                border-spacing: 0 !important;
                border-collapse: collapse !important;
                table-layout: fixed !important;
                margin: 0 auto !important;
            }
            table table table {
                table-layout: auto; 
            }
            
            /* What it does: Uses a better rendering method when resizing images in IE. */
            img {
                -ms-interpolation-mode:bicubic;
            }
            
            /* What it does: A work-around for iOS meddling in triggered links. */
            .mobile-link--footer a,
            a[x-apple-data-detectors] {
                color:inherit !important;
                text-decoration: none !important;
            }
          
        </style>
        
        <!-- Progressive Enhancements -->
        <style>
            
            /* What it does: Hover styles for buttons */
            .button-td,
            .button-a {
                transition: all 100ms ease-in;
            }
            .button-td:hover,
            .button-a:hover {
                background: #555555 !important;
                border-color: #555555 !important;
            }


        </style>

    </head>
    <body width="100%" bgcolor="#ffffff" style="margin: 0; background: #ffffff;">
        <center style="width: 100%; background: #ffffff;">

            <!-- Visually Hidden Preheader Text : BEGIN -->
            <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
                Hola, queremos saber como te fue con la oferta ' . $name_o . ' despues de ' . $time . ' días publicada.
            </div>
            <!-- Visually Hidden Preheader Text : END -->

            <!--    
                Set the email width. Defined in two places:
                1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 600px.
                2. MSO tags for Desktop Windows Outlook enforce a 600px width.
            -->
            <div style="max-width: 600px; margin: auto;">
                <!--[if mso]>
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="600" align="center">
                <tr>
                <td>
                <![endif]-->

                <!-- Email Header : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
                    <tr>
                        <td style="padding: 20px 0; text-align: center;">
                            <img alt="Header mi hv" src="https://imagenes.mihv.com.co/email/header-logo.jpg" width="90%" height="50" alt="alt_text" border="0" style="background: none; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                        </td>
                    </tr>
                </table>
                <!-- Email Header : END -->
                
                <!-- Email Body : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
                    
                    <!-- 1 Column Text + Button : BEGIN -->
                    <tr>
                        <td bgcolor="#ffffff">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td style="padding: 40px; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                        Hola <b>' . $name_ci . '</b>
                                        <br><br>
                                        El motivo de este correo es notificarte el estado de tu oferta publicada.
                                        <br><br>
                                        tu oferta <b>' . $name_o . '</b> aun se encuentra abierta despues de <b>' . $time . ' días</b>. si ya concluiste con tu oferta, porfavor haznolo saber finalizando la oferta, dale click en el botón Búscar Oferta y una ves ingreses a <b>MI HV</b> finaliza la oferta!
                                        <br><b>"debe estar su sesión iniciada"</b><br><br>
                                        <!-- Button : Begin -->
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto;">
                                            <tr>
                                                <td style="border-radius: 3px; background: #222222; text-align: center;" class="button-td">
                                                    <a href="' . $protocol . $_SERVER['HTTP_HOST'] . '/views/profile_company.php?tab=2&name-offer=' . $name_o . '" style="background: #337AB7; border: 15px solid #337AB7; font-family: sans-serif; font-size: 13px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" class="button-a" target="_blank">
                                                        &nbsp;&nbsp;&nbsp;&nbsp;<span style="color:#ffffff">Búscar Oferta</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- Button : END -->
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto;" >
                                        <br>                                    
                                        Si presionaste el botón y aún no pasa nada, no hay lio; dale click en el siguiente link y si no funciona, cópialo:<br><br>
                                        <a href="' . $protocol . $_SERVER['HTTP_HOST'] . '/views/profile_company.php?tab=2&name-offer=' . $name_o . '" style="word-wrap: break-word;">
                                          ' . $protocol . $_SERVER['HTTP_HOST'] . '/views/profile_company.php?tab=2&name-offer=' . $name_o . '
                                        </a>
                                        </table>
                                        <br><br>

                                        Éxitos en tu búsqueda y felíz resto de día.
                                        <br><br><br>
                                        El equipo de Mi HV.
                                        <br><br><br>
                                        PD: Posiblemente te interese...<br>
                                    </td>
                                    </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- Email Body : END -->

                <!-- 2 Even Columns : BEGIN -->
                    <tr>                    
                        <td bgcolor="#ffffff" align="center" height="100%" valign="top" width="100%" style="padding-bottom: 40px">
                            <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:560px;">
                                <tr>
                                    <td align="center" valign="top" width="50%">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                            <tr>
                                                <td style="text-align: center; padding: 0 10px;">
                                                    <a href="https:/www.google.com.co" target="_blank">
                                                    <img alt="pauta email" src="https://imagenes.mihv.com.co/email/200-200.png" width="200" height="200" alt="alt_text" class="center-on-narrow" style="border: 0;width: 100%;max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                                    </a>
                                                </td>
                                            </tr>                                        
                                        </table>
                                    </td>
                                    <td align="center" valign="top" width="50%">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                            <tr>
                                                <td style="text-align: center; padding: 0 10px;">
                                                    <a href="https:/www.google.com.co" target="_blank">
                                                    <img alt="pauta email" src="https://imagenes.mihv.com.co/email/200-200.png" width="200" height="200" alt="alt_text" class="center-on-narrow" style="border: 0;width: 100%;max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                                    </a>
                                                </td>
                                            </tr>
                                            
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Two Even Columns : END -->
              
                <!-- Email Footer : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;">
                    <tr>
                        <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;">                        
                            <span class="mobile-link--footer"><a href="https://www.codection.pro" target="_blank">Mi HV (CODECTION SAS)</a></span><br><span class="mobile-link--footer">Carrera 13 # 13 - 24 Oficina 725<br>Bogotá D.C., C/marca, 110311 CO</span><br><span class="mobile-link--footer"><a href="tel:+573165269362"><img alt="whatsapp" src="https://imagenes.mihv.com.co/email/whatsapp.jpg" width="100%" style="max-width:9px;"> (+57) 316 526 9362</a></span>
                            <!--<br><br>
                            <unsubscribe style="color:#888888; text-decoration:underline;">unsubscribe</unsubscribe>-->
                        </td>
                    </tr>
                </table>
                <!-- Email Footer : END -->

                <!--[if mso]>
                </td>
                </tr>
                </table>
                <![endif]-->
            </div>
        </center>
    </body>
    </html>
    ';

    $subject = 'Estado de oferta - MI HV';

    //incluir libreria para correo
    require_once($_SERVER['DOCUMENT_ROOT'] . "/libraries/PHPMailer-master/class.phpmailer.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/libraries/PHPMailer-master/class.smtp.php");

    $mail = new PHPMailer;
    $mail->IsSMTP();
    //permite modo debug para ver mensajes de las cosas que van ocurriendo
    //$mail->SMTPDebug = 2;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = "starttls";
    $mail->Host = "mail.mihv.co";
    $mail->Port = 587;
    $mail->Username = "informacion";
    $mail->Password = "Info2016*..*";
    $mail->From = "informacion@envios.mihv.co";   
    $mail->FromName = "MI HV - Estado de Oferta";
    $mail->Subject = $subject;
    $mail->AddAddress($email_ci);
    $mail->MsgHTML(utf8_decode($html));
    $mail->IsHTML(true);
    //$mail->AddAttachment('../uploadAlDia/mi_archivo.jpg');
    
    if ($mail->Send()) {
      return true;
    }else {
      return false;
    }
  }


  /*
   * Envia correo de notificacion de oferta finalizada por el sistema
   */
  function control_utilities_send_mail_user_offer_life_cycle_finalize($id_o, $name_o, $email_ci, $name_ci, $time) {

    $protocol = '';
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") { 
      $protocol = "https://";
    } else { 
      $protocol = "http://";
    }

    $html = '';
    $html .='

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8"> <!-- utf-8 works for most cases -->
        <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldnt be necessary -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
        <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

        <style>

            /* What it does: Remove spaces around the email design added by some email clients. */
            /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
            html,
            body {
                margin: 0 auto !important;
                padding: 0 !important;
                height: 100% !important;
                width: 100% !important;
            }
            
            /* What it does: Stops email clients resizing small text. */
            * {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
            }
            
            /* What is does: Centers email on Android 4.4 */
            div[style*="margin: 16px 0"] {
                margin:0 !important;
            }
            
            /* What it does: Stops Outlook from adding extra spacing to tables. */
            table,
            td {
                mso-table-lspace: 0pt !important;
                mso-table-rspace: 0pt !important;
            }
                    
            /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
            table {
                border-spacing: 0 !important;
                border-collapse: collapse !important;
                table-layout: fixed !important;
                margin: 0 auto !important;
            }
            table table table {
                table-layout: auto; 
            }
            
            /* What it does: Uses a better rendering method when resizing images in IE. */
            img {
                -ms-interpolation-mode:bicubic;
            }
            
            /* What it does: A work-around for iOS meddling in triggered links. */
            .mobile-link--footer a,
            a[x-apple-data-detectors] {
                color:inherit !important;
                text-decoration: none !important;
            }
          
        </style>
        
        <!-- Progressive Enhancements -->
        <style>
            
            /* What it does: Hover styles for buttons */
            .button-td,
            .button-a {
                transition: all 100ms ease-in;
            }
            .button-td:hover,
            .button-a:hover {
                background: #555555 !important;
                border-color: #555555 !important;
            }


        </style>

    </head>
    <body width="100%" bgcolor="#ffffff" style="margin: 0; background: #ffffff;">
        <center style="width: 100%; background: #ffffff;">

            <!-- Visually Hidden Preheader Text : BEGIN -->
            <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
                Hola, tu oferta ' . $name_o . ' ha sido finalizada automaticamente.
            </div>
            <!-- Visually Hidden Preheader Text : END -->

            <!--    
                Set the email width. Defined in two places:
                1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 600px.
                2. MSO tags for Desktop Windows Outlook enforce a 600px width.
            -->
            <div style="max-width: 600px; margin: auto;">
                <!--[if mso]>
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="600" align="center">
                <tr>
                <td>
                <![endif]-->

                <!-- Email Header : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
                    <tr>
                        <td style="padding: 20px 0; text-align: center;">
                            <img alt="Header mi hv" src="https://imagenes.mihv.com.co/email/header-logo.jpg" width="90%" height="50" alt="alt_text" border="0" style="background: none; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                        </td>
                    </tr>
                </table>
                <!-- Email Header : END -->
                
                <!-- Email Body : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
                    
                    <!-- 1 Column Text + Button : BEGIN -->
                    <tr>
                        <td bgcolor="#ffffff">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td style="padding: 40px; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                        Hola <b>' . $name_ci . '</b>
                                        <br><br>
                                        El motivo de este correo es notificarte que tu oferta ha sido finalizada.
                                        <br><br>
                                        tu oferta <b>' . $name_o . '</b> ha sido finalizada automaticamente por el sistema de <b>MI HV</b>, porque ha terminado el periodo de publicacion de <b>' . $time . ' días</b>. esperamos que hayas encontrado el perfil para tu oferta!
                                        
                                        <!-- Button : END -->
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto;" >
                                        <br>                                    
                                        Puedes ingresar a <b>MI HV</b> y crear una nueva oferta!<br><br>
                                        <a href="' . $protocol . $_SERVER['HTTP_HOST'] . '/views/profile_company.php" style="word-wrap: break-word;">
                                          ' . $protocol . $_SERVER['HTTP_HOST'] . '/views/profile_company.php
                                        </a>
                                        </table>
                                        <br><br>

                                        Éxitos en tu búsqueda y felíz resto de día.
                                        <br><br><br>
                                        El equipo de Mi HV.
                                        <br><br><br>
                                        PD: Posiblemente te interese...<br>
                                    </td>
                                    </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- Email Body : END -->

                <!-- 2 Even Columns : BEGIN -->
                    <tr>                    
                        <td bgcolor="#ffffff" align="center" height="100%" valign="top" width="100%" style="padding-bottom: 40px">
                            <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:560px;">
                                <tr>
                                    <td align="center" valign="top" width="50%">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                            <tr>
                                                <td style="text-align: center; padding: 0 10px;">
                                                    <a href="https:/www.google.com.co" target="_blank">
                                                    <img alt="pauta email" src="https://imagenes.mihv.com.co/email/200-200.png" width="200" height="200" alt="alt_text" class="center-on-narrow" style="border: 0;width: 100%;max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                                    </a>
                                                </td>
                                            </tr>                                        
                                        </table>
                                    </td>
                                    <td align="center" valign="top" width="50%">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                            <tr>
                                                <td style="text-align: center; padding: 0 10px;">
                                                    <a href="https:/www.google.com.co" target="_blank">
                                                    <img alt="pauta email" src="https://imagenes.mihv.com.co/email/200-200.png" width="200" height="200" alt="alt_text" class="center-on-narrow" style="border: 0;width: 100%;max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                                    </a>
                                                </td>
                                            </tr>
                                            
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Two Even Columns : END -->
              
                <!-- Email Footer : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;">
                    <tr>
                        <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;">                        
                            <span class="mobile-link--footer"><a href="https://www.codection.pro" target="_blank">Mi HV (CODECTION SAS)</a></span><br><span class="mobile-link--footer">Carrera 13 # 13 - 24 Oficina 725<br>Bogotá D.C., C/marca, 110311 CO</span><br><span class="mobile-link--footer"><a href="tel:+573165269362"><img alt="whatsapp" src="https://imagenes.mihv.com.co/email/whatsapp.jpg" width="100%" style="max-width:9px;"> (+57) 316 526 9362</a></span>
                            <!--<br><br>
                            <unsubscribe style="color:#888888; text-decoration:underline;">unsubscribe</unsubscribe>-->
                        </td>
                    </tr>
                </table>
                <!-- Email Footer : END -->

                <!--[if mso]>
                </td>
                </tr>
                </table>
                <![endif]-->
            </div>
        </center>
    </body>
    </html>
    ';

    $subject = 'Oferta Finalizada - MI HV';

    //incluir libreria para correo
    require_once($_SERVER['DOCUMENT_ROOT'] . "/libraries/PHPMailer-master/class.phpmailer.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/libraries/PHPMailer-master/class.smtp.php");

    $mail = new PHPMailer;
    $mail->IsSMTP();
    //permite modo debug para ver mensajes de las cosas que van ocurriendo
    //$mail->SMTPDebug = 2;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = "starttls";
    $mail->Host = "mail.mihv.co";
    $mail->Port = 587;
    $mail->Username = "informacion";
    $mail->Password = "Info2016*..*";
    $mail->From = "informacion@envios.mihv.co";    
    $mail->FromName = "MI HV - Oferta Finalizada";
    $mail->Subject = $subject;
    $mail->AddAddress($email_ci);
    $mail->MsgHTML(utf8_decode($html));
    $mail->IsHTML(true);
    //$mail->AddAttachment('../uploadAlDia/mi_archivo.jpg');
    
    if ($mail->Send()) {
      return true;
    }else {
      return false;
    }
  }
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
}