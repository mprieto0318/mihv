<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();

if (!isset($_POST['tokenPage']) || !in_array($_POST['tokenPage'], $_SESSION['token']['pages'])) {
  header('Location: ../index.php');
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/model_load_oferts.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");

$functions_sql = new functions_sql();
$modelLoadOferts = new modelLoadOferts();
$messages = new messages_system();
$controlUtilities = new controlUtilities();

switch ($_POST["id_load"]) {
	case 'LOAD_OFERTS':

    if (!isset($_SESSION['advertising']['result_oferts']['current'])) {
      $_SESSION['advertising']['result_oferts']['current'] = 1;
    }

    $list_oferts = '';

    $page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);

    if ($page_number == 0) {
      $result_priority = $modelLoadOferts->model_load_oferts_search_oferts_priority($_POST['it_search'], $_POST['filters'], $_POST['selCity'], $_POST['selDate'], $_POST['selGenre'], $_POST['selSalary']);
      $list_oferts .= $controlUtilities->_control_utilities_render_oferts_priority($result_priority);
    }
    
    //Numero de resultados por consulta
    $item_per_page = 10;
    $position = ($page_number * $item_per_page);
    
    $result = $modelLoadOferts->model_load_oferts_search_oferts($position, $item_per_page, $_POST['it_search'], $_POST['filters'], $_POST['selCity'], $_POST['selDate'], $_POST['selGenre'], $_POST['selSalary']);

    $url_logo = $_SERVER['DOCUMENT_ROOT'] . '/src/img/company/logo/';
    $count_adverting = 0;
    foreach ($result as $key => $value) {
      $count_adverting++;
     $validate_next_ofert = 0;
      foreach ($value as $key_data => $value_data) {
        if($validate_next_ofert == 0) {
          $created_o = new DateTime($value['created_o']);
          $list_oferts .= '
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 block-ofert-header">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                      <h4 class="list-group-item-heading">
                        ' . utf8_encode($value['name_o']) . '
                      </h4>
                    </div> 
                    <div class="col-xs-4 hidden-lg hidden-md hidden-sm">';

                      if (file_exists($url_logo . $value['image_ci']) && is_file($url_logo . $value['image_ci'])) {
                        $list_oferts .= '
                          <img alt="' . $value['image_ci'] . '" class="logo-company" src="../src/img/company/logo/' . $value['image_ci'] .'">';
                        clearstatcache();
                      }else {
                        $list_oferts .= '<p class="logo-company-default-list">
                          '. strtoupper(substr($value['name_ci'], 0, 1)) . ' 
                        </p>';
                      }

                    $list_oferts .= '</div>
                    <div class="col-xs-8 col-sm-4 col-md-3">
                      <p><span class="glyphicon glyphicon-briefcase"></span> ' . utf8_encode($value['name_ci']) . '</p>
                      <p class="hidden-lg hidden-md hidden-sm">
                        <span class="glyphicon glyphicon-time"></span> ' . $created_o->format('d/m/Y') . '
                      </p>
                    </div> 
                    <div class="hidden-xs col-xs-12 col-sm-3 col-md-3">
                      <p class="text-right">
                        <span class="glyphicon glyphicon-time"></span> ' . $created_o->format('d/m/Y') . '
                      </p>
                    </div>    
                  </div>
                </div>
                <div class="hidden-xs col-xs-2 col-sm-2 col-md-2 text-center">';

                  if (file_exists($url_logo . $value['image_ci']) && is_file($url_logo . $value['image_ci'])) {
                        $list_oferts .= '
                          <img alt="' . $value['image_ci'] .'" class="logo-company" src="../src/img/company/logo/' . $value['image_ci'] .'">';
                        clearstatcache();
                      }else {
                        $list_oferts .= '<p class="logo-company-default-list">
                          '. strtoupper(substr($value['name_ci'], 0, 1)) . ' 
                        </p>';
                      }

                $list_oferts .= '</div>
                <div class="col-xs-12 col-sm-8 col-md-8 block-ofert-body-description">
                  <p class="list-group-item-text">
                    ' . utf8_encode($controlUtilities->control_utilities_clip_text($value['description_o'], 200)) . '
                  </p>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2">
                  <input type="hidden" id="t0k3n_1d_0" name="t0k3n_1d_0" value=' .  $value['id_o'] . '>
                  <button type="button" class="btn btn-success show-ofert btn-block" data-toggle="modal" data-target="#ofert-selected-modal-person">
                    <span class="glyphicon glyphicon-eye-open"> </span> Ver
                  </button>
                  <div class="animation_image" style="display:none;">
                      <img alt="Cargando" src="/src/img/mihv/site/ajax-loader.gif"> Cargando...
                  </div>
                </div>
              </div>
              <br><hr><br>
            </div>   
          ';

          // Agregando publicidad cada 9 resultados
          if ($count_adverting == 9) {
            
            $list_oferts .= '
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <a href="/views/info/advertising_info.php" target="_blank">
                    <img alt="pauta aqui" class="img-adverting-oferts" src="/src/img/mihv/site/pauta_aqui_scroll_offer.png" />
                  </a>
                </div>
              </div>
              <hr><br>
            </div>';
            //$list_oferts .= $controlUtilities->_control_utilities_build_result_oferts_adverting();
            $count_adverting = 0;
          }
        }
        
        $validate_next_ofert++;
      }
    } 

    echo $list_oferts;
	break;

  case 'LOAD_OFERT_SELECTED_MODAL':
    $token_search = $controlUtilities->control_utilities_create_token_pages('profile_company');          
    $data_ofert = $modelLoadOferts->model_load_oferts_get_full_data_ofert_seleted($_POST['id_ofert']);  
    //Cargando Lista de Ciudades
    // // COMENTANDO EDISION DE OFERTA 
    //$select_city = $controlUtilities->_control_utilities_get_citys_select($data_ofert['id_c']);

    $select_city = $controlUtilities->_control_utilities_get_citys_name($data_ofert['id_c']);

    // // COMENTANDO EDISION DE OFERTA
    // // Cargando Lista de Generos
    // $options_genre = '';
    // switch ($data_ofert['genre_o']) {
    //   case 'all':
    //     $options_genre = '
    //       <option value="all" selected>Ambos</option>
    //       <option value="male">Masculino</option>
    //       <option value="female">Femenino</option>
    //     ';
    //     break;
    //   case 'male':
    //     $options_genre = '
    //       <option value="all">Ambos</option>
    //       <option value="male" selected>Masculino</option>
    //       <option value="female">Femenino</option>
    //     ';
    //     break;
    //   case 'female':
    //     $options_genre = '
    //       <option value="all">Ambos</option>
    //       <option value="male">Masculino</option>
    //       <option value="female" selected>Femenino</option>
    //     ';
    //     break;
    // }

    // Cargando Genero Generos
    $options_genre = '';
    switch ($data_ofert['genre_o']) {
      case 'all':
        $options_genre = '<input type="text" value="Ambos" class="form-control" readonly>';
        break;
      case 'male':
        $options_genre = '<input type="text" value="Masculino" class="form-control" readonly>';
        break;
      case 'female':
        $options_genre = '<input type="text" value="Femenino" class="form-control" readonly>';
        break;
    }

    // // COMENTANDO EDISION DE OFERTA
    // // Cargando Lista de Tipos de Contrato
    // $options_type_contract = '';
    // switch ($data_ofert['type_contract_o']) {
    //   case 'Indefinido':
    //     $options_type_contract = '
    //       <option value="Indefinido" selected>Indefinido</option>
    //       <option value="Definido">Definido</option>
    //       <option value="Prestacion de Servicios">Prestacion de Servicios</option>
    //       <option value="Freelance">Freelance</option>
    //     ';
    //     break;
    //   case 'definido':
    //     $options_type_contract = '
    //       <option value="Indefinido">Indefinido</option>
    //       <option value="Definido" selected>Definido</option>
    //       <option value="Prestacion de Servicios">Prestacion de Servicios</option>
    //       <option value="Freelance">Freelance</option>
    //     ';
    //     break;
    //   case 'Prestacion de Servicios':
    //     $options_type_contract = '
    //       <option value="Indefinido">Indefinido</option>
    //       <option value="Definido">Definido</option>
    //       <option value="Prestacion de Servicios" selected>Prestacion de Servicios</option>
    //       <option value="Freelance">Freelance</option>
    //     ';
    //     break;
    //   case 'Freelance':
    //     $options_type_contract = '
    //       <option value="Indefinido">Indefinido</option>
    //       <option value="Definido">Definido</option>
    //       <option value="Prestacion de Servicios">Prestacion de Servicios</option>
    //       <option value="Freelance" selected>Freelance</option>
    //     ';
    //     break;
    // }


    // Cargando Lista de Tipos de Contrato
    $options_type_contract = '';
    switch ($data_ofert['type_contract_o']) {
      case 'Indefinido':
        $options_type_contract = '<input type="text" value="Indefinido" class="form-control" readonly>';
        break;
      case 'definido':
        $options_type_contract = '<input type="text" value="Definido" class="form-control" readonly>';
        break;
      case 'Prestacion de Servicios':
        $options_type_contract = '<input type="text" value="Prestacion de Servicios" class="form-control" readonly>';
        break;
      case 'Freelance':
        $options_type_contract = '<input type="text" value="Freelance" class="form-control" readonly>';
        break;
    }

    // // COMENTANDO EDISION DE OFERTA
    // // Cargando Lista de Prioridad
    // $options_priority = '';
    // switch ($data_ofert['priority_o']) {
    //   case 'na':
    //     $options_priority = '
    //       <option value="na" selected>Normal</option>
    //       <option value="urgent">Urgente</option>
    //     ';
    //     break;
    //   case 'urgent':
    //     $options_priority = '
    //       <option value="na">Normal</option>
    //       <option value="urgent" selected>Urgente</option>
    //     ';
    //     break;
    // }

    // Cargando Lista de Prioridad
    $options_priority = '';
    switch ($data_ofert['priority_o']) {
      case 'na':
        $options_priority = '<input type="text" value="Normal" class="form-control" readonly>';
        break;
      case 'urgent':
        $options_priority = '<input type="text" value="Urgente" class="form-control" readonly>';
        break;
    }
    
    // Cargando Estado de la Oferta / PUBLICADO/DESPUBLICADO
    if ($data_ofert['status_o'] == 'OPEN') {
      $btn_status = '<div class="pull-center">
        <input type="submit" id="btn-remove-offer" value="Finalizar Oferta" class="btn-remove-offer btn btn-danger">
        <br>
        <small class="text-muted"><span class="glyphicon glyphicon-exclamation-sign"></span> Al finalizar la oferta no se podrá recuperar</small>
        </div>';
    }else {
      //$btn_status = '<input type="button" id="btn-publish" value="Publicar" class="btn-publish btn btn-primary pull-left">';
      $btn_status = '<p>La oferta no esta publicada</p>';
    }

    $date_created_o = new DateTime($data_ofert['created_o']);

    //------------------------------------------------------------
    // Iniciando Modal
    $modal_ofert = '';

    // - HEADER
    $modal_ofert .= '<div class="modal-header modal-header-ofert-selected-company">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="title-modal-ofert-selected">
          <span class="glyphicon glyphicon-time"> </span> Creada: ' . $date_created_o->format('d/m/Y') . '
        </h4>
      </div>';

    // - BODY
    $modal_ofert .= '
    <div class="modal-body modal-body-ofert-selected-company">
      <form id="form-remove-offer" method="POST" action="/controls/control_profile_company_operations.php" class="form-create-ofert">
                  
        <div class="form-group"> 
          <label class="title-field">NOMBRE</label>
          <div>
            <input type="text" value="' . utf8_encode($data_ofert['name_o']) . '" class="form-control" name="itName" placeholder="Desarrollador de Software" required id="edit-name-ofert" readonly>
            <label id="edit-name-ofert-error" class="error error-edit-ofert" for="itName"></label>
          </div>
        </div>

        <div class="form-group">
          <label class="title-field">DESCRIPCIÓN</label>
          <div>
            <textarea class="form-control" rows="4" required name="itaDescription" placeholder="Requerimos ingenieros, tegnologos o tecnicos en desarrollo de sotfware" id="edit-description-ofert" readonly>' . utf8_encode($data_ofert['description_o']) . '</textarea>
            <label id="edit-descripcion-ofert-error" class="error error-edit-ofert" for="itName"></label>
          </div>
        </div>

        <div class="form-group">
          <label class="title-field">CONOCIMIENTOS / HABILIDADES / DESTREZAS</label>
          <div>
            <textarea class="form-control" rows="4" required name="itakKowledge" placeholder="php, jquery, html5, css3, js, drupal7 backend" id="edit-kowledge-ofert" readonly>' . utf8_encode($data_ofert['knowledge_o']) . '</textarea>
            <label id="edit-kowledge-ofert-error" class="error error-edit-ofert" for="itakKowledge"></label>
          </div>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-sm-4 col-md-4">
              <label class="title-field">PROFESIÓN</label>
              <div>
                <input type="text" value="' . utf8_encode($data_ofert['profession_o']) . '" class="form-control" name="itProfession" placeholder="Sistemas, call center, auxiliar..." id="edit-profession-ofert" readonly>
                <label id="edit-profession-ofert-error" class="error error-edit-ofert" for="itProfession"></label>
              </div>
            </div>
            <div class="col-sm-4 col-md-4">
              <label class="title-field">GENERO</label>
              <div>
                <!-- <select name="selGenre" class="form-control" readonly> -->
                ' . $options_genre . '
                <!-- </select> -->  
              </div>
            </div>
            <div class="col-sm-4 col-md-4">
              <label class="title-field">CIUDAD</label>
              <div>
                <!-- <select name="selCity" data-size="8" class="selectpicker form-control sel-list-city" data-live-search="true" readonly> -->  
                <input type="text" value="' . $select_city . '" class="form-control" readonly>
                <!-- </select> -->  
                <label id="edit-city-ofert-error" class="error error-edit-ofert" for="itName"></label>
              </div>
            </div>
          </div>                
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-sm-3 col-md-3">
              <label class="title-field">SALARIO</label>
              <div>';
                $salary = ($data_ofert['value_o'] > 0) ? $data_ofert['value_o'] : 'A Convenir';
                $modal_ofert .= '<input type="number" value="' . $salary . '" class="form-control" name="selValue" placeholder="1800000" id="edit-value-ofert" readonly>
                <label id="edit-value-ofert-error" class="error error-edit-ofert" for="itName"></label>
              </div>
            </div>

            <div class="col-sm-3 col-md-3">
              <label class="title-field">CONTRATO</label>
              <div>
                <!-- <select class="form-control" name="selTypeContract" readonly> -->  
                  ' . $options_type_contract . '
                <!-- </select> -->  
              </div>
            </div>

            <div class="col-sm-3 col-md-3">
              <label class="title-field">PRIORIDAD</label>
              <div>
                <!-- <select class="form-control" name="selPriority" readonly> -->  
                  ' . $options_priority . '
                <!-- </select> -->  
              </div>
            </div>
          
            <div class="col-sm-3 col-md-3">
              <label class="title-field"># VACANTES</label>
              <div>
                <input type="number" class="form-control" name="selVacancies" value="' . $data_ofert['vacancies_o'] . '" readonly>
              </div>
            </div>
          </div>
        </div>';

        
        $modal_ofert .= '<hr>
        <div class="form-group form-operations">
          <div class="input-group-addon">
            <input type="hidden" name="tokenPage" value="' . $token_search . '" id="tokenPage">
            <input type="hidden" id="t0k3n_1d_0" name="t0k3n_1d_0" value=' .  $data_ofert['id_o'] . '>
            <input type="hidden" name="profileCompanyToken" value="REMOVE_OFFER">
            ' . $btn_status . '
            <div class="animation_image" style="display:none;">
                <img alt="Cargando" src="/src/img/mihv/site/ajax-loader.gif"> Cargando...
            </div>';

            // COMENTANDO OPCIONES DE EDICION DE LA OFERTA
            // <button type="button" name="submitFormUpdateOfert" class="btn btn-success pull-right btn-edit-ofert" onclick="editOfertValidate()">
            //   Guardar Cambios
            // </button>

          $modal_ofert .= '</div>
        </div>

      </form>
    </div>
    ';
    //

    // - FOOTER
    $modal_ofert .= '
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
    </div>
    ';

    echo $modal_ofert;
  break;

  
  case 'LOAD_OFERT_SELECTED_MODAL_PERSON':
    $data_ofert = $modelLoadOferts->model_load_oferts_get_full_data_ofert_seleted($_POST['id_ofert']);
    if(isset($_SESSION['user']["id_pi"])) {
      $validate_apply_ofert = $modelLoadOferts->model_load_oferts_validate_apply_ofert_seleted($_POST['id_ofert']);  
    }else {
      $validate_apply_ofert = 0;
    }

    // Cargando Generos
    $genre = '';
    switch ($data_ofert['genre_o']) {
      case 'all':
        $genre = '
          <p class="data-ofert">Ambos</p>
        ';
        break;
      case 'male':
        $genre = '
          <p class="data-ofert">Masculino</p>
        ';
        break;
      case 'female':
        $genre = '
          <p class="data-ofert">Femenino</p>
        ';
        break;
    }

    $priority_o = '';
    if ($data_ofert['priority_o'] == 'na') {
      $priority_o = 'Normal';
    }else if ($data_ofert['priority_o'] == 'urgent') {
      $priority_o = 'Urgente';
    }else {
      $priority_o = 'No Especifica';
    }

    // Formatenado fecha
    $data_created_o = new DateTime($data_ofert['created_o']);

    // validando usuario logeado para aplicar
    if (isset($_SESSION['user']["rol_pi"]) && $_SESSION['user']["rol_pi"] == 'PERSON' && isset($_SESSION['user']["id_pi"])) {

      if ($validate_apply_ofert["COUNT(id_oa)"] == 0) {
        $btn_apply_ofert = '
          <div class="row">
            <div class="col-md-12 wrapper-btn-operation-ofert">
              <button id="ofert-apply" type="button" class="btn btn-success pull-center">
                <span class="glyphicon glyphicon-hand-right"></span> Aplicar
              </button>
              <div class="animation_image" style="display:none;">
                  <img alt="Cargando" src="/src/img/mihv/site/ajax-loader.gif"> Cargando...
              </div>
            </div>
          </div>
        ';
      }else {
        $btn_apply_ofert = '
          <div class="row">
            <div class="col-md-12 wrapper-btn-operation-ofert">
              <button id="ofert-disapply" type="button" class="btn btn-success pull-center" data-toggle="modal" data-target="#ofert-disapply-modal">
                <span class="glyphicon glyphicon-hand-right"></span> Desaplicar
              </button>
              <div class="animation_image" style="display:none;">
                <img alt="Cargando" src="/src/img/mihv/site/ajax-loader.gif"> Cargando...
              </div>
            </div>
          </div>
        ';
      }

    }else {

      if (!isset($_SESSION['user']['id_su'])) {
        $btn_apply_ofert = '
        <div class="row">
          <div class="col-md-6">
            <p>Para aplicar debes estar registrado en <strong>MI HV</strong></p>
          </div>
          <div class="col-md-6 text-center">
            <button id="register-modal" type="button" class="btn btn-success" data-toggle="modal" data-target="#registerModal">
              <span class="glyphicon glyphicon-paste"></span> Registrate
            </button>
            <button id="login-modal" type="button" class="btn btn-success" data-toggle="modal" data-target="#loginModal">
              <span class="glyphicon glyphicon-user"></span> Ingresa
            </button>
          </div>
        </div>';
      }else {
        $btn_apply_ofert = '';
      }
    }
    
    //------------------------------------------------------------
    // Iniciando Modal
    $modal_ofert = '';

    // - HEADER
    $modal_ofert .= '<div class="modal-header modal-header-ofert-selected-person">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="title-modal-ofert-selected-person">
         ' . utf8_encode($data_ofert['name_o']) . ' 
        </h4>
      </div>';

    // - BODY
    $modal_ofert .= '
    <div class="modal-body modal-body-ofert-selected-person">
      <form>
        
        <div class="form-group">
          <div class="row">
            <div class="col-xs-8 col-sm-6 col-md-6">
              <label><span class="glyphicon glyphicon-briefcase"></span> EMPRESA</label>
              <div>
                <p class="data-ofert">' . utf8_encode($data_ofert['name_ci']) . '</p>
              </div>
            </div>
            <div class="col-xs-4 col-sm-6 col-md-3">
              <label><span class="glyphicon glyphicon-time"></span> CREADA</label>
              <div>
                <p class="data-ofert">' . $data_created_o->format('d/m/Y') . '</p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3">
              <label>PROFESIÓN</label>
              <div>
                <p class="data-ofert">' . utf8_encode($data_ofert['profession_o']) . '</p>
              </div>
            </div>
          </div>                
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-sm-6 col-md-6">
              <label>DESCRIPCIÓN</label>
              <div>
                <p class="data-ofert">' . utf8_encode($data_ofert['description_o']) . '</p>
              </div>
            </div>

            <div class="col-sm-6 col-md-6">
              <label>CONOCIMIENTOS / HABILIDADES / DESTREZAS</label>
              <div>
                <p class="data-ofert">' . utf8_encode($data_ofert['knowledge_o']) . '</p>
              </div>
            </div>
          </div> 
        </div>

        <hr>

        <div class="form-group">
          <div class="row">
            <div class="col-xs-6 col-sm-3 col-md-2">
              <label>SALARIO</label>';
              $salary = ($data_ofert['value_o'] > 0) ? $data_ofert['value_o'] : 'A Convenir';
              $modal_ofert .= '<div>
                <p class="data-ofert">' . $salary . '</p>
              </div>
            </div>

            <div class="col-xs-6 col-sm-3 col-md-2">
              <label>CONTRATO</label>
              <div>
                <p class="data-ofert text-align-normal">' . utf8_encode($data_ofert['type_contract_o']) . '</p>
              </div>
            </div>

            <div class="col-xs-6 col-sm-3 col-md-2">
              <label>PRIORIDAD</label>
              <div>
                <p class="data-ofert">' . utf8_encode($priority_o) . '</p>
              </div>
            </div>
          
            <div class="col-xs-6 col-sm-3 col-md-2">
              <label># VACANTES</label>
              <div>
                <p class="data-ofert">' . $data_ofert['vacancies_o'] . '</p>
              </div>
            </div>

            <div class="col-xs-6 col-sm-4 col-md-2">
              <label>GENERO</label>
              <div>
                ' . $genre . '
              </div>
            </div>

            <div class="col-xs-6 col-sm-4 col-md-2">
              <label>CIUDAD</label>
              <div>
                <p class="data-ofert">' . utf8_encode($data_ofert['name_c']) . '</p>
              </div>
            </div>

          </div>
        </div>
        
        <div id="form-apply-operations" class="form-group form-operations">
          <input type="hidden" id="t0k3n_1d_0" name="t0k3n_1d_0" value=' .  $data_ofert['id_o'] . '>' . $btn_apply_ofert . '
        </div>
      </form>
    </div>';

    // - FOOTER
    $modal_ofert .= '
    <div class="modal-footer">
      <div class="wrap-message-face">
        <!-- respuesta de facebook --> 
      </div>
      
      <span data-toggle="tooltip" title="Ayuda a tus contactos!" class="element-tooltip">
        <button style="padding: 4px;" type="button" class="btn btn-primary button--share" data-link="' . $_SERVER['HTTP_REFERER'] . '" data-image="https://imagenes.mihv.com.co/social/offer-networks-v1.png" data-caption="' . utf8_encode('Oferta Laboral en MI HV') . '" data-name="' . utf8_encode($data_ofert['name_o']) . '" data-description="' . utf8_encode($data_ofert['description_o']) . '">
          <i class="fa fa-2x fa-fw fa-facebook fa-border" style="font-size: 13px; border-radius: 4px;"></i><span style="font-size: 12px;"> Compartir Oferta</span>
        </button>
      </span>
      <button type="button" class="btn btn-default btn-close-modal" data-dismiss="modal">Cerrar</button>
    </div>';

    echo $modal_ofert;
  break;
}
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com