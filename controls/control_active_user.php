<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

if (isset($_GET['active_user']) && isset($_GET['type'])) {
  session_start();
  require_once($_SERVER['DOCUMENT_ROOT'] . "/models/models_utilities.cls.php");
  require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");

  $modelUtilities = new modelUtilities();
  $functions_sql = new functions_sql();
  $messages = new messages_system();
  
  $values = $functions_sql->functions_sql_clear_dates($_GET);
  $response = $modelUtilities->models_utilities_active_user($values['type'], $values['active_user']);
  
  if ($response == 1) {
    $messege = $messages->messages_success('user actived success');
    $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Usuario Activado!', $messege); 
  }else {
    $messege = $messages->messages_failed('user actived failed');
    $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Error al Activar,', $messege); 
  }
  
  header('Location: /views/login.php');
}else {
  header('Location: /index.php');
}
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
