<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/models/model_profile_company.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");

class controlProfileCompany  
{
  function control_profile_company_get_balances() {
    $modelProfileCompany = new modelProfileCompany();
    $available_balance = $modelProfileCompany->model_profile_company_get_balances();
    return $available_balance;
  }

  function control_profile_company_get_value_offer($priority = NULL, $payment_type = NULL) {
    $modelProfileCompany = new modelProfileCompany();
    $value_offer = $modelProfileCompany->model_profile_company_get_value_offer($priority, $payment_type);
    return $value_offer;
  }

  function control_profile_company_get_publish_free($id_ci) {
    $modelProfileCompany = new modelProfileCompany();
    $data = $modelProfileCompany->model_profile_company_get_publish_free($id_ci);
    return $data;
  }

	function control_profile_company_get_all_oferts_by_user() {
		$modelProfileCompany = new modelProfileCompany();
		$response = $modelProfileCompany->model_profile_company_get_all_oferts_by_user();
		return $response;
	}

  function control_profile_company_get_all_oferts_by_user_rows_table($rows_query) {
    $modelProfileCompany = new modelProfileCompany();
    $rows = '';
    foreach ($rows_query as $key => $value) {
      $rows .= '<tr>';
      foreach ($value as $key2 => $value2) {
        switch ($key2) {
          // case 'id_o':
          //   $rows .= '<td>' . $value['id_o'] . '
          //   </td>';
          //   break;

          case 'name_o':
            $rows .= '<td><button type="button" class="btn btn-link show-ofert" data-toggle="modal" data-target="#ofert-selected-modal" data-ofert="' . $value['id_o'] . '">' . utf8_encode($value2) . '</button>
              <div class="animation_image" style="display:none;">
                <img alt="Cargando" src="/src/img/mihv/site/ajax-loader.gif"> Cargando...
              </div>
            </td>';
            break;

          case 'created_o':
            $data_applied_oa = new DateTime($value2);
            $rows .= '<td><p style="    padding: 7px; margin: 0px;">' .  $data_applied_oa->format('d/m/Y') . '</p></td>';

            
            $today = new DateTime(date('Y-m-d H:i:s'));
            $date_finalize = $data_applied_oa->diff($today);
            //echo $date_finalize->format('%R%a días');

            $life_cycle = ($value['priority_o'] == 'na') ? 20 : 30;

            $rows .= '
              <td>
                <p style="    padding: 7px; margin: 0px;">' .  $date_finalize->format('%a') . '/<b>' . $life_cycle . ' días</b></p>
              </td>';

            break;
          
          case 'options':

            $rows_send = $modelProfileCompany->model_profile_company_load_apply_ofert_selected($value['id_o'], 'APPLIED');

            $rows .= '<td>
              <p class="btn btn-link glyphicon glyphicon-briefcase show-ofert-apply" data-name-ofert="'. utf8_encode($value['name_o']) .'" data-ofert="'. $value['id_o'] .'" data-toggle="modal" data-target="#modal-ofert-apply"> <span class="badge badge-applied style-mihv-count">' . count($rows_send) . '</span></p>
            </td>';
            break;
        } 
      }
      $rows .= '</tr>';
    }

    return $rows;
  }

  function control_profile_company_get_all_billing_company() {
    $modelProfileCompany = new modelProfileCompany();
    $response = $modelProfileCompany->model_profile_company_get_all_billing_company();
    return $response;
  }

  function control_profile_company_get_all_billing_company_rows_table($rows_query, $tokenPage) {
    $path = 'bill/';
    $rows = '';
    foreach ($rows_query as $key => $value) {
      $rows .= '<tr>';
      foreach ($value as $key2 => $value2) {
        switch ($key2) {
          case 'id_sb':
            $rows .= '<td><strong>' . $value2 . '</strong></td>';
            break;

          case 'cost_o':
            $rows .= '<td>$ ' . number_format($value2, 0, ",", ".") . '</td>';
            break;

          case 'date_sb':
            $data_applied_oa = new DateTime($value2);
            $rows .= '<td><p>' .  $data_applied_oa->format('d/m/Y') . '</p></td>';
            break;

          case 'url_bill_pdf':
            $rows .= '<td>
              <a class="btn btn-link" href="/controls/control_force_download.php?file=' . $value2 . '&tokenPage=' . $tokenPage . '&path=' . $path . '">Descargar</a>
            </td>';
            break;
        } 
      }
      $rows .= '</tr>';
    }

    return $rows;
  }


  function control_profile_company_get_recharge_history_by_id($id_ci) {
    $modelProfileCompany = new modelProfileCompany();
    $response = $modelProfileCompany->model_profile_company_get_recharge_history_by_id($id_ci);
    return $response;
  }

  function control_profile_company_get_recharge_history_by_id_rows_table($rows_query, $tokenPage) {
    $path = 'recharge_balance/';
    $rows = '';
    foreach ($rows_query as $key => $value) {
      $rows .= '<tr>';
      foreach ($value as $key2 => $value2) {
        switch ($key2) {
          case 'id_sbr':
            $rows .= '<td><strong>' . $value2 . '</strong></td>';
            break;

          case 'id_transaction_sbr':
            $rows .= '<td>' . $value2 . '</td>';
            break;

          case 'value_sbr':

            $balance_previus = 0;
            
            for ($i=1; $i <= $key; $i++) { 
              $balance_previus += (isset($rows_query[$key-$i]['value_sbr'])) ? $rows_query[$key-$i]['value_sbr'] : 0; 
            }

            $balance_new = $balance_previus + $value2;

            $rows .= '<td>$ ' . number_format($value2, 0, ",", ".") . '</td>';

            $rows .= '<td>$ <span style="text-decoration: line-through;">' . number_format($balance_previus, 0, ",", ".") . '</span>/' . number_format($balance_new, 0, ",", ".") . '</td>';
            
            break;

          case 'created_sbr':
            $data_applied_oa = new DateTime($value2);
            $rows .= '<td><p>' .  $data_applied_oa->format('d/m/Y') . '</p></td>';
            break;

          case 'url_recharge_pdf':
            $rows .= '<td>
              <a class="btn btn-link" href="/controls/control_force_download.php?file=' . $value2 . '&tokenPage=' . $tokenPage . '&path=' . $path . '">Descargar</a>
            </td>';
            break;
        } 
      }
      $rows .= '</tr>';
    }

    return $rows;
  }

  

  function control_profile_company_load_list_ofert_apply($rows_applied, $rows_selected, $rows_discarded, $id_ofert) {
    $list_applied    = $this->control_profile_company_list_ofert_apply($rows_applied, $id_ofert, 'APPLIED');
    $list_selected = $this->control_profile_company_list_ofert_apply($rows_selected, $id_ofert, 'SELECTED');
    $list_discarded = $this->control_profile_company_list_ofert_apply($rows_discarded, $id_ofert, 'DISCARDED');

    $count_status = array(
      'APPLIED' => count($rows_applied), 
      'SELECTED' => count($rows_selected), 
      'DISCARDED' => count($rows_discarded)
    );

    $content = $this->control_profile_company_tabs($list_applied, $list_selected, $list_discarded, $count_status);
    
    return $content;
  }

  function control_profile_company_list_ofert_apply($row_state, $id_ofert, $status) {
    $count = 1;
    $rows = '';
    $rows .= '<div class="col-sm-12 col-md-12 wrapper-rows-person">';
      
    if (empty($row_state)) {
      $rows .= '<div class="row without-results"><div class="col-sm-12 col-md-12 text-center"><b>No hay contenido</b></div></div>';
    }else {
      foreach ($row_state as $key => $value) {
        
        if($status == 'APPLIED') {

          $date = date('Y-m-d H:i:s');
          $date = strtotime ( '-1 day' , strtotime ( $date ) ) ;
          $date = date ('Y-m-d H:i:s' , $date);

          if ($value['date_applied_oa'] >= $date) {
             $new_applied = '<span style="color: green; font-size: 10px;"><b>Nuevo</b></span>';
          }else {
            $new_applied = '';
          }
        }else {
          $new_applied = '';
        }

        $rows .= '<div class="row rows-oferts-apply row-ofert-' . $id_ofert . '-person-' . $value['id_pi'] . '">';
        foreach ($value as $key2 => $value2) {
          switch ($key2) {
            case 'full_name':
              $rows .= '<div class="col-xs-12 col-sm-4 col-md-4"><p class="cursor-pointer btn-link show-person show-person-'. $value['id_pi'] .'" data-person="'. $value['id_pi'] .'" data-person-status="' . $status . '" data-toggle="modal" data-target="#modal-person-selected">' . $value2 . ' ' . $new_applied . '</p></div>';
              break;

            case 'email_pi':
              $rows .= '<div class="col-xs-12 col-sm-4 col-md-4">' . $value2 . '</div>';
              break;

            case 'movil_pi':
              $rows .= '<div class="col-xs-6 col-sm-2 col-md-2">#Cel ' . $value2 . '</div>';
              break;

            case 'date_applied_oa':
              $data_applied_oa = new DateTime($value2);
              $rows .= '<div class="col-xs-6 col-sm-2 col-md-2">' . $data_applied_oa->format('d/m/Y') . '</div>';
              break;
          } 
        }
        $rows .= '</div>';
        $count++;
      }
    }

    $rows .= '</div>';

    return $rows;
  }

  function control_profile_company_tabs($list_send, $list_contact, $list_discard, $count_status) {
    $tabs = '<div class="col-sm-12 col-md-12">';

    //<!--  CONTENEDOR MENSAJES AJAX -->
    $tabs .= '
      <div class="row wrapper-messages-system-ajax">
        
      </div>';

    //<!--  CONTENEDOR BOTONES TABS  -->
    $tabs .= '<div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
                <div class="btn-group" role="group">
                  <button type="button" id="stars" class="btn btn-primary" href="#tab1-ofert-apply" data-toggle="tab">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    <div class="hidden-xs"><span class="badge badge-applied">' . $count_status['APPLIED'] . '</span> Aplicados</div>
                  </button>
                </div>
                <div class="btn-group" role="group">
                  <button type="button" id="favorites" class="btn btn-success" href="#tab2-ofert-apply" data-toggle="tab">
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                    <div class="hidden-xs"><span class="badge badge-selected">' . $count_status['SELECTED'] . '</span> Seleccionados</div>
                  </button>
                </div>
                <div class="btn-group" role="group">
                  <button type="button" id="favorites" class="btn btn-danger" href="#tab3-ofert-apply" data-toggle="tab">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    <div class="hidden-xs"><span class="badge badge-discarded">' . $count_status['DISCARDED'] . '</span>  Descartados</div>
                  </button>
                </div>
              </div>';//<!--  /CONTENEDOR BOTONES TABS  -->

    //<!--  CONTENEDOR TABS  -->
    $tabs .= '
              <div class="well">
                <div class="tab-content">

                  <div class="tab-pane fade in active" id="tab1-ofert-apply">
                    <div class="row">' . $list_send . '</div>
                  </div>

                  <div class="tab-pane fade in" id="tab2-ofert-apply">
                    <div class="row">' . $list_contact . '</div>
                  </div>

                  <div class="tab-pane fade in" id="tab3-ofert-apply">
                    <div class="row">' . $list_discard . '</div>
                  </div>

                </div>
              </div>';

    $tabs .= '</div>';

    return $tabs;
  }


  function control_profile_company_load_full_data_person($rows_query, $id_tab) {
    $controlUtilities = new controlUtilities();
    
    $rows = '';
    $rows .= '<div class="col-sm-12 col-md-12">';
    $rows .= '<div class="row">';
      
      foreach ($rows_query as $key => $value) {
        switch ($key) {
          case 'name_pi':
            $rows .= '<div class="col-xs-12 col-sm-6 col-md-6"><p><b class="text-primary">Nombre: </b>' . utf8_encode($rows_query['name_pi']) . ' ' . utf8_encode($rows_query['last_name_pi']) . '</p></div>';
            break;

          case 'email_pi':
            $rows .= '<div class="col-xs-12 col-sm-6 col-md-6"><p><b class="text-primary">Correo: </b>' . $value . '</p></div>';
            break;

          case 'phone_pi':
            $rows .= '<div class="col-xs-12 col-sm-6 col-md-6"><p><b class="text-primary">Telefono: </b>' . utf8_encode($value) . '</p></div>';
            break;

          case 'movil_pi':
            $rows .= '<div class="col-xs-12 col-sm-6 col-md-6"><p><b class="text-primary">Movil: </b>' . $value . '</p></div>';
            break;

          case 'profession_pi':
            $rows .= '<div class="col-xs-12 col-sm-6 col-md-6"><p><b class="text-primary">Profesión: </b>' . utf8_encode($value) . '</p></div>';
            break;

          case 'experience_pi':
            $rows .= '<div class="col-xs-12 col-sm-6 col-md-6"><p><b class="text-primary">Experiencia: </b>' . $value . ' años</p></div>';
            break;

          case 'age_pi':
            $rows .= '<div class="col-xs-12 col-sm-6 col-md-6"><p><b class="text-primary">Edad: </b>' . $value . '</p></div>';
          break;

          case 'name_c':
            $rows .= '<div class="col-xs-12 col-sm-6 col-md-6"><p><b class="text-primary">Ciudad: </b>' . utf8_encode($value) . '</p></div>';
          break;

          case 'address_pi':
            $rows .= '<div class="col-xs-12 col-sm-12 col-md-12"><p><b class="text-primary">Dirección: </b>' . utf8_encode($value) . '</p></div>';
          break;

          case 'knowledge_pi':
            $rows .= '<div class="col-xs-12 col-sm-12 col-md-12">
                        <fieldset>
                          <legend class="text-primary">Conocimientos / Habilidades / Destrezas</legend>
                          <p>' . utf8_encode($value) . '</p>
                        </fieldset>
                      </div>';
            break;

          case 'studies_pi':
            $rows .= '<div class="col-xs-12 col-sm-12 col-md-12">
                        <fieldset>
                          <legend class="text-primary">Estudios</legend>
                          <p>' . utf8_encode($value) . '</p>
                        </fieldset>
                      </div>';
            break;
        } 
      }

    $rows .= '</div>';
    $rows .= '<div class="form-group">
                <div class="input-group-addon">
                  <input type="hidden" name="id_tab" value="' . $id_tab . '" class="id-tab">
                  <input type="reset" value="Descartar" class="btn btn-danger pull-left" id="btn-discard-person" data-person-discard=' . $rows_query['id_pi'] . '>
                  <div class="animation_image text-primary" style="display:none;">
                      <img alt="Cargando" src="/src/img/mihv/site/ajax-loader.gif"> Cargando...
                  </div>
                  <input type="submit" name="selected" value="Seleccionar" class="btn btn-success pull-right" id="btn-selected-person" data-person-selected=' . $rows_query['id_pi'] . '>
                </div>
              </div>';

    $rows .= '</div>';

    return $rows;
  }

  function control_profile_company_selected_person_offer($id_ofert, $id_person) {
    // Traer Informacion para envio de correo a persona
    $modelProfileCompany = new modelProfileCompany();
    $rows_info_contact = $modelProfileCompany->model_profile_company_get_info_for_contact_person($id_ofert, $id_person);

    // Envio de email
    $response_email = $this->control_profile_company_template_selected_person_email($rows_info_contact);

    return $response_email;
  }

  function control_profile_company_create_pdf_bill($value_offer, $number_billing, $id_offer) {

    try {     

      $controlUtilities = new controlUtilities();
      
      $city = $controlUtilities->control_utilities_get_city($_SESSION["user"]['city_ci']);

      require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/pdf/bill.inc");

      //Calcular Base & Iva de la Factura
      $varValorUnidadBase = $value_offer * 0.81;
      $varValorBaseIva    = $value_offer - $varValorUnidadBase;
      $varValorTotal      = $varValorUnidadBase + $varValorBaseIva;

      $header_1  = PDF_HEADER_STRING;
      $header_2 = PDF_HEADER_TITLE . $number_billing . "\n";
      $header_3 = PDF_HEADER_STRING2;

      // instance
      $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

      $text_qr = "Este es un código QR, escanéalo y te llevará a la publicación que creaste.\n";//Texto del qr
      $invoice_recipient = "\n\nFactura expedida a nombre de\n";

      $addressee = 'Bogotá D.C. - ' . date("d/m/Y") . "\n\n\n" . $_SESSION["user"]['name_ci'] . "\n" . $_SESSION['user']['nit_ci'] . "\n" . $_SESSION['user']['email_ci'] . "\n" . $_SESSION["user"]['address_ci'] . "\n" . "\n" . $city . ' - Colombia . ' . "\n" .'E.     S.     D. ' . "\n\n" . '                  ref: ' . PDF_HEADER_TITLE . ' ' . $number_billing . "\n";

      $body = "Codection te informa que se ha generado correctamente la Factura de Venta número $number_billing con los datos de tu empresa como referencia junto con los siguientes parámetros:\n";

      $offer_detail = "Publicación de la oferta N° $number_billing que contiene: 1) Título de la oferta. 2) Breve descripción. 3) Conocimientos requeridos para aplicar. 4) Selección de la(s) profesión(es) a aplicar. 5) Género a aplicar. 6) Ciudad. 7) Salario a devengar. 8) Tipo de contrato. 9) Número de vacantes. 10) Tipo de prioridad. Vigencia de tu publicación: 30 días calendario a partir de la fecha de publicación.\n\nLos datos del formulario que fueron llenados por ti o la persona que autorizaste ya se han publicado en la oferta mencionada.\n";

      $boll_billing = "Requisitos para la factura:\n";

      $text_billing = "Esta factura por computador cumple con los requisitos señalados en el artículo 774 del Código de Comercio.";
      
      $bold_retefuente = "Retención en la fuente:\n";
      
      $text_retefuente = "En base al artículo 392 del Estatuto Tributario y el decreto 3110 de 2004 indica que para hacer retención en la fuente el valor debe ser igual o superior a 4 UVT y teniendo en cuenta el decreto 2224 de 2004 es de aclarar que la generación de varias facturas por prestación de servicios con un mismo agente retenedor por el mes de vigencia se tendrán en cuenta como operaciones individuales y no se acumularán en una sola operación.\n";
      
      $bold_bill_sale = "Factura de venta:\n";
      
      $text_bill_sale = "Contemplando el artículo 647 del estatuto tributario se indica que si una empresa informa a sus proveedores una fecha límite para la recepción de facturas y una vez se reanuden labores la empresa está en la obligación de recibir la(s) factura(s) que no se recibieron en el receso laboral (no importa el motivo) sin pedir alguna modificación a la(s) ya existente.\nEsta factura de venta es válida en caso de haber alguna inconsistencia que se presente durante la vigencia de la publicación.\n";

      $collections_line = "_____________________\n";
      $signature_receipts="Autorización: Recaudos\n";
      $conformity_line="_____________________\n";
      $sign_compliance="De Conformidad\n";

      //$varCuerpo2="Queremos darte las gracias por seguir confiando en nosotros. Sabes que si recargas tu cuenta tienes varios beneficios y evitas estar pagando con el botón PSE por cada publicación.\n¡¡¡TU TIEMPO VALE!!!, por ello estamos en la mejora de herramientas que hagan de este sitio web el más rápido, ágil y productivo para ti y tu empresa.\nÉxitos tanto en tu publicación como en las entrevistas y nos vemos en otra ocasión.\n";

      // set document information
      $pdf->SetCreator(PDF_CREATOR);
      $pdf->SetAuthor('Codection SAS');
      $pdf->SetTitle('Factura de Venta');
      $pdf->SetSubject('Factura de Venta Cliente');

      // set default header data
      $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_MARGIN);

      // set header and footer fonts
      $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
      $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

      // set default monospaced font
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

      // set margins
      $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
      $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

      // set auto page breaks
      $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

      // set image scale factor
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

      // set some language-dependent strings (optional)
      if (@file_exists(dirname(__FILE__).'/lang/spa-orden.php')) {
        require_once(dirname(__FILE__).'/lang/spa-orden.php');
        $pdf->setLanguageArray($l);
      }

      // add a page
      $pdf->AddPage();
      $pdf->SetFont('helvetica', '', 10);
      //$pdf->Image($_SERVER['DOCUMENT_ROOT'] . '/src/img/mihv/pdf/codection-logo.png',15,5,63,7,'PNG');
      $pdf->MultiCell(93, 27, $header_1, 0, 'J', false, 1, 15, 5, true, 0, false, true, 0, 'M', false);
      $pdf->SetFont('helvetica', 'B', 10);
      $pdf->MultiCell(93, 5, $header_2, 0, 'J', false, 1, 108, 5, true, 0, false, true, 0, 'M', false);
      $pdf->SetFont('helvetica', '', 10);
      $pdf->MultiCell(93, 22, $header_3, 0, 'J', false, 1, 108, 9.5, true, 0, false, true, 0, 'M', false);
      // print a message
      $pdf->MultiCell(75, 10, $text_qr, 0, 'J', false, 1, 126, 30, true, 0, false, true, 0, 'T', false);
      $pdf->SetFont('helvetica', '', 10);
          
      // set style for barcode
      $style = array(
          'border' => false,
          'padding' => 0,
          'fgcolor' => array(51,122,183),
          'bgcolor' => false
      );

      $protocol = '';
      if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") { 
        $protocol = "https://";
      } else { 
        $protocol = "http://";
      }

      // QRCODE,H : QR-CODE Best error correction
      $pdf->write2DBarcode($protocol . $_SERVER['HTTP_HOST'] . '/views/offer.php?id=318'. mt_rand() . $id_offer, 'QRCODE, H', 138, 40.5, 53, 53, $style, 'N');
      //$pdf->MultiCell(75, 0, 'QRCODE H - NO PADDING', 0, 'C', false, 0, 126, 78, true, 0, false, true, 0, 'M', false);

      //$pdf->write2DBarcode('https://www.mihv.com.co/views/offer.php?id='.$id_offer, 'PDF417', 126, 47, 100, 30, $style, 'N');
      //$pdf->MultiCell(75, 0, 'PDF417 (ISO/IEC 15438:2006)', 0, 'C', false, 0, 126, 78, true, 0, false, true, 0, 'M', false);
      $pdf->SetFont('helvetica', 'B', 12);
      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      $pdf->MultiCell(106, 0, $invoice_recipient, 0, 'J', false, 1, 15, 32, true, 0, false, true, 0, 'M', false);

      $pdf->SetFont('helvetica', '', 12);
      //Texto Destinatario
      $pdf->MultiCell(106, 0, $addressee, 0, 'J', false, 1, 15, 32, true, 0, false, true, 0, 'M', false);

      //Texto Cuerpo 1
      $pdf->MultiCell(186, 10, $body, 0, 'J', false, 1, 15, 101, true, 0, false, true, 0, 'M', false);

      //Tabla de descripción de items
      //Fila 0:
      //Cambiar a Negrita pdf por la tabla
      $pdf->SetFont('helvetica', 'B', 12);
      //Columna 1 Encabezado
      $pdf->MultiCell(21, 5, 'Cantidad', 1, 'C', false, 1, 15, 114, true, 0, false, true, 0, 'M', false);
      //Columna 2 Encabezado
      $pdf->MultiCell(105, 5, 'Detalle del servicio a prestar', 1, 'C', false, 1, 36, 114, true, 0, false, true, 0, 'M', false);
      //Columna 3 Encabezado
      $pdf->MultiCell(30, 5, 'Valor Unitario', 1, 'C', false, 1, 141, 114, true, 0, false, true, 0, 'M', false);
      //Columna 4 Encabezado
      $pdf->MultiCell(30, 5, 'Valor Total', 1, 'C', false, 1, 171, 114, true, 0, false, true, 0, 'M', false);

      //Fila 1:
      //Columna 1
      //Aumentar tamaño letra pdf por la columna 1 fila 1 de la tabla
      $pdf->SetFont('helvetica', 'B', 25);
      $pdf->MultiCell(21, 40, '1', 1, 'C', false, 1, 15, 119.5, true, 0, false, true, 40, 'M');
      //Columna 2
      //Cambiar tamaño pdf por la columna 2
      $pdf->SetFont('helvetica', 'B', 9);
      $pdf->MultiCell(105, 40, $offer_detail, 1, 'J', false, 1, 36, 119.5, true, 0, false, true, 40, 'M');
      //Columna 3
      //Cambiar tamaño letra por la tabla PDF Columna 3 y 4
      $pdf->SetFont('helvetica', 'B', 20);
      $pdf->MultiCell(30, 40, "$".$varValorUnidadBase, 1, 'C', false, 1, 141, 119.5, true, 0, false, true, 40, 'M');
      //Columna 4
      $pdf->MultiCell(30, 40, "$".$varValorUnidadBase, 1, 'C', false, 1, 171, 119.5, true, 0, false, true, 40, 'M');
      //Fila 3
      //Columna 3
      //Cambiar tamaño letra por la tabla PDF Columna 3
      $pdf->SetFont('helvetica', 'B', 15);
      $pdf->MultiCell(45, 10, "Subtotal Base ", 0, 'R', false, 1, 126, 159.5, true, 0, false, true, 10, 'M');
      //Columna 4
      //Cambiar tamaño letra por la tabla PDF Columna 4
      $pdf->SetFont('helvetica', 'B', 20);
      $pdf->MultiCell(30, 10, "$".$varValorUnidadBase, 1, 'R', false, 1, 171, 159.5, true, 0, false, true, 10, 'M');
      //Fila 4
      //Columna 3
      //Cambiar tamaño letra por la tabla PDF Columna 3
      $pdf->SetFont('helvetica', 'B', 15);
      $pdf->MultiCell(25, 10, "Iva 19% ", 0, 'R', false, 1, 146, 169.5, true, 0, false, true, 10, 'M');
      //Columna 4
      //Cambiar tamaño letra por la tabla PDF Columna 4
      $pdf->SetFont('helvetica', 'B', 20);
      $pdf->MultiCell(30, 10, "$".$varValorBaseIva, 1, 'R', false, 1, 171, 169.5, true, 0, false, true, 10, 'M');
      //Fila 5
      //Columna 3
      //Cambiar tamaño letra por la tabla PDF Columna 3
      $pdf->SetFont('helvetica', 'B', 15);
      $pdf->MultiCell(30, 10, "Total COP", 0, 'R', false, 1, 140, 179.5, true, 0, false, true, 10, 'M');
      //Columna 4
      //Cambiar tamaño letra por la tabla PDF Columna 4
      $pdf->SetFont('helvetica', 'B', 20);
      $pdf->MultiCell(30, 10, "$".$varValorTotal, 1, 'R', false, 1, 171, 179.5, true, 0, false, true, 10, 'M');

      //Retornando a letra normal de PDF
      $pdf->SetFont('helvetica', '', 14);
      //Firma de recaudos y deconformidad
      $pdf->MultiCell(60, 5, $collections_line, 0, 'C', false, 1, 18, 198, true, 0, false, true, 0, 'B', false);
      $pdf->MultiCell(60, 5, $signature_receipts, 0, 'C', false, 1, 18, 203.5, true, 0, false, true, 0, 'T', false);
      $pdf->MultiCell(60, 5, $conformity_line, 0, 'C', false, 1, 90, 198, true, 0, false, true, 0, 'B', false);
      $pdf->MultiCell(60, 5, $sign_compliance, 0, 'C', false, 1, 90, 203.5, true, 0, false, true, 0, 'T', false);
      $pdf->Image($_SERVER['DOCUMENT_ROOT'] . '/src/img/mihv/pdf/sello-codection-firma.png',20,160,55,55,'PNG');
      $pdf->SetAlpha(0.17);
      $pdf->StartTransform();

      $pdf->Rotate(40, 35, 222);
      $pdf->Image($_SERVER['DOCUMENT_ROOT'] . '/src/img/mihv/pdf/sello-cancelado.png',10,218,206,48,'PNG');
      $pdf->StopTransform();


      //Retornando a letra normal de PDF color de la letra y margen
      $pdf->SetAlpha(1);

      //Texto Cuerpo 2
      //Fila 1
      //Cambiar tamaño letra por la tabla PDF Columna 4
      $pdf->SetFont('helvetica', 'B', 13);
      $pdf->MultiCell(186, 10, "Observaciones", 0, 'J', false, 1, 15, 207.5, true, 0, false, true, 10, 'M', false);
      //Fila 2
      $pdf->SetFont('helvetica', 'B', 9);
      $pdf->MultiCell(186, 5, $boll_billing, 'TRL', 'J', false, 1, 15, 215.5, true, 0, false, true, 5, 'M', false);
      //Fila 3
      $pdf->SetFont('helvetica', '', 9);
      $pdf->MultiCell(186, 5, $text_billing."\n", 'LR', 'J', false, 1, 15, 220.5, true, 0, false, true, 16, 'T', false);
      //Fila 4
      $pdf->SetFont('helvetica', 'B', 9);
      $pdf->MultiCell(186, 5, $bold_retefuente, 'RL', 'J', false, 1, 15, 225.5, true, 0, false, true, 5, 'M', false);
      //Fila 5
      $pdf->SetFont('helvetica', '', 9);
      $pdf->MultiCell(186, 15, $text_retefuente."\n", 'LR', 'J', false, 1, 15, 230.5, true, 0, false, true, 16, 'T', false);
      //Fila 6
      $pdf->SetFont('helvetica', 'B', 9);
      $pdf->MultiCell(186, 7, $bold_bill_sale, 'LR', 'J', false, 1, 15, 245.7, true, 0, false, true, 7, 'B', false);
      //Fila 7
      $pdf->SetFont('helvetica', '', 9);
      $pdf->MultiCell(186, 17, $text_bill_sale."\n", 'LBR', 'J', false, 1, 15, 252.5, true, 0, false, true, 18, 'T', false);

      
      //Close and output PDF document
      //$pdf->Output('Factura de Venta '.$number_billing.'.pdf', 'I'); // solo muestra
      $pdf->Output($_SERVER['DOCUMENT_ROOT'] . '/src/bill/factura-de-venta_' . $number_billing . '.pdf', 'F'); // guarda

      return 'factura-de-venta_' . $number_billing . '.pdf';
    } catch (Exception $e) {
      return $e->getMessage();
    }  
  }

  /*
   * Enviar email para contactar a la persona
   */
  function control_profile_company_template_selected_person_email($info_email) {

    $protocol = '';
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") { 
      $protocol = "https://";
    } else { 
      $protocol = "http://";
    }
  
    $html = '';
    $html .='
          <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8"> <!-- utf-8 works for most cases -->
        <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldnt be necessary -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
        <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

        <style>

            /* What it does: Remove spaces around the email design added by some email clients. */
            /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
            html,
            body {
                margin: 0 auto !important;
                padding: 0 !important;
                height: 100% !important;
                width: 100% !important;
            }
            
            /* What it does: Stops email clients resizing small text. */
            * {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
            }
            
            /* What is does: Centers email on Android 4.4 */
            div[style*="margin: 16px 0"] {
                margin:0 !important;
            }
            
            /* What it does: Stops Outlook from adding extra spacing to tables. */
            table,
            td {
                mso-table-lspace: 0pt !important;
                mso-table-rspace: 0pt !important;
            }
                    
            /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
            table {
                border-spacing: 0 !important;
                border-collapse: collapse !important;
                table-layout: fixed !important;
                margin: 0 auto !important;
            }
            table table table {
                table-layout: auto; 
            }
            
            /* What it does: Uses a better rendering method when resizing images in IE. */
            img {
                -ms-interpolation-mode:bicubic;
            }
            
            /* What it does: A work-around for iOS meddling in triggered links. */
            .mobile-link--footer a,
            a[x-apple-data-detectors] {
                color:inherit !important;
                text-decoration: none !important;
            }
          
        </style>
        
        <!-- Progressive Enhancements -->
        <style>
            
            /* What it does: Hover styles for buttons */
            .button-td,
            .button-a {
                transition: all 100ms ease-in;
            }
            .button-td:hover,
            .button-a:hover {
                background: #555555 !important;
                border-color: #555555 !important;
            }

        </style>

    </head>
    <body width="100%" bgcolor="#ffffff" style="margin: 0;">
        <center style="width: 100%; background: #ffffff;">

            <!-- Visually Hidden Preheader Text : BEGIN -->
            <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
                Hola ' . $info_email['name_pi'] . ', te damos una muy buena noticia. Abre este mail y entérate de que se trata.
            </div>
            <!-- Visually Hidden Preheader Text : END -->

            <!--    
                Set the email width. Defined in two places:
                1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 600px.
                2. MSO tags for Desktop Windows Outlook enforce a 600px width.
            -->
            <div style="max-width: 600px; margin: auto;">
                <!--[if mso]>
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="600" align="center">
                <tr>
                <td>
                <![endif]-->

                <!-- Email Header : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
                    <tr>
                        <td style="padding: 20px 0; text-align: center;">
                          <img alt="Seleccionado a la oferta" src="https://imagenes.mihv.com.co/email/te-han-elegido.png" alt="alt_text" border="0" align="center" style="width: 100%; max-width: 600px; background: none; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding-bottom:10px;">
                        </td>
                    </tr>
                </table>
                <!-- Email Header : END -->
                
                <!-- Email Body : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
                    
                    

                    <!-- 1 Column Text + Button : BEGIN -->
                    <tr>
                        <td bgcolor="#ffffff">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td style="padding: 40px; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;" align="justify">
                                        Hola ' . $info_email['name_pi'] . ' que tal tu día?:
                                        <br><br>
                                        Te damos una excelente noticia. Fuiste seleccionad@ para la oferta ' . $info_email['name_o'] . '.
                                        <br><br>
                                        Felicidades por tu aplicación y queremos recordarte lo siguiente:                                        
                                        <ol type="1">
                                            <li>Tener tu hoja de vida actualizada con los documentos básicos los cuales son:</li>
                                                <ul>
                                                    <li>Datos personales completos.</li>
                                                    <li>Estudios realizados.</li>
                                                    <li>Experiencia laboral.</li>
                                                    <li>Referencias personales y  LABORALES.</li>
                                                    <li>Tu firma con tus datos de contacto.</li>
                                                </ul>                                                                                                
                                                <p>CUANDO TE LLAMEN, RECUERDA PREGUNTAR QUÉ DOCUMENTOS SE REQUIEREN ANEXAR EN TU HOJA DE VIDA PARA LA ENTREVISTA.</p>
                                                <li>Probablemente te hagan algún test, de ser así, respóndelo bajo tus conocimientos y trata de no extenderte en las respuestas, otro tip, generalmente si es escrito lo primero que miran es tu ortografía.</li>
                                            <br/>
                                            <li>Por tus conocimientos has aplicado a esta oferta, EVITA LOS NERVIOS durante la entrevista y confía en lo que sabes.</li>
                                            <br/>
                                            <li>Durante la entrevista siempre mira al entrevistador cuando hables y jámas lo interrumpas (sólo habla cuando te pregunte).</li>
                                        </ol>
                                        Recuerda que te van a contactar, normalmente se toman 3 días en llamar, ten tu móvil a la mano y con suficiente batería y si tienes un fijo estar pendiente del teléfono (normalmente llaman primero al fijo).
                                        <br><br>
                                        Si pasados 3 días hábiles no te han llamado sería bueno que te pongas en contacto con la empresa, antes no porque quedas muy mal con la misma - sé paciente. Te dejamos los datos:
                                        <ul>
                                        <li>Nombre: ' . $info_email['name_ci'] . '</li>
                                        <li>Email: ' . $info_email['email_ci'] . '</li>
                                        </ul>
                                        Sería excelente que nos recomendaras con tu familia y amigos para que se enteren de este innovador portal y consigan una entrevista laboral como ya lo has logrado, recuerda, díles que ingresen a <a href="https://www.mihv.com.co">www.mihv.com.co</a> y realicen su búsqueda.
                                        <br><br>
                                        Esperamos que te vaya muy bien en tu entrevista y consígas el empleo.
                                        <br><br><br>
                                        El equipo de Mi HV.
                                        <br><br><br>
                                        PD: Quizás te interese...<br>
                                    </td>
                                    </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- Email Body : END -->

                <!-- 2 Even Columns : BEGIN -->
                    <tr>                    
                        <td bgcolor="#ffffff" align="center" height="100%" valign="top" width="100%" style="padding-bottom: 40px">
                            <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:560px;">
                                <tr>
                                    <td align="center" valign="top" width="50%">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                            <tr>
                                                <td style="text-align: center; padding: 0 10px;">
                                                    <a href="https:/www.google.com.co" target="_blank">
                                                    <img alt="Pauta email" src="https://imagenes.mihv.com.co/email/200-200.png" width="200" height="200" alt="alt_text" class="center-on-narrow" style="border: 0;width: 100%;max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                                    </a>
                                                </td>
                                            </tr>                                        
                                        </table>
                                    </td>
                                    <td align="center" valign="top" width="50%">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                            <tr>
                                                <td style="text-align: center; padding: 0 10px;">
                                                    <a href="https:/www.google.com.co" target="_blank">
                                                    <img alt="Pauta email" src="https://imagenes.mihv.com.co/email/200-200.png" width="200" height="200" alt="alt_text" class="center-on-narrow" style="border: 0;width: 100%;max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                                    </a>
                                                </td>
                                            </tr>
                                            
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Two Even Columns : END -->
              
                <!-- Email Footer : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;">
                    <tr>
                        <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;">                        
                            <span class="mobile-link--footer">
                              <a href="https://www.codection.pro" target="_blank">Mi HV (CODECTION SAS)</a>
                            </span>
                            <br>
                            <span class="mobile-link--footer">
                              Carrera 13 # 13 - 24 Oficina 725<br>Bogotá D.C., C/marca, 110311 CO
                            </span><br>
                            <span class="mobile-link--footer">
                              <a href="tel:+573165269362">
                                <img alt="whatsapp" src="https://imagenes.mihv.com.co/email/whatsapp.jpg" width="100%" style="max-width:9px;"> (+57) 316 526 9362
                              </a>
                            </span>
                            <!--<br><br>
                            <unsubscribe style="color:#888888; text-decoration:underline;">unsubscribe</unsubscribe>-->
                        </td>
                    </tr>
                </table>
                <!-- Email Footer : END -->

                <!--[if mso]>
                </td>
                </tr>
                </table>
                <![endif]-->
            </div>
        </center>
    </body>
    </html>
    ';

    $subject = 'Te han elegido en la oferta ' . $info_email['name_o'] . '';

    //incluir libreria para correo
    require_once($_SERVER['DOCUMENT_ROOT'] . "/libraries/PHPMailer-master/class.phpmailer.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/libraries/PHPMailer-master/class.smtp.php");
    
    $mail = new PHPMailer;
    //indico a la clase que use SMTP
    $mail->IsSMTP();
    //permite modo debug para ver mensajes de las cosas que van ocurriendo
    //$mail->SMTPDebug = 2;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = "starttls";
    $mail->Host = "mail.mihv.co";
    $mail->Port = 587;
    $mail->Username = "informacion";
    $mail->Password = "Info2016*..*";
    $mail->From = "informacion@envios.mihv.co";  
    $mail->FromName = "MI HV - Felicidades";
    $mail->Subject = utf8_decode($subject);
    $mail->AddAddress($info_email['email_pi']);
    $mail->MsgHTML(utf8_decode($html));
    $mail->IsHTML(true);
    //$mail->AddAttachment('../uploadAlDia/mi_archivo.jpg');
    
    if ($mail->Send()) {
      return true;
    }else {
      return false;
    }
  }


  function control_profile_company_discard_person_for_ofert($id_ofert, $id_person) {
    // Traer Informacion para envio de correo a persona
    $modelProfileCompany = new modelProfileCompany();
    $rows_info_discard = $modelProfileCompany->model_profile_company_get_info_for_contact_person($id_ofert, $id_person);

    // Envio de email
    $response_email = $this->control_profile_company_template_discard_person_email($rows_info_discard);

    return $response_email;
  }

  /*
   * Enviar email para descartar a la persona
   */
  function control_profile_company_template_discard_person_email($info_email) {
  
    $protocol = '';
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") { 
      $protocol = "https://";
    } else { 
      $protocol = "http://";
    }
    
    $html = '';
    $html .='
          <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8"> <!-- utf-8 works for most cases -->
        <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldnt be necessary -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
        <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

        <style>

            /* What it does: Remove spaces around the email design added by some email clients. */
            /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
            html,
            body {
                margin: 0 auto !important;
                padding: 0 !important;
                height: 100% !important;
                width: 100% !important;
            }
            
            /* What it does: Stops email clients resizing small text. */
            * {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
            }
            
            /* What is does: Centers email on Android 4.4 */
            div[style*="margin: 16px 0"] {
                margin:0 !important;
            }
            
            /* What it does: Stops Outlook from adding extra spacing to tables. */
            table,
            td {
                mso-table-lspace: 0pt !important;
                mso-table-rspace: 0pt !important;
            }
                    
            /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
            table {
                border-spacing: 0 !important;
                border-collapse: collapse !important;
                table-layout: fixed !important;
                margin: 0 auto !important;
            }
            table table table {
                table-layout: auto; 
            }
            
            /* What it does: Uses a better rendering method when resizing images in IE. */
            img {
                -ms-interpolation-mode:bicubic;
            }
            
            /* What it does: A work-around for iOS meddling in triggered links. */
            .mobile-link--footer a,
            a[x-apple-data-detectors] {
                color:inherit !important;
                text-decoration: none !important;
            }
          
        </style>
        
        <!-- Progressive Enhancements -->
        <style>
            
            /* What it does: Hover styles for buttons */
            .button-td,
            .button-a {
                transition: all 100ms ease-in;
            }
            .button-td:hover,
            .button-a:hover {
                background: #555555 !important;
                border-color: #555555 !important;
            }

        </style>

    </head>
    <body width="100%" bgcolor="#ffffff" style="margin: 0;">
        <center style="width: 100%; background: #ffffff;">

            <!-- Visually Hidden Preheader Text : BEGIN -->
            <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
                Hola ' . $info_email['name_pi'] . ', Te tenemos una notícia que debes revisar respecto a la oferta que te postulaste.
            </div>
            <!-- Visually Hidden Preheader Text : END -->

            <!--    
                Set the email width. Defined in two places:
                1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 600px.
                2. MSO tags for Desktop Windows Outlook enforce a 600px width.
            -->
            <div style="max-width: 600px; margin: auto;">
                <!--[if mso]>
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="600" align="center">
                <tr>
                <td>
                <![endif]-->

                <!-- Clear Spacer : BEGIN -->
                    <tr>
                        <td height="20" style="font-size: 0; line-height: 0;">
                            &nbsp;
                        </td>
                    </tr>
                    <!-- Clear Spacer : END -->                

                <!-- Email Header : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
                    <tr>
                        <td style="padding: 20px 0; text-align: center">
                            <img alt="header mi hv" src="https://imagenes.mihv.com.co/email/header-logo.jpg" width="90%" height="auto" alt="alt_text" border="0" style="background: none; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                        </td>
                    </tr>
                </table>
                <!-- Email Header : END -->
                
                <!-- Email Body : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
                    
                    

                    <!-- 1 Column Text + Button : BEGIN -->
                    <tr>
                        <td bgcolor="#ffffff">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td style="padding: 40px; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;" align="justify">
                                        Como vas ' . $info_email['name_pi'] . ':
                                        <br><br>
                                        Queremos decirte que fuiste descartad@ de la oferta ' . $info_email['name_o'] . '.
                                        <br><br>
                                        Como sugerencia debes tener en cuenta leer detenidamente los requisitos que pide la empresa solicitante (incluido los documentos) y si cumples con estos, postúlate.
                                        <br><br>
                                        Entre más conocimiento tengas, mejores son tus oportunidades laborales. Núnca dejes de estudiar.
                                        <br><br>
                                        Busca más ofertas de empleo ingresando en <a href="https://www.mihv.com.co">www.mihv.com.co</a>.
                                        <br><br><br>
                                        El equipo de Mi HV.
                                        <br><br><br>
                                        PD: Probablemente te interese...<br>
                                    </td>
                                    </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- Email Body : END -->

                <!-- 2 Even Columns : BEGIN -->
                    <tr>                    
                        <td bgcolor="#ffffff" align="center" height="100%" valign="top" width="100%" style="padding-bottom: 40px">
                            <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:560px;">
                                <tr>
                                    <td align="center" valign="top" width="50%">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                            <tr>
                                                <td style="text-align: center; padding: 0 10px;">
                                                    <a href="https:/www.google.com.co" target="_blank">
                                                    <img alt="pauta email" src="https://imagenes.mihv.com.co/email/200-200.png" width="200" height="200" alt="alt_text" class="center-on-narrow" style="border: 0;width: 100%;max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                                    </a>
                                                </td>
                                            </tr>                                        
                                        </table>
                                    </td>
                                    <td align="center" valign="top" width="50%">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                            <tr>
                                                <td style="text-align: center; padding: 0 10px;">
                                                    <a href="https:/www.google.com.co" target="_blank">
                                                    <img alt="pauta email" src="https://imagenes.mihv.com.co/email/200-200.png" width="200" height="200" alt="alt_text" class="center-on-narrow" style="border: 0;width: 100%;max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                                    </a>
                                                </td>
                                            </tr>
                                            
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Two Even Columns : END -->
              
                <!-- Email Footer : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;">
                    <tr>
                        <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;">                        
                            <span class="mobile-link--footer"><a href="https://www.codection.pro" target="_blank">Mi HV (CODECTION SAS)</a></span><br><span class="mobile-link--footer">Carrera 13 # 13 - 24 Oficina 725<br>Bogotá D.C., C/marca, 110311 CO</span><br><span class="mobile-link--footer"><a href="tel:+573165269362"><img src="https://imagenes.mihv.com.co/email/whatsapp.jpg" width="100%" style="max-width:9px;"> (+57) 316 526 9362</a></span>
                            <!--<br><br>
                            <unsubscribe style="color:#888888; text-decoration:underline;">unsubscribe</unsubscribe>-->
                        </td>
                    </tr>
                </table>
                <!-- Email Footer : END -->

                <!--[if mso]>
                </td>
                </tr>
                </table>
                <![endif]-->
            </div>
        </center>
    </body>
    </html>
    ';

    $subject = 'Resultado de la oferta ' . $info_email['name_o'] . '';

    //incluir libreria para correo
    require_once($_SERVER['DOCUMENT_ROOT'] . "/libraries/PHPMailer-master/class.phpmailer.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/libraries/PHPMailer-master/class.smtp.php");
    
    $mail = new PHPMailer;
    $mail->IsSMTP();
    //permite modo debug para ver mensajes de las cosas que van ocurriendo
    //$mail->SMTPDebug = 2;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = "starttls";
    $mail->Host = "mail.mihv.co";
    $mail->Port = 587;
    $mail->Username = "informacion";
    $mail->Password = "Info2016*..*";
    $mail->From = "informacion@envios.mihv.co";
    $mail->FromName = "MI HV - Resultado oferta aplicada";
    $mail->Subject = utf8_decode($subject);
    $mail->AddAddress($info_email['email_pi']);
    $mail->MsgHTML(utf8_decode($html));
    $mail->IsHTML(true);
    //$mail->AddAttachment('../uploadAlDia/mi_archivo.jpg');
    
    if ($mail->Send()) {
      return true;
    }else {
      return false;
    }
  }

  function control_profile_company_get_full_data_company() {
    $modelProfileCompany = new modelProfileCompany();
    $data_comapny = $modelProfileCompany->model_profile_company_get_full_data_company();
    return $data_comapny;
  }


  function control_profile_company_create_pdf_recharge_balance($id_recharge, $id_ci, $nit_ci, $name_ci, $email_ci, $value_recharge) {

    try {      

      require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/pdf/balance_recharge.inc");

      // create new PDF document
      $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(78,174), true, 'UTF-8', false);

      // set document information
      $pdf->SetCreator(PDF_CREATOR);
      $pdf->SetAuthor(PDF_AUTHOR);
      $pdf->SetTitle(PDF_HEADER_TITLE);
      $pdf->SetSubject('Recarga Tu Cuenta');
      $pdf->SetKeywords('PDF, recarga, codection');

      // set default header data
      $pdf->SetHeaderData(PDF_HEADER_LOGO);

      // set header and footer fonts
      $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
      $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

      // set default monospaced font
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

      // set margins
      $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
      $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

      // set auto page breaks
      $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

      // set image scale factor
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

      // set some language-dependent strings (optional)
      if (@file_exists(dirname(__FILE__).'/lang/spa-transaccion.php')) {
        require_once(dirname(__FILE__).'/lang/spa-transaccion.php');
        $pdf->setLanguageArray($l);
      }

      // Agregar pagina en blanco
      $pdf->AddPage();

      //Encabezado
      $varComprobante="Comprobante de transacción N°:\n";

      $varNroComprobante="$id_recharge\n";

      $varInicio="Recibimos una solicitud de recarga a tu cuenta la cuál fue procesada exitosamente.\n\nAñadiremos el valor que recargaste y así podrás aprovechar los beneficios y descuentos que recibes al ser cliente VIP.\n\nMira la info detallada de tu transacción:\n";

      $varDetalle1="Beneficiario: $name_ci\n";

      $varDetalle2="Cuenta: $nit_ci\n";

      $varDetalle3="Valor a recargar: $value_recharge COP.\n";

      $varObservaciones="Observaciones:\nEsta es una recarga para ser usada en la creación de tus ofertas de empleo mediante el sitio web mihv.com.co en el territorio colombiano.\nTe recordamos que no hacemos devolución de dinero y tu saldo recargado no tiene fecha de vencimiento.\nPara más información sobre el servicio de recarga de cuenta por internet favor tener en cuenta el concepto 074577 del 12 de octubre de 2005 y para tu contabilidad el valor lo justificas como 'gastos pagados por anticipado' PUC 1705.\n";

      $varLineaRecaudos="____________________\n";

      $varFirmaRecaudos="Autorización: Recaudos\n";
      //$varLineaConformidad="____________________\n";
      //$varFirmaConformidad="De Conformidad\n";
      // $pdf->Image($_SERVER['DOCUMENT_ROOT'] . '/src/img/mihv/pdf/sello-cancelado.png',10,43,62,48,'PNG');

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

      $pdf->SetFont('helvetica', '', 9);
      //Texto Título
      $pdf->MultiCell(62, 10, PDF_HEADER_STRING, 0, 'C', false, 1, 8, 22.1, true, 0, false, true, 0, 'M', false);
      //Texto Encabezado
      $pdf->MultiCell(62, 5, "Fecha: $varFecha", 0, 'C', false, 1, 8, 34.6, true, 0, false, true, 0, 'M', false);
      $pdf->MultiCell(62, 5, $varComprobante, 0, 'C', false, 1, 8, 39.3, true, 0, false, true, 0, 'M', false);
      $pdf->SetFont('helvetica', 'B', 9);
      $pdf->MultiCell(62, 5, $varNroComprobante, 0, 'C', false, 1, 8, 44.3, true, 0, false, true, 0, 'M', false);
      $pdf->SetFont('helvetica', '', 9);
      $pdf->MultiCell(62, 35, $varInicio, 0, 'J', false, 1, 8, 52, true, 0, false, true, 0, 'M', false);
      $pdf->SetFont('helvetica', 'N', 9);
      $pdf->MultiCell(62, 5, $varDetalle1, 0, 'J', false, 1, 8, 88.5, true, 0, false, true, 0, 'M', false);
      $pdf->MultiCell(62, 5, $varDetalle2, 0, 'J', false, 1, 8, 93.5, true, 0, false, true, 0, 'M', false);
      $pdf->MultiCell(62, 5, $varDetalle3, 0, 'J', false, 1, 8, 98.5, true, 0, false, true, 0, 'M', false);
      $pdf->SetFont('helvetica', 'N', 9);
      $pdf->MultiCell(62, 2, $varLineaRecaudos, 0, 'C', false, 1, 8, 129, true, 0, false, true, 0, 'B', false);
      $pdf->MultiCell(62, 2, $varFirmaRecaudos, 0, 'C', false, 1, 8, 132, true, 0, false, true, 0, 'T', false);
      //$pdf->MultiCell(31, 3, $varLineaConformidad, 0, 'C', false, 1, 39, 129, true, 0, false, true, 0, 'B', false);
      //$pdf->MultiCell(31, 3, $varFirmaConformidad, 0, 'C', false, 1, 39, 132, true, 0, false, true, 0, 'T', false);

      $pdf->SetFont('helvetica', '', 6.5);
      $pdf->MultiCell(62, 20, $varObservaciones, 1, 'J', false, 1, 8, 137.3, true, 0, false, true, 0, 'M', false);

      //$pdf->Output('Factura de Venta '.$number_billing.'.pdf', 'I'); // solo muestra
      $pdf->Output($_SERVER['DOCUMENT_ROOT'] . '/src/recharge_balance/comprobante-recarga-saldo_' . $id_recharge . '.pdf', 'F'); // F guarda
            
      return '/src/recharge_balance/comprobante-recarga-saldo_' . $id_recharge . '.pdf';
    } catch (Exception $e) {
      return $e->getMessage();
    }  
  }

  /*
   * Enviar email comprobante de recarga de saldo
   */
  function control_profile_company_send_email_recharge_balance($id_recharge, $name_ci, $email_ci, $value_recharge, $response_pdf) {
  
    $protocol = '';
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") { 
      $protocol = "https://";
    } else { 
      $protocol = "http://";
    }
    
    $html = '';
    $html .='
          <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8"> <!-- utf-8 works for most cases -->
        <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldnt be necessary -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
        <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

        <style>

            /* What it does: Remove spaces around the email design added by some email clients. */
            /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
            html,
            body {
                margin: 0 auto !important;
                padding: 0 !important;
                height: 100% !important;
                width: 100% !important;
            }
            
            /* What it does: Stops email clients resizing small text. */
            * {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
            }
            
            /* What is does: Centers email on Android 4.4 */
            div[style*="margin: 16px 0"] {
                margin:0 !important;
            }
            
            /* What it does: Stops Outlook from adding extra spacing to tables. */
            table,
            td {
                mso-table-lspace: 0pt !important;
                mso-table-rspace: 0pt !important;
            }
                    
            /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
            table {
                border-spacing: 0 !important;
                border-collapse: collapse !important;
                table-layout: fixed !important;
                margin: 0 auto !important;
            }
            table table table {
                table-layout: auto; 
            }
            
            /* What it does: Uses a better rendering method when resizing images in IE. */
            img {
                -ms-interpolation-mode:bicubic;
            }
            
            /* What it does: A work-around for iOS meddling in triggered links. */
            .mobile-link--footer a,
            a[x-apple-data-detectors] {
                color:inherit !important;
                text-decoration: none !important;
            }
          
        </style>
        
        <!-- Progressive Enhancements -->
        <style>
            
            /* What it does: Hover styles for buttons */
            .button-td,
            .button-a {
                transition: all 100ms ease-in;
            }
            .button-td:hover,
            .button-a:hover {
                background: #555555 !important;
                border-color: #555555 !important;
            }

        </style>

    </head>
    <body width="100%" bgcolor="#ffffff" style="margin: 0;">
        <center style="width: 100%; background: #ffffff;">

            <!-- Visually Hidden Preheader Text : BEGIN -->
            <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
                Hola ' . $name_ci . ', hemos generado el COMPROBANTE DE RECARGA DEL SALDO
            </div>
            <!-- Visually Hidden Preheader Text : END -->

            <!--    
                Set the email width. Defined in two places:
                1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 600px.
                2. MSO tags for Desktop Windows Outlook enforce a 600px width.
            -->
            <div style="max-width: 600px; margin: auto;">
                <!--[if mso]>
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="600" align="center">
                <tr>
                <td>
                <![endif]-->

                <!-- Clear Spacer : BEGIN -->
                    <tr>
                        <td height="20" style="font-size: 0; line-height: 0;">
                            &nbsp;
                        </td>
                    </tr>
                    <!-- Clear Spacer : END -->                

                <!-- Email Header : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
                    <tr>
                        <td style="padding: 20px 0; text-align: center">
                            <img alt="header mi hv" src="https://imagenes.mihv.com.co/email/header-logo.jpg" width="90%" height="auto" alt="alt_text" border="0" style="background: none; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                        </td>
                    </tr>
                </table>
                <!-- Email Header : END -->
                
                <!-- Email Body : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
                    
                    

                    <!-- 1 Column Text + Button : BEGIN -->
                    <tr>
                        <td bgcolor="#ffffff">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td style="padding: 40px; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;" align="justify">
                                        Hola ' . $name_ci . '!
                                        <br><br>
                                        Hemos generado tu <b>Comprobante de Recarga del Saldo</b>
                                        <br><br>
                                        Numero de Comprobante: ' . $id_recharge . ' <br>
                                        Valor de la Recarga: ' . $value_recharge . '
                                        <br><br>
                                        Adjunto podras encontrar el detalle del comprobante en un archivo .pdf.
                                        <br><br>
                                        Ahora puedes crear las ofertas ingresando a <a href="https://www.mihv.com.co">www.mihv.com.co</a>.
                                        <br><br><br>
                                        El equipo de Mi HV.
                                        <br><br><br>
                                        PD: Probablemente te interese...<br>
                                    </td>
                                    </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- Email Body : END -->

                <!-- 2 Even Columns : BEGIN -->
                    <tr>                    
                        <td bgcolor="#ffffff" align="center" height="100%" valign="top" width="100%" style="padding-bottom: 40px">
                            <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:560px;">
                                <tr>
                                    <td align="center" valign="top" width="50%">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                            <tr>
                                                <td style="text-align: center; padding: 0 10px;">
                                                    <a href="https:/www.google.com.co" target="_blank">
                                                    <img alt="pauta email" src="https://imagenes.mihv.com.co/email/200-200.png" width="200" height="200" alt="alt_text" class="center-on-narrow" style="border: 0;width: 100%;max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                                    </a>
                                                </td>
                                            </tr>                                        
                                        </table>
                                    </td>
                                    <td align="center" valign="top" width="50%">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                            <tr>
                                                <td style="text-align: center; padding: 0 10px;">
                                                    <a href="https:/www.google.com.co" target="_blank">
                                                    <img alt="pauta email" src="https://imagenes.mihv.com.co/email/200-200.png" width="200" height="200" alt="alt_text" class="center-on-narrow" style="border: 0;width: 100%;max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                                    </a>
                                                </td>
                                            </tr>
                                            
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Two Even Columns : END -->
              
                <!-- Email Footer : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;">
                    <tr>
                        <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;">                        
                            <span class="mobile-link--footer"><a href="https://www.codection.pro" target="_blank">Mi HV (CODECTION SAS)</a></span><br><span class="mobile-link--footer">Carrera 13 # 13 - 24 Oficina 725<br>Bogotá D.C., C/marca, 110311 CO</span><br><span class="mobile-link--footer"><a href="tel:+573165269362"><img src="https://imagenes.mihv.com.co/email/whatsapp.jpg" width="100%" style="max-width:9px;"> (+57) 316 526 9362</a></span>
                            <!--<br><br>
                            <unsubscribe style="color:#888888; text-decoration:underline;">unsubscribe</unsubscribe>-->
                        </td>
                    </tr>
                </table>
                <!-- Email Footer : END -->

                <!--[if mso]>
                </td>
                </tr>
                </table>
                <![endif]-->
            </div>
        </center>
    </body>
    </html>
    ';

    $subject = 'Comprobante de recarga del saldo #' . $id_recharge . '';

    //incluir libreria para correo
    require_once($_SERVER['DOCUMENT_ROOT'] . "/libraries/PHPMailer-master/class.phpmailer.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/libraries/PHPMailer-master/class.smtp.php");
    
    $mail = new PHPMailer;
    $mail->IsSMTP();
    //permite modo debug para ver mensajes de las cosas que van ocurriendo
    //$mail->SMTPDebug = 2;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = "starttls";
    $mail->Host = "mail.mihv.co";
    $mail->Port = 587;
    $mail->Username = "informacion";
    $mail->Password = "Info2016*..*";
    $mail->From = "informacion@envios.mihv.co";
    $mail->FromName = "MI HV";
    $mail->Subject = utf8_decode($subject);
    $mail->AddAddress($email_ci);
    $mail->MsgHTML(utf8_decode($html));
    $mail->IsHTML(true);
    $mail->AddAttachment($_SERVER['DOCUMENT_ROOT'] . $response_pdf);
    
    $mail->Send();

    if ($mail->Send()) {
      return true;
    }else {
      return false;
    }
  }
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
} // End Class

