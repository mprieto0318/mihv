<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();

if (!isset($_POST['tokenPage']) || !in_array($_POST['tokenPage'], $_SESSION['token']['pages'])) {
  header('Location: ../index.php');
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/model_user.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");

$modelUser = new modelUser();
$functions_sql = new functions_sql();
$messages = new messages_system();
$controlUtilities = new controlUtilities();

switch ($_POST["logRegToken"]) {
  case 'login':
    if ($_POST["loginToken"] === 'loginPerson') {
      $values = $functions_sql->functions_sql_clear_dates($_POST);
      $pass = md5($values["itPassword"]);
      $status_user = $modelUser->model_user_validate_status_user('tbl_person_info' , 'pi', $values["itEmail"], $pass);
      
      switch ($status_user['status']) {
        // Usuario registro y activado
        case 'REGISTERED_USER':
          $reponse_user = $modelUser->model_user_login('tbl_person_info' , 'pi', $status_user['user_data']['id_pi']);
      
          if (!empty($reponse_user)) {
            $_SESSION["user"] = array(
              'id_pi' => $reponse_user['id_pi'],
              'name_pi' => $reponse_user['name_pi'],
              'last_name_pi' => $reponse_user['last_name_pi'],
              'email_pi' => $reponse_user['email_pi'],
              'profession_pi' => $reponse_user['profession_pi'],
              'rol_pi' => $reponse_user['rol_pi'],
            );

            $messege = $messages->messages_success('login success', null, $reponse_user['name_pi']);

            $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Bienvenido!', $messege); 
            //$_SESSION["message_system"][] = $messages->messages_build('INFO', 'Recuerda!', 'Recuerda tener tu perfil actualizado'); 

            $redirect = (isset($values['redirect'])) ? $values['redirect'] : '/views/search.php';
            header('Location: ' . $redirect);
          }else {
            $redirect = (isset($values['redirect'])) ? $values['redirect'] : '/views/login.php';
            header('Location: ' . $redirect);
          }
          
          break;

        // Si el usuario no se ha activado
        case 'UNREGISTERED_USER':
          $token_active_user = md5($status_user['user_data']['name_pi'] . '_' . mt_rand());

          $update_token = $modelUser->model_user_update_token_active_user('person' , 'pi', $status_user['user_data']['id_pi'], $token_active_user);

          if ($update_token == 1) {
            $response_mail_register = $controlUtilities->control_utilities_send_mail_user_register('PERSON', $token_active_user, $status_user['user_data']['name_pi'], $values['itEmail'], $values['itPassword']);

            if ($response_mail_register == true) {
              $messege = $messages->messages_success('send token active user success');
              $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Activa tu cuenta!', $messege);
            }else {
              $messege = $messages->messages_failed('send token active user failed');
              $_SESSION["message_system"][] = $messages->messages_build('WARNING', 'Ups!,', $messege);  
            }
          }

          $redirect = (isset($values['redirect'])) ? $values['redirect'] : '/views/login.php';
          header('Location: ' . $redirect);
        break;

        //usuario no encontrado
        case 'BAD_CREDENTIALS':
          $messege = $messages->messages_failed('login failed');
          $_SESSION["message_system"][] = $messages->messages_build('WARNING', 'Datos Incorrectos,', $messege);
          $redirect = (isset($values['redirect'])) ? $values['redirect'] : '/views/login.php';
          header('Location: ' . $redirect);
          break;
      }
      
    }else if ($_POST["loginToken"] === "loginCompany") { // - - - - - - - LOGIN COMPANY - - - - - -
      $values = $functions_sql->functions_sql_clear_dates($_POST);
      $pass = md5($values["itPassword"]);

      $status_user = $modelUser->model_user_validate_status_user('tbl_company_info' , 'ci', $values["itEmail"], $pass);
      
      switch ($status_user['status']) {
        // Usuario registrado y activado
        case 'REGISTERED_USER':
          $info_user = $modelUser->model_user_login('tbl_company_info' , 'ci', $status_user['user_data']['id_ci']);

          if (!empty($info_user)) {
            $_SESSION["user"] = array(
              'id_ci' => $info_user['id_ci'],
              'name_ci' => $info_user['name_ci'],
              'nit_ci' => $info_user['nit_ci'],
              'phone_ci' => $info_user['phone_ci'],
              'email_ci' => $info_user['email_ci'],
              'rol_ci' => $info_user['rol_ci'],
              'city_ci' => $info_user['city_ci'],
              'address_ci' => $info_user['address_ci'],
              'image_ci' => $info_user['image_ci'],
            );

            $messege = $messages->messages_success('login success', null, $info_user['name_ci']);
            $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Bienvenido!', $messege); 
            //$_SESSION["message_system"][] = $messages->messages_build('INFO', 'Recuerda!', 'Recuerda tener tu perfil actualizado'); 
            header('Location: /views/profile_company.php');
          }else {
            header('Location: /views/login.php');
          }

        break;

        // Si el usuario esta registrado pero no se ha activado la cuenta
        case 'UNREGISTERED_USER':

          $token_active_user = md5($status_user['user_data']['name_ci'] . '_' . mt_rand());

          $update_token = $modelUser->model_user_update_token_active_user('company' , 'ci', $status_user['user_data']['id_ci'], $token_active_user);

          if ($update_token == 1) {
            $response_mail_register = $controlUtilities->control_utilities_send_mail_user_register('COMPANY', $token_active_user, $status_user['user_data']['name_ci'], $values['itEmail'], $values['itPassword']);

            if ($response_mail_register == true) {
              $messege = $messages->messages_success('send token active user success');
              $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Activa tu cuenta!', $messege);
            }else {
              $messege = $messages->messages_failed('send token active user failed');
              $_SESSION["message_system"][] = $messages->messages_build('WARNING', 'Ups!,', $messege);  
            }
          }

          header('Location: /views/login.php');
        break;

        case 'BAD_CREDENTIALS':
          $messege = $messages->messages_failed('login failed');
          $_SESSION["message_system"][] = $messages->messages_build('WARNING', 'Datos Incorrectos,', $messege);
          header('Location: /views/login.php');
          break;
      }
    
    }else if ($_POST["loginToken"] === "loginAdmin") { // - - - - - - - LOGIN ADMIN - - - - - -
      $values = $functions_sql->functions_sql_clear_dates($_POST);
      $pass = md5($values["pass"]);

      $info_user = $modelUser->model_user_validate_status_user_admin($values["email"], $pass);
      
      if ($info_user != NULL) {
        $_SESSION["user"] = array(
          'id_su' => $info_user['id_su'],
          'name_su' => $info_user['name_su'],
          'email_su' => $info_user['email_su'],
          'movil_su' => $info_user['movil_su'],
          'document_su' => $info_user['document_su'],
          'rol_su' => $info_user['rol_su'],        
        );

        $messege = $messages->messages_success('login success', null, $info_user['name_su']);
        $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Bienvenido!', $messege); 

        header('Location: /views/system/dashboard.php');
      }else {
        $messege = $messages->messages_failed('login failed');
        $_SESSION["message_system"][] = $messages->messages_build('WARNING', 'Datos Incorrectos,', $messege);
        header('Location: /views/system/login_admin.php');
      }
    }else {
      header('Location: /index.php');
    }
  break;

  /*
   * - - - - - - - - - REGISTRO - - - - - - - - - -
   */
  case 'registerPerson':

    $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', '<span class="glyphicon glyphicon-alert"></span> Función Inabilitada', 'El registro de personas estará habilitado desde el día <b>Jueves 13 de Abril del 2017</b>');

    header('Location: /index.php');
    exit;

    $values = $functions_sql->functions_sql_clear_dates($_POST);
    $user_exits = $modelUser->validate_exits_user('tbl_person_info' , 'pi', $values['itEmail']);

    if ($user_exits === FALSE) {

      $token_active_user = md5($values['itName'] . '_' . mt_rand());
      $pass = md5($values["itPassword"]);

      $values['itProfession'] = (!empty($values['itProfession'])) ? $values['itProfession'] : 'otro';

      $model_user_insert_person = $modelUser->model_user_insert_person($values['itName'], $values['itLastName'], $values['itEmail'], $pass, $values['itPhone'], $values['itMovil'], $values['itProfession'], $values['selExperiencie'], $values['itaKnowledge'], $values['itaStudies'], $values['age'], $values['selCity'], $values['itAddress'], $token_active_user);

      if(!empty($model_user_insert_person) && $model_user_insert_person == 1) {

        $response_mail_register = $controlUtilities->control_utilities_send_mail_user_register('PERSON', $token_active_user, $values['itName'], $values['itEmail'], $values['itPassword']);

        if ($response_mail_register == true) {
          $messege = $messages->messages_success('insert user success');
          $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Estas Registrado!', $messege);

          $redirect = (isset($values['redirect'])) ? $values['redirect'] : '/index.php';
          header('Location: ' . $redirect);
        }else {

          $provisional_access_status = 1;
          $result_provisional_access = $modelUser->model_user_provisional_access('person', 'pi', $token_active_user, $provisional_access_status);

          $email_support = 'soporte@mihv.com.co';
          $from_name = 'MI HV - Fallo envio email activacion';

          if ($result_provisional_access == 1) {
            $subject = 'Fallo envio email de activacion, acceso ok';
            $content ='
              <div>
                <p>No se puedo enviar el correo de activacion al siguiente usuario y se le consedio acceso provisional por 24 horas:</p>
                <br>
                <p>Email: ' . $values['itEmail'] . '</p>
                <p>Nombre: ' . $values['itName'] . '</p>
                <p>Apellidos: ' . $values['itLastName'] . '</p>
                <p>Token Activar: ' . $token_active_user . '</p>
              </div>
            ';

            $messege = $messages->messages_failed('insert user and email failed');
            $_SESSION["message_system"][] = $messages->messages_build('WARNING', 'Ups!,', $messege);
          }else {
            $subject = 'URGENTE! Fallo envio email de activacion, ACCESO PROVISIONAL FALLIDO';
            $content ='
              <div>
                <p>No se puedo enviar el correo de activacion al siguiente usuario y tampoco se pudo conceder el acceso provisional por 24 horas:</p>
                <br>                
                <p>Email: ' . $values['itEmail'] . '</p>
                <p>Nombre: ' . $values['itName'] . '</p>
                <p>Apellidos: ' . $values['itLastName'] . '</p>
                <p>Token Activar: ' . $token_active_user . '</p>
              </div>
            ';

            $messege = $messages->messages_failed('insert user - access provisional failed', $values['itEmail']);
            $_SESSION["message_system"][] = $messages->messages_build('WARNING', 'Ups!,', $messege);
          }
          
          $response_mail_support = $controlUtilities->control_utilities_send_mail_support($email_support, $content, $subject, $from_name);
        }
      }else {
        $messege = $messages->messages_failed('insert user failed');
        $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Lo sentimos,', $messege);
      }
    }else {
      $messege = $messages->messages_failed('insert user exists', $_POST['itEmail']);
      $_SESSION["message_system"][] = $messages->messages_build('WARNING', 'Email en uso,', $messege);
    }
    
    $redirect = (isset($values['redirect'])) ? $values['redirect'] : '/views/login.php';
    header('Location: ' . $redirect);
  break;

  case 'registerCompany':

    $values = $functions_sql->functions_sql_clear_dates($_POST);
    $nit_dv = $values['itNit'] . '-' . $values['dv_hidden'];

    $user_exits = $modelUser->validate_exits_user('tbl_company_info' , 'ci', $values['itEmail'], $nit_dv);

    if ($user_exits === FALSE) {
      $file          = $_FILES['fLogo'];
      $dir_upload    = '';
      $new_name_logo = '';

      if (!empty($file['name']) && !empty($file['tmp_name'] )) {
        $ext = pathinfo($file['name'], PATHINFO_EXTENSION);

        $new_name_logo = $values['itName'] . '_' . time() . '.' . $ext;
        $dir_upload = '../src/img/company/logo/';
        $max_size = 2000000;
              
        if ($file['size'] <= $max_size && $file['size'] > 0) {
          if (move_uploaded_file($file['tmp_name'], $dir_upload . $new_name_logo)) {

          }else {
            $messege = $messages->messages_failed('upload logo company failed');
            $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Ups,', $messege);
            $new_name_logo = '';
          }
        }else {
          //$message = 'Max file size 2mb!';
          $messege = $messages->messages_failed('max size logo company failed');
          $_SESSION["message_system"][] = $messages->messages_build('WARNING', 'Logo Pesado,', $messege);
          $new_name_logo = '';
        }
      }else {
        $new_name_logo = '';
      }

      $token_active_user = md5($values['itName'] . '_' . mt_rand());
      $pass = md5($values["itPassword"]);

      $model_user_insert_company = $modelUser->insert_company($values['itName'], $values['itaDescription'], $new_name_logo, $nit_dv, $values['itEconomicActivity'], $values['itPhone'], $values['itMovil'], $values['itEmail'], $pass, $values['selCity'], $values['itAddress'], $token_active_user);

      if(!empty($model_user_insert_company) && $model_user_insert_company == 1) {
        
        $response_mail_register = $controlUtilities->control_utilities_send_mail_user_register('COMPANY', $token_active_user, $values['itName'], $values['itEmail'], $values['itPassword']);

        if ($response_mail_register == true) {
            $messege = $messages->messages_success('insert user success');
            $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Estas registrado!', $messege);
            header('Location: /index.php');
        }else {
          
          $provisional_access_status = 1;
          $result_provisional_access = $modelUser->model_user_provisional_access('company', 'ci', $token_active_user, $provisional_access_status);

          $email_support = 'soporte@mihv.com.co';
          $from_name = 'MI HV - Fallo envio email activacion';

          if ($result_provisional_access == 1) {
            $subject = 'Fallo envio email de activacion, acceso ok';
            $content ='
              <div>
                <p>No se puedo enviar el correo de activacion al siguiente usuario y se le consedio acceso provisional por 24 horas:</p>
                <br><br>
                <p>Email: ' . $values['itEmail'] . '</p>
                <br>
                <p>Nombre: ' . $values['itName'] . '</p>
                <br>
                <p>Nit: ' . $values['itNit'] . '</p>
                <br>
                <p>Token Activar: ' . $token_active_user . '</p>
                <br>
              </div>
            ';

            $messege = $messages->messages_failed('insert user and email failed');
            $_SESSION["message_system"][] = $messages->messages_build('WARNING', 'Ups!,', $messege);
          }else {
            $subject = 'URGENTE! Fallo envio email de activacion, ACCESO PROVISIONAL FALLIDO';
            $content ='
              <div>
                <p>No se puedo enviar el correo de activacion al siguiente usuario y tampoco se pudo conceder el acceso provisional por 24 horas:</p>
                <br><br>
                <p>Email: ' . $values['itEmail'] . '</p>
                <br>
                <p>Nombre: ' . $values['itName'] . '</p>
                <br>
                <p>Nit: ' . $values['itNit'] . '</p>
                <br>
                <p>Token Activar: ' . $token_active_user . '</p>
                <br>
              </div>
            ';

            $messege = $messages->messages_failed('insert user - access provisional failed', $values['itEmail']);
            $_SESSION["message_system"][] = $messages->messages_build('WARNING', 'Ups!,', $messege);
          }
          
          $response_mail_support = $controlUtilities->control_utilities_send_mail_support($email_support, $content, $subject, $from_name);
        }
      }else {
        $messege = $messages->messages_failed('insert user failed');
        $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Lo sentimos,', $messege);
      }
    }else {
      $messege = $messages->messages_failed('insert user exists', $_POST['itEmail']);
      $_SESSION["message_system"][] = $messages->messages_build('WARNING', 'Usuario ya registrado,', $messege);
      header('Location: /views/login.php');
    }
    header('Location: /views/login.php');
  break;

  /*
   * - - - - - - - - - - - Recuperar Contrasenia - - - - - - - - - - -
   */
  case 'RECOVER_PASSWORD':
    $values = $functions_sql->functions_sql_clear_dates($_POST);

    if($values['typeUser'] == 'PI') {
      $type = 'pi';
      $tbl_user = 'tbl_person_info';
    }else if($values['typeUser'] == 'CI') {
      $type = 'ci';
      $tbl_user = 'tbl_company_info';
    }else {
      header('Location: /index.php');
    }
    
    $user_exits = $modelUser->validate_exits_user($tbl_user , $type, $values['itEmailRecover']);

    if ($user_exits === TRUE) {
      $token_new_password = md5($type . '_' . mt_rand(0, 50));

      $result_recover_pass = $modelUser->model_user_update_token_recover_password($tbl_user, $type, $values['itEmailRecover'], $token_new_password);

      if(!empty($result_recover_pass) && $result_recover_pass == 1) {
        $data_user = $modelUser->model_user_get_data_user($tbl_user, $type, 'EMAIL', $values['itEmailRecover']);

        $response_mail_register = $controlUtilities->control_utilities_send_mail_recover_password($data_user['id_' . $type], $data_user['name_' . $type], $data_user['email_' . $type], $type, $token_new_password);

        if ($response_mail_register == true) {
            $messege = $messages->messages_success('recover password success');
            $_SESSION["message_system"][] = $messages->messages_build('SUCCESS', 'Clave Recuperada!', $messege);
            $redirect = (isset($values['redirect'])) ? $values['redirect'] : '/index.php';
            header('Location: ' . $redirect);
        }else {
            $messege = $messages->messages_failed('recover password and email failed');
            $_SESSION["message_system"][] = $messages->messages_build('WARNING', 'Ups!,', $messege);
        }
        
      }else {
        $messege = $messages->messages_failed('recover password failed');
        $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Lo sentimos,', $messege);
      }
    }else {
      $messege = $messages->messages_failed('recover password user no exists', $values['itEmailRecover']);
      $_SESSION["message_system"][] = $messages->messages_build('WARNING', 'El Email no existe,', $messege);
    }
    $redirect = (isset($values['redirect'])) ? $values['redirect'] : '/views/login.php';
    header('Location: ' . $redirect);
  break;
}
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com