<?php
/*
 * Codigo que traia el listado de profesiones dependiendo del area seleccionado
 *

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();

if (!isset($_POST['tokenPage']) || !in_array($_POST['tokenPage'], $_SESSION['token']['pages'])) {
  header('Location: ../index.php');
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/models_utilities.cls.php");

$modelUtilities = new modelUtilities();

switch ($_POST["id_request"]) {

	case 'LOAD_LIST_PROFESSION': 
    $id_profession = filter_var($_POST["id_profession"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
    $response = $modelUtilities->model_profession_get_list_profession_selected($id_profession);
    switch ($_POST['tag_html']) {
      case 'select':
        $data_list_profession = (isset($_POST['data_list_profession'])) ? $_POST['data_list_profession'] : NULL;
        $result = _control_utilities_callback_ajax_list_profession_select($response, $data_list_profession);
        echo $result;
        break;
    } 	
	break;

  echo $result;
}


function _control_utilities_callback_ajax_list_profession_select($response, $data_list_profession = NULL) {

  $class_list_profession = ($data_list_profession === NULL) ? 'sel-list-profession' : 'sel-list-profession-modal';

    $select = '';
    $select .= '
      <label>Lista de Profesion</label>
      <div>
        <select name="selListProfession[]" data-size="8" class="selectpicker form-control '. $class_list_profession .' refresh-select" data-live-search="true" data-max-options="5" multiple>
    ';

    if ($data_list_profession === NULL) {
      foreach ($response as $key => $value) {
        $select .= '<option value="' . $key . '">' . $value . '</option>';
      }
    }else {
      
      $data_options = explode(';', $data_list_profession);
      $option_seleted = '';

      foreach ($response as $key => $value) {
        $option_seleted = (in_array($key, $data_options)) ? 'selected="selected"' : '';
        $select .= '<option value="' . $key . '" ' . $option_seleted . '>' . $value . '</option>';
      }
    }
        
    $select .= '
        </select>
      </div>
    ';        

    return $select;
}
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
*/