<?php 

session_start();

if (isset($_SESSION['user']['id_ci'])) {
  header('Location: /views/profile_company.php');
}

define("PAGE_CURRENT", "INDEX"); // llamarlo con: constant("PAGE_CURRENT")

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/model_load_oferts.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");
$modelLoadOferts = new modelLoadOferts();
$controlUtilities = new controlUtilities();
$messages = new messages_system();

?>
<!DOCTYPE html>
<!--
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
-->
<html>
<head>
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/head.php';
  ?>
  <title>INICIO</title>
</head>
<body class="page-home">

  <!-- START HEADER -->
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/header.php';
  ?><!-- END HEADER -->


  <!-- START SECTION SEARCH -->
  <div class="section wrapper-search">
    <div class="container">
      <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * -->

      <!-- CONTENEDOR MENSAJES  -->
      <div class="row wrapper-messages-system">
        <?php 
        if (isset($_SESSION["message_system"])) {
          $messeges = $messages->menssages_render($_SESSION["message_system"]);
          unset($_SESSION["message_system"]);
          echo $messeges;
        }
        ?>
      </div><!-- CONTENEDOR MENSAJES  -->


      <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * -->
      <br>
      <div class="row">
        <form method="GET" action=<?php $_SERVER['DOCUMENT_ROOT']; ?>"/views/search.php" class="form-search-oferts">
          <div class="col-xs-12 col-sm-10 col-md-10">
            <div class="input-group">
              <div class="input-group-btn search-panel">
                <select id="selCity" name="selCity" data-size="8" class="selectpicker form-control" data-live-search="true">
                  <?php
                  $_GET['selCity'] = (isset($_GET['selCity'])) ? $_GET['selCity'] : '';
                  // Cargando lista de ciudades
                  $select_city = $controlUtilities->_control_utilities_get_citys_select($_GET['selCity']);
                  echo $select_city;
                  ?>
                </select>
              </div>
              <?php
              $_GET['it_search'] = (isset($_GET['it_search'])) ? $_GET['it_search'] : '';
              ?>      
              <input id="it_search" required type="text" class="form-control input-search element-tooltip" name="it_search" placeholder="Sistemas, call center, auxiliar..." value="<?php echo $_GET['it_search']; ?>" autofocus data-toggle="tooltip" title="Realiza una busqueda!">
              <span class="input-group-btn">
                <button class="btn btn-default btn-block btn-icon-search element-tooltip" type="submit" data-toggle="tooltip" title="Buscar!">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </span>

            </div>
          </div>

          <div class="col-xs-12 col-sm-2 col-md-2">
            <span data-toggle="tooltip" title="Mostrar filtros" class="element-tooltip">
              <button type="button" class="btn btn-primary btn-search btn-block" data-toggle="collapse" data-target="#filter-panel">
                <span class="glyphicon glyphicon-filter"></span> Filtros
              </button>
            </span>
          </div>

          <br><br>
          <!-- FILTROS -->
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div id="filter-panel" class="collapse filter-panel panel panel-body">
              <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4">
                  <div class="form-group">
                    <label class="filter-col" style="margin-right:0;">
                      Salario
                    </label>
                    <select id="selSalary" name="selSalary" class="form-control">
                      <?php
                      $_GET['selSalary'] = (isset($_GET['selSalary'])) ? $_GET['selSalary'] : '';
                      // Cargando opciones de Salario
                      $option_salary = $controlUtilities->_control_utilities_get_salary_select($_GET['selSalary']);
                      echo $option_salary
                      ?>
                    </select>                                
                  </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                  <div class="form-group">
                    <label class="filter-col">
                      Fecha
                    </label>
                    <!-- <input type="hidden" name="selDateHdn" id="selDateHdn" value=""> -->
                    <select id="selDate" name="selDate" class="form-control">
                      <?php
                      $_GET['selDate'] = (isset($_GET['selDate'])) ? $_GET['selDate'] : '';
                    // Cargando opciones de fecha
                      $option_select = $controlUtilities->_control_utilities_filter_date_select($_GET['selDate']);
                      echo $option_select
                      ?>
                    </select> 
                  </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                  <div class="form-group">
                    <label>Genero</label>
                    <div>
                      <select id="selGenre" name="selGenre" class="form-control">
                        <?php
                        $_GET['selGenre'] = (isset($_GET['selGenre'])) ? $_GET['selGenre'] : '';
                      // Cargando opciones de Salario
                        $option_genre = $controlUtilities->_control_utilities_get_genre_select($_GET['selGenre']);
                        echo $option_genre
                        ?>
                      </select> 
                    </div>
                    <br>                    
                  </div>
                </div>
              </div>
            </div>
          </div><!-- /FILTROS -->
        </form>
      </div>
      <br>
    </div><!-- Container -->
  </div><!-- END SECTION SEARCH -->

  <!-- START SECTION SLIDER OFERTS -->
  <div class="section wrapper-count-oferts">
    <br>
    <div class="container">
      <div class="row title-page">
        <div class="col-md-12 text-center">
          <h3 class="text-inverse">MI HV</h3>
          <p class="text-inverse">Estan ofertas requieren personal urgente, revísalas cuanto antes!</p> 
        </div>
      </div>
      <div class="row wrapper-oferts-home">
        <div class="col-xs-12">

          <?php
            $token_search = $controlUtilities->control_utilities_create_token_pages('index');
            echo '<input type="hidden" name="tokenPage" value="' . $token_search . '" id="tokenPage">';
  
            //if ($mobile_detect->isMobile()) {}
            $oferts_prioriry = $controlUtilities->control_utilities_get_oferts_priority();
            echo $oferts_prioriry;
          ?>
        </div>
      
      </div>

    </div><!-- Container -->
    <br>
    <br>
  </div><!-- END SECTION SLIDER OFERTS -->

  <!-- START SECTION SLIDER HOME 1 - 2 -->
  <div class="section wrapper-carousel-home">
    <br>
    <br>
    <div class="container">
      <div class="row title-page">
        <div class="col-md-12 text-center">
          <h3 class="text-inverse">MI HV</h3>
          <p class="text-inverse">Porque somos un portal de empleo diferente?.</p>

        </div>
      </div>
      <br>
      <div class="row">
        <!-- Boxes de Acoes -->
        <div class="col-xs-12 col-sm-6 col-lg-4">
          <div class="box">             
            <div class="icon">
              <div class="image"></div>
              <div class="info">
                <h3 class="title">Te ahorramos tiempo</h3>
                <p style="text-align: justify;">
                  No necesitas escanear archivos ni subirlos a nuestro portal, sabiendo que los empleadores en su mayoría no revisan las hojas de vida.<br><br>Solo te pedimos unos cuantos datos y listo.<br><br> Igual cuando te llamen a entrevista debes llevar tu Hoja de Vida.
                </p>
              </div>
            </div>
            <div class="space"></div>
          </div> 
        </div>
        
        <div class="col-xs-12 col-sm-6 col-lg-4">
          <div class="box">             
            <div class="icon">
              <div class="image"></div>
              <div class="info">
                <h3 class="title">Portal colombiano</h3>
                <p style="text-align: justify;">
                  Este portal es hecho para colombia.<br><br>Porque sabemos que tus datos son privados, aparte de protegerlos mediante seguridad que la mayoria de portales no lo ofrecen, también te pedimos pocos datos. Asegurando que una vez que te registres (que toma menos de 10 minutos) ya puedes aplicar a ofertas laborales de tu sector.
                </p>
              </div>
            </div>
            <div class="space"></div>
          </div> 
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-4">
          <div class="box">             
            <div class="icon">
              <div class="image"></div>
              <div class="info">
                <h3 class="title">Otras Herramientas</h3>
                <p style="text-align: justify;">
                  Porque aparte de los hechos ya nombrados tenemos más cosas que innovan en un portal web de empleo que una vez te registres te darás cuenta.<br><br>Que esperas, anímate y consigue empleo y de paso comparte esta información con tu familia y amigos.<br><br>Síguenos en nuestras redes sociales. Te esperamos.
                </p>
              </div>
            </div>
            <div class="space"></div>
          </div> 
        </div>        
        <!-- /Boxes de Acoes -->
      </div>
      <br><br>
    </div>
  </div>


  <!-- Your share button code -->
  <!-- <div class="fb-share-button button--share" 
    data-href="https://dev.mihv.com.co/views/login.php" 
    data-layout="button_count">
  </div>
 -->

<!-- <span data-toggle="tooltip" title="Ayuda a tus contactos!" class="element-tooltip">
  <button style="padding: 4px;" type="button" class="btn btn-primary button--share" data-link="https://dev.mihv.com.co" data-image="https://imagenes.mihv.com.co/email/header-logo.jpg" data-caption="THE CAPTION" data-description="THE DESCRIPTION">
    <i class="fa fa-2x fa-fw fa-facebook fa-border" style="font-size: 13px; border-radius: 4px;"></i><span style="font-size: 12px;"> Compartir Oferta</span>
  </button>
</span> -->



  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/footer.php';
  // MODAL QUE CONTIENE TODA LA INFORMACION DE LA OFERTA SELECCIONADA
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/modals/modal_ofert_selected_person.php';
  // MODAL PARA INICIO DE SESION O REGISTRO DE PERSONA  
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/modals/modal_login_register.php';
  // Modal Contenido Normal - Terminos y Condiciones
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/modals/modal_content_normal.php';
  // SCRIPTS DEL SITIO
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/scripts_footer.php';
  ?>
</body>
</html>