<?php 

session_start();

require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");
$messages = new messages_system();

define("PAGE_CURRENT", "TERMS_AND_CONDITIONS");

$protocol = '';

if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") { 
  $protocol = "https://";
} else { 
  $protocol = "http://";
}

$domain = $protocol . $_SERVER['HTTP_HOST'];

?>
<!DOCTYPE html>
<!--
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
-->
<html>
<head>
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/head.php';
  ?>
  <title>Terminos y Condiciones - MI HV</title>
</head>
<body class="page-terms-and-conditions">

  <!-- START HEADER -->
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/header.php';
  ?><!-- END HEADER -->



  <!-- WRAPPER RECOVER PASSWORD-->
  <div class="section wrapper-terms-conditions">
    <div class="container">  
      <!-- CONTENEDOR MENSAJES  -->
      <div class="row wrapper-messages-system">
        <?php 
        if (isset($_SESSION["message_system"])) {
          $messeges = $messages->menssages_render($_SESSION["message_system"]);
          unset($_SESSION["message_system"]);
          echo $messeges;
        }
        ?>
      </div><!-- CONTENEDOR MENSAJES  --> 
      <div class="row">
        <div class="col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1">
          <div class="panel panel-default">
            <div class="panel-heading text-center">
              <h4 class="text-primary">TÉRMINOS Y CONDICIONES DEL SITIO WEB MIHV.COM.CO</h4>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1">
                  <div class="form-group">
                    <br>
                    <p style="text-align: justify;">
                      Los TÉRMINOS Y CONDICIONES aquí establecidos, bajo los cuales podrás usar el sitio web mihv.com.co, debes leerlas detenidamente y si no estás de acuerdo, no hay problema, pero no usarías el sitio web <b>mihv.com.co</b> ni los servicios que se prestan. Para efectos de aclaraciones los términos usados de aquí en adelante serán: el sitio web mihv.com.co se llamará ‘sitio web’; <b>Code Connection Professional SAS</b> (Propietario del sitio web) ‘Codection’; y, en tu caso ‘usuario’ así seas persona natural, jurídica u otra entidad de cualquier naturaleza debidamente legalizada que acceda al sitio web sin importar la razón por la cual ingresó; para derivaciones de ‘usuario’ dentro del sitio web dependiendo de tu caso serás ‘persona’ o ‘empresa’.
                      <br><br>
                      Al usar el sitio web manifiestas que aceptas y estás de acuerdo con estos los términos y condiciones publicados por Codection los cuales podrán ser modificados en cualquier momento. Si quieres estar al tanto de los cambios realizados, estos estarán disponibles en la url <a href="<?php echo $domain; ?>/tyc">https://www.mihv.com.co/tyc</a>
                      <br><br><br>  
                      <b>DE LAS DISPOSICIONES GENERALES:</b>
                      <br><br>
                      Codection no asegura que el material pueda ser legalmente accesible dentro del territorio colombiano, al tratarse de un dominio con interés público cualquier persona podrá acceder al sitio web y dependiendo en qué calidad de rol entre, podrá ver la información que suministraste a Codection. En algunos países este sitio web no podrá ser legal, si eres una persona, empresa u otra entidad que está legalmente constituida en tu país, pero por las leyes que rigen allá, no podrás usar el sitio web, pero si lo haces es bajo tu propia responsabilidad y exoneras a Codection de cualquier responsabilidad civil, penal u otra que aplique en tu país.
                      <br><br>
                      Estos términos y condiciones son regidos por los decretos, leyes, y demás artículos que aplican en el territorio de Colombia sin que se dé aplicación a los principios y normas establecidas sobre el conflicto de leyes. Si quieres hacer alguna reclamación de estos términos y condiciones será única y exclusivamente aplicable con los tribunales y los jueces de Colombia. Si algún ítem de estos términos y condiciones es declarado ineficaz, inválido, nulo u otra sentencia proferida por un juez o un tribunal de la república de Colombia, esto no anulará la validez de los demás ítems que se encuentran en estos términos y condiciones.
                      <br><br><br>
                      <b>DEL USO DEL MATERIAL PERTENECIENTE AL SITIO WEB:</b>
                      <br><br>
                      Codection te autoriza a usar el material que se encuentra en el sitio web, ya sea para consultar, revisar, publicar (tu hoja de vida virtual como persona o las ofertas de empleo como empresa) y utilizar las demás herramientas disponibles, recuerda que su uso es exclusivamente personal mas no comercial y según sea lo establecido en estos términos y condiciones.
                      <br><br>
                      En el sitio web verás imágenes, textos, íconos, software y otro material que son propiedad exclusiva de Codection y están protegidos bajo todas las leyes que rigen en Colombia sobre derechos de autor, propiedad industrial y las demás leyes que aplicables al respecto. Para el caso de otras imágenes como logotipos de las empresas que publiquen una oferta laboral (dentro del sitio web) son de propiedad exclusiva del mismo y en caso de que haya alguna inconsistencia, el sitio web ofrece un correo de información que es <a href="mailto:info@mihv.com.co?Subject=Caso%20logotipo" target="_top">info@mihv.com.co</a> a través del cual el afectado se pondrá en contacto con el sitio web e informará el caso (con pruebas) para que el sitio web redirija el caso a la empresa del logotipo y se seguirá el trámite correspondiente ya que, como se aclara, el sitio web ofrece una opción para que la empresa publique (mas no es obligatorio) una imagen que represente a la empresa bajo la responsabilidad de la misma y al usar esta opción la empresa exonera de toda responsabilidad al sitio web por la publicación de ese logotipo.
                      <br><br>
                      Te recordamos que (para el usuario) el uso no autorizado de este material (de parte de Codection) puede constituir una violación de las leyes colombianas y/o extranjeras que protegen los derechos de autor, así como las de propiedad industrial y las demás leyes que aplicasen.
                      <br><br>
                      Si no tienes el permiso de Codection para el fin que requieras, no podrás vender o modificar el material del sitio web ni anunciarlo o ejecutarlo pública o privadamente para tus propósitos comerciales. Te recordamos que tampoco puedes adaptar, copiar o clonar el material del sitio web incluyendo el código de programación que tiene dentro del mismo para crear tus páginas web o aplicaciones online u offline (con o sin conexión a internet) ya que como te reiteramos están protegidos por los derechos de autor de Codection.
                      <br><br><br>
                      <b>DEL USO PROHIBIDO DEL SITIO WEB:</b>
                      <br><br>
                      <span style="text-decoration: underline;">MARCO USO GENERAL:</span>
                      <br><br>
                      Tú en calidad de usuario no puedes usar el sitio web con el fin de almacenar o eliminar el material de sitio web, así como de distribuirlo, transmitir y/o retransmitirlo bajos los siguientes fines:
                      <br><br>
                    </p>
                      <ol type="a">
                        <li>De manera difamatoria, amenazante, obscena, abusiva, etc.</li>
                        <li>
                          En que los derechos de autor, de propiedad industrial 
                          o cualquier otro derecho de la misma se vean infringidos. 
                        </li>
                        <li>
                            Violar la privacidad, publicidad, difamación y otros derechos personales de usuarios y terceras personas que tengan alguna relación con el sitio web.
                        </li>
                        <li>
                          En otras violaciones de cualquier ley, intermediación y/o 
                          regulación que aplicase.
                        </li>
                      </ol>
                    <p style="text-align: justify;">
                      <br><br><br>
                      <span style="text-decoration: underline;">MARCO DE SEGURIDAD GENERAL:</span>
                      <br><br>
                      Tú en calidad de usuario te queda rotundamente prohibido:
                      <br><br>
                    </p>
                      <ol type="a">
                        <li>
                          Intentar violar la seguridad del sitio web sin importar el motivo, a excepción que estés autorizado por Codection.
                        </li>
                        <li>
                          Hacer pruebas con el sitio web, no importa el motivo y probar su vulnerabilidad que use el sitio web de forma directa, indirecta o que no se use, pero pertenezca al sitio web.
                        </li>
                        <li>
                          Si el acceso te redirige a otro usuario, <span style="text-decoration: underline;">NO PODRÁS</span> acceder, eliminar, editar, y/o sabotear dicha información de forma parcial o total sin el previo consentimiento del mismo.
                        </li>
                        <li>
                          Enviar información sin el consentimiento de Codection a través de cualquier medio de publicidad en el cual se pueda transmitir incluyendo correos electrónicos, pautas publicitarias, entre otras, dentro y fuera del sitio web, tanto de forma digital, como presencial y no presencial.
                        </li>
                        <li>
                          Acceder a otras plataformas, servidores y/o cuentas de otros usuarios.
                        </li>
                        <li>
                          Interferir con la prestación de servicio de otros usuarios.
                        </li>
                        <li>
                          Enviar y/o incluir cualquier tipo de amenaza (virtual ya sea virus, malware entre otros o físico) que atente contra la correcta funcionalidad del sitio web o su inhabilidad de funcionamiento parcial y/o total del mismo.
                        </li>
                        <li>
                          Otros intentos de dañar de forma parcial o total el funcionamiento del sitio web incluyendo tanto software como hardware.
                        </li>
                      </ol>
                    <p style="text-align: justify;">
                      <br><br>
                      <span style="text-decoration: underline;">TEN EN CUENTA:</span>
                      <br><br>
                      En caso que omitas alguna de estas prohibiciones e intentes ingresar al sistema de forma indebida y/o sin la previa autorización de Codection, procederemos a hacer la investigación y de encontrar alguna prueba que te involucre, tomaremos acciones legales en responsabilidades civiles y penales.
                      <br><br><br>
                      <b>DE LAS PROHIBICIONES DE LOS USUARIOS EN EL SITIO WEB:</b>
                      <br><br>
                      Codection suspenderá y en el peor de los casos eliminará tu cuenta (como usuario) del sitio web en cualquiera de los siguientes casos:
                    </p>
                      <ol type="a">
                        <li>
                          Que llenes en el formulario de registro del sitio web con información inexacta, falsa o que no concuerde con la realidad. (Esto también impide que haya una correcta comunicación entre usuarios).
                        </li>
                        <li>
                          Hagas modificaciones en otros perfiles de usuarios y hacer uso de las herramientas que usan sin la debida autorización del mismo.
                        </li>
                        <li>
                          Descifrar, desensamblar, compilar, clonar, duplicar, eliminar y/o modificar parte o todo el sitio web tanto software como hardware.
                        </li>
                        <li>
                          Modifiques un elemento que atente contra el funcionamiento del sitio web en su día a día o uses un nuevo elemento que genere conflictos con el mismo.
                        </li>
                        <li>
                          Incluir en el sitio web cualquier derecho de: pirámide, agencia comercial, franquicia, membresía a un grupo o institución, en representación de las ventas o cualquier derecho que genere oportunidad de negocio que requiera un pago anticipado o pagos periódicos a uno o varios usuarios (incluyendo reclutamiento), como sub-agentes o sub-distribuidores, entre otros tipos de negocios que pidan por anticipado dinero (para todos los usuarios) y/o en su oferta (aplica a empresas) así como en la descripción se perciba o se trate de un negocio ilegal y/o de dudosa procedencia.
                        </li>
                        <li>
                          En el caso de las empresas: generar varias ofertas sin hacer la verificación previamente solicitada por Codection (si lo requiere).
                        </li>
                        <li>
                          Insertes en el sitio web una información falsa, incompleta, engañosa o inexacta.
                        </li>
                      </ol>
                    <p style="text-align: justify;">
                      <br><br>
                      Si tienes acceso a un área privada del sitio web, te queda rotundamente prohibido revelar o compartir esa contraseña con terceras personas sin el previo consentimiento de Codection o usar esta misma para propósitos no autorizados.
                      <br><br><br>
                      <b>DE LA INFORMACIÓN PERTINENTE A LOS USUARIOS PERSONAS:</b>
                      <br><br>
                      Tú en calidad de usuario persona que ingresas tus datos en el formulario de registro persona para generar tu hoja de vida virtual, autorizas a Codection a guardar la información otorgada aclarando que tus datos son reales y de demostrarse cualquier inconsistencia hecha en el registro, tú como usuario persona asumes las consecuencias civiles y penales. Aclaras que exoneras a Codection de toda responsabilidad por mostrar los datos previamente guardados por ti. También informas que el correo que registraste en el sitio web es personal y de uso en el mismo para el ingreso y hacer las gestiones que sean de interés, además en este llegará las notificaciones que el sitio web genere.
                      <br><br>
                      Agregando a los términos y condiciones que serán previstos de otras políticas de privacidad del sitio web entre estos el aviso de privacidad; aceptas, entiendes y acuerdas que Codection puede dar a conocer a terceras personas sobre bases anónimas parte de información contenida en tu formulario de solicitud de registro. Codection no revelará a terceras personas sobre datos importantes como tu nombre, email, teléfono de contacto y/o dirección de residencia sin tu consentimiento el cuál es expresado en los diferentes medios de difusión y otras herramientas dentro y fuera del sitio web, a excepción, en el caso que amerite, sea necesario y/o acorde para cumplir las leyes, procesos y/o procedimientos legales en lo pertinente.
                      <br><br>
                      Codection se reserva el derecho de ofrecerte productos y/o servicios de nuestra parte o de terceras personas, ya sea a modo general o en base a los datos que hayas puesto en el formulario de registro para actualizar tus datos (perfil) en cualquier momento desde tu registro inicial.
                      <br><br>
                      Te recordamos que los posibles productos y/o servicios podrán ser dados por Codection o por terceras personas que autorice Codection.
                      <br><br><br>
                      <b>DE LA POLÍTICA DE AVISO DE PRIVACIDAD:</b>
                      <br><br>
                      La política de aviso de privacidad tiene un segmento independiente y forma parte de los términos y condiciones. Para más información favor consulta la política de aviso de privacidad en la siguiente url <a href="<?php echo $domain; ?>/pap">https://www.mihv.com.co/pap</a>
                      <br><br><br>
                      <b>DE LA PUBLICIDAD EN EL SITIO WEB:</b>
                      <br><br>
                      La publicidad en el sitio web tiene un segmento independiente y forma parte de los términos y condiciones. Para más información favor consulta la publicidad en el sitio web en la siguiente url <a href="<?php echo $domain; ?>/publicidad">https://www.mihv.com.co/publicidad</a>
                      <br><br><br>
                      <b>DE LA PUBLICIDAD EN EL SITIO WEB:</b>
                      <br><br>
                      Como usuario, eres responsable por lo que hagas en el sitio web, entre estos la información y comunicación que des y de ser el caso las consecuencias que traería al incluir datos erróneos, inválidos, falsos o irreales ya que esto perjudicaría los fines perseguidos.
                      <br><br>
                      Además, te queda rotundamente prohibido:
                      <br>
                    </p>
                      <ol type="a">
                        <li>
                          Incluir en el sitio web cualquier tipo de archivo o texto que implique una violación a los derechos de propiedad industrial, intelectual, humano u otro derecho.
                        </li>
                        <li>
                          Incluir en el sitio web cualquier tipo de archivos o texto que incluya directa o indirectamente, tanto en el territorio colombiano como en el internacional, aspectos que con lleven cualquier tipo de pornografía.
                        </li>
                        <li>
                          Incluir cualquier tipo de material que sea: abusivo, ofensivo, obsceno, amenazante o difamatorio para cualquier otro usuario.
                        </li>
                        <li>
                          Insertar en el sitio web material que esté protegido bajo los derechos de autor. Si tú eres el dueño, no hay lio, lo puedes publicar, pero tú serás el único responsable de causar daños a otros usuarios o al sitio web.
                        </li>
                        <li>
                          Insertar en el sitio web cualquier tipo de publicidad sin que Codection te lo autorice.
                        </li>
                        <li>
                          Incluir en el sitio web cualquier tipo de virus o programas con la finalidad de hackear ya sea interceptando, apropiándose de cualquier dato, interfiriendo, o la intención de dañar y/o apropiarse de datos o información de cualquier usuario o al sitio web y su funcionalidad.
                        </li>
                        <li>
                          Insertar y/o publicar en el sitio web archivos o texto que su contenido tenga secretos comerciales, industriales u otro tipo de secreto que atente con la producción de otros usuarios a menos que seas el propietario o tengas la autorización del mismo.
                        </li>
                      </ol>
                    <p style="text-align: justify;">
                      <br><br>
                      <span style="text-decoration: underline;">TEN EN CUENTA:</span>
                      <br><br>
                      Codection no otorga ninguna garantía, ya sea implícita o expresa acerca de la confiabilidad, exactitud o veracidad del ingreso de la información incluida en el sitio web por los usuarios, tampoco apoya o respalda las opiniones expresadas por los mismos usuarios. Entiendes, sabes, reconoces y declaras que la confianza depositada en Codection de cualquier tipo de archivo o texto que incluyas en el sitio web es bajo tu propio riesgo, o cuando leas y/o veas esa información de parte de otro usuario, exoneras a Codection de alguna responsabilidad que genere esa información.
                      <br><br>
                      Te recordamos que Codection es un intercomunicador para la publicación y distribución de la información exhibida por los usuarios en la internet dentro del sitio web y/o en la internet (world wide web) y no está obligado a revisar con anterioridad dicha información que ingresaste en el sitio web. Si recibimos una notificación de parte de otro o varios usuarios en donde hayas infringido cualquier punto de estos términos y condiciones incluyendo la información que nos diste, Codection investigaría tal información y de ser cierta, se procederá a eliminar tu cuenta de forma discreta del sitio web anotando tu antecedente en nuestro registro de usuarios; si vuelves a inscribirte en el sitio web y sigues incumpliendo y se descubre que eres reincidente en violar alguno de estos términos y condiciones procederemos a bloquear tus datos en el sitio web y de ser el caso se notificará a las autoridades competentes para que se continúe con el trámite. Por otra parte, si en nuestra búsqueda de información errónea o inexacta detectamos que ingresaste datos inconclusos, de dudosa procedencia, falsa o inexacta, procederemos a suspender tu cuenta y te notificaremos mediante un correo electrónico y te pediremos nos hagas llegar información real y una vez validada la ingresaremos a tu perfil y habilitaremos tu cuenta, pero si no nos envías dicha información, procederemos a eliminar tu cuenta y de ser el caso bloquear tus datos.
                      <br><br><br>
                      <b>DE LA CONTRASEÑA DE ACCESO AL SITIO WEB:</b>
                      <br><br>
                      Tú en calidad de usuario eres el único responsable de mantener la confidencialidad de la misma. Como sabrás dicha clave o contraseña de acceso es exclusiva de ese usuario tuyo, por ende, exoneras a Codection de toda responsabilidad ya que el manejo de esa contraseña es exclusivamente tuyo. Si tu compartes tu clave o contraseña de acceso al sitio web es bajo tu criterio, pero serás el único responsable por los cambios que otra o varias personas hagan sobre tu cuenta.
                      <br><br>
                      Si no puedes acceder a tu cuenta con la contraseña que tienes, te damos la opción que recuperes tu contraseña mediante el correo electrónico; una vez llenes los campos requeridos (debemos hacer una verificación básica para saber que realmente seas tú) te enviaremos por los diferentes medios de comunicación que estén disponibles de parte de Codection el cambio de contraseña y una vez lo recibas, restaures tu acceso al sitio web cambiando la contraseña por una nueva, si no se supera el validador de recuperar contraseña no podremos hacer nada por ti y si deseas recuperar esa cuenta podrás ponerte en contacto con Codection en los diferentes medios que el sitio web tiene a tu disposición.
                      <br><br>
                      <span style="text-decoration: underline;">
                        TE RECORDAMOS QUE EN CASO QUE LA RECUPERACIÓN DE CONTRASEÑA SEA POR CORREO ELECTRÓNICO:
                      </span>
                      <br><br>
                      No le des la contraseña a otra persona (si lo haces que sea alguien de confianza), eres la única persona que tiene acceso a tu correo electrónico, nuestro sistema básico de validación analiza los datos suministrados en el formulario de restauración de contraseña, si coinciden, te enviaremos un correo electrónico y deberás ver los pasos a seguir. SI COMPARTES TU CONTRASEÑA CON OTRA PERSONA sobre tu correo electrónico es bajo tu responsabilidad. Si otra persona tiene acceso a este correo, sigue los pasos para recuperar la contraseña, ingresa al sitio web y ejecuta acciones que van en contra de estos términos y condiciones, tu cuenta podrá ser eliminada y notificada a las autoridades competentes por los delitos que se haya generado dentro del sitio web. NUESTRA RECOMENDACIÓN que en lo posible no le des contraseñas a otras personas, en especial la del sitio web y la de tu correo electrónico personal (que dejaste registrada en el sitio web) pero si las compartes, cambia periódicamente las claves, evítate un dolor de cabeza por culpa de otras personas.
                      <br><br><br>
                      <b>
                        DE LOS TÉRMINOS SOBRE LA CESIÓN O USO PARA FINES COMERCIALES NO AUTORIZADOS (INCLUYE REVENTA):
                      </b>
                      <br><br>
                      Tú en calidad de usuario acuerdas con Codection no ceder tu acceso a ninguna otra persona o empresa u forma de negocio que esté legalmente constituida y legalizada en el mundo. Tampoco harás de tu cuenta un uso comercial a menos que estés autorizado por Codection.
                      <br><br><br>
                      <b>DE LA TERMINACIÓN DE LA PRESTACIÓN DEL SERVICIO:</b>
                      <br><br>
                      Codection se reserva el derecho de finalizar la prestación de servicio con el usuario por:
                      <br><br>
                    </p>
                      <ol type="a">
                        <li>
                          Inconvenientes al validar la información suministrada por el usuario.
                        </li>
                        <li>
                          Mal manejo de la cuenta por parte del usuario.
                        </li>
                        <li>
                          Inconvenientes con otros usuarios.
                        </li>
                        <li>
                          Por publicidad sin consentimiento previo de parte de Codection.
                        </li>
                        <li>
                          Por algún incumplimiento en estos términos y condiciones.
                        </li>
                        <li>
                          Por otros hechos que Codection considere un motivo para dejar de prestar sus servicios a ese usuario.
                        </li>
                      </ol>
                    <p style="text-align: justify;">
                      <br><br>
                      Una vez se cumpla alguno de estos ítems, se procederá (si Codection así lo cree pertinente) a borrar toda información que hayas incluido en el sitio web y eliminar tu cuenta (acceso al sitio web).
                      <br><br><br>
                      <b>DE LA URL O LINK A OTRO SITIO WEB:</b>
                      <br><br>
                      En este sitio web no hay url o link que redirija a otro sitio web de terceras personas de parte de los usuarios, si ves una url o link publicado por otro usuario estará en formato de texto plano, es decir, no podrás acceder a dicha url dando click, deberás copiar la url y pegarla en otra pestaña o navegador para que puedas acceder a ella, todo bajo tu exclusiva responsabilidad exonerando a Codection. Si tú eres el usuario que publica esta url, serás el único responsable por los daños y perjuicios que le puedas causar a los usuarios que ingresen a esa url y exoneras a Codection de toda responsabilidad que le puedas causar a otros usuarios dentro del sitio web.
                      <br><br><br>
                      <b>DE LA BASE DE DATOS CON INFORMACIÓN DE LOS USUARIOS:</b>
                      <br><br>
                      Tú como usuario al llenar el formulario de registro en pleno uso de tus facultades, autorizas de modo expreso a Codection y sus filiales y/o subsidiarias a: Recolectar, procesar y comercializar los datos contenidos en el formulario. Por tanto, Codection junto con sus filiales y/o subsidiarias avaladas por la misma (Exigiendo las garantías mínimas del uso de tratamiento de datos) con esta información podrán traducir, extraer, agrupar, adaptar y/o publicar dichos datos o información dada por ti (como usuario).
                      <br><br>
                      Así mismo tu como usuario le confieres plena facultad para disponer de tus datos a título oneroso o gratuito (depende de tu rol en el sitio web) bajo las condiciones que el libre criterio de Codection dictamine.
                      <br><br><br>
                      <b>DE LA HOJA DE VIDA VIRTUAL PERSONAL:</b>
                      <br><br>
                      Codection quiere agilizar la forma en que se muestran tus datos de tu hoja de vida, para ello al momento de registrarte, diligenciarás un formulario con los datos básicos para crear tu hoja de vida virtual (estos datos deben ser reales en su totalidad). Al registrarte en el sitio web entiendes el tratamiento que se le darán a tus datos personales. Si requieres más información favor consulta la política de aviso de privacidad en la siguiente url <a href="<?php echo $domain; ?>/pap">https://www.mihv.com.co/pap</a>
                      <br><br><br>
                      <b>DE LA RESPONSABILIDAD DE CODECTION HACIA LOS USUARIOS:</b>
                      <br><br>
                      Codection en su calidad de prestador de servicio ha dispuesto una plataforma de intercomunicación de oferta y demanda con el fin de ayudar a disminuir la tasa de desempleo que actualmente hay en el territorio colombiano. En esta plataforma se encuentran las herramientas tecnológicas mínimas para prestar un eficiente servicio, así como de otras herramientas para aumentar la efectividad y rapidez del sitio web para evitar demoras dentro del mismo (y fuera de este si se aplicase).
                      <br><br>
                      <span style="text-decoration: underline;">Para efectos de los usuarios con rol de persona.</span> Se han dispuesto un formulario que a consideración de Codection es básico para que lo diligencies con tus datos reales, ya que con esto el sitio web generará una hoja de vida virtual detallando lo más importante de la misma. Luego de haberse generado tu hoja de vida virtual, podrás acceder a oferta laborales que estarán visibles en ciertos lugares del sitio web.
                      <br><br>
                      <span style="text-decoration: underline;">Para efectos de buscar una oferta laboral.</span> Como sabemos que tu tiempo es valioso, hemos puesto a tu disposición herramientas que te ayudarán a agilizar la búsqueda de la oferta de tu interés, estas herramientas te ayudarán a reducir tu tiempo de búsqueda (entre lectura de 2 o más ofertas).
                      <br><br>
                      <span style="text-decoration: underline;">Para efectos de aplicar a una oferta laboral.</span> Si estás en la búsqueda de una o varias ofertas laborales, hemos puesto a tu disposición herramientas que te ayuden a disminuir y agilizar el proceso de postulación. Una vez que hayas leído la oferta de interés y crees que cumples los requisitos mínimos exigidos por la empresa solicitante, como requisito de parte del sitio web y de Codection es: Primero (1ro.) debes estar activo en el sitio web (haberte registrado correctamente, esto incluye el activar tu cuenta) y Segundo (2do.) iniciar sesión en el sitio web.
                      <br><br>
                      <span style="text-decoration: underline;">Para efectos de los usuarios con rol de empresa.</span> Se han dispuesto herramientas tecnológicas que ayudarán a reducir el tiempo de la producción y evitar malos entendidos que puedan surgir entre Codection y la empresa. Codection sabe que tu tiempo es lo más importante y por ello cuenta con dichas herramientas tecnológicas para agilizar tu tiempo de producción.
                      <br><br>
                      Para efectos de publicar tu oferta laboral se ha puesto a tu disposición un simple formulario que a consideración de Codection es lo básico para poder dejar en claro a los demás usuarios el motivo de publicar tu oferta laboral. Como sabrás Codection piensa en tu tiempo y ha puesto dos tipos de modalidad para publicar tu oferta (normal y urgente), así como de otras herramientas tecnológicas que podrás usar en tiempo real para hacer la correcta gestión (administración) de tu oferta publicada.
                      <br><br><br>
                      <b>DE LA RESPONSABILIDAD DE CODECTION EN GENERAL:</b>
                      <br><br>
                      Codection actúa como un sitio virtual en el cual habrá oferta y demanda sobre los servicios de empleo y relacionados, estos podrán incluir propuestas y/o servicios de parte de la empresa con interés en especial en los usuarios personas. Codection no revisa ni censura servicios, ofertas y/o bienes a publicar. Codection no se involucrará en las gestiones, tratos o transacciones que surjan en la oferta y demanda entre los usuarios del sitio web.
                      <br><br>
                      De la selección de un producto o servicio es responsabilidad única y exclusiva de los usuarios del sitio web, por ende, Codection no es responsable de los daños y perjuicios que se realicen entre los usuarios, incluyéndose la responsabilidad civil y penal.
                      <br><br>
                      Codection no certifica, recomienda, avala ni garantiza los productos y/o servicios ofrecidos dentro del sitio web ni a través del mismo.
                      <br><br>
                      Codection no tiene ni ejerce algún tipo de control sobre la idoneidad, legalidad, seguridad, calidad de algún servicio que se preste y/o producto ofrecido en el sitio web. Así mismo, Codection no tiene ni ejerce algún control sobre la exactitud, y/o veracidad de la información publicada por los usuarios en general, ni por la oferta y demanda de los productos y/o servicios ofrecidos por parte de los mismos.
                      <br><br>
                      Si llegas a generar algún tipo de acuerdo, contrato, términos y condiciones u otro tipo de acuerdo entre tú y otro usuario del sitio web incluido (de ser el caso) el cumplimiento de las leyes aplicables a ese tipo de acuerdo o contrato, será exclusiva responsabilidad entre los usuarios y no de Codection y/o el sitio web.
                      <br><br>
                      Teniendo en cuenta que la verificación de la autenticidad de los usuarios es bastante dispendiosa, Codection no podrá confirmar la veracidad de la información suministrada por cada usuario, razón por la cual no se involucrará en los tratos que se genere entre los mismos ni controla dicho comportamiento.
                      <br><br>
                      Codection no controla el registro del usuario ni la información que pueda ser ingresada por el mismo (incluido en caso de actualizar sus datos) y que pueda mostrarse o estar disponible dentro y a través del sitio web.
                      <br><br>
                      Codection no te garantiza que el sitio web siempre esté disponible en su totalidad o que opere libre de errores, que en el sitio web y su servidor de alojamiento se encuentre libre de virus de dispositivos tecnológicos de la información u otros mecanismos que generen o dañen parcial o totalmente tu dispositivo, es decir, si el uso del sitio web (de tu parte) resulta en la afectación del medio con el cuál ingresas al mismo y resultase en la necesidad de prestar el servicio de reparación o mantenimiento de tus equipos, perdida de información o de reemplazar dichos dispositivos y la información borrada, Codection no es responsable por los gastos que esto genere.
                      <br><br>
                      Codection protegerá tus datos incluyendo ciertos sistemas de seguridad, pero no está exento el sitio web de que sea atacado por un tipo dañino de programa y/o hacker (o grupo de hackers) con el fin de rentalizar, suspender, dañar y/o detener servicio del sitio web de forma parcial o total.
                      <br><br>
                      El sitio web junto con el material incluido, se pone a disposición en el estado que se encuentren a los usuarios. Codection no da alguna garantía sobre la confiabilidad, oportunidad de material, exactitud, sus servicios, el software, los gráficos, los textos y/o los links (o vínculos) que se encuentren dentro o a través del sitio web.
                      <br><br>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /END WRAPPER RECOVER PASSWORD-->


  <!-- START FOOTER -->
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/footer.php';
  // SCRIPTS DEL SITIO
  // require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/scripts_footer.php';

  ?><!-- END FOOTER -->



</body>
</html>
