<?php 

session_start();

require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");
$messages = new messages_system();

define("PAGE_CURRENT", "TERMS_AND_CONDITIONS");

?>
<!DOCTYPE html>
<!--
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
-->
<html>
<head>
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/head.php';
  ?>
  <title>Terminos y Condiciones - MI HV</title>
</head>
<body class="page-terms-and-conditions">

  <!-- START HEADER -->
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/header.php';
  ?><!-- END HEADER -->



  <!-- WRAPPER RECOVER PASSWORD-->
  <div class="section wrapper-terms-conditions">
    <div class="container">  
      <!-- CONTENEDOR MENSAJES  -->
      <div class="row wrapper-messages-system">
        <?php 
        if (isset($_SESSION["message_system"])) {
          $messeges = $messages->menssages_render($_SESSION["message_system"]);
          unset($_SESSION["message_system"]);
          echo $messeges;
        }
        ?>
      </div><!-- CONTENEDOR MENSAJES  --> 
      <div class="row">
        <div class="col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1">
          <div class="panel panel-default">
            <div class="panel-heading text-center">
              <h4 class="text-primary">Terminos y Condiciones</h4>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1">
                  <div class="form-group">
                    <br>
                    <p style="text-align: justify;">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ex nisl, tincidunt eu velit sed, tempus ullamcorper mauris. Etiam suscipit pretium libero, eu feugiat turpis. Cras vulputate, purus vitae finibus semper, lacus ligula imperdiet diam, non tempus lorem nibh ac lacus. Aliquam fermentum maximus dui a malesuada. Vivamus consectetur, dolor vitae lacinia euismod, dolor dui feugiat arcu, at molestie nibh tellus vitae ex. Cras arcu tellus, bibendum a odio a, congue auctor erat. Donec facilisis sagittis nisl, sit amet volutpat ligula. Aliquam mattis ligula elit, ut hendrerit dolor efficitur quis. Nam tincidunt feugiat volutpat.
                      <br><br>
                      Nulla condimentum nec erat eu viverra. Praesent ut enim in nulla vulputate mattis lacinia nec erat. Vestibulum tempus auctor nibh convallis ultricies. Cras interdum arcu vitae erat pretium, id rhoncus elit posuere. Quisque erat orci, tristique vitae eleifend non, tempor ut orci. Aenean dapibus auctor lacinia. Suspendisse efficitur lectus sit amet quam fermentum scelerisque. Maecenas pretium risus a dolor aliquam, vitae placerat purus porta. Pellentesque sollicitudin libero eu magna auctor eleifend quis at nunc. Praesent consequat massa eu nulla vestibulum, ut semper est gravida.
                      <br><br>  
                      Duis placerat dapibus sem, ut consequat est convallis et. Praesent sodales euismod euismod. Nunc rhoncus nisi risus, vel tincidunt elit tincidunt id. Morbi nec condimentum purus. Vivamus eget diam id turpis laoreet feugiat nec a turpis. Suspendisse efficitur bibendum sapien in fringilla. Aenean tristique sed nunc ut molestie. Aenean blandit commodo ante. Aenean lorem nibh, iaculis non dignissim eget, semper sit amet diam. Proin tempor venenatis commodo. Proin pretium, lectus sed malesuada vestibulum, velit ex luctus mauris, eget suscipit leo dolor id leo. Fusce semper eleifend lacinia.
                      <br><br>
                      Quisque mattis vestibulum accumsan. Praesent pulvinar erat in massa commodo volutpat. Ut mi risus, porta nec fringilla vel, ullamcorper et lorem. Duis eros lectus, iaculis nec diam vel, tempus faucibus orci. Ut vel iaculis velit, a scelerisque massa. Integer posuere rhoncus laoreet. Donec sagittis sem at gravida ornare. Quisque eros nunc, maximus a feugiat elementum, egestas ac dolor. Duis at volutpat nunc. Nam porta vehicula neque, ac varius lacus congue vitae. Nulla varius malesuada fringilla. Donec nec elementum odio. Integer pharetra, nulla eu rutrum egestas, nibh velit porta neque, nec blandit enim lorem id nibh. Quisque rhoncus consectetur blandit. Sed eget ex accumsan tellus elementum molestie et eget lorem. Morbi a lacus condimentum, laoreet neque eget, accumsan mauris.
                     <br><br>
                      Curabitur iaculis pellentesque condimentum. Proin id pulvinar metus. Integer hendrerit dui vitae eros dictum maximus. Morbi feugiat, lectus sodales dignissim molestie, justo velit gravida purus, eu laoreet eros sem et libero. Sed nec leo vitae justo euismod maximus id id augue. Sed interdum vitae dolor sed suscipit. Praesent sodales libero ante, nec aliquet quam malesuada quis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut vel ullamcorper orci. Proin fringilla vulputate sagittis.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /END WRAPPER RECOVER PASSWORD-->


  <!-- START FOOTER -->
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/footer.php';
  // SCRIPTS DEL SITIO
  // require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/scripts_footer.php';

  ?><!-- END FOOTER -->



</body>
</html>
