<?php 

session_start();

require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");
$messages = new messages_system();

define("PAGE_CURRENT", "TERMS_AND_CONDITIONS");

$protocol = '';

if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") { 
  $protocol = "https://";
} else { 
  $protocol = "http://";
}

$domain = $protocol . $_SERVER['HTTP_HOST'];

?>
<!DOCTYPE html>
<!--
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
-->
<html>
<head>
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/head.php';
  ?>
  <title>Politica de Aviso y Privacidad - MI HV</title>
</head>
<body class="page-terms-and-conditions">

  <!-- START HEADER -->
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/header.php';
  ?><!-- END HEADER -->



  <!-- WRAPPER RECOVER PASSWORD-->
  <div class="section wrapper-terms-conditions">
    <div class="container">  
      <!-- CONTENEDOR MENSAJES  -->
      <div class="row wrapper-messages-system">
        <?php 
        if (isset($_SESSION["message_system"])) {
          $messeges = $messages->menssages_render($_SESSION["message_system"]);
          unset($_SESSION["message_system"]);
          echo $messeges;
        }
        ?>
      </div><!-- CONTENEDOR MENSAJES  --> 
      <div class="row">
        <div class="col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1">
          <div class="panel panel-default">
            <div class="panel-heading text-center">
              <h4 class="text-primary">POLÍTICA DE AVISO DE PRIVACIDAD</h4>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1">
                  <div class="form-group">
                    <br>
                    <p style="text-align: justify;">
                      <b>Code Connection Professional SAS (en calidad de propietario del sitio web www.mihv.com.co)</b>, para efecto de aclaraciones, de aquí en adelante llamado ‘Codection’, con domicilio en la ciudad de Bogotá D.C., Colombia; el sitio web <a href="www.mihv.com.co">www.mihv.com.co</a> se llamará ‘sitio web’; toda persona natural o empresa legalmente constituida en el territorio colombiano se llamará ‘usuario’. Como sabes Codection es una empresa perteneciente al régimen común debidamente legalizada en el territorio colombiano y es responsable sobre el tratamiento de los datos personales de todos los usuarios que se encuentren debidamente inscritos al sitio web.
                      <br><br>
                      Para efectos de proteger tus datos personales, Codection ha dispuesto de mecanismos de seguridad entre estos un protocolo de seguridad que hará que cada vez que visites el sitio web tenga un alto porcentaje de encriptación para las gestiones que hagas en el mismo. Te recordamos que ningún sistema es perfecto y no está exento de ataques de cualquier tipo de virus programados para ese fin o hackers, pero hacemos lo posible por hacer que el sitio web sea cada día más seguro y proteger tus datos de terceros indeseados.
                      <br><br>  
                      Tú como usuario, debido al artículo 8° de la ley 1581 del año 2012 y las demás leyes, normas y/o artículos que los complementen y/o sustituya, podrás: Saber, conocer, consultar, reclamar, rectificar, actualizar y los demás derechos que se indiquen en los artículos mencionados, todo esto sobre tus datos personales almacenados en el sitio web. Para hacer valer tus derechos te indicamos los medios en los cuales estamos disponibles para estar atentos a las gestiones que quieras realizar:
                      <br><br>
                      <ul style="list-style: none;">
                        <li>
                          <i class="fa fa-check-square-o" aria-hidden="true"></i> 
                          En el sitio web, cuando inicies sesión, en el menú de tu perfil.
                        </li>
                        <li>
                          <i class="fa fa-check-square-o" aria-hidden="true"></i> 
                          En el correo electrónico: <a href="mailto:info@mihv.com.co?Subject=Solicitud%20política%20de%20aviso%20de%20privacidad" target="_top">info@mihv.com.co</a>
                        </li>
                        <li>
                          <i class="fa fa-check-square-o" aria-hidden="true"></i> 
                          En nuestra oficina: Carrera 13 # 13 – 24 Oficina 725
                        </li>
                        <li>
                          <i class="fa fa-check-square-o" aria-hidden="true"></i> 
                          En nuestro teléfono de contacto: 316 5269362
                        </li>
                      </ul>
                      <br><br>
                      Tus datos personales se incluirán en una base de datos y serán usados para los siguientes fines:
                      <br><br>
                      <ol type="a">
                        <li>Mejorar nuestro servicio.</li>
                        <li>Proveer de manera eficiente todos nuestros productos y servicios.</li>
                        <li>
                            Mantenerte al tanto de los cambios y/o nuevos servicios 
                            que podrás usar para una mejor experiencia dentro del sitio web.
                        </li>
                        <li>Cumplir con las obligaciones hacia nuestros usuarios.</li>
                        <li>
                          En caso de haber actividades externas al sitio web, 
                          informarte de las mismas.
                        </li>
                        <li>
                          Informarte sobre las actualizaciones de los 
                          términos y condiciones para el uso del sitio web.
                        </li>
                        <li>
                          Tus datos serán usados dentro y/o fuera del territorio 
                          colombiano por terceros que sean responsables del 
                          tratamiento de datos para los siguientes fines: 
                          Logística, distribución, administración, marketing (en general), 
                          call o contact centers y/o demás servicios que requiera 
                          el responsable de datos con el objetivo del buen 
                          desarrollo del servicio que preste o prestará; 
                          aclarando que deberá cumplir con la política de
                          tratamiento y procesamiento de datos personales 
                          que da Codection (para tú seguridad).
                        </li>
                        <li>
                          Tus datos serán transmitidos y/o transferidos 
                          dentro o fuera del territorio colombiano
                          a las empresas vinculadas al tratamiento de datos personales responsables una vez que haya cumplido la política de tratamiento y procesamiento de datos personales que da Codection. Para los casos del exterior (fuera del territorio colombiano), al país que sea transmitido y/o transferido tus datos, deberá cumplir con los artículos, decretos, leyes y otras normas que exija dicho país.
                        </li>
                      </ol>
                      <br><br>
                      <b style="text-decoration: underline;">AVISO DE INTERÉS GENERAL:</b>
                      <br><br>
                      Si te quieres enterar sobre la política de tratamiento y procesamiento de datos, puedes ingresar a la url <a href="<?php echo $domain; ?>/tratamientoyprocesamientodedatos">https://www.mihv.com.co/tratamientoyprocesamientodedatos</a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /END WRAPPER RECOVER PASSWORD-->


  <!-- START FOOTER -->
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/footer.php';
  // SCRIPTS DEL SITIO
  // require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/scripts_footer.php';

  ?><!-- END FOOTER -->



</body>
</html>
