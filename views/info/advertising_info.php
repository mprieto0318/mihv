<?php 

session_start();

require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");
$messages = new messages_system();

define("PAGE_CURRENT", "ADVERTISING_INFO"); // llamarlo con: constant("PAGE_CURRENT")

?>
<!DOCTYPE html>
<!--
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
-->
<html>
<head>
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/head.php';
  ?>
  <title>Info Publicidad - MI HV</title>
</head>
<body class="page-advertising-info">

  <!-- START HEADER -->
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/header.php';
  ?><!-- END HEADER -->


  <!-- WRAPPER RECOVER PASSWORD-->
  <div class="section wrapper-advertising-info">
    <div class="container">  
      <!-- CONTENEDOR MENSAJES  -->
      <div class="row wrapper-messages-system">
        <?php 
        if (isset($_SESSION["message_system"])) {
          $messeges = $messages->menssages_render($_SESSION["message_system"]);
          unset($_SESSION["message_system"]);
          echo $messeges;
        }
        ?>
      </div><!-- CONTENEDOR MENSAJES  --> 
      <div class="row">
        <div class="col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1">
          <div class="panel panel-default">
            <div class="panel-heading text-center">
              <h4 class="text-primary">PAUTA CON NOSOTROS</h4>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1">
                  <div class="form-group">
                    <br>
                    <p style="text-align: justify;">
                      Para que puedas pautar con nosotros es un proceso muy sencillo, pero antes de decirte que debes hacer para pautar en nuestra web, te mostramos las opciones que manejamos y así puedas elegir la que más se acomode a tus necesidades.
                    </p>
                    <br><br>
                    <ul style="list-style: none; text-align: justify;">
                      <li>
                        <span class="glyphicon glyphicon-ok"> </span>
                        Scroll o deslizamiento: Será visible en el scroll de búsqueda de ofertas, es decir, se mostrará en todos los dispositivos que puedan reproducir este scroll de búsqueda de ofertas laborales, esto a nivel público incluyendo nuestros usuarios (es donde más se vería tu pauta ya que se puede ver desde un dispositivo móvil* en adelante).
                      </li>
                      <br>
                      <li>
                        <span class="glyphicon glyphicon-ok"> </span>
                        Panel derecho: Será visible en la sección de publicidad siempre que se cumpla la resolución mínima para que se muestre en la parte derecha tu pauta, normalmente en modo desktop, esto exclusivamente para todos los usuarios registrados en el sitio web.
                      </li>
                    </ul>
                    <br>
                    <p style="text-align: justify;">
                      Estas modalidades se pueden adquirir en conjunto o individualmente junto con otras opciones que tenemos para que tu pauta se difunda en todas partes (las que nosotros manejamos).
                      <br><br>
                      Nuestro modelo de cobro no aplica a los que se manejan hoy en día (incluyendo cobro por click), manejamos cobro de pautas por días, semanas, mes(es), etc.
                      <br><br><br>
                      <b><span style="text-decoration: underline;">PROCESO PARA PAUTAR:</span></b>
                      <br><br>
                      Es muy fácil, una vez hayas elegido la o las opciones para publicar tu pauta, sólo debes enviarnos un correo a <a href="mailto:pauta@mihv.com.co?Subject=Interesado%20en%20pautar" target="_top">pauta@mihv.com.co</a> y en la descripción nos envías tus datos de contacto los cuales serían:
                      <br><br>
                      <ul>
                        <li>
                          Nombre tuyo y el de tu empresa con el NIT ya sea empresa o cédula si eres independiente.
                        </li>
                        <li>
                          Teléfono móvil (si es en el exterior, antecede el indicativo de tu país),
                        </li>
                        <li>
                          Teléfono fijo (si es en el exterior, antecede el indicativo de tu país),
                        </li>
                        <li>
                          Segundo contacto (en caso que no podamos contactarte, si es muy urgente tu pauta)
                        </li>
                      </ul>
                      <br><br>
                      Una vez hayas enviado tu correo, prontamente te responderemos, en la respuesta te enviaremos una cotización con los datos que necesitaríamos para publicar tu pauta; si la apruebas, te diremos los pasos a seguir.
                      <br><br>
                      Pautar con nosotros es muy fácil y se hace en pocos pasos. Anímate, te esperemos.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> 
  <!-- /END WRAPPER RECOVER PASSWORD-->


  <!-- START FOOTER -->
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/footer.php';
  // SCRIPTS DEL SITIO
  // require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/scripts_footer.php';

  ?><!-- END FOOTER -->



</body>
</html>
