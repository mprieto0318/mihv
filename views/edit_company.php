<?php
session_start();
if (!isset($_SESSION['user']['id_ci'])) {
  header('Location: ../views/login.php');
}

define("PAGE_CURRENT", "EDIT_COMPANY"); // llamarlo con: constant("PAGE_CURRENT")

require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_profile_company.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");
  //print_r($_SESSION["user"]);

$controlUtilities = new controlUtilities();
$controlProfileCompany = new controlProfileCompany(); 
$messages = new messages_system();
?>
<!DOCTYPE html>
<!--
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
-->
<html>
<head>
  <title>Editar Perfil - MI HV</title>
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/head.php';
  ?>
</head>
<body class="page-edit-profile-company">

  <!-- START HEADER -->
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/header.php';
  ?><!-- END HEADER -->

  <div class="section">
    <div class="container wrapper-profile">
      <div class="row title-page">
        <div class="col-md-12 text-center">
          <h3 class="text-inverse">PERFIL</h3>
          <p class="text-inverse">Aqui puedes puedes editar la información de tu perfil!</p> 
        </div>
      </div>
      <!-- CONTENEDOR MENSAJES  -->
      <div class="row wrapper-messages-system">
        <?php 
        if (isset($_SESSION["message_system"])) {
          $messeges = $messages->menssages_render($_SESSION["message_system"]);
          unset($_SESSION["message_system"]);
          echo $messeges;
        }
        ?>
      </div><!-- CONTENEDOR MENSAJES  -->

    <div class="row">

      <!--  COLUMNA CONTENIDO  -->
      <div class="col-sm-9 col-md-9">
        <div class="row">

          <div class="col-sm-12 col-md-12 wrap-recharge">
            <?php
              $data_company = $controlProfileCompany->control_profile_company_get_full_data_company();
              $balances     = $controlProfileCompany->control_profile_company_get_balances();
            ?>
            <br>
            <div class="text-center">
              <p class="text-primary text-center lead">
                Saldo Actual 
                <b>
                  $ <?php echo number_format($balances['available'], 0, ",", "."); ?> 
                </b> 
              </p> 
              <p class="lead text-primary">
                <a class="link-buy" href="" data-toggle="modal" data-target="#modal_steps_reload_balance"> 
                  <span class="glyphicon glyphicon-credit-card"> </span> 
                  Recarga tu saldo aqui
                </a>
              </p>
            </div>
          </div>

          <div class="col-sm-12 col-md-12 white-bg">
          <br>
            <form method="POST" id="form-edit-company" action="/controls/control_profile_company_operations.php" class="form-edit-profile" enctype="multipart/form-data">
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <label class="title-field">Nombres:</label>
                    <div>
                      <input type="text" class="form-control" name="itName" value="<?php echo $data_company['name_ci'] ?>" required placeholder="Mi Empresa">
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-6">

                    <?php 
                    if(!empty($data_company['image_ci'])) {
                      $logo = '<span class="glyphicon glyphicon-eye-open"> </span> [' . $data_company['image_ci'] . ']';
                      $flag_class_show_logo = 'text-show-logo';
                    }else {
                      $logo = 'Carga tu logo';
                      $flag_class_show_logo = '';
                    }
                    ?>

                    <label class="title-field">Logo Actual: </label> 
                    <span class="text-logo-company <?php echo $flag_class_show_logo; ?>"> <?php echo $logo; ?></span>
                    <div>
                      <img alt="<?php echo $data_company['image_ci']; ?>" class="logo-company" src="../src/img/company/logo/<?php echo $data_company['image_ci']; ?>">
                      <input type="file" name="fLogo" accept="image/*" class="filestyle f-logo" data-buttonText="Buscar Logo" data-size="sm" >
                      <input type="hidden" name="logo_current" value="<?php echo $data_company['image_ci']; ?>">
                    </div>
                    <br> 
                  </div>
                </div>                
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <label class="title-field">Nit:</label>
                    <div>
                      <input type="text" disabled class="form-control" name="itNit" value="<?php echo $data_company['nit_ci'] ?>" required readonly>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-6">
                    <label class="title-field"># Actividad Economica:</label>
                    <div>
                      <input type="text" class="form-control" name="itEconomicActivity" value="<?php echo $data_company['economic_activity_ci'] ?>" required placeholder="Call center, Contadores, etc...">
                    </div>
                    <br> 
                  </div>
                </div>                
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <label class="title-field"># Fijo:</label>
                    <div>
                      <input type="text" class="form-control" name="itPhone" value="<?php echo $data_company['phone_ci'] ?>" required placeholder="De tu compañía y si tienes extensión">
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-6">
                    <label class="title-field"># Movil:</label>
                    <div>
                      <input type="text" class="form-control" name="itMovil" value="<?php echo $data_company['movil_ci'] ?>" required placeholder="Indica los 10 digitos">
                    </div>
                    <br> 
                  </div>
                </div>     
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <label class="title-field">Ciudad:</label>
                    <div>
                      <select id="selCity" name="selCity" data-size="8" class="selectpicker form-control" data-live-search="true">
                        <?php
                        // Cargando lista de ciudades
                        $select_city = $controlUtilities->_control_utilities_get_citys_select($data_company['city_ci']);
                        echo $select_city;
                        ?>
                      </select>
                      <input type="hidden" id="selCityValidate" name="selCityValidate" value="all">
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-6">
                    <label class="title-field">Dirección:</label>
                    <div>
                      <input type="text" class="form-control" name="itAddress" value="<?php echo $data_company['address_ci'] ?>" required placeholder="Indica la dirección">
                    </div>
                    <br> 
                  </div>
                </div>     
              </div>        

              <div class="form-group">
                <div class="row">
                  <div class="col-sm-12 col-md-12">
                    <label class="title-field">Descripción:</label>
                    <div>
                      <textarea class="form-control" rows="5" required name="itaDescription" placeholder="Somos una empresa dedicada a ...., con x años de experiencia, con clientes como: cliente 1, cliente 2. estamos ubicados en ..."><?php echo $data_company['description_ci']; ?></textarea>
                    </div>
                  </div>
                </div>
              </div>

              <fieldset class="border-fieldset">
                <legend class="text-center">
                  <span class="glyphicon glyphicon-lock"></span> Configuración de acceso
                </legend>

                <div class="form-group">
                  <div class="row">
                    
                    <div class="col-sm-4 col-md-4">
                      <label class="title-field">Email:</label>
                      <div>
                        <input type="text" class="form-control" name="itEmail" value="<?php echo $data_company['email_ci'] ?>" required>
                      </div>
                    </div>

                    <div class="col-sm-4 col-md-4">
                      <label class="title-field">Clave Actual:</label>
                      <div>
                        <input type="password" class="form-control" name="itCurrentPassword" placeholder="Mi clave actual">
                      </div>
                    </div>

                    <div class="col-sm-4 col-md-4">
                      <label class="title-field">Nueva Clave:</label>
                      <div>
                        <input type="password" class="form-control" name="itPassword" placeholder="Mi nueva clave">
                      </div>
                    </div>

                  </div>     
                </div>
              </fieldset>

              <hr>
              <div class="form-group">
                <div class="input-group-addon">
                  <?php
                    $token_search = $controlUtilities->control_utilities_create_token_pages('profile_company');
                    echo '<input type="hidden" name="tokenPage" value="' . $token_search . '" id="tokenPage">';
                  ?>
                  <input type="hidden" name="profileCompanyToken" value="UPDATE_DATA_COMPANY">
                  <input type="hidden" name="cl4w0rd" value="<?php echo $data_company['password_ci'] ?>">
                  <input type="button" name="delete-account" value="Eliminar Cuenta" class="btn btn-danger pull-left delete-account">
                  <input type="submit" name="submitFormUpdateCompany" value="Guardar" class="btn btn-success pull-right">
                  <button id="show-modal-confirm-delete-user" type="button" data-toggle="modal" data-target="#modal-confirm-delete-user" style="display: none">
                </div>
              </div>
            </form>   
          </div>

        </div>
     
      </div><!--  /COLUMNA CONTENIDO  -->

      <!--  WRAPPER MENU LATERAL  -->
      <?php
      require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/advertising/menu-side.php';
      ?><!--  /WRAPPER MENU LATERAL  -->

    </div> <!-- /row -->
  </div><!-- /container -->
  </div><!-- /section -->
  
  <?php
    // Footer
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/footer.php';

    // MODAL CONFIRMAR ELIMINAR CUENTA
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/modals/modal_delete_user.php';
    // MODAL PASOS RECARGAR SALDO
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/modals/modal_steps_reload_balance.php';
    // SCRIPTS DEL SITIO
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/scripts_footer.php';
  ?>

  <!-- valida formulario editar usuario compania -->
  <script src="../js/mihv/validate_edit_user_company.js" type="text/javascript"></script>
</body>
</html>