<?php 
session_start();

if (isset($_SESSION['user']['id_pi']) || isset($_SESSION['user']['id_ci']) || isset($_SESSION['user']['id_su'])) {
  header('Location: ../index.php');
}

define("PAGE_CURRENT", "LOGIN_REGISTER"); // llamarlo con: constant("PAGE_CURRENT")

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/model_load_oferts.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");
$modelLoadOferts = new modelLoadOferts();
$controlUtilities = new controlUtilities();
$messages = new messages_system();
 
?>

<!DOCTYPE html>
<!--
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
-->
<html>
<head>
  <title>Inicio de Sesión - MI HV</title>
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/head.php';
  ?>
</head>
<body class="page-login-register">
  
  <!-- START HEADER -->
  <?php
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/header.php';
  ?><!-- END HEADER -->

  <div class="section wrapper-section wrapper-page-login">
    <div class="container">
      
      <!-- START MESSAGES -->
      <div class="row wrapper-messages-system">
        <?php 
        if (isset($_SESSION["message_system"])) {
          $messeges = $messages->menssages_render($_SESSION["message_system"]);
          unset($_SESSION["message_system"]);
          echo $messeges;
        }
        ?>
      </div><!-- END MESSAGES -->

      <div class="row title-page">
        <div class="col-md-12 text-center">
          <h3 class="text-inverse">ENTRAR A MI HV</h3>
          <p class="text-inverse">Aqui puedes iniciar sesión o resgistrarte</p> 
        </div>
      </div>
      <div class="row">
        
        <div class="col-xs-12 col-sm-6 col-md-6 wrapper-login-person">
          <div class="row">
            <div class="col-xs-4 col-sm-12">
              <img alt="login persona" src="/src/img/mihv/site/login-person.png"
          class="loginPerson img-login center-block img-responsive img-rounded" data-toggle="modal" data-target="#loginModal">
            </div>
            <div class="col-xs-8 col-sm-12">
              <h5 class="loginPerson text-center" data-toggle="modal" data-target="#loginModal">PERSONA</h5>
              <hr>
              <p class="text-center" data-toggle="modal" data-target="#registerModal">
                <span class="glyphicon glyphicon-hand-right"> </span> Regístrate como persona</p>
                <br>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 wrapper-login-company">
          <div class="row">
            <div class="col-xs-4 col-sm-12">
              <img alt="login empresa" src="/src/img/mihv/site/login-company.png"
          class="loginCompany img-login center-block img-responsive img-rounded" data-toggle="modal" data-target="#loginModal">
            </div>
            <div class="col-xs-8 col-sm-12">
              <h5 class="loginCompany text-center" data-toggle="modal" data-target="#loginModal">EMPRESA</h5>
              <hr>
              <p class="text-center" data-toggle="modal" data-target="#registerCompanyModal">
                <span class="glyphicon glyphicon-hand-right"> </span> Regístrate como empresa</p>
                <br>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  <?php
  // Footer
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/footer.php';
  // Modal login - Registro
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/modals/modal_login_register.php';
  // Modal Contenido Normal - Terminos y Condiciones
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/modals/modal_content_normal.php';
  // SCRIPTS DEL SITIO
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/scripts_footer.php';
  ?>

  <!-- valida formulario crear usuario persona -->
  <script src="../js/mihv/validate_create_user_person.js" type="text/javascript"></script>
  <!-- valida formulario crear usuario empresa -->
  <script src="../js/mihv/validate_create_user_company.js" type="text/javascript"></script>
  
</body>
</html>