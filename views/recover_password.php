<?php 

error_reporting(E_ALL);
ini_set("display_errors", 1);

if (!isset($_GET['token_new_password']) && !isset($_GET['type']) && !isset($_GET['1dus3r'])) {
   header('Location: /index.php');
}
  session_start();
  require_once($_SERVER['DOCUMENT_ROOT'] . "/models/models_utilities.cls.php");
  require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");
  require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");

  $modelUtilities = new modelUtilities();
  $functions_sql = new functions_sql();
  $messages = new messages_system();
  $controlUtilities = new controlUtilities();

  $values = $functions_sql->functions_sql_clear_dates($_GET);
  $response = $modelUtilities->models_utilities_validate_token_recover_password($values['1dus3r'], $values['type'], $values['token_new_password']);

  if ($response == FALSE) {
    header('Location: /index.php');
  }
  

define("PAGE_CURRENT", "RECOVER_PASSWORD"); // llamarlo con: constant("PAGE_CURRENT")

?>
<!DOCTYPE html>
<!--
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
-->
<html>
<head>
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/head.php';
  ?>
  <title>Cambiar Clave - MI HV</title>
</head>
<body class="page-recover-password">

  <!-- START HEADER -->
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/header.php';
  ?><!-- END HEADER -->



  <!-- WRAPPER RECOVER PASSWORD-->
  <div class="section wrapper-recover-password">
    <div class="container">   
      <div class="row">
      <div class="col-sm-6 col-md-4 col-md-offset-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <strong>Aquí vas a cambiar tu contraseña</strong>
          </div>
          <div class="panel-body">
            <form id="form-recover-password" role="form" action="/controls/control_recover_password.php" method="POST" autocomplete="off">
              <fieldset>
                <div class="row">
                  <div class="col-sm-12 col-md-10  col-md-offset-1 ">
                    <div class="form-group">
                      <label class="text-left control-label">Escribe tu nueva contraseña:</label>
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="glyphicon glyphicon-lock"></i>
                        </span> 
                        <input class="form-control" id="newPassword" name="newPassword" type="password" autofocus autocomplete="off">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="text-left control-label">Confirma tu nueva contraseña:</label>
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="glyphicon glyphicon-lock"></i>
                        </span>
                        <input class="form-control" id="repeatNewPassword" name="repeatNewPassword" type="password" autocomplete="off">
                      </div>
                    </div>
                    <div class="form-group">
                      <?php
                        $token_search = $controlUtilities->control_utilities_create_token_pages('recover_password');
                        echo '<input type="hidden" name="tokenPage" value="' . $token_search . '" id="tokenPage">';
                        echo '<input type="hidden" name="type" value="' . $values['type'] . '" id="type">';
                        echo '<input type="hidden" name="1dus3r" value="' . $values['1dus3r'] . '" id="type">';                        
                      ?>
                      <input type="submit" class="btn btn-primary btn-block" value="Cambiar Contraseña">
                    </div>
                  </div>
                </div>
              </fieldset>
            </form>
          </div>
         
        </div>
      </div>
    </div>
    </div>
    </div>
  </div> <!-- /END WRAPPER RECOVER PASSWORD-->


  
  <?php
  // Footer
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/footer.php';
  // SCRIPTS DEL SITIO
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/scripts_footer.php';
  ?>
  <!-- valida formulario de recuperar contraseña -->
  <script src="../js/mihv/validate_recover_password.js" type="text/javascript"></script>

</body>
</html>
