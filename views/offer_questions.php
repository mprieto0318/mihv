<?php
  session_start();
  
  if (!isset($_SESSION['user']['id_ci'])) {
    header('Location: ../views/login.php');
  }
    
  define("PAGE_CURRENT", "OFFER_QUESTIONS"); // llamarlo con: constant("PAGE_CURRENT")

  require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");
  require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_profile_company.cls.php");
  require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");

  $controlUtilities = new controlUtilities();
  $controlProfileCompany = new controlProfileCompany(); 
  $messages = new messages_system();

?>
<!DOCTYPE html>
<!--
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
-->
<html>
<head>
  <title>Facturación - MI HV</title>
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/head.php';
  ?>
</head>
<body class="page-billing-company">
  
  <!-- START HEADER -->
  <?php
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/header.php';
  ?><!-- END HEADER -->

  <!-- START SECTION CONTENT PROFILE -->
  <div class="section wrapper-profile">
  <div class="container">
     <div class="row title-page">
        <div class="col-md-12 text-center">
          <h3 class="text-inverse">FACTURACIÓN</h3>
          <p class="text-inverse">Aqui encuentras el listado de tus facturas!</p> 
        </div>
      </div>

      <!-- CONTENEDOR MENSAJES  -->
      <div class="row wrapper-messages-system">
        <?php 
        if (isset($_SESSION["message_system"])) {
          $messeges = $messages->menssages_render($_SESSION["message_system"]);
          unset($_SESSION["message_system"]);
          echo $messeges;
        }
        ?>
      </div><!-- CONTENEDOR MENSAJES  -->
    <div class="row">
        <!--  COLUMNA CONTENIDO  -->
        <div class="col-sm-9 col-md-9 well">
          
          <div class="panel panel-default">
            <div class="panel-heading">
              <strong>Ingresa las preguntas que serviran como filtro al aplicar a la oferta</strong>
            </div>
            <div class="panel-body">
              <form id="form-recharge-balance" role="form" action="/controls/control_recharge_balance.php" method="POST" autocomplete="off">
                
                

                <div class="row wrap-question wrap-question-1">
                  <div class="col-md-12">
                    
                    <div class="row">
                      <div class="col-md-12">
                        <h3 class="question-number-1"><span>1</span></h3>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col-sm-3 text-center">
                        <label class="text-primary">Pregunta:</label>
                      </div>
                      <div class="col-sm-9">
                        <textarea class="form-control" rows="3" required name="question-1" placeholder="esta es mi pregunta selecciona la respuesta correcta"></textarea>
                        <br>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-3 text-center">
                        <label class="text-primary">Opción A:</label>
                      </div>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="itProfession" id="itProfession" placeholder="Ingeniero de Software">
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col-sm-3 text-center">
                        <label class="text-primary">Opción B:</label>
                      </div>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="itProfession" id="itProfession" placeholder="Medico">
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-3 text-center">
                        <label class="text-primary">Opción C:</label>
                      </div>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="itProfession" id="itProfession" placeholder="Periodista">
                        <br>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-3 text-center col-sm-offset-3">
                        <label class="text-primary">Respuesta:</label>
                      </div>
                      <div class="col-sm-6">
                        <input style="width: 37px;" type="text" class="form-control" name="itProfession" id="itProfession" placeholder="B">
                      </div>
                    </div>

                  </div>
                </div>

                <hr>
                  
                  
                  <div class="form-group">
                    <div class="input-group-addon">
                      <input type="hidden" name="profileCompanyToken" value="createOfert">
                      
                      <?php
                        $token_search = $controlUtilities->control_utilities_create_token_pages('profile_company');
                        echo '<input type="hidden" name="tokenPage" value="' . $token_search . '" id="tokenPage">';
                      ?>
                      <input type="submit" name="add-question" value="Nueva Pregunta" class="btn btn-success pull-left">
                      <input type="submit" name="submitFormCreateOfert" id="btn-create-ofert" value="Terminar Preguntas" class="btn btn-primary pull-right">

                      <button id="show-modal-create-ofert-urgent" type="button" class="btn btn-success pull-center" data-toggle="modal" data-target="#modal-create-ofert-urgent" style="display: none">

                    </div>
                  </div>
                  
                
              </form>
            </div>
           
          </div>

        </div><!--  /COLUMNA CONTENIDO  -->

        <!--  WRAPPER MENU LATERAL  -->
        <?php
        require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/advertising/menu-side.php';
        ?><!--  /WRAPPER MENU LATERAL  -->


    </div> <!-- /row -->
  </div><!-- /container -->
  </div><!-- END SECTION CONTENT PROFILE -->

  <?php
    // Footer
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/footer.php';
    // SCRIPTS DEL SITIO
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/scripts_footer.php';
  ?>

  <script src="../js/bootstrap/bootstrap-table.js" type="text/javascript"></script>
  <script src="../js/bootstrap/bootstrap-table-es-MX.js" type="text/javascript"></script>
  <link href="../css/bootstrap/bootstrap-table.css" rel="stylesheet" type="text/css"/>
</body>
</html>