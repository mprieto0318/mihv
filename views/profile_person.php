<?php
session_start();

if (!isset($_SESSION['user']['id_pi'])) {
  header('Location: ../views/login.php');
}

define("PAGE_CURRENT", "PROFILE_PERSON"); // llamarlo con: constant("PAGE_CURRENT")

require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_profile_person.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");

//print_r($_SESSION["user"]);

$controlUtilities = new controlUtilities();
$controlProfilePerson = new controlProfilePerson(); 
$messages = new messages_system();
?>
<!DOCTYPE html>
<!--
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
-->
<html>
<head>
  <title>Panel de Control - MI HV</title>
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/head.php';
  ?>
  <link href="../css/mihv/profile.css" rel="stylesheet" type="text/css"/>
</head>
<body class="page-profile page-profile-person">
  
  <!-- START HEADER -->
  <?php
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/header.php';
  ?><!-- END HEADER -->

  <div class="container wrapper-profile">
    <!-- TITULO DE LA PAGINA -->
      <div class="row title-page">
        <div class="col-md-12 text-center">
          <h3 class="text-inverse">PANEL DE CONTROL</h3>
          <p class="text-inverse">Aqui puedes gestionar las ofertas a las que haz aplicado y editar tu perfil!</p> 
        </div>
      </div>
      <!-- CONTENEDOR MENSAJES  -->
      <div class="row wrapper-messages-system">
        <?php 
        if (isset($_SESSION["message_system"])) {
          $messeges = $messages->menssages_render($_SESSION["message_system"]);
          unset($_SESSION["message_system"]);
          echo $messeges;
        }
        ?>
      </div><!-- CONTENEDOR MENSAJES  -->

    <div class="row">

      <!--  COLUMNA CONTENIDO  -->
      <div class="col-sm-9 col-md-9">

      <!--  CONTENEDOR HEADER  -->            
        <div class="card hovercard">
          <div class="card-background">
            <img alt="perfil" class="card-bkimg" alt="" src="/src/img/mihv/site/header-profile.jpg">
          </div>
          <div class="useravatar">
            <img alt="logo default persona perfil" src="/src/img/mihv/site/logo-person-default.jpg">
          </div>
          <div class="card-info"> 
            <span class="card-title"><?php echo $_SESSION['user']['name_pi']; ?></span>
          </div>
        </div><!--  /CONTENEDOR HEADER  -->            

        <!--  CONTENEDOR BOTONES TABS  -->
        <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
          <div class="btn-group" role="group">
            <button type="button" id="stars" class="btn btn-primary" href="#tab1" data-toggle="tab">
              <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
              <div class="hidden-xs">OFERTAS</div>
            </button>
          </div>
          <div class="btn-group" role="group">
            <button type="button" id="favorites" class="btn btn-default" href="#tab2" data-toggle="tab">
              <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
              <div class="hidden-xs">PERFIL</div>
            </button>
          </div>
        </div><!--  /CONTENEDOR BOTONES TABS  -->

        <!--  CONTENEDOR TABS  -->
        <div class="well">
          <div class="tab-content">

            <!--  TAB 1  -->
            <div class="tab-pane fade in active" id="tab1">

              <?php
              $rows_query = $controlProfilePerson->control_profile_person_get_all_oferts_apply_by_user();
              $rows_table = $controlProfilePerson->control_profile_person_get_all_oferts_apply_by_user_rows_table($rows_query);
              ?>
              
              <div id="result-ofert-apply" class="table-responsive">

                <!--<table data-url="inicio.php" data-toggle="table"   data-pagination="true" data-search="true"  data-height="450"  data-show-refresh="true" data-show-toggle="true" >-->
                <table class="table table-hover table-striped" data-url="profile_person.php" data-toggle="table"   data-pagination="true" data-search="true"  data-height="450"  data-show-toggle="true" ><!--  se quito data-search="true" para el boton actualizar la tabla  -->
                  <thead>
                    <tr>
                      <!-- <th data-field="id" data-align="left" data-sortable="true">ID</th> -->
                      <th data-align="center">Nombre</th>
                      <th data-align="center">Empresa</th>
                      <th data-align="center" data-sortable="true">Aplicado</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php echo $rows_table; ?>
                  </tbody>  
                </table>

              </div>

            </div><!-- /TAB 1 -->

            <!-- TAB 2 -->
            <div class="tab-pane fade in" id="tab2">
            <!-- <div class="tab-pane fade in active" id="tab1"> -->
              
                <?php
                  $data_person = $controlProfilePerson->control_profile_person_get_full_data_person();
                ?>

                <div class="text-center title-edit-profile">
                  <br> 
                  <b><span class="glyphicon glyphicon-edit"></span>  EDITAR PERFIL</b>
                  <p>Recuerda verificar la informacion antes de guardar, que no se te pase la ortografía</p>
                </div>
                <hr>
                <form id="form-edit-user-person" method="POST" action="/controls/control_profile_person_operations.php" class="form-edit-profile">
                  
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-5 col-md-5">
                        <label  class="title-field">Nombres:</label>
                        <div>
                          <input type="text" class="form-control" name="itName" value="<?php echo $data_person['name_pi'] ?>" required>
                        </div>
                      </div>
                      <div class="col-sm-4 col-md-4">
                        <label  class="title-field">Apellidos:</label>
                        <div>
                          <input type="text" class="form-control" name="itLastName" value="<?php echo $data_person['last_name_pi'] ?>" required>
                        </div>
                      </div>
                      <div class="col-sm-3 col-md-3">
                        <label  class="title-field">Edad:</label>
                        <div>
                          <input type="number" class="form-control" value="<?php echo $data_person['age_pi'] ?>" name="age" id="age" min="16" max="70" value="16" required>
                        </div>
                      </div>
                    </div>                
                  </div>


                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-5 col-md-5">
                        <label  class="title-field"># Telefono:</label>
                        <div>
                          <input type="text" class="form-control" name="itPhone" value="<?php echo $data_person['phone_pi'] ?>">
                        </div>
                      </div>
                      <div class="col-sm-4 col-md-4">
                        <label  class="title-field"># Celular:</label>
                        <div>
                          <input type="text" class="form-control" name="itMovil" value="<?php echo $data_person['movil_pi'] ?>" required>
                        </div>
                      </div>

                      <div class="col-sm-3 col-md-3">
                        <label  class="title-field">Años de Experiencia:</label>
                        <div>
                          <input type="number" class="form-control" name="itExperience" min="0" max="65" value="<?php echo $data_person['experience_pi'] ?>">
                        </div>
                      </div>

                    </div>                
                  </div>

                  <div class="form-group">
                    <div class="row">

                      <div class="col-sm-5 col-md-5">
                        <label  class="title-field">Profesión</label>
                        <div>
                          <input type="text" class="form-control" name="itProfession" value="<?php echo $data_person['profession_pi'] ?>">
                        </div>
                      </div>
                      
                      <div class="col-sm-4 col-md-4">
                        <label  class="title-field">Ciudad:</label>
                        <div>
                          <select id="selCity" name="selCity" data-size="8" class="selectpicker form-control" data-live-search="true">
                            <?php
                            // Cargando lista de ciudades
                            $select_city = $controlUtilities->_control_utilities_get_citys_select($data_person['city_pi']);
                            echo $select_city;
                            ?>
                          </select>
                          <input type="hidden" id="selCityValidate" name="selCityValidate" value="all">
                        </div>
                      </div>

                      <div class="col-sm-3 col-md-3">
                        <label  class="title-field">Dirección:</label>
                        <div>
                          <input type="text" class="form-control" name="itAddress" value="<?php echo $data_person['address_pi'] ?>" required>
                        </div>
                      </div>

                    </div>                
                  </div>
        
                  <!-- </div> -->

                  <div class="form-group">
                    <label  class="title-field">Certificaciones / Conocimientos / Habilidades</label>
                    <p>"Separar por comas (,)"</p>
                    <div>
                       <textarea class="form-control" rows="5" required name="itaKnowledge" id="itaKnowledge" placeholder="excel, inventarios, base de datos, php, ingles, metodologias de trabajo, dj crosover, ensaladas italianas, escritor de musica, etc..."><?php echo $data_person['knowledge_pi'] ?></textarea>
                    </div> 
                  </div>

                  <div class="form-group">
                    <label  class="title-field">Estudios</label>
                    <p>"Si no tienes dejalo vacío, separar por comas (,)"</p>
                    <div>
                       <textarea class="form-control" rows="5" name="itaStudies" placeholder="estos son mis estudios"><?php echo $data_person['studies_pi'] ?></textarea>
                    </div>
                    <br> 
                  </div>

                   <fieldset class="border-fieldset">
                    <legend class="text-center">
                      <span class="glyphicon glyphicon-lock"></span> Configuración de acceso
                    </legend>

                    <div class="form-group">
                      <div class="row">
                        <div class="col-sm-4 col-md-4">
                          <label  class="title-field">Email:</label>
                          <div>
                            <input type="text" class="form-control" name="itEmail" value="<?php echo $data_person['email_pi'] ?>" required>
                          </div>
                        </div>
                        <div class="col-sm-4 col-md-4">
                          <label  class="title-field">Clave Actual:</label>
                          <div>
                            <input type="password" class="form-control" id="itCurrentPassword" name="itCurrentPassword" placeholder="Mi clave actual">
                          </div>
                        </div>
                        <div class="col-sm-4 col-md-4">
                          <label  class="title-field">Nueva Clave:</label>
                          <div>
                            <input type="password" class="form-control" name="itNewPassword" placeholder="Mi nueva clave">
                          </div>
                        </div>
                      </div>     
                    </div>
                  </fieldset>


                  

                  <hr>
                  <div class="form-group">
                    <div class="input-group-addon">
                      <input type="hidden" name="profilePersonToken" value="UPDATE_DATA_PERSON">
                      <input type="hidden" name="cl4w0rd" value="<?php echo $data_person['password_pi'] ?>">
                      <?php
                        $token_search = $controlUtilities->control_utilities_create_token_pages('profile_person');
                        echo '<input type="hidden" name="tokenPage" value="' . $token_search . '" id="tokenPage">';
                      ?>
                      <input type="button" name="delete-account" value="Eliminar Cuenta" class="btn btn-danger pull-left delete-account">
                      <input type="submit" name="submitFormUpdatePerson" value="Guardar" class="btn btn-success pull-right">

                      <button id="show-modal-confirm-delete-user" type="button" data-toggle="modal" data-target="#modal-confirm-delete-user" style="display: none">
                    </div>
                  </div>
                </form>

            </div><!-- /TAB 2 -->

          </div>
        </div><!--  /CONTENEDOR TABS  -->

      </div><!--  /COLUMNA CONTENIDO  -->

      <!--  WRAPPER MENU LATERAL  -->
      <?php
      require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/advertising/menu-side.php';
      ?><!--  /WRAPPER MENU LATERAL  -->


    </div> <!-- /row -->
  </div><!-- /container -->

  <?php
    // Footer
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/footer.php';
    // MODAL QUE CONTIENE TODA LA INFORMACION DE LA OFERTA SELECCIONADA
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/modals/modal_ofert_selected_person.php';
    // SCRIPTS DEL SITIO
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/scripts_footer.php';
    // MODAL CONFIRMAR ELIMINAR CUENTA
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/modals/modal_delete_user.php';

  ?>

  <!-- valida formulario editar usuario persona -->
  <script src="../js/mihv/validate_edit_user_person.js" type="text/javascript"></script>
  <!-- Scripts de la tabla -->
  <script src="../js/bootstrap/bootstrap-table.js" type="text/javascript"></script>
  <script src="../js/bootstrap/bootstrap-table-es-MX.js" type="text/javascript"></script>
  <link href="../css/bootstrap/bootstrap-table.css" rel="stylesheet" type="text/css"/>
</body>
</html>