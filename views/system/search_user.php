<?php 

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
if (!isset($_SESSION['user']['id_su'])) {
  header('Location: /index.php');
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");

$messages = new messages_system();
$controlUtilities = new controlUtilities();

define("PAGE_CURRENT", "SEARCH_COMPANY");

?>
<!DOCTYPE html>
<!--
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
-->
<html>
<head>
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/head.php';
  ?>
  <title>Buscar Usuario</title>
</head>
<body class="page-search-company">

  <!-- START HEADER -->
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/header_admin.php';
  ?><!-- END HEADER -->

  <!-- WRAPPER RECOVER PASSWORD-->
  <div class="section wrp-page wrp-search-company">
    <div class="container">  
      <!-- CONTENEDOR MENSAJES  -->
      <div class="row wrapper-messages-system">
        <?php 
        if (isset($_SESSION["message_system"])) {
          $messeges = $messages->menssages_render($_SESSION["message_system"]);
          unset($_SESSION["message_system"]);
          echo $messeges;
        }
        ?>
      </div><!-- CONTENEDOR MENSAJES  --> 
      <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
          <div class="panel panel-default">
            <div class="panel-heading text-center">
              <h4 class="text-primary">Buscar Usuario</h4>
            </div>
            <div class="panel-body">
            <?php

              $name_form = 'form';
              if (isset($_GET['rol']) && $_GET['rol'] == 'company') {
                $name_form = 'form_register_company';
              }
              ?>
              
              <form id="form-recharge-balance" name="<?php echo $name_form; ?>" action="/controls/system/control_search.php" method="POST" autocomplete="off">
                  <div class="row">
                    <div class="col-sm-12 col-md-10  col-md-offset-1">
                      <?php

                        if (isset($_GET['rol']) && $_GET['rol'] == 'company') {
                          ?>
                            <div class="form-group">
                              <label class="text-left control-label">Digita el NIT</label>
                              <div class="input-group">
                                <span class="input-group-addon">
                                  <i class="fa fa-building-o" aria-hidden="true"></i>
                                </span> 
                                <input class="form-control validate-nit validate-only-number" name="itNit" id="itNit" type="text" autofocus autocomplete="off" required>
                                <input type="text" class="form-control result-dv text-center" name="dv" disabled readonly placeholder="DV">
                                <input type="hidden" class="result-dv" name="dv_hidden">
                                <p class="help-block text-danger error-nit" style="display: none">
                                  <span class="glyphicon glyphicon-info-sign"></span> 
                                  El valor digitado no es un numero valido
                                </p>
                              </div>
                                
                            </div>
                          <?php
                        }else if (isset($_GET['rol']) && $_GET['rol'] == 'person') {
                          ?>
                          <div class="form-group">
                            <label class="text-left control-label">Digita el Email</label>
                            <div class="input-group">
                              <span class="input-group-addon">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                              </span> 
                              <input class="form-control" name="email" type="text" autocomplete="off">
                            </div>
                          </div>
                          <?php
                        }
                      ?>
                      
                                            
                      <div class="form-group">
                        <?php
                          $rol_token = (isset($_GET['rol'])) ? $_GET['rol'] : 'undefined';
                          $action_token = (isset($_GET['action'])) ? $_GET['action'] : 'undefined';
                          $back = $_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING'];
                          $token = $controlUtilities->control_utilities_create_token_pages('search_company');
                          echo '<input type="hidden" name="tokenPage" value="' . $token . '" id="tokenPage">';
                          echo '<input type="hidden" name="tokenRol" value="' . $rol_token . '">';
                          echo '<input type="hidden" name="tokenAction" value="' . $action_token . '">';
                          echo '<input type="hidden" name="tokenBack" value="' . $back . '">';
                        ?>

                        <input type="submit" class="btn btn-primary btn-block" value="Buscar">
                      </div>
                    </div>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> <!-- /END WRAPPER RECOVER PASSWORD-->
  <?php
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/scripts_footer.php';
  ?>
  <!-- valida formulario crear usuario empresa -->
  <script src="/js/mihv/validate_create_user_company.js" type="text/javascript"></script>
</body>
</html>
