<?php 

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
if (!isset($_SESSION['user']['id_su'])) {
  header('Location: /index.php');
}

if (!isset($_GET['tokenPage']) || !in_array($_GET['tokenPage'], $_SESSION['token']['pages'])) {
  header('Location: /index.php');
}

if (!isset($_GET['id_ci'], $_GET['name_ci'], $_GET['nit_ci'], $_GET['email_ci'])) {
  header('Location: /index.php'); 
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");

$messages = new messages_system();
$controlUtilities = new controlUtilities();

define("PAGE_CURRENT", "RECHARGE_BALANCE"); // llamarlo con: constant("PAGE_CURRENT")

?>
<!DOCTYPE html>
<!--
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
-->
<html>
<head>
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/head.php';
  ?>
  <title>Recargar Saldo</title>
</head>
<body class="page-recharge-balance">

  <!-- START HEADER -->
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/header_admin.php';
  ?><!-- END HEADER -->

  <!-- WRAPPER RECOVER PASSWORD-->
  <div class="section wrp-page wrapper-recharge-balance">
    <div class="container">  
      <!-- CONTENEDOR MENSAJES  -->
      <div class="row wrapper-messages-system">
        <?php 
        if (isset($_SESSION["message_system"])) {
          $messeges = $messages->menssages_render($_SESSION["message_system"]);
          unset($_SESSION["message_system"]);
          echo $messeges;
        }
        ?>
      </div><!-- CONTENEDOR MENSAJES  --> 
      <div class="row">
        <div class="col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1">
          <div class="panel panel-default">
            <div class="panel-heading text-center">
              <h4 class="text-primary">Recargar Saldo</h4>
            </div>

            <div class="panel-body">
              <div class="row">
                <div class="col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1">
                  <div class="row">
                    <div class="col-md-12 text-center">
                      <h4 class="text-primary">Verifica la informacion de la empresa antes de realizar la recarga</h4>
                      <br>
                    </div>
                    <div class="col-md-6">
                      <p><b>Id Usuario:</b> <?php echo $_GET['id_ci']; ?></p>
                    </div>
                    <div class="col-md-6">
                      <p><b>Documento:</b> <?php echo $_GET['nit_ci']; ?></p>
                    </div>
                    <div class="col-md-6">
                      <p><b>Nombre:</b> <?php echo $_GET['name_ci']; ?></p>
                      <p class="help-block">
                        <span class="glyphicon glyphicon-info-sign"></span> 
                        Este dato puede cambiar, si la empresa modifica su info.
                      </p>
                    </div>
                    <div class="col-md-6">
                      <p><b>Correo:</b> <?php echo $_GET['email_ci']; ?></p>
                      <p class="help-block">
                        <span class="glyphicon glyphicon-info-sign"></span> 
                        Este dato puede cambiar, si la empresa modifica su info.
                      </p>
                    </div>
                    <div class="col-md-12">
                      <p class="text-center text-primary">
                        <b>Salso Disponible:</b> 
                        <?php echo '$ ' . number_format($_GET['available_balance_ci'], 0, ",", "."); ?>
                      </p>
                    </div>
                  </div>
                </div>
              </div>

              <hr>

              <form id="form-recharge-balance" role="form" action="/controls/system/control_functions_company.php" method="POST" autocomplete="off">
                <div class="row">
                  <div class="col-sm-12 col-md-6 col-md-offset-3">
                    <div class="form-group">
                      <label class="text-left control-label">Digita el valor a recargar</label>
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="glyphicon glyphicon-piggy-bank"></i>
                        </span> 
                        <input class="form-control" id="value-recharge" required name="value_recharge" type="number" autofocus autocomplete="off" min="40000" placeholder="40000">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="text-left control-label">Digita id de la transacción</label>
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="fa fa-key" aria-hidden="true"></i>
                        </span> 
                        <input class="form-control" required name="id_transaction" type="number" autofocus autocomplete="off" min="1" placeholder="47">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <?php
                        $token_search = $controlUtilities->control_utilities_create_token_pages('recharge_balance');
                        echo '<input type="hidden" name="tokenPage" value="' . $token_search . '" id="tokenPage">';
                        echo '<input type="hidden" name="tokenAction" value="RECHARGE_BALANCE">';
                        echo '<input type="hidden" name="id_ci" value="' . $_GET['id_ci'] . '">';
                        echo '<input type="hidden" name="nit_ci" value="' . $_GET['nit_ci'] . '">';
                        echo '<input type="hidden" name="name_ci" value="' . $_GET['name_ci'] . '">';
                        echo '<input type="hidden" name="email_ci" value="' . $_GET['email_ci'] . '">';
                      ?>
                      <input type="submit" class="btn btn-primary btn-block confirmAction" value="Recargar Saldo">
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> <!-- /END WRAPPER RECOVER PASSWORD-->
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/scripts_footer.php';
  ?>
</body>
</html>
