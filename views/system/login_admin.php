<?php 

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
if (isset($_SESSION['user']['id_su'])) {
  header('Location: /views/system/dashboard.php');
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");

$messages = new messages_system();
$controlUtilities = new controlUtilities();

define("PAGE_CURRENT", "LOGIN_ADMIN");

?>
<!DOCTYPE html>
<!--
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
-->
<html>
<head>
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/head.php';
  ?>
  <title>Login Admin</title>
</head>
<body class="page page-login-admin">

  <!-- START HEADER -->
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/header_admin.php';
  ?><!-- END HEADER -->

  <!-- WRAPPER RECOVER PASSWORD-->
  <div class="section wrp-page wrp-login-admin">
    <div class="container">  
      <!-- CONTENEDOR MENSAJES  -->
      <div class="row wrapper-messages-system">
        <?php 
        if (isset($_SESSION["message_system"])) {
          $messeges = $messages->menssages_render($_SESSION["message_system"]);
          unset($_SESSION["message_system"]);
          echo $messeges;
        }
        ?>
      </div><!-- CONTENEDOR MENSAJES  --> 
      <div class="row">
        <div class="col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1">
          <div class="panel panel-default">
            <div class="panel-heading text-center">
              <h4 class="text-primary">Iniciar Sesión</h4>
            </div>
            <div class="panel-body">
              <form action=<?php $_SERVER['DOCUMENT_ROOT']; ?>"/controls/control_login_reg.php" method="POST">
                <div class="modal-body">
                  <div class="form-horizontal" >
                    <div class="form-group">
                      <div class="col-sm-2">
                        <label class="text-left control-label text-primary">Email:</label>
                      </div>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" name="email" id="email" placeholder="tucuenta@tudominio.com" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-2">
                        <label class="text-left control-label text-primary">Clave:</label>
                      </div>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" name="pass" id="pass" placeholder="Tu Clave" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <?php
                    $token = $controlUtilities->control_utilities_create_token_pages('login_admin');
                    echo '<input type="hidden" name="tokenPage" value="' . $token . '" id="tokenPage">';
                  ?>
                  <input type="hidden" name="loginToken" value="loginAdmin">
                  <input type="hidden" name="logRegToken" value="login">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  <input type="submit" name="login" class="btn btn-primary" value="Entrar">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> <!-- /END WRAPPER RECOVER PASSWORD-->


  <!-- START FOOTER -->
  <?php
  // SCRIPTS DEL SITIO
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/scripts_footer.php';

  ?><!-- END FOOTER -->



</body>
</html>
