<?php 

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
if (!isset($_SESSION['user']['id_su'])) {
  header('Location: /index.php');
}

if (!isset($_GET['tokenPage']) || !in_array($_GET['tokenPage'], $_SESSION['token']['pages'])) {
  header('Location: /index.php');
}

if (!isset($_GET['id_ci'], $_GET['name_ci'], $_GET['nit_ci'], $_GET['email_ci'])) {
  header('Location: /index.php'); 
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_profile_company.cls.php");

$messages = new messages_system();
$controlUtilities = new controlUtilities();
$controlProfileCompany = new controlProfileCompany();

define("PAGE_CURRENT", "RECHARGE_HISTORY"); // llamarlo con: constant("PAGE_CURRENT")

?>
<!DOCTYPE html>
<!--
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
-->
<html>
<head>
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/head.php';
  ?>
  <title>Historial de Recargas</title>
</head>
<body class="page-recharge-balance">

  <!-- START HEADER -->
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/header_admin.php';
  ?><!-- END HEADER -->

  <!-- WRAPPER RECOVER PASSWORD-->
  <div class="section wrp-page wrapper-recharge-balance">
    <div class="container">  
      <!-- CONTENEDOR MENSAJES  -->
      <div class="row wrapper-messages-system">
        <?php 
        if (isset($_SESSION["message_system"])) {
          $messeges = $messages->menssages_render($_SESSION["message_system"]);
          unset($_SESSION["message_system"]);
          echo $messeges;
        }
        ?>
      </div><!-- CONTENEDOR MENSAJES  --> 
      <div class="row">
        <div class="col-sm-12 col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading text-center">
              <h4 class="text-primary">Informacion de la empresa</h4>
            </div>

            <div class="panel-body">
              <div class="row">
                <div class="col-sm-12 col-md-12">
                  <div class="row">
                    <div class="col-md-12 text-center">
                      
                      <br>
                    </div>
                    <div class="col-md-6">
                      <p><b>Id Usuario:</b> <?php echo $_GET['id_ci']; ?></p>
                    </div>
                    <div class="col-md-6">
                      <p><b>Documento:</b> <?php echo $_GET['nit_ci']; ?></p>
                    </div>
                    <div class="col-md-6">
                      <p><b>Nombre:</b> <?php echo $_GET['name_ci']; ?></p>
                      <p class="help-block">
                        <span class="glyphicon glyphicon-info-sign"></span> 
                        Este dato puede cambiar, si la empresa modifica su info.
                      </p>
                    </div>
                    <div class="col-md-6">
                      <p><b>Correo:</b> <?php echo $_GET['email_ci']; ?></p>
                      <p class="help-block">
                        <span class="glyphicon glyphicon-info-sign"></span> 
                        Este dato puede cambiar, si la empresa modifica su info.
                      </p>
                    </div>
                    <div class="col-md-12">
                      <p class="text-center text-primary">
                        <b>Salso Disponible:</b> 
                        <?php echo '$ ' . number_format($_GET['available_balance_ci'], 0, ",", "."); ?>
                      </p>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-12 col-md-12 well">
                  <?php

                  $tokenPage = $controlUtilities->control_utilities_create_token_pages('recharge_history');
                          
                  $rows_query = $controlProfileCompany->control_profile_company_get_recharge_history_by_id($_GET['id_ci']);   

                  $rows_table = $controlProfileCompany->control_profile_company_get_recharge_history_by_id_rows_table($rows_query, $tokenPage);
                  ?>
                  <div id="table-billing-company" class="table-responsive">
                    <!--<table data-url="inicio.php" data-toggle="table"   data-pagination="true" data-search="true"  data-height="450"  data-show-refresh="true" data-show-toggle="true" >-->
                    <table id="table-offers-created" class="table table-hover table-striped" data-url="billing_company.php" data-toggle="table" data-pagination="true" data-search="true"  data-height="450"  data-show-toggle="true" data-sort-name="id_sbr" data-sort-order="desc"><!--  se quito data-search="true" para el boton actualizar la tabla  -->
                      <thead>
                        <tr>
                          <th data-field="id_sbr" data-align="center" data-sortable="true">Id Recarga</th>
                          <th data-field="id_transaction_sbr" data-align="center">Id Iatai</th>
                          <th data-field="value_sbr" data-align="center">Valor</th>
                          <th data-field="balance_previus" data-align="center">Saldo Anterior/Nuevo</th>
                          <th data-field="created_sbr" data-align="center" data-sortable="true">Fecha</th>
                          <th data-field="url_recharge_pdf" data-align="center">Opción</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php echo $rows_table; ?>
                      </tbody>  
                    </table>

                  </div>          
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> <!-- /END WRAPPER RECOVER PASSWORD-->
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/scripts_footer.php';
  ?>
  <link href="/css/bootstrap/bootstrap-table.css" rel="stylesheet" type="text/css"/>
  <script src="/js/bootstrap/bootstrap-table.js" type="text/javascript"></script>
  <script src="/js/bootstrap/bootstrap-table-es-MX.js" type="text/javascript"></script>
  
</body>
</html>
