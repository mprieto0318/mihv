<?php 

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
if (!isset($_SESSION['user']['id_su'])) {
  header('Location: /index.php');
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");

$messages = new messages_system();
$controlUtilities = new controlUtilities();

define("PAGE_CURRENT", "DASHBOARD");

?>
<!DOCTYPE html>
<!--
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
-->
<html>
<head>
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/head.php';
  ?>
  <title>Dashboard</title>
</head>
<body class="page page-dashboard">

  <!-- START HEADER -->
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/header_admin.php';
  ?><!-- END HEADER -->

  <!-- WRAPPER RECOVER PASSWORD-->
  <div class="section wrp-page wrp-dashboard">
    <div class="container">  
      <!-- CONTENEDOR MENSAJES  -->
      <div class="row wrapper-messages-system">
        <?php 
        if (isset($_SESSION["message_system"])) {
          $messeges = $messages->menssages_render($_SESSION["message_system"]);
          unset($_SESSION["message_system"]);
          echo $messeges;
        }
        ?>
      </div><!-- CONTENEDOR MENSAJES  --> 
      <div class="row">
        <div class="col-sm-12 col-md-12">
          
          <div class="row">
            <div class="col-lg-3">
              <div class="panel panel-info">
                <a href="/views/system/search_user.php?rol=company&action=recharge_balance">
                  <div class="panel-heading">
                    <div class="row">
                      <div class="col-xs-6">
                        <i class="fa fa-money fa-5x"></i>
                      </div>
                      <div class="col-xs-6 text-right">
                        <p class="announcement-heading"><span class="glyphicon glyphicon-pushpin"></span></p>
                        <p class="announcement-text">Recargar Saldo</p>
                      </div>
                    </div>
                  </div>
                
                  <div class="panel-footer announcement-bottom">
                    <div class="row">
                      <div class="col-xs-6">
                        Ir
                      </div>
                      <div class="col-xs-6 text-right">
                        <i class="fa fa-arrow-circle-right"></i>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="panel panel-warning">
                <a href="/views/system/search_user.php?rol=company&action=recharge_history">
                  <div class="panel-heading">
                    <div class="row">
                      <div class="col-xs-6">
                        <i class="fa fa-barcode fa-5x text-warning"></i>
                      </div>
                      <div class="col-xs-6 text-right">
                        <p class="announcement-heading"><span class="glyphicon glyphicon-pushpin"></span></p>
                        <p class="announcement-text"> Historial Saldo</p>
                      </div>
                    </div>
                  </div>
                  
                  <div class="panel-footer announcement-bottom">
                    <div class="row">
                      <div class="col-xs-6">
                        Ir
                      </div>
                      <div class="col-xs-6 text-right">
                        <i class="fa fa-arrow-circle-right"></i>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
            </div>
            <!-- 
            <div class="col-lg-3">
              <div class="panel panel-danger">
                <div class="panel-heading">
                  <div class="row">
                    <div class="col-xs-6">
                      <i class="fa fa-users fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                      <p class="announcement-heading"><span class="glyphicon glyphicon-pushpin"></span></p>
                      <p class="announcement-text">Disponible</p>
                    </div>
                  </div>
                </div>
                <a href="#">
                  <div class="panel-footer announcement-bottom">
                    <div class="row">
                      <div class="col-xs-6">
                        Disponible
                      </div>
                      <div class="col-xs-6 text-right">
                        <i class="fa fa-arrow-circle-right"></i>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="panel panel-success">
                <div class="panel-heading">
                  <div class="row">
                    <div class="col-xs-6">
                      <i class="fa fa-comments fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                      <p class="announcement-heading"><span class="glyphicon glyphicon-pushpin"></span></p>
                      <p class="announcement-text"> Disponible</p>
                    </div>
                  </div>
                </div>
                <a href="#">
                  <div class="panel-footer announcement-bottom">
                    <div class="row">
                      <div class="col-xs-6">
                        Disponible
                      </div>
                      <div class="col-xs-6 text-right">
                        <i class="fa fa-arrow-circle-right"></i>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
            </div> -->
          </div><!-- /.row -->

        </div>
      </div>
    </div>
  </div> <!-- /END WRAPPER RECOVER PASSWORD-->


  <!-- START FOOTER -->
  <?php
  //require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/footer.php';
  // SCRIPTS DEL SITIO
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/scripts_footer.php';

  ?><!-- END FOOTER -->



</body>
</html>
