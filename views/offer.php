<?php
session_start();

if (!isset($_GET['id']) || empty($_GET['id'])) {
  header('Location: ../views/login.php');
}

define("PAGE_CURRENT", "OFFER"); // llamarlo con: constant("PAGE_CURRENT")

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/model_load_oferts.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_profile_person.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");

$controlUtilities = new controlUtilities();
$controlProfilePerson = new controlProfilePerson(); 
$messages = new messages_system();
$modelLoadOferts = new modelLoadOferts();

//147750127209232
if (strlen($_GET['id']) <= 13) {
  $messege = $messages->messages_failed('url show offer failed');
  $_SESSION["message_system"][] = $messages->messages_build('FAILED', 'Lo Sentimos,', $messege);
  header('Location: /index.php');
}

$id_offer = substr($_GET['id'], 13);
$url_search = $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'] . '?id=' . $id_offer;

?>
<!DOCTYPE html>
<!--
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
-->
<html>
<head>
  <title>Oferta - MI HV</title>
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/head.php';
  ?>
</head>
<body class="page-offer">
  
  <!-- START HEADER -->
  <?php
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/header.php';
  ?><!-- END HEADER -->

    <!-- START SECTION SEARCH -->
  <div class="section wrapper-search-page-offer">
    <div class="container">
      <br>
      <div class="row">
        <form method="GET" action=<?php $_SERVER['DOCUMENT_ROOT']; ?>"/views/search.php" class="form-search-oferts">
          <div class="col-xs-12 col-sm-10 col-md-10">
            <div class="input-group">
              <div class="input-group-btn search-panel">
                <select id="selCity" name="selCity" data-size="8" class="selectpicker form-control" data-live-search="true">
                  <?php
                  $_GET['selCity'] = (isset($_GET['selCity'])) ? $_GET['selCity'] : '';
                // Cargando lista de ciudades
                  $select_city = $controlUtilities->_control_utilities_get_citys_select($_GET['selCity']);
                  echo $select_city;
                  ?>
                </select>
              </div>
              <?php
              $_GET['it_search'] = (isset($_GET['it_search'])) ? $_GET['it_search'] : '';
              ?>      
              <input id="it_search" required type="text" class="form-control input-search element-tooltip" name="it_search" placeholder="Sistemas, call center, auxiliar..." value="<?php echo $_GET['it_search']; ?>" autofocus data-toggle="tooltip" data-placement="bottom" title="Realiza una busqueda!">
              <span class="input-group-btn">
                <button class="btn btn-default btn-block btn-icon-search element-tooltip" type="submit" data-toggle="tooltip" title="Buscar!">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </span>

            </div>
          </div>

          <div class="col-xs-12 col-sm-2 col-md-2">
            <span data-toggle="tooltip" title="Mostrar filtros" class="element-tooltip">
              <button type="button" class="btn btn-primary btn-search btn-block" data-toggle="collapse" data-target="#filter-panel">
                <span class="glyphicon glyphicon-filter"></span> Filtros
              </button>
            </span>
          </div>

          <br><br>
          <!-- FILTROS -->
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div id="filter-panel" class="collapse filter-panel panel panel-body">
              <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4">
                  <div class="form-group">
                    <label class="filter-col" style="margin-right:0;" >
                      Salario
                    </label>
                    <select id="selSalary" name="selSalary" class="form-control">
                      <?php
                      $_GET['selSalary'] = (isset($_GET['selSalary'])) ? $_GET['selSalary'] : '';
                    // Cargando opciones de Salario
                      $option_salary = $controlUtilities->_control_utilities_get_salary_select($_GET['selSalary']);
                      echo $option_salary
                      ?>
                    </select>                                
                  </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                  <div class="form-group">
                    <label class="filter-col">
                      Fecha
                    </label>
                    <!-- <input type="hidden" name="selDateHdn" id="selDateHdn" value=""> -->
                    <select id="selDate" name="selDate" class="form-control">
                      <?php
                      $_GET['selDate'] = (isset($_GET['selDate'])) ? $_GET['selDate'] : '';
                    // Cargando opciones de fecha
                      $option_select = $controlUtilities->_control_utilities_filter_date_select($_GET['selDate']);
                      echo $option_select
                      ?>
                    </select> 
                  </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                  <div class="form-group">
                    <label>Genero</label>
                    <div>
                      <select id="selGenre" name="selGenre" class="form-control">
                        <?php
                        $_GET['selGenre'] = (isset($_GET['selGenre'])) ? $_GET['selGenre'] : '';
                      // Cargando opciones de Salario
                        $option_genre = $controlUtilities->_control_utilities_get_genre_select($_GET['selGenre']);
                        echo $option_genre
                        ?>
                      </select> 
                    </div>
                    <br>                    
                  </div>
                </div>
              </div>
            </div>
          </div><!-- /FILTROS -->
        </form>
      </div>
      <br>
    </div><!-- Container -->
  </div><!-- END SECTION SEARCH -->

   <!-- START SECTION SEARCH -->
  <div class="section section-messages-system">
    <div class="container">
      <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * -->

      <!-- CONTENEDOR MENSAJES  -->
      <div class="row wrapper-messages-system">
        <?php 
        if (isset($_SESSION["message_system"])) {
          $messeges = $messages->menssages_render($_SESSION["message_system"]);
          unset($_SESSION["message_system"]);
          echo $messeges;
        }
        ?>
      </div><!-- CONTENEDOR MENSAJES  -->
    </div>
  </div>

  <div class="container">
    <!-- TITULO DE LA PAGINA -->
    <div class="row title-page">
      <div class="col-md-12 text-center">
        <h3 class="text-inverse">OFERTA</h3>
        <p class="text-inverse">Aqui puedes ver la informacion completa de la oferta!</p> 
      </div>
    </div>
     
    <div class="row">

      <!--  COLUMNA CONTENIDO  -->
      <div class="col-sm-9 col-md-9 wrap-view-offer">
        <?php

        $data_ofert = $modelLoadOferts->model_load_oferts_get_full_data_ofert_seleted($id_offer);
        
        if (!empty($data_ofert)) {
          
            if(isset($_SESSION['user']["id_pi"])) {
              $validate_apply_ofert = $modelLoadOferts->model_load_oferts_validate_apply_ofert_seleted($id_offer);  
            }else {
              $validate_apply_ofert = 0;
            }

            // Cargando Generos
            $genre = '';
            switch ($data_ofert['genre_o']) {
              case 'all':
                $genre = '
                  <p class="data-ofert">Ambos</p>
                ';
                break;
              case 'male':
                $genre = '
                  <p class="data-ofert">Masculino</p>
                ';
                break;
              case 'female':
                $genre = '
                  <p class="data-ofert">Femenino</p>
                ';
                break;
            }

            // Formatenado fecha
            $data_created_o = new DateTime($data_ofert['created_o']);

            // validando usuario logeado para aplicar
            if (isset($_SESSION['user']["rol_pi"]) && $_SESSION['user']["rol_pi"] == 'PERSON' && isset($_SESSION['user']["id_pi"])) {

              if ($validate_apply_ofert["COUNT(id_oa)"] == 0) {
                $btn_apply_ofert = '
                  <div class="row">
                    <div class="col-md-12 wrapper-btn-operation-ofert">
                      <button id="ofert-apply" type="button" class="btn btn-success pull-center">
                        <span class="glyphicon glyphicon-hand-right"></span> Aplicar
                      </button>
                      <div class="animation_image" style="display:none;">
                          <img alt="Cargando" src="/src/img/mihv/site/ajax-loader.gif"> Cargando...
                      </div>
                    </div>
                  </div>
                ';
              }else {
                $btn_apply_ofert = '
                  <div class="row">
                    <div class="col-md-12 wrapper-btn-operation-ofert">
                      <button id="ofert-disapply" type="button" class="btn btn-success pull-center" data-toggle="modal" data-target="#ofert-disapply-modal">
                        <span class="glyphicon glyphicon-hand-right"></span> Desaplicar
                      </button>
                      <div class="animation_image" style="display:none;">
                        <img alt="Cargando" src="/src/img/mihv/site/ajax-loader.gif"> Cargando...
                      </div>
                    </div>
                  </div>
                ';
              }

            }else {

              if (!isset($_SESSION['user']['id_su'])) {
                $btn_apply_ofert = '
                <div class="row">

                  <div class="col-md-6">
                    <p>Para aplicar debes estar registrado en <strong>MI HV</strong></p>
                  </div>
                  
                  <div class="col-md-6 text-center">
                    <button id="register-modal" type="button" class="btn btn-success" data-toggle="modal" data-target="#registerModal">
                      <span class="glyphicon glyphicon-paste"></span> Registrate
                    </button>
                    <button id="login" type="button" class="btn btn-success" data-toggle="modal" data-target="#loginModal">
                      <span class="glyphicon glyphicon-user"></span> Ingresa
                    </button>

                  </div>
                </div>';
              }else {
                $btn_apply_ofert = '';
              }
            }

            $priority_o = '';
            if ($data_ofert['priority_o'] == 'na') {
              $priority_o = 'Normal';
            }else if ($data_ofert['priority_o'] == 'urgent') {
              $priority_o = 'Urgente';
            }else {
              $priority_o = 'No Especifica';
            }

            ?>

          
            <form>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-9 col-md-9">
                    <br>
                    <h4 class="text-center">
                     <?php echo utf8_encode($data_ofert['name_o']); ?>
                    </h4>
                  </div>
                  <div class="col-sm-3 col-md-3">
                    <br>

                    <?php
                      $link_facebook = $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING'];
                    ?>

                    <div class="wrap-message-face">
                      <!-- respuesta de facebook --> 
                    </div>
                    
                    <button style="padding: 4px;" type="button" class="btn btn-primary button--share" data-link="<?php echo $link_facebook; ?>" data-image="https://imagenes.mihv.com.co/social/offer-networks-v1.png" data-caption="Oferta Laboral en MI HV" data-name="<?php echo utf8_encode($data_ofert['name_o']); ?>" data-description="<?php echo utf8_encode($data_ofert['description_o']); ?>">
                      <i class="fa fa-2x fa-fw fa-facebook fa-border" style="font-size: 13px; border-radius: 4px;"></i><span style="font-size: 12px;"> Compartir Oferta</span>
                    </button>
                  </div>
                  <hr>
                </div> 
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <label class="title-field"><span class="glyphicon glyphicon-briefcase"></span> EMPRESA</label>
                    <div>
                      <p class="data-ofert"><?php echo utf8_encode($data_ofert['name_ci']); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-3">
                    <label class="title-field"><span class="glyphicon glyphicon-time"></span> CREADA</label>
                    <div>
                      <p class="data-ofert"><?php echo $data_created_o->format('d/m/Y'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-3 col-md-3">
                    <label class="title-field">PROFESIÓN</label>
                    <div>
                      <p class="data-ofert"><?php echo utf8_encode($data_ofert['profession_o']); ?></p>
                    </div>
                  </div>
                </div>                
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <label class="title-field">DESCRIPCIÓN</label>
                    <div>
                      <p class="data-ofert"><?php echo utf8_encode($data_ofert['description_o']); ?></p>
                    </div>
                  </div>

                  <div class="col-sm-6 col-md-6">
                    <label class="title-field">CONOCIMIENTOS / HABILIDADES / DESTREZAS</label>
                    <div>
                      <p class="data-ofert"><?php echo  utf8_encode($data_ofert['knowledge_o']); ?></p>
                    </div>
                  </div>
                </div> 
              </div>

              <hr>

              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3 col-md-2">
                    <label class="title-field">SALARIO</label>
                    <div>
                      <?php
                        $salary = ($data_ofert['value_o'] > 0) ? $data_ofert['value_o'] : 'A convenir';
                      ?>
                      <p class="data-ofert"><?php echo $salary; ?></p>
                    </div>
                  </div>

                  <div class="col-sm-3 col-md-2">
                    <label class="title-field">CONTRATO</label>
                    <div>
                      <p class="data-ofert"><?php echo utf8_encode($data_ofert['type_contract_o']); ?></p>
                    </div>
                  </div>

                  <div class="col-sm-3 col-md-2">
                    <label class="title-field">PRIORIDAD</label>
                    <div>
                      <p class="data-ofert"><?php echo $priority_o; ?></p>
                    </div>
                  </div>
                
                  <div class="col-sm-3 col-md-2">
                    <label class="title-field"># VACANTES</label>
                    <div>
                      <p class="data-ofert"><?php echo $data_ofert['vacancies_o']; ?></p>
                    </div>
                  </div>

                  <div class="col-sm-4 col-md-2">
                    <label class="title-field">GENERO</label>
                    <div>
                      <?php echo $genre; ?>
                    </div>
                  </div>

                  <div class="col-sm-4 col-md-2">
                    <label class="title-field">CIUDAD</label>
                    <div>
                      <p class="data-ofert"><?php echo utf8_encode($data_ofert['name_c']); ?></p>
                    </div>
                  </div>

                </div>
              </div>
              <div id="form-apply-operations" class="form-group form-operations"> 
                <input type="hidden" id="t0k3n_1d_0" name="t0k3n_1d_0" value="<?php echo $data_ofert['id_o']; ?>">
                <?php echo $btn_apply_ofert; ?>
              </div>
            </form> 
          <?php
        }else {
          ?>
          <br>
          <p class="text-center lead"><span class="glyphicon glyphicon-remove-circle"></span> La oferta no se ha encontrado</p>
          <p class="text-center">Realiza una búsqueda!</p>
          <?php
        }
        ?>
        



      </div><!--  /COLUMNA CONTENIDO  -->

      <!--  WRAPPER MENU LATERAL  -->
      <?php
      require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/advertising/menu-side.php';
      ?><!--  /WRAPPER MENU LATERAL  -->


    </div> <!-- /row -->
  </div><!-- /container -->

  <?php
  // Footer
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/footer.php';
  // MODAL PARA INICIO DE SESION O REGISTRO DE PERSONA  
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/modals/modal_login_register.php';
  // Modal Contenido Normal - Terminos y Condiciones
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/modals/modal_content_normal.php';
  // SCRIPTS DEL SITIO
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/scripts_footer.php';
  ?>
</body>
</html>