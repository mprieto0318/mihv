<?php
if ($_SERVER['REQUEST_URI'] == '/views/sections/footer.php') {
  header('Location: ../views/login.php');
}
?>
<footer>
  <div class="section">
    <div class="container">
      <div class="row">
        
        <div class="col-xs-12 col-sm-4 col-lg-4 text-center">
          <h3 class="title-block">Ofertas generales de:</h3>
          <ul>
            <li><a href="/views/search.php?it_search=ingenieria">Ingenieria</a></li>
            <li><a href="/views/search.php?it_search=medicina">Medicina</a></li>
            <li><a href="/views/search.php?it_search=seguridad">Seguridad</a></li>
            <li><a href="/views/search.php?it_search=administracion">Administracion</a></li>
            <li><a href="/views/search.php?it_search=contabilidad">Contabilidad</a></li>
            <li><a href="/views/search.php?it_search=estado">Estado</a></li>
            <li><a href="/views/search.php?it_search=transporte">Transporte</a></li>
          </ul>
        </div>
        <div class="col-xs-12 col-sm-4 col-lg-4 text-center">
          <h3 class="title-block">Contacto:</h3>
          <p><a href="tel:+573165269362">Móvil: (+57) 316 526 9362</a></p>
          <p><a href="mailto:contacto@mihv.com.co" title="Contactanos"><i class="fa fa-envelope"></i> contacto@mihv.com.co</a></p>
          <h3 class="title-block">Siguenos en:</h3>
            <a href="https://www.facebook.com/mihvcolombia" target="_blank"><i class="fa fa-2x fa-fw fa-facebook"></i></a>
            <a href="https://www.instagram.com/mihvcolombia" target="_blank"><i class="fa fa-2x fa-fw fa-instagram"></i></a>
            <a href="https://www.twitter.com/mihvcolombia" target="_blank"><i class="fa fa-2x fa-fw fa-twitter"></i></a>
        </div>

        <div class="col-xs-12 col-sm-4 col-lg-4">
          <div class="box box-footer">             
            <div class="icon">
              <div class="info">
                <h3 class="title-block">Codection SAS</h3>
                <p>
                  Este sitio es desarrollado y soportado por la tecnologia de <b>CODECTION</b>.
                </p>
                <div class="more">
                  <a href="https://www.codection.pro" target="_blank" title="Title Link">
                    www.codection.pro <i class="fa fa-angle-double-right"></i>
                  </a>
                </div>
              </div>
            </div>
          </div> 
        </div>

        <div class="col-xs-12 copy-right">
          <hr>
          <p class="text-center">Copyright <?php echo date('Y'); ?> &copy;<b> Codection SAS</b>.</p>
        </div>
      </div>
    </div>
  </div>
  
</footer>


<!-- Facebook popup -->
<div id="fb-root"></div>



