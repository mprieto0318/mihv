<?php
if ($_SERVER['REQUEST_URI'] == '/views/sections/head.php') {
  header('Location: ../views/login.php');
}
?>
<link rel="shortcut icon" type="image/x-icon" href="/src/img/mihv/site/favicon.ico" />
<!-- Fuente para los iconos de social network -->
<link href="/css/font-awesome-4.7.0/css/font-awesome.min.css"
    rel="stylesheet" type="text/css">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- STYLES CONTRIB -->
<link href="/js/contrib/bootstrap-select-1.10.0/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>
<link href="/css/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<!-- STYLES MI HV -->
<link href="/css/mihv/mihv.css" rel="stylesheet" type="text/css"/>
<link href="/css/mihv/media.css" rel="stylesheet" type="text/css"/>


<!-- <meta property="og:url"           content="https://dev.mihv.com.co" />
<meta property="og:type"          content="website" /> 
<meta property="og:title"         content="mick Your Website Title" />
<meta property="og:description"   content="mick Your description" />
<meta property="og:image"         content="https://imagenes.mihv.com.co/email/saludo.jpg" /> -->