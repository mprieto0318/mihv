<?php
if ($_SERVER['REQUEST_URI'] == '/views/sections/header_admin.php') {
  header('Location: /views/system/login_admin.php');
}
	
$item_login = '';
$item_dashboard = '';

if(defined('PAGE_CURRENT')){
  
  switch (constant("PAGE_CURRENT")) {
    case 'LOGIN_ADMIN':
      $item_login = 'active';
      break;
    case 'DASHBOARD':
      $item_dashboard = 'active';
      break;
    // case 'EDIT_COMPANY':
    // case 'PROFILE_PERSON':
    // case 'PROFILE_COMPANY':
    // case 'BILLING_COMPANY':
    //   $item_profile = 'active';
    //   break;
    // case 'LOGIN_REGISTER':
    //   $item_login_register = 'active';
    //   break;
  }
}

$opc_logout = '
  <li>
		<a href="/models/logout.php"><span class="glyphicon glyphicon-log-out"></span> SALIR</a>
	</li>';

if (isset($_SESSION['user']["id_su"])) {   
 	$opc_user = '
 		<li class="' . $item_dashboard .'">
			<a href="/views/system/dashboard.php">
				<span class="glyphicon glyphicon-cog"></span> 
				PANEL DE CONTROL
			</a>
		</li>';
}else {
	$opc_user = '
		<li class="'. $item_login . '">
			<a href="/views/system/login_admin.php">
			<span class="glyphicon glyphicon-user"></span> LOGIN</a>
		</li>';
	$opc_logout = '';
}
?>

<div class="navbar navbar-default navbar-static-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/views/system/dashboard.php"><span><b>ADMIN MI HV</b></span></a>
		</div>
		<div class="collapse navbar-collapse" id="navbar-ex-collapse">
			<ul class="nav navbar-nav navbar-right">
				<?php 
					echo $opc_user; // Opciones del Usuario
					echo $opc_logout; // Cerrar Sesion Usuario
				?>
			</ul>
		</div>
	</div>
</div>