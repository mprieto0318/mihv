<?php
if ($_SERVER['REQUEST_URI'] == '/views/sections/scripts_footer.php') {
  header('Location: ../views/login.php');
}
?>

<!-- SCRIPTS CONTRIB -->
<script src="/js/jquery-2.2.2.min.js" type="text/javascript"></script>
<script src="/js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="/js/contrib/bootstrap-select-1.10.0/bootstrap-select.min.js" type="text/javascript" ></script>
<script src="/js/contrib/bootstrap-select-1.10.0/i18n/defaults-es_CL.min.js" type="text/javascript"></script>
<script src="/js/contrib/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="/js/contrib/jquery-validation/additional-methods.min.js" type="text/javascript"></script>
<script src="/js/contrib/bootstrap-filestyle.min.js" type="text/javascript"></script>


<!-- SCRIPTS MI HV -->
<script src="/js/mihv/mihv.js" type="text/javascript"></script>
