<?php
if ($_SERVER['REQUEST_URI'] == '/views/sections/modals/modal_create_ofert_normal.php') {
  header('Location: ../../views/login.php');
}
?>
<!-- MODAL PERSON SELECTED -->
<div id="modal-confirm-delete-user" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-confirm-delete-user">
	<div class="modal-dialog modal-big">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title text-primary" id="modal-confirm-delete-user">
					Eliminar Cuenta
				</h4>
			</div>
			<div class="modal-body">
				<form id="form-confirm-delete-user" method="POST" action=<?php $_SERVER['DOCUMENT_ROOT']; ?>"/controls/control_delete_user.php">

					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="row"> 
								<div class="col-sm-12 col-md-12 text-center">
									<h4 class="text-danger">ESTAS APUNTO DE ELIMNAR TU CUENTA EN MI HV</h4>
                  <p class="help-block">
                    Ten en cuenta estos puntos antes de eliminar tu cuenta
                  </p>
								</div>
								<hr>
								<div class="col-sm-12 col-md-12">
									<p><b><span class="glyphicon glyphicon-info-sign text-danger"></span> </b>
										Al <b>eliminar tu cuenta</b> se borrara toda la información ingresada y relacionada en <b>MI HV</b>
									</p>
								</div>
								<div class="col-sm-12 col-md-12">
									<p><b><span class="glyphicon glyphicon-info-sign text-danger"> </span></b>
										Al <b>eliminar tu cuenta</b> no podras recuperar la información ingresada y relacionada en <b>MI HV</b>
              		</p>
								</div>

								<div class="col-sm-12 col-md-12">
									<p><b><span class="glyphicon glyphicon-info-sign text-danger"> </span></b>
										Al <b>eliminar tu cuenta</b> no recibiras más correos electronicos de <b>MI HV</b> los cuales pueden tener información de tu interes.
                  </p>
								</div>

								<div class="form-group form-operations">
				          <div class="input-group-addon">    
                    <?php
                      $token_search = $controlUtilities->control_utilities_create_token_pages('profile_company');
                      echo '<input type="hidden" name="tokenPageModal" value="' . $token_search . '" id="tokenPageModal">';
                    ?>        

                    <input type="submit" name="submit-confirm-delete-user" value="ELIMINAR CUENTA" class="btn btn-danger submit-confirm-delete-user">	 

                    <div class="animation_image" style="display:none;">
                      <img alt="Cargando" src="/src/img/mihv/site/ajax-loader.gif"> Cargando, por favor espere...
                    </div>           

				          </div>
				        </div>
							
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
      	<button type="button" class="btn btn-default" data-dismiss="modal">Volver</button>
    	</div>

		</div>
	</div>
</div>