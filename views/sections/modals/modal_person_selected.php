<?php
if ($_SERVER['REQUEST_URI'] == '/views/sections/modals/modal_person_selected.php') {
  header('Location: ../../views/login.php');
}
?>
<!-- MODAL PERSON SELECTED -->
<div id="modal-person-selected" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-title-person-selected">
	<div class="modal-dialog modal-big">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title text-primary" id="modal-title-person-selected">
					Informacion de la Persona.
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="row" id="person-selected"> 
							<!-- AGREGA CONTENIDO DEL MODAL VIA AJAX getFullDataPerson() -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>