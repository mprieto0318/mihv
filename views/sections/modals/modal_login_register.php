<?php
if ($_SERVER['REQUEST_URI'] == '/views/sections/modals/modal_login_register.php') {
  header('Location: ../../views/login.php');
}
?>

<!-- MODAL LOGIN -->
<div id="loginModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header modal-header-ofert-selected-person">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="loginModalLabel">Iniciar Sesión en <strong>MI HV</strong></h4>
      </div>

      <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab1primary" data-toggle="tab">Iniciar Sesión</a></li>
            <li><a href="#tab2primary" data-toggle="tab">Olvide mi Contraseña</a></li>
          </ul>
        </div>
        <div class="panel-body">
          <div class="tab-content">
            <div class="tab-pane fade in active" id="tab1primary">
              <form action=<?php $_SERVER['DOCUMENT_ROOT']; ?>"/controls/control_login_reg.php" method="POST">
                <div class="modal-body">
                  <div class="form-horizontal" >
                    <div class="form-group">
                      <div class="col-sm-2">
                        <label class="text-left control-label text-primary">Email:</label>
                      </div>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" name="itEmail" id="email" placeholder="tucuenta@tudominio.com" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-2">
                        <label class="text-left control-label text-primary">Clave:</label>
                      </div>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" name="itPassword" id="pass" placeholder="Tu Clave" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="tokenRol">
                  <!-- se agrega Token por Js para validar user & empresa-->
                </div>
                <div class="modal-footer">
                  <?php
                    $token_search = $controlUtilities->control_utilities_create_token_pages('login_reg');
                    echo '<input type="hidden" name="tokenPage" value="' . $token_search . '" id="tokenPage">';
                  ?>
                  <input type="hidden" name="logRegToken" value="login">
                  <input type="hidden" name="redirect" class="redirect" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  <input type="submit" name="submitLgnRgt" id="submitLgnRgt" class="btn btn-primary" value="Entrar">
                </div>
              </form>
            </div>
            <div class="tab-pane fade" id="tab2primary">
              <form action=<?php $_SERVER['DOCUMENT_ROOT']; ?>"/controls/control_login_reg.php" method="POST">
                <div class="modal-body">
                  <div class="form-horizontal" >
                    <div class="form-group">
                      <div class="col-sm-2">
                        <label class="text-left control-label text-primary">Email:</label>
                      </div>
                      <div class="col-sm-10">
                        <input type="email" name="itEmailRecover" class="form-control" id="itEmailRecover" placeholder="tucuenta@tudominio.com" required>
                        <p class="text-muted">Ingresa tu email para recuperar la clave</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <div id="tokenRolRecover">
                    <!-- se agrega Token por Js para validar user & empresa-->
                  </div>
                  <?php
                    $token_search = $controlUtilities->control_utilities_create_token_pages('login_reg');
                    echo '<input type="hidden" name="tokenPage" value="' . $token_search . '" id="tokenPage">';
                  ?>
                  
                  <input type="hidden" name="redirect" class="redirect" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
                  <input type="hidden" name="logRegToken" value="RECOVER_PASSWORD">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  <input type="submit" name="submitRecoverPassword" id="submitRecoverPassword" class="btn btn-primary" value="Recuperar Clave">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- ###########################################  -->
<!-- ###########################################  -->

<!-- MODAL REGISTRO DE PERSONA -->
<div id="registerModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="form-register-person" action=<?php $_SERVER['DOCUMENT_ROOT']; ?>"/controls/control_login_reg.php" method="POST">
        <div class="modal-header modal-header-ofert-selected-person">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="registerModalLabel">Registrate como persona.</h4>
        </div>

        <div class="modal-body">
          <p class="error text-center lead">
            El registro de personas estará habilitado desde el día <br><b>Jueves 13 de Abril del 2017</b>
          </p>
          <p class="text-center text-muted">
            Ingresa los campos del formulario para crear tu cuenta en <strong>MI HV</strong>
          </p>

          <fieldset class="border-fieldset">
            <legend class="text-center text-primary">
              PASO #1
            </legend>

            <div class="form-horizontal">

              <div class="form-group">
                <div class="col-sm-3">
                  <label class="text-left control-label text-primary">* Nombre(s):</label>
                </div>
                <div class="col-sm-9">
                        <input type="text" class="form-control" name="itName" id="itName" placeholder="Tu nombre completo" required>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-3">
                  <label class="text-left control-label text-primary">* Apellidos:</label>
                </div>
                <div class="col-sm-9">
                        <input type="text" class="form-control" name="itLastName" id="itLastName" placeholder="Tu apellido completo" required>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-3">
                  <label class="control-label text-primary">* Email:</label>
                </div>
                <div class="col-sm-9">
                        <input type="email" class="form-control" name="itEmail" id="itEmail" placeholder="Tu correo de contacto" required>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-3">
                  <label class="control-label text-primary">* Clave:</label>
                </div>
                <div class="col-sm-9">
                        <input type="password" class="form-control" name="itPassword" id="itPassword" placeholder="Mínimo 6 dígitos" required>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-3">
                  <label class="control-label text-primary">Fijo:</label>
                </div>
                <div class="col-sm-9">
                        <input type="text" class="form-control" name="itPhone" id="itPhone" placeholder="indicativo + 7 números">
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-3">
                  <label class="control-label text-primary">* Móvil:</label>
                </div>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="itMovil" id="itMovil" placeholder="Indica tu número móvil">
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-3">
                  <label class="control-label text-primary">* Ciudad:</label>
                </div>
                <div class="col-sm-9">
                  <select id="selCity" name="selCity" data-size="8" class="selectpicker form-control" data-live-search="true">
                    <?php
                    // Cargando lista de ciudades
                    $select_city = $controlUtilities->_control_utilities_get_citys_select();
                    echo $select_city;
                    ?>
                  </select>
                  <input type="hidden" id="selCityValidate" name="selCityValidate" value="all">
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-3">
                  <label class="control-label text-primary">* Dirección:</label>
                </div>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="itAddress" id="itAddress" placeholder="Indica tu dirección de residencia">
                </div>
              </div>

            </div>
          </fieldset>
          <hr>
          <fieldset class="border-fieldset">
            <legend class="text-center text-primary">
              PASO #2
            </legend>

            <div class="form-horizontal">
              
              <div class="form-group">
                 <div class="col-sm-3">
                    <label class="control-label text-primary">Profesión:</label>
                </div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="itProfession" id="itProfession" placeholder="Ingeniero de Software">
                    <p class="help-block">
                      <span class="glyphicon glyphicon-info-sign"></span> 
                      Si no tienes profesión dejalo vacío
                    </p>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-3">
                  <label class="text-primary">Edad:</label>
                </div>
                <div class="col-sm-9">
                  <input type="number" class="form-control" name="age" id="age" min="16" max="60" value="16">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-3">
                  <label class="text-primary">Años de Experiencia:</label>
                </div>
                <div class="col-sm-9">
                  <input type="number" class="form-control" name="selExperiencie" id="selExperiencie" min="0" max="70" value="0">
                  <p class="help-block">
                    <span class="glyphicon glyphicon-info-sign"></span> 
                    Deja en 0 si no tienes experiencia
                  </p>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-3">
                  <label class="text-primary">*Conocimientos / Habilidades / Destresas:</label>
                </div>
                <div class="col-sm-9">
                  <textarea class="form-control" rows="5" required name="itaKnowledge" id="itaKnowledge" placeholder="excel, inventarios, base de datos, php, ingles, metodologias de trabajo, dj crosover, ensaladas italianas, escritor de musica, etc..."></textarea>
                  <p class="help-block">
                    <span class="glyphicon glyphicon-info-sign"></span> 
                    Separar por comas (,)
                  </p>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-3">
                  <label class="text-primary">Estudios:</label>
                </div>
                <div class="col-sm-9">
                  <textarea class="form-control" rows="5" name="itaStudies" placeholder="estos son mis estudios"></textarea>
                  <p class="help-block">
                    <span class="glyphicon glyphicon-info-sign"></span> 
                    Si no tienes estudios dejalo vacío, separar por comas (,)
                  </p>
                </div>
              </div>
                                 
              <div class="form-group">
                <div class="col-sm-12">
                  <hr>          
                  <p class="wrap-check-required text-center">
                    <label><input type="checkbox" name="ckTerms" id="ckTerms" value="YES"> 
                      * Acepto los 
                      <a href="#" data-toggle="modal" id="show-modal-terms-register" data-content="terms-conditions" data-target="#modal-content-normal">Terminos y Condiciones</a>
                    </label>
                  </p>
                  <div class="animation_image block-load-ajax" style="display:none; color: #49b9ea;">
                    <img alt="Cargando" id="ajax-loader" src="/src/img/mihv/site/ajax-loader.gif"> Cargando...
                  </div>
                </div>
              </div>

            </div>
          </fieldset>

        </div>
        <div class="modal-footer">
          <input type="hidden" name="logRegToken" value="registerPerson">
          <?php
            $token_search = $controlUtilities->control_utilities_create_token_pages('login_reg');
            echo '<input type="hidden" name="tokenPage" value="' . $token_search . '" id="tokenPage">';
          ?>
          <input type="hidden" name="redirect" class="redirect" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
          <button type="button" class="btn btn-default" data-dismiss="modal">Volver</button>
          <input type="submit" name="submitLgnRgt" id="submitRegisterPerson" class="btn btn-primary" value="Regístrate">
        </div>
      </form>
    </div>
  </div>
</div>


<!-- ###########################################  -->
<!-- ###########################################  -->


<!-- MODAL - REGISTRO EMPRESA -->
<div id="registerCompanyModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="registerCompanyModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="form-register-company" action=<?php $_SERVER['DOCUMENT_ROOT']; ?>"/controls/control_login_reg.php" method="POST" enctype="multipart/form-data" name="form_register_company">
        <div class="modal-header modal-header-ofert-selected-person">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="registerCompanyModalLabel">Registrate como empresa</h4>
        </div>
        <div class="modal-body">
          <p class="text-center text-muted">
            Ingresa los campos del formulario para crear tu cuenta en <strong>MI HV</strong>
          </p>

          <fieldset class="border-fieldset">
            <legend class="text-center">
              PASO #1
            </legend>

            <div class="form-horizontal" >
              <div class="form-group">
                <div class="col-sm-3">
                  <label class="text-left control-label text-primary">Nombre:</label>
                </div>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="itName" id="itName" placeholder="Mi Empresa" required>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-3">
                  <label class="control-label text-primary">Descripción:</label>
                </div>
                <div class="col-sm-9">
                  <textarea class="form-control" rows="3" required name="itaDescription" id="itaDescription" placeholder="Somos una empresa dedicada a ...., con x años de experiencia, con clientes como: cliente 1, cliente 2. estamos ubicados en ..."></textarea>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-3">
                  <label class="control-label text-primary">Logo:</label>
                </div>
                <div class="col-sm-9">
                  <input type="file" name="fLogo" accept="image/*" class="filestyle f-logo" data-buttonText="Buscar Logo" data-size="sm" >
                  <p class="help-block">
                    <span class="glyphicon glyphicon-info-sign"></span> 
                    Tambien puedes cargar el logo desde tu perfil
                  </p>
                </div>
              </div>


            </div>
          </fieldset>

          <hr>

          <fieldset class="border-fieldset">
            <legend class="text-center">
              PASO #2
            </legend>

            <div class="form-horizontal" >

                    <div class="form-group">
                      <div class="col-sm-3">
                        <label class="text-left control-label text-primary">Nit:</label>
                      </div>
                      <div class="col-sm-7">
                        <input type="number" min="0" class="form-control validate-nit validate-only-number" name="itNit" id="itNit" placeholder="Nit o CC sin dígito de Verificación" required>
                        <p class="help-block text-danger error-nit" style="display: none">
                          <span class="glyphicon glyphicon-info-sign"></span> 
                          El valor digitado no es un numero valido
                        </p>

                      </div>
                      <div class="col-sm-2">
                        <input type="text" class="form-control result-dv text-center" name="dv" disabled readonly placeholder="DV">
                        <input type="hidden" class="result-dv" name="dv_hidden">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-3">
                        <label class="text-primary">Actividad Economica:</label>
                      </div>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="itEconomicActivity" id="itEconomicActivity" placeholder="Call center, Contadores, etc..." required>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-3">
                        <label class="control-label text-primary"># Fijo:</label>
                      </div>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="itPhone" id="itPhone" placeholder="De tu compañía y si tienes extensión" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-3">
                        <label class="control-label text-primary"># Móvil:</label>
                      </div>
                      <div class="col-sm-9">
                        <input type="number" class="form-control" name="itMovil" id="itMovil" placeholder="Indica los 10 digitos" required>
                      </div>
                    </div>

                    <div class="form-group">
                    <div class="col-sm-3">
                      <label class="control-label text-primary">Ciudad:</label>
                    </div>
                    <div class="col-sm-9">
                      <select id="selCity" name="selCity" data-size="8" class="selectpicker form-control" data-live-search="true">
                        <?php
                        // Cargando lista de ciudades
                        $select_city = $controlUtilities->_control_utilities_get_citys_select();
                        echo $select_city;
                        ?>
                      </select>
                      <input type="hidden" id="selCityValidate" name="selCityValidate" value="all">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-3">
                      <label class="control-label text-primary">Dirección:</label>
                    </div>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name="itAddress" id="itAddress" placeholder="Indica la dirección">
                    </div>
                  </div>
                </div>
          </fieldset>

          <hr>

          <fieldset class="border-fieldset">
            <legend class="text-center">
              PASO #3
            </legend>

            <div class="form-horizontal" >
                    
                    <div class="form-group">
                      <div class="col-sm-3">
                        <label class="control-label text-primary">Email:</label>
                      </div>
                      <div class="col-sm-9">
                        <input type="email" class="form-control" name="itEmail" id="itEmail" placeholder="soportehv@mihv.co" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-3">
                        <label class="control-label text-primary">Clave:</label>
                      </div>
                      <div class="col-sm-9">
                        <input type="password" class="form-control" name="itPassword" id="itPassword" placeholder="Mihv.2016_" required>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-sm-12">
                        <hr>          
                        <p class="wrap-check-required text-center">
                          <label><input type="checkbox" name="ckTerms" id="ckTerms" value="YES"> 
                            Acepto los 
                            <a href="#" data-toggle="modal" id="show-modal-terms-register" data-content="terms-conditions" data-target="#modal-content-normal">Términos y Condiciones</a>
                          </label>
                        </p>
                        <div class="animation_image block-load-ajax" style="display:none; color: #49b9ea;">
                          <img alt="Cargando" id="ajax-loader" src="/src/img/mihv/site/ajax-loader.gif"> Cargando...
                        </div>
                      </div>
                    </div>

                  </div>

          </fieldset>

        </div>
        <div class="modal-footer">
          <input type="hidden" name="logRegToken" value="registerCompany">
          <?php
            $token_search = $controlUtilities->control_utilities_create_token_pages('login_reg');
            echo '<input type="hidden" name="tokenPage" value="' . $token_search . '" id="tokenPage">';
          ?>
          <input type="hidden" name="redirect" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
          <button type="button" class="btn btn-default" data-dismiss="modal">Volver</button>
          <input type="submit" name="submitLgnRgt" class="btn btn-primary" value="Registra tu compañía" onclick="CalcularDv();">
        </div>
      </form>
    </div>
  </div>
</div>