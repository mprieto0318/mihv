<?php
if ($_SERVER['REQUEST_URI'] == '/views/sections/modals/modal_create_ofert_normal.php') {
  header('Location: ../../views/login.php');
}
?>
<!-- MODAL PERSON SELECTED -->
<div id="modal-create-ofert-normal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-create-ofert-normal">
	<div class="modal-dialog modal-big">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title text-primary" id="modal-create-ofert-normal">
					Oferta de Prioridad Normal
				</h4>
			</div>
			<div class="modal-body">
				<form id="form-create-ofert-normal" method="POST" action=<?php $_SERVER['DOCUMENT_ROOT']; ?>"/controls/control_profile_company_operations.php">

					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="row" id="summary-create-ofert-normal"> 
								
								<div class="col-sm-12 col-md-12 text-center modal-wrap-info-cost-offer">
									<!-- Se agrega contenido al pasar la validacion de la oferta por js -->
								</div>
								<hr>
								<div class="col-sm-12 col-md-12 text-center">
									<h4 class="text-primary">BENEFICIOS AL PUBLICAR UNA OFERTA NORMAL</h4>
								</div>
								<div class="col-sm-12 col-md-12">
									<p><b><span class="glyphicon glyphicon-ok text-primary"> </span> </b>
										Tu oferta saldrá en <b>el motor de búsqueda de mihv.com.co con prioridad normal</b> (tu oferta estará en las sección intermedia del motor de búsqueda).
									</p>
								</div>
								<div class="col-sm-12 col-md-12">
									<p><b><span class="glyphicon glyphicon-ok text-primary"> </span></b>
										Tu publicación tendrá una vigencia de <b>20</b> días calendario a partir de la creación de la oferta.
									</p>
								</div>

								<div class="col-sm-12 col-md-12">
									<p><b><span class="glyphicon glyphicon-ok text-primary"> </span></b>
										Se te enviará una <b>factura de venta</b> a tu correo (el que tienes registrado) por la prestación del servicio al publicar esta oferta laboral.
									</p>
								</div>

								<div class="form-group form-operations">
				          <div class="input-group-addon">

				          	<!-- COMENTANDO PAGO DE PSE Y SALDO DE LA CUENTA
				            <button type="button" name="submit-create-ofert-normal-pse" id="btn-create-ofert-normal-pse" class="btn btn-success">
				              <img alt="pago pse" src="/src/img/mihv/site/pse-logo.png" width="30" height="30"> PAGAR POR PSE
				            </button>
				            
				            <button type="button" name="submit-create-ofert-normal-balance" id="btn-create-ofert-normal-balance" class="btn btn-primary">
				              DEBITAR DE SALDO CUENTA
				            </button> -->				            

				            <button type="button" name="submit-create-ofert-normal" id="btn-create-ofert-normal" class="btn btn-primary">
				              CREAR OFERTA
				            </button>				            

				          </div>
				        </div>
							
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
      	<button type="button" class="btn btn-default" data-dismiss="modal">Volver</button>
    	</div>

		</div>
	</div>
</div>