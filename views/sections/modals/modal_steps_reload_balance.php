<?php
if ($_SERVER['REQUEST_URI'] == '/views/sections/modals/modal_steps_reload_balance.php') {
  header('Location: ../../views/login.php');
}
?>
<!-- MODAL PERSON SELECTED -->
<div id="modal_steps_reload_balance" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_steps_reload_balance">
	<div class="modal-dialog modal-big">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title text-primary">
					Pasos para recargar tu cuenta
				</h4>
			</div>
			<div class="modal-body">
				<form id="form-create-ofert-normal" method="POST" action=<?php $_SERVER['DOCUMENT_ROOT']; ?>"/controls/control_profile_company_operations.php">

					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="row" id="summary-create-ofert-normal"> 
								
								<div class="col-sm-12 col-md-12 text-center">
									<h4 class="text-primary">EN 3 SENCILLOS PASOS</h4>
								</div>
								<div class="col-sm-12 col-md-12">
									<ol>
                    <li>
                      <p>Dale click en el botón <b>"Pagar con Onepocket"</b> (Actualmente sistema Débito PSE o tarjetas de crédito)</p>
                    </li>     
                    <br>    
                    <li>
                      <p><b>En el formulario debes tener en cuenta: </b></p>
                      <ul>
                        <li><b>Apellido:</b> SAS, LTDA, SA, etc "si tu empresa es de un solo nombre"</li>
                        <li><b>Nro de Id:</b> escribe tu nit o cc (importante para tu recarga)</li>
                        <li><b>IVA:</b> 0</li>
                        <li><b>Referencia:</b> Recarga de saldo.</li>
                      </ul>
                      <p>Luego presiona el botón "Continuar" y sigue con el medio de pago ya sea PSE o tarjeta de credito y finaliza la transacción.</p>
                    </li>
                    <br>        
                    <li>
                      Una vez recargues tu saldo, en el menor tiempo posible validaremos tu recarga y la agregaremos a tu saldo para que sigas publicando tus ofertas laborales.
                    </li>
                    <br>       
                  </ol>

                  <hr>

                  <p class="text-primary"><span class="glyphicon glyphicon-alert"> </span> <b>RECUERDA QUE:</b></p>

                  <p>
                    <b> HORARIO RECARGA DE SALDO:</b>
                  </p>
                  <ul>
                    <li>
                      <p style="font-size: 14px;">
                        Lunes a viernes: de 07:00 AM a 07:00 PM
                      </p>
                    </li>
                    <li>
                      <p style="font-size: 14px;">
                        Sábados: 07:00 AM a 03:00 PM
                      </p>
                    </li>
                  </ul>
                  <p class="error" style="font-size: 12px; text-align: justify;">
                    La(s) recarga(s) que realices a través del botón de pago en horas fuera del <b>horario de recarga (incluyendo días domingos y festivos)</b> será agregado al saldo de tu cuenta el siguiente día hábil en el horario que corresponda (por motivos de tu seguridad).
                  </p>
                  <br>
                  <p>Entre más grande sea el monto de tu recarga, menos son las veces que uses este medio.</p>
                  <ul>
                    <li>
                      <p style="font-size: 11px;">
                        Si escribes mal tu nit o cc y finalizaste la recarga, debes ponerte en contacto a <a href="mailto:recarga@mihv.com.co?Subject=Recarga%20del%20saldo" target="_top">recarga@mihv.com.co</a> y allí te indicaremos los pasos a seguir para agregar tu recarga a tu saldo.
                      </p>
                    </li>
                  </ul>
								</div>
								
								<div class="form-group form-operations">

				          <div class="input-group-addon">
                    <b>Recarga tu saldo con tarjeta crédito o débito</b><br>
                    <a class="link-buy-credit" href="https://secureacceptance.allegraplatform.com/botondepago/code-connection-professional-sas/" target="_blank">
                      <img alt="Recarga saldo onepocket" src="/src/img/mihv/site/onepocket-btn-desktop.png">
                    </a>			            	            
				          </div>
				        </div>
						
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
      	<button type="button" class="btn btn-default" data-dismiss="modal">Volver</button>
    	</div>

		</div>
	</div>
</div>