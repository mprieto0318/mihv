<?php
if ($_SERVER['REQUEST_URI'] == '/views/sections/modals/modal_ofert_apply.php') {
  header('Location: ../../views/login.php');
}
?>
<!-- MODAL OFERT APPLY -->
<div id="modal-ofert-apply" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-title-ofert-apply">
	<div class="modal-dialog modal-big">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title text-primary" id="modal-title-ofert-apply">
					<!-- Agrega titulo via js  -->
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="row" id="list-apply-ofert"> 

							<!-- AGREGA CONTENIDO DEL MODAL VIA AJAX getListApplyOfertSelected() -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>