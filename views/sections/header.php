<?php
if ($_SERVER['REQUEST_URI'] == '/views/sections/header.php') {
  header('Location: ../views/login.php');
}
	
$item_index = '';
$item_search = '';
$item_profile = '';
$item_login_register = '';	

if(defined('PAGE_CURRENT')){
  
  switch (constant("PAGE_CURRENT")) {
    case 'INDEX':
      $item_index = 'active';
      break;
    case 'SEARCH':
      $item_search = 'active';
      break;
    case 'EDIT_COMPANY':
    case 'PROFILE_PERSON':
    case 'PROFILE_COMPANY':
    case 'BILLING_COMPANY':
      $item_profile = 'active';
      break;
    case 'LOGIN_REGISTER':
      $item_login_register = 'active';
      break;
  }
}

$opc_logout = '
  <li>
		<a href="/models/logout.php"><span class="glyphicon glyphicon-log-out"></span> SALIR</a>
	</li>';

if (isset($_SESSION['user']["id_pi"]) && $_SESSION['user']["rol_pi"] == 'PERSON' ) {   
 	$opc_user = '
 		<li class="'. $item_index . '">
			<a href="/index.php"><span class="glyphicon glyphicon-home"></span> INICIO</a>
		</li>
 		<li class="' . $item_search .'">
				<a href="/views/search.php"><span class="glyphicon glyphicon-search"></span> BUSCAR</a>
		</li>
		<li class="dropdown ' . $item_profile . '">
		 	<a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-user"></span> PERSONA<span class="caret"></span></a>
      <ul class="dropdown-menu">
        <li><a href="/views/profile_person.php"><span class="glyphicon glyphicon-th-large"></span> Panel de Control</a></li>
      </ul>
    </li>';
}elseif (isset($_SESSION['user']["id_ci"]) && $_SESSION['user']["rol_ci"] == 'COMPANY' ) {
	$opc_user = '
		<li class="dropdown ' . $item_profile . '">
		 	<a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-user"></span> EMPRESA <span class="caret"></span></a>
      <ul class="dropdown-menu">
        <li><a href="/views/profile_company.php"><span class="glyphicon glyphicon-th-large"></span> Gestionar</a></li>
        <li><a href="/views/edit_company.php"><span class="glyphicon glyphicon-cog"></span> Perfil</a></li>
        <li><a href="/views/billing_company.php"><span class="glyphicon glyphicon-book"></span> Facturación</a></li>
      </ul>
    </li>';
}else {
	$opc_user = '
		<li class="'. $item_index . '">
			<a href="/index.php"><span class="glyphicon glyphicon-home"></span> INICIO</a>
		</li>
		<li class="' . $item_search .'">
				<a href="/views/search.php"><span class="glyphicon glyphicon-search"></span> BUSCAR</a>
		</li>';
    if (!isset($_SESSION['user']['id_su'])) {
      $opc_user .= '<li class="' . $item_login_register . '">
        <a href="/views/login.php"><span class="glyphicon glyphicon-user"></span> INGRESA / REGISTRATE</a>
      </li>';
    }
    
	$opc_logout = '';
}
?>

<div class="navbar navbar-default navbar-static-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/"><span><b>MI HV</b></span></a>
		</div>
		<div class="collapse navbar-collapse" id="navbar-ex-collapse">
			<ul class="nav navbar-nav navbar-right">
				<?php 
					echo $opc_user; // Opciones del Usuario
					echo $opc_logout; // Cerrar Sesion Usuario
				?>
			</ul>
		</div>
	</div>
</div>