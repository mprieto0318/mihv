<?php
  session_start();
  
  if (!isset($_SESSION['user']['id_ci'])) {
    header('Location: ../views/login.php');
  }
    
  define("PAGE_CURRENT", "PROFILE_COMPANY");

  require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");
  require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_profile_company.cls.php");
  require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");

  $controlUtilities = new controlUtilities();
  $controlProfileCompany = new controlProfileCompany(); 
  $messages = new messages_system();

?>
<!DOCTYPE html>
<!--
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
-->
<html>
<head>
  <title>Gestionar - MI HV</title>
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/head.php';
  ?>
  <link href="/css/mihv/profile.css" rel="stylesheet" type="text/css"/>
</head>
<body class="page-profile page-profile-company">
  
  
  <?php
    // Header
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/header.php';
  ?>

  <!-- START SECTION CONTENT PROFILE -->
  <div class="section wrapper-profile">
    <div class="container">
       <div class="row title-page">
          <div class="col-md-12 text-center">
            <h3 class="text-inverse">GESTIONAR</h3>
            <p class="text-inverse">Aqui puedes crear ofertas, editarlas y eliminarlas. tambien puedes getionar las personas que aplicaron a tus ofertas!</p> 
          </div>
        </div>
        <!-- CONTENEDOR MENSAJES  -->
        <div class="row wrapper-messages-system">
          <?php 
          if (isset($_SESSION["message_system"])) {
            $messeges = $messages->menssages_render($_SESSION["message_system"]);
            unset($_SESSION["message_system"]);
            echo $messeges;
          }
          ?>
        </div><!-- CONTENEDOR MENSAJES  -->
      <div class="row">
          <!--  COLUMNA CONTENIDO  -->
          <div class="col-sm-9 col-md-9">

            <!--  CONTENEDOR IMAGEN TABS  -->            
            <div class="card hovercard">
              <div class="card-background">
                <img class="card-bkimg" alt="perfil" src="/src/img/mihv/site/header-profile.jpg">
                <!-- http://lorempixel.com/850/280/people/9/ -->
              </div>
              <div class="useravatar">
                <?php

                $url_logo = $_SERVER['DOCUMENT_ROOT'] . '/src/img/company/logo/';

                if (file_exists($url_logo . $_SESSION['user']['image_ci']) && is_file($url_logo . $_SESSION['user']['image_ci'])) {
                  $logo = '
                    <img alt="' . $_SESSION['user']['image_ci'] .'" class="logo-company" src="/src/img/company/logo/' . $_SESSION['user']['image_ci'] .'">';
                  clearstatcache();
                }else {
                  $logo = '<img alt="logo empresa predeterminada" src="/src/img/mihv/site/logo-company-default.jpg">';
                }

                echo $logo;

                ?>

              </div>
              <div class="card-info"> 
                <span class="card-title"><?php echo $_SESSION['user']['name_ci']; ?></span>
              </div>
            </div><!--  /CONTENEDOR IMAGEN TABS  -->

            <?php
              $show_tab = (isset($_GET['tab'])) ? $_GET['tab'] : 'default';

              if ($show_tab == 2) { //tab ofertas creadas
                $class_tabs = array(
                  'create_offer' => 'btn-default', 
                  'offers_created' => 'btn-primary', 
                );

                $content_tabs = array(
                  'create_offer' => '', 
                  'offers_created' => 'active',
                );
              }else {
                $class_tabs = array(
                  'create_offer' => 'btn-primary', 
                  'offers_created' => 'btn-default', 
                );

                $content_tabs = array(
                  'create_offer' => 'active', 
                  'offers_created' => '',
                );
              }
            ?>        

            <!--  CONTENEDOR BOTONES TABS  -->
            <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
              <div class="btn-group" role="group">
                <button type="button" id="stars" class="btn <?php echo $class_tabs['create_offer']; ?>" href="#tab1" data-toggle="tab">
                  <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                  <div class="hidden-xs">CREAR OFERTA</div>
                </button>
              </div>
              <div class="btn-group" role="group">
                <button type="button" id="favorites" class="btn <?php echo $class_tabs['offers_created']; ?>" href="#tab2" data-toggle="tab">
                  <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
                  <div class="hidden-xs">OFERTAS CREADAS</div>
                </button>
              </div>
            </div><!--  /CONTENEDOR BOTONES TABS  -->

            <!--  CONTENEDOR TABS  -->
            <div class="well">
              <div class="tab-content">

                <!--  TAB 1  -->
                <div class="tab-pane fade in <?php echo $content_tabs['create_offer']; ?>" id="tab1">
                  
                  <form id="form-create-ofert" method="POST" action=<?php $_SERVER['DOCUMENT_ROOT']; ?>"/controls/control_profile_company_operations.php" class="form-create-ofert">
                    <?php 
                      $balances = $controlProfileCompany->control_profile_company_get_balances();
                      $value_offer = $controlProfileCompany->control_profile_company_get_value_offer();
                      $publish_free = $controlProfileCompany->control_profile_company_get_publish_free($_SESSION['user']['id_ci']);
                    ?>
                    <div id="wrap-info-cost-offer" class="row">

                      <div class="col-sm-12 col-md-12 block-balance">

                        <?php 
                            if ($publish_free['publish_free_ci'] > 0) {
                              echo '<p class="text-center">Tienes ('. $publish_free['publish_free_ci'] .') publicacion <b>GRATIS!</b></p>';
                            }
                          ?>

                        <p class="text-primary text-center lead">
                          Saldo Actual 
                          <b>
                            $ <?php echo number_format($balances['available'], 0, ",", "."); ?> 
                          </b> 
                        </p>

                        <p class="text-primary text-center">
                          <a class="link-buy" href="" data-toggle="modal" data-target="#modal_steps_reload_balance"> 
                            <span class="glyphicon glyphicon-credit-card"> </span> 
                            Recarga tu saldo aqui
                          </a>
                        </p>
                        
                      </div>

                      <div class="col-sm-6 col-md-6 block-urgent">
                        <h5 class="text-info text-center">
                          <span class="glyphicon glyphicon-asterisk"></span> 
                          Precio Urgente 
                          <b>
                            $ <?php echo number_format($value_offer['urgent_balance'], 0, ",", "."); ?> 
                          </b> 
                        </h5>
                      </div>

                      <div class="col-sm-6 col-md-6 block-na">
                        <h5 class="text-info text-center">
                          <span class="glyphicon glyphicon-asterisk"></span> 
                          Precio Normal 
                          <b>
                            $ <?php echo number_format($value_offer['na_balance'], 0, ",", "."); ?> 
                          </b> 
                        </h5>
                      </div>

                    </div>

                    <hr>
                    <div class="form-group"> 
                      <label class="title-field">Nombre</label>
                      <div>
                        <input type="text" class="form-control" name="itName" placeholder="Escribe el titulo de tu oferta laboral...">
                      </div>        
                    </div>

                    <div class="form-group">
                      <label class="title-field">Descripción</label>
                      <div>
                        <textarea class="form-control" rows="4" name="itaDescription" placeholder="Escribe una breve descripción como: requerimos ingenieros, tecnólogos y/o técnicos en ..."></textarea>
                      </div>                  
                    </div>

                    <div class="form-group">
                      <label class="title-field">Conocimientos / Habilidades / Destrezas</label>
                      <div>
                        <textarea class="form-control" rows="4" name="itakKowledge" placeholder="Escribe los conomientos que necesitas como: Php, logistica, servidores, soporte de computo, backoffice, etc..."></textarea>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="row">

                        <div class="col-sm-4 col-md-4">
                          <label class="title-field">Profesión</label>
                          <div>
                            <input type="text" class="form-control" name="itProfession" placeholder="Profesión a solicitar...">
                          </div>
                        </div>

                        <div class="col-sm-4 col-md-4">
                          <label class="title-field">Genero</label>
                          <div>
                            <select name="selGenre" class="form-control">
                              <option value="all">Todos</option>
                              <option value="male">Masculino</option>
                              <option value="female">Femenino</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-sm-4 col-md-4 wrp-select wra-select-city">
                          <label class="title-field">Ciudad</label>
                          <div>
                            <select name="selCity" data-size="8" class="selectpicker form-control select-city" data-live-search="true">
                            <?php
                            // Cargando lista de ciudades
                            $select_city = $controlUtilities->_control_utilities_get_citys_select();
                            echo $select_city;
                            ?>
                            </select>
                            <input type="hidden" id="selCityValidate" name="selCityValidate" value="all">
                          </div>
                        </div>

                      </div>                
                    </div>

                    <br>
                 
                    <div class="form-group">
                      <div class="row">
                        <div class="col-sm-3 col-md-3">
                          <label class="title-field">SALARIO</label>
                          <div>
                            <input type="text" class="form-control validate-only-number" name="selValue" placeholder="1800000">
                            <p class="help-block">
                              <span class="glyphicon glyphicon-info-sign"></span> 
                              Deja en "0" para salario a convenir
                            </p>
                          </div>
                          <br> 
                        </div>

                        <div class="col-sm-3 col-md-3">
                          <label class="title-field">CONTRATO</label>
                          <div>
                            <select class="form-control" name="selTypeContract">
                              <option value="Indefinido">Indefinido</option>
                              <option value="Definido">Definido</option>
                              <option value="Prestacion de Servicios">Prestacion de Servicios</option>
                              <!-- <option value="Freelance">Freelance</option> -->
                            </select>
                          </div>
                        </div>
                      
                        <div class="col-sm-3 col-md-3">
                          <label class="title-field"># VACANTES</label>
                          <div>
                            <input type="number" class="form-control" name="selVacancies" value="1" min="1" max="70">
                          </div>
                        </div>

                        <div class="col-sm-3 col-md-3">
                          <label class="title-field"><span class="glyphicon glyphicon-star-empty"></span> PRIORIDAD</label>
                          <div>
                            <select id="priority-create-ofert" class="form-control" name="selPriority">
                              <option value="na">Normal</option>
                              <?php
                              if ($publish_free['publish_free_ci'] == 0) {
                                echo '<option value="urgent">Urgente</option>';
                              }
                              ?>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group-addon">
                        <input type="hidden" name="profileCompanyToken" value="createOfert">

                        <!-- COMENTANDO PAGO DE PSE Y SALDO DE LA CUENTA
                        <input type="hidden" name="payment_type" id="payment_type" value=""> -->                      
                        <?php
                          $token_search = $controlUtilities->control_utilities_create_token_pages('profile_company');
                          echo '<input type="hidden" name="tokenPage" value="' . $token_search . '" id="tokenPage">';
                        ?>
                        <input type="reset" value="Reiniciar" class="btn btn-danger pull-left">

                        <?php
                          if ($publish_free['publish_free_ci'] == 0) {
                            echo '<input type="submit" name="submitFormCreateOfert" id="btn-create-ofert" value="Siguiente" class="btn btn-success pull-right">';
                            echo '<input type="hidden" name="showModal" value="0" class="showModal">';
                          }else {
                            echo '<input type="submit" name="submitFormCreateOfert" id="btn-create-ofert-free" value="Crear Oferta" class="btn btn-success pull-right">';
                            echo '<input type="hidden" name="showModal" value="1" class="showModal">';
                          }
                        ?>
                        

                        <button id="show-modal-create-ofert-urgent" type="button" class="btn btn-success pull-center" data-toggle="modal" data-target="#modal-create-ofert-urgent" style="display: none">
                        <button id="show-modal-create-ofert-normal" type="button" class="btn btn-success pull-center" data-toggle="modal" data-target="#modal-create-ofert-normal" style="display: none">

                      </div>
                    </div>
                  </form>

                </div><!-- /TAB 1 -->


                <!-- TAB 2 -->
                <div class="tab-pane fade in <?php echo $content_tabs['offers_created']; ?>" id="tab2">
                  <?php
                    $rows_query = $controlProfileCompany->control_profile_company_get_all_oferts_by_user();
                        
                    $rows_table = $controlProfileCompany->control_profile_company_get_all_oferts_by_user_rows_table($rows_query);

                    if (isset($_GET['name-offer'])) {
                      echo '<input type="hidden" class="hidden_name_offer" name="hidden_name_offer" value="' . $_GET['name-offer'] . '">';
                    }

                  ?>
                  

                  <div id="table-ofert-created" class="table-responsive">
                    
                    <!--<table data-url="inicio.php" data-toggle="table"   data-pagination="true" data-search="true"  data-height="450"  data-show-refresh="true" data-show-toggle="true" >-->
                    <table id="table-offers-created" class="table table-hover table-striped" data-url="profile_company.php" data-toggle="table" data-pagination="true" data-search="true"  data-height="450"  data-show-toggle="true" ><!--  se quito data-search="true" para el boton actualizar la tabla  -->
                      <thead>
                        <tr>
                          <!-- <th data-field="id" data-align="left" data-sortable="true">ID</th> -->
                          <th data-field="name" data-align="center">Nombre</th>
                          <th data-field="date" data-align="center" data-sortable="true">Creado</th>
                          <th data-field="publish" data-align="center">Publicado</th>
                          <th data-field="apply" data-align="center">Aplicados</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php echo $rows_table; ?>
                      </tbody>  
                    </table>

                  </div>

                </div><!-- /TAB 2 -->

              </div>
            </div><!--  /CONTENEDOR TABS  -->

          </div><!--  /COLUMNA CONTENIDO  -->

          <?php
          // Publicidad lateral
          require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/advertising/menu-side.php';
          ?>


      </div> <!-- /row -->
    </div><!-- /container -->
  </div><!-- END SECTION CONTENT PROFILE -->
      
  <?php
  // Footer
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/footer.php';
  // MODAL QUE CONTIENE TODA LA INFORMACION DE LA OFERTA SELECCIONADA
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/modals/modal_ofert_selected_company.php';
  // MODAL QUE CONTIENE TODA LA PERSONAS QUE APLICARON A LA OFERTA SELECCIONADA
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/modals/modal_ofert_apply.php';
  // MODAL QUE CONTIENE TODA LA INFORMACION LA PERSONA SELECCIONADA
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/modals/modal_person_selected.php';
   // MODAL DE CREAR OFERTA URGENTE
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/modals/modal_create_ofert_priority.php';
  // MODAL DE CREAR OFERTA NORMAL
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/modals/modal_create_ofert_normal.php';
  // MODAL PASOS RECARGAR SALDO
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/modals/modal_steps_reload_balance.php';
  // SCRIPTS DEL SITIO
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/scripts_footer.php';

  ?>

  <!-- valida formulario de crear oferta -->
  <script src="../js/mihv/validate_create_ofert.js" type="text/javascript"></script>
  <!-- scripst de la tabla -->
  <script src="../js/bootstrap/bootstrap-table.js" type="text/javascript"></script>
  <script src="../js/bootstrap/bootstrap-table-es-MX.js" type="text/javascript"></script>
  <link href="../css/bootstrap/bootstrap-table.css" rel="stylesheet" type="text/css"/>
</body>
</html>