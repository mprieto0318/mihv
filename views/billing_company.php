<?php
  session_start();
  
  if (!isset($_SESSION['user']['id_ci'])) {
    header('Location: ../views/login.php');
  }
    
  define("PAGE_CURRENT", "BILLING_COMPANY"); // llamarlo con: constant("PAGE_CURRENT")

  require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");
  require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_profile_company.cls.php");
  require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");

  $controlUtilities = new controlUtilities();
  $controlProfileCompany = new controlProfileCompany(); 
  $messages = new messages_system();

?>
<!DOCTYPE html>
<!--
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
-->
<html>
<head>
  <title>Facturación - MI HV</title>
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/head.php';
  ?>
</head>
<body class="page-billing-company">
  
  <!-- START HEADER -->
  <?php
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/header.php';
  ?><!-- END HEADER -->

  <!-- START SECTION CONTENT PROFILE -->
  <div class="section wrapper-profile">
  <div class="container">
     <div class="row title-page">
        <div class="col-md-12 text-center">
          <h3 class="text-inverse">FACTURACIÓN</h3>
          <p class="text-inverse">Aqui encuentras el listado de tus facturas!</p> 
        </div>
      </div>

      <!-- CONTENEDOR MENSAJES  -->
      <div class="row wrapper-messages-system">
        <?php 
        if (isset($_SESSION["message_system"])) {
          $messeges = $messages->menssages_render($_SESSION["message_system"]);
          unset($_SESSION["message_system"]);
          echo $messeges;
        }
        ?>
      </div><!-- CONTENEDOR MENSAJES  -->
    <div class="row">
        <!--  COLUMNA CONTENIDO  -->
        <div class="col-sm-9 col-md-9 well">
          <?php

          $tokenPage = $controlUtilities->control_utilities_create_token_pages('billing_company');
                   
                  
          $rows_query = $controlProfileCompany->control_profile_company_get_all_billing_company();   
          $rows_table = $controlProfileCompany->control_profile_company_get_all_billing_company_rows_table($rows_query, $tokenPage);
          ?>
          <div id="table-billing-company" class="table-responsive">
            <!--<table data-url="inicio.php" data-toggle="table"   data-pagination="true" data-search="true"  data-height="450"  data-show-refresh="true" data-show-toggle="true" >-->
            <table id="table-offers-created" class="table table-hover table-striped" data-url="billing_company.php" data-toggle="table"   data-pagination="true" data-search="true"  data-height="450"  data-show-toggle="true" ><!--  se quito data-search="true" para el boton actualizar la tabla  -->
              <thead>
                <tr>
                  <th data-align="center" data-sortable="true">Número de Factura</th>
                  <th data-align="center">Valor</th>
                  <th data-align="center" data-sortable="true">Creado</th>
                  <th data-align="center">Opción</th>
                </tr>
              </thead>
              <tbody>
                  <?php echo $rows_table; ?>
              </tbody>  
            </table>

          </div>

        </div><!--  /COLUMNA CONTENIDO  -->

        <!--  WRAPPER MENU LATERAL  -->
        <?php
        require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/advertising/menu-side.php';
        ?><!--  /WRAPPER MENU LATERAL  -->


    </div> <!-- /row -->
  </div><!-- /container -->
  </div><!-- END SECTION CONTENT PROFILE -->

  <?php
    // Footer
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/footer.php';
    // SCRIPTS DEL SITIO
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/scripts_footer.php';
  ?>

  <script src="../js/bootstrap/bootstrap-table.js" type="text/javascript"></script>
  <script src="../js/bootstrap/bootstrap-table-es-MX.js" type="text/javascript"></script>
  <link href="../css/bootstrap/bootstrap-table.css" rel="stylesheet" type="text/css"/>
</body>
</html>