<?php 

session_start();  
if (isset($_SESSION['user']['id_ci'])) {
  header('Location: ../views/profile_company.php');
}

define("PAGE_CURRENT", "SEARCH"); // llamarlo con: constant("PAGE_CURRENT")

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/model_load_oferts.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/messages/messages.cls.php");

$functions_sql = new functions_sql();
$modelLoadOferts = new modelLoadOferts();
$controlUtilities = new controlUtilities();
$messages = new messages_system();

?>
<!DOCTYPE html>
<!--
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
-->
<html lang="es">
<head>
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/head.php';
  ?>
  <title>Buscar - MI HV</title>
</head>
<body class="page-search">

  <!-- START HEADER -->
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/header.php';
  ?><!-- END HEADER -->

  <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * -->

  
  <div class="section section-page-search">
    <div class="container">

      <div class="row">

        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * -->
        <!--  WRAPPER CONTENT  -->
        <div class="col-sm-9 col-md-9 wrapper-section">

          <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * -->

          <!-- CONTENEDOR MENSAJES  -->
          <div class="row wrapper-messages-system">
            <?php 
            if (isset($_SESSION["message_system"])) {
              $messeges = $messages->menssages_render($_SESSION["message_system"]);
              unset($_SESSION["message_system"]);
              echo $messeges;
            }
            ?>
          </div><!-- CONTENEDOR MENSAJES  -->

          
          <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * -->


          <!--  CONTENEDOR BUSCAR  -->
          <form method="GET" action=<?php $_SERVER['DOCUMENT_ROOT']; ?>"/views/search.php" class="form-create-ofert">
            <div class="row">

              <div class="col-sm-12 col-md-12 block-search">
                <div class="row ">

                  <div class="col-xs-12 col-sm-9 col-md-9">
                    <div class="input-group">
                      <div class="input-group-btn search-panel">
                        <select id="selCity" name="selCity" data-size="8" class="selectpicker form-control" data-live-search="true">
                          <?php
                          $_GET['selCity'] = (isset($_GET['selCity'])) ? $_GET['selCity'] : 'all';
                          // Cargando lista de ciudades
                          $select_city = $controlUtilities->_control_utilities_get_citys_select($_GET['selCity']);
                          echo $select_city;
                          ?>
                        </select>
                      </div>
                      <?php
                      $it_search = (isset($_GET['it_search'])) ? $_GET['it_search'] : '';
                      $autofocus = (empty($it_search)) ? 'autofocus' : '';
                      ?>       
                      <input id="it_search" required type="text" class="form-control input-search element-tooltip" name="it_search" placeholder="Sistemas, call center, auxiliar..." value="<?php echo $it_search; ?>" <?php echo $autofocus; ?> data-toggle="tooltip" title="Realiza una busqueda!" data-placement="bottom">
                      <span class="input-group-btn">
                        <button class="btn btn-default btn-block element-tooltip" type="submit" data-toggle="tooltip" title="Buscar!" data-placement="bottom">
                          <span class="glyphicon glyphicon-search"></span>
                        </button>
                      </span>

                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-3 col-md-3">
                    <span data-toggle="tooltip" title="Mostrar filtros" class="element-tooltip">
                      <button type="button" class="btn btn-primary btn-search btn-block" data-toggle="collapse" data-target="#filter-panel" data-placement="bottom">
                        <span class="glyphicon glyphicon-filter"></span> Filtros
                      </button>
                    </span>
                  </div>

                  <br><br>

                  <!-- Filtros -->
                  <div class="col-xs-12 col-sm-12 col-md-12">
                    <div id="filter-panel" class="collapse filter-panel panel panel-body">
                      <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4">
                          <div class="form-group">
                            <label class="filter-col" style="margin-right:0;" >
                              Salario
                            </label>
                            <select id="selSalary" name="selSalary" form-remove-offer class="form-control">
                              <?php
                              $_GET['selSalary'] = (isset($_GET['selSalary'])) ? $_GET['selSalary'] : 'all';
                                // Cargando opciones de Salario
                                $option_salary = $controlUtilities->_control_utilities_get_salary_select($_GET['selSalary']);
                                echo $option_salary
                              ?>
                            </select>                                
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                          <div class="form-group">
                            <label class="filter-col">
                              Fecha
                            </label>
                            <!-- <input type="hidden" name="selDateHdn" id="selDateHdn" value=""> -->
                            <select id="selDate" name="selDate" form-remove-offer class="form-control">
                              <?php
                              $_GET['selDate'] = (isset($_GET['selDate'])) ? $_GET['selDate'] : 'all';
                        // Cargando opciones de fecha
                              $option_select = $controlUtilities->_control_utilities_filter_date_select($_GET['selDate']);
                              echo $option_select
                              ?>
                            </select> 
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                          <div class="form-group">
                            <label>Genero</label>
                            <div>
                              <select id="selGenre" name="selGenre" form-remove-offer class="form-control">
                                <?php
                                $_GET['selGenre'] = (isset($_GET['selGenre'])) ? $_GET['selGenre'] : 'all';
                          // Cargando opciones de Salario
                                $option_genre = $controlUtilities->_control_utilities_get_genre_select($_GET['selGenre']);
                                echo $option_genre
                                ?>
                              </select> 
                            </div>
                            <br>                    
                          </div>
                        </div>
                      </div>
                    </div>

                    <?php

                    $total_result = 0;

                    if ($it_search != '') {

                      $values = $functions_sql->functions_sql_clear_dates($_GET);

                      //validando filtros si no existen
                      
                      if (isset($values['selCity']) && is_string($values['selCity'])) {
                        $values['selCity'] = 'all';
                      }else {
                        $values['selCity'] = (isset($values['selCity'])) ? $values['selCity'] : 'all';
                      }
                      
                      $values['selDate'] = (isset($values['selDate'])) ? $values['selDate'] : 'all';
                      $values['selGenre'] = (isset($values['selGenre'])) ? $values['selGenre'] : 'all';

                      if (isset($values['selSalary']) && is_string($values['selSalary'])) {
                        $values['selSalary'] = 'all';
                      }else {
                        $values['selSalary'] = (isset($values['selSalary'])) ? $values['selSalary'] : 'all';  
                      }
                      

                      if ($values['selCity'] == 'all' && $values['selDate'] == 'all' && $values['selGenre'] == 'all' && $values['selSalary'] == 'all') {
                        $total_oferts = $modelLoadOferts->model_load_oferts_count_search_oferts($values['it_search']);
                        //echo '<p class="text-center">SIN FILTROS TOTAL: ' . $total_oferts['COUNT(*)'] . '</p>';
                        $total_result =$total_oferts['COUNT(*)'];
                      }else {

                        $total_oferts = $modelLoadOferts->model_load_oferts_count_search_oferts($values['it_search'], 'YES', $values['selCity'], $values['selDate'], $values['selGenre'], $values['selSalary']);
                        //echo '<p class="text-center">CON FILTROS TOTAL: ' . $total_oferts['COUNT(*)'] . '</p>';
                        $total_result = $total_oferts['COUNT(*)'];
                      }
                      $item_per_page = 10;
                      // Total de paginas de la consulta 
                      $total_pages = ceil($total_oferts['COUNT(*)']/$item_per_page); 
                      echo '<input type="hidden" name="total_pages" value=' . $total_pages . ' id="total_pages">';
                    }
                    ?>
                  </div><!-- /Filtros -->
                </div>
              </div>
            </div>
            <?php
              $token_search = $controlUtilities->control_utilities_create_token_pages('search');
              echo '<input type="hidden" name="tokenPage" value="' . $token_search . '" id="tokenPage">';
            ?>



          </form><!--  CONTENEDOR BUSCAR  -->
          <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * -->

          <!--  CONTENEDOR OFERTAS  -->
          <div class="row">

            <div class="col-sm-12 col-md-12 block-oferts">

              <div class="list-group">
                <div class="list-group-item">
                  <div class="row" id="result_oferts"> 

                    <?php
                    if ($it_search == '') {
                      ?>
                      <div class="col-xs-12 col-sm-12 col-md-12">
                        <h2 class="text-center">
                          <span class="glyphicon glyphicon-search"></span> 
                          Realiza una búsqueda! 
                          <span class="glyphicon glyphicon-arrow-up"></span>
                        </h2>
                      </div>
                      <?php
                    }elseif ($total_result == 0) {
                      ?>
                      <div class="col-xs-12 col-sm-12 col-md-12">
                        <h2 class="text-center">
                          No hemos encontrado ofertas, busca otra! 
                          <span class="glyphicon glyphicon-arrow-up"></span>
                        </h2>
                      </div>
                      <?php
                    }
                    ?>  

                    <!-- AQUI SE CARGAN LAS OFERTAS VIA AJAX! -->
                    
                  </div>
                  <div align="center">  
                    <div class="animation_image block-load-ajax" style="display:none; color: #49b9ea;">
                      <img alt="Cargando" id="ajax-loader" src="/src/img/mihv/site/ajax-loader.gif"> Cargando...
                    </div>
                    <div class="end-oferts" style="display:none; color: #49b9ea;">
                      <p>--- No hay mas ofertas ---</p>
                    </div>
                  </div>
                </div>
              </div>

            </div>

          </div><!--  CONTENEDOR OFERTAS  -->
        </div><!--  /WRAPPER CONTENT  -->
         <!--  WRAPPER MENU LATERAL  -->
        <?php    
        require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/advertising/menu-side.php';
        ?><!--  /WRAPPER MENU LATERAL  -->
      </div>
    </div>
  </div>

  <?php
  // MODAL QUE CONTIENE TODA LA INFORMACION DE LA OFERTA SELECCIONADA
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/modals/modal_ofert_selected_person.php';
  // MODAL PARA INICIO DE SESION O REGISTRO DE PERSONA  
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/modals/modal_login_register.php';
  // Modal Contenido Normal - Terminos y Condiciones
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/modals/modal_content_normal.php';
  // SCRIPTS DEL SITIO
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/scripts_footer.php';
  ?>
</body>
</html>