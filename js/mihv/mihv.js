$(function () {

  //--------------------------------------------------------
  //----------------- START GLOBAL PAGES ------------------
  //--------------------------------------------------------

  // Tooltip - agregando descripcion a los campos
  $('.element-tooltip').tooltip();

  // Terminos y condiciones compania - Modal Contenido Normal
  $('body').on('click', '#show-modal-terms-register', function() {
    var idContent = $(this).attr('data-content');
    getContentNormalModal(idContent, $(this));
  });

   // ------- VALIDATE INPUT ONLY NUMBERS
  $('.validate-only-number').keypress(function(e) {
    return validateOnlyNumbers(e);
  });

  // Validar si existe un elemento con intervalo (interval)
  function validateExistingElementInterval(query_element){ 
    if ($(query_element).length) {
      clearInterval(varInterval);
      return true;
    }
    return false;
  }  

  $('body').on('click', '.confirmAction', function(e) {
    if(!confirm("¿Estas seguro de continuar?")) {
      e.preventDefault();
    }
  });
  

  //  - - - -  INICIO FACEBOOK - - - - - - 
  $('body').on('click', '.button--share', function() {
    var data = $(this).data();
    shareOffer(data);
  });

  window.fbAsyncInit = function() {
    FB.init({
      appId: '396070384060219', //'1248225241880804',
      channelUrl: document.location.origin, //'https://dev.mihv.com.co',
      status: true,
      xfbml: true,
      version: 'v2.8'
    });
  };

  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8&appId=396070384060219";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  function shareOffer(data) {
    FB.ui(
      {
        method: 'feed',
        name: data.name, //'Facebook Dialogs',
        link: data.link,
        picture: data.image,
        caption: data.caption,
        description: data.description
      },
      function(response) {
        if (response && response.post_id) {
          $('.wrap-message-face').html('<p class="confirm-facebook-ok" style="color: #44556c; padding-right: 4%;"><span class="glyphicon glyphicon-pushpin"></span> Gracias por compartir!</p>');
          setTimeout(function() {
            $('.confirm-facebook-ok').hide(1000);
          },3000);

        }else {
          $('.wrap-message-face').html('<p class="confirm-facebook-error error" style="padding-right: 4%;"><span class="glyphicon glyphicon-pushpin"></span> No compartiste</p>');
          setTimeout(function() {
            $('.confirm-facebook-error').hide(1000);
          },3000);
        }
      }
    );
  } 
  //  - - - -  END FACEBOOK - - - - - - 


  // // (((((((( NO ESTA EN USO ))))))))
  // // --------- LOAD LIST PROFESSION SELETED  
  // if ($('.selTypeProfession').hasClass('load-list-ajax')) {
  //   addEventLoadTypeProfession();
  // }//-- /LOAD LIST PROFESSION SELETED

  //---------------------------------------------------------
  //------------------ /END GLOBAL PAGES -------------------
  //---------------------------------------------------------


  if ($('.block-search').hasClass('block-search')) {

    // Agregando timeoit para que se cargue la posicion del bloque ok
    setTimeout(function() {
      var positionInit = $('.block-search').position().top,
      topScroll = positionInit ,
      wrapperMessagesHeight = $('.wrapper-messages-system').height(); 

      // Actuaizar Variable  var positionInit
      $('.wrapper-messages-system').on('click', '.close', function() {
        setTimeout(function() {
          positionInit = $('.block-search').position().top;
          topScroll = positionInit; 
        },450);
      });

      $(window).scroll(function() {
        var scrollTop = $("body").scrollTop();

        if (scrollTop <= wrapperMessagesHeight){
          $('.block-search').css({'position':'initial' , 'margin-top': 0 , 'width' : '100%'});
        }else {
         if (scrollTop >= positionInit && scrollTop < 700) {
            var widthWindows = $(window).width();

            if (widthWindows <= 768) {
              topScrollSmall = topScroll - 15;
              $('.block-search').css({'position':'fixed' , 'margin-top': '-' + topScrollSmall + 'px' , 'width' : '100%'});
            }
            if (widthWindows > 768 && widthWindows < 992) {
              $('.block-search').css({'position':'fixed' , 'margin-top': '-' + topScroll + 'px' , 'width' : '562.5px'});
            }
            if (widthWindows >= 992 && widthWindows < 1200) {
              $('.block-search').css({'position':'fixed' , 'margin-top': '-' + topScroll + 'px' , 'width' : '727.5px'});
            }   
            if (widthWindows >= 1200) {
              $('.block-search').css({'position':'fixed' , 'margin-top': '-' + topScroll + 'px' , 'width' : '877px'});
            }   
          }
        }
      });
    },300);
  }

  // --------- TOKEN LOGIN/RECOVER USER - PAGE LOGIN / REGISTER --------
  if ($('body').hasClass('page-login-register')) {
    $( ".loginPerson" ).click(function() {
      $( "#tokenRol" ).html('<input type="hidden" name="loginToken" value="loginPerson">');
      $( "#tokenRolRecover" ).html('<input type="hidden" name="typeUser" value="PI">');
    });
    
    $( ".loginCompany" ).click(function() {
      $( "#tokenRol" ).html('<input type="hidden" name="loginToken" value="loginCompany">');
      $( "#tokenRolRecover" ).html('<input type="hidden" name="typeUser" value="CI">');
    });

  }

  // --------- TAMAÑO FIJO TERMINOS Y COND... DE MODAL --------
  if ($('body').is('.page-login-register, .page-home, .page-search, .page-offer')) {
    // alto para modals de crear usuario
    var heightBody = $(window).height() - 45;
    $('#registerModal #form-register-person, #registerCompanyModal #form-register-company').css({
      'height' : heightBody
    });
  }


  //---------------------------------------------------------------------
  //------------------------ START PAGE HOME --------------------------
  //---------------------------------------------------------------------
  if ($('body').hasClass('page-home')) {
    
    // ------- VER OFERTA DESDE HOME
    var urlCurrent = '';
    $('.wrapper-oferts-home').on('click', '.show-ofert', function() {
      var idOfert = $(this).attr('data-ofert');

      var urlParams = document.URL.split('?'),
          objDato = new Date(),
          timeCurrent = objDato.getTime().toString(),
          page = 'offer',
          urlViewOffer = '/views/offer.php?id=' + timeCurrent + idOfert,
          tokenPage = $('#tokenPage').val();

          if (urlParams[1] == null) {
            urlParamsValidate = '';
          }else {
            urlParamsValidate = '?' + urlParams[1];
          }
          
      urlCurrent = document.location.pathname + urlParamsValidate;
  
      changeUrl(page, urlViewOffer);
      getFullDataOfertSelectedPerson(idOfert, tokenPage, $(this));
      callbackChangeUrlAndRedirect('home', urlCurrent);
    });

    // --------- APLICAR A OFERTA --------
    $('#ofert-selected-modal-person').on('click', '#ofert-apply', function() {
      idOfert = $('#form-apply-operations').find('#t0k3n_1d_0').val();
      var tokenPage = $('#tokenPage').val();
      applyOfert(idOfert, tokenPage, $(this));
    });

    // --------- DESAPLICAR A OFERTA --------
    $('#ofert-selected-modal-person').on('click', '#ofert-disapply', function() {
      idOfert = $('#form-apply-operations').find('#t0k3n_1d_0').val();
      var tokenPage = $('#tokenPage').val();
      disapplyOfert(idOfert, tokenPage, $(this)); 
    });

    // --------- TOKEN LOGIN/RECOVER PERSON - page search --------
    $('#ofert-selected-modal-person').on('click', '#login-modal', function() {
      $( "#tokenRol" ).html('<input type="hidden" name="loginToken" value="loginPerson">');
      $( "#tokenRolRecover" ).html('<input type="hidden" name="typeUser" value="PI">');
    });
  }
  //--------------------------------------------------------------------
  //------------------------- /END PAGE HOME ---------------------------
  //--------------------------------------------------------------------


  //--------------------------------------------------------------------------
  //---------------------- START PAGE SEARCH OFERTS ------------------------
  //--------------------------------------------------------------------------
  if ($('body').hasClass('page-search')) {

    var is_loading = false, // initialize is_loading by false to accept new loading
        page = 0,
        total_pages = $('#total_pages').val(),
        windowHeight = $(window).height();
    
    // Inputs Filters
    var itSearch  = $('#it_search').val(),
        filters   = 'NO',
        selCity   = $('#selCity').val(),
        selDate   = $('#selDate').val(),
        selGenre  = $('#selGenre').val(),
        selSalary = $('#selSalary').val();
        tokenPage = $('#tokenPage').val();

    if (selCity != 'all' || selDate != 'all' || selGenre != 'all' || selSalary != 'all') {
      filters = 'YES';
    }
    
    // Carga inicial de Ofertas!
    if (total_pages > 0) {
      loadOfertsScroll(page, itSearch, filters, selCity, selDate, selGenre, selSalary, tokenPage);

      // Cargar mas ofertas al hacer scroll
      $(window).scroll(function() {
        if(page <= total_pages) {

          if($(window).scrollTop() + windowHeight - 100 <= $(document).height() &&
            $(window).scrollTop() + windowHeight + 100 >= $(document).height()
            ) {

            if (is_loading == false) { // stop loading many times for the same page
              // set is_loading to true to refuse new loading
              is_loading = true;

              $('.block-load-ajax').show(); //show loading image
              
              setTimeout(function(){
                loadOfertsScroll(page, itSearch, filters, selCity, selDate, selGenre, selSalary, tokenPage);
              }, 1000);
            }
          }
        }else {
          $('.end-oferts').show('slow');
        }
      });
    }

    // Get Offers
    function loadOfertsScroll(page, itSearch, filters, selCity, selDate, selGenre, selSalary, tokenPage) {
      $.ajax({
        url: '../../controls/control_load_oferts.php',
        type: 'POST',
        data: { 
          id_load : 'LOAD_OFERTS',
          page : page,
          it_search : itSearch,
          filters   : filters,
          selCity   : selCity,
          selDate   : selDate,
          selGenre  : selGenre,
          selSalary  : selSalary,
          tokenPage  : tokenPage
        }, 
        beforeSend: function(){
          $('.block-load-ajax').show();
        },
        success: function(data){
          $("#result_oferts").append(data).fadeIn('slow');
          is_loading = false;
          sumNextPage(); 
        },   
        complete: function(){
          $(".block-load-ajax").hide();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          alert("Status: " + textStatus + "Error: " + errorThrown); 
        }
      });
    }

    // Siguiente Pagina de ofertas
    function sumNextPage() {
      return page++;
    }

    var idOfert = null,
        urlCurrent = '';

    // VER OFERTA DESDE PERFIL PERSON
    $('#result_oferts').on('click', '.show-ofert', function() {
      
      idOfert = $(this).parent().find('#t0k3n_1d_0').val();
      var urlParams = document.URL.split('?'),
          objDato = new Date(),
          timeCurrent = objDato.getTime().toString(),
          page = 'offer',
          urlViewOffer = '/views/offer.php?id=' + timeCurrent + idOfert,
          tokenPage = $('#tokenPage').val();

      urlCurrent = document.location.pathname + '?' + urlParams[1];
      
      changeUrl(page, urlViewOffer);
      getFullDataOfertSelectedPerson(idOfert, tokenPage, $(this));
      callbackChangeUrlAndRedirect('search', urlCurrent);
    });

    // APLICAR A OFERTA
    $('#ofert-selected-modal-person').on('click', '#ofert-apply', function() {
      var tokenPage = $('#tokenPage').val();
      applyOfert(idOfert, tokenPage, $(this));
    });

    // DESAPLICAR A OFERTA
    $('#ofert-selected-modal-person').on('click', '#ofert-disapply', function() {
      var tokenPage = $('#tokenPage').val();
      disapplyOfert(idOfert, tokenPage, $(this)); 
    });

    // TOKEN LOGIN/RECOVER PERSON - page search
    $('#ofert-selected-modal-person').on('click', '#login-modal', function() {
      $( "#tokenRol" ).html('<input type="hidden" name="loginToken" value="loginPerson">');
      $( "#tokenRolRecover" ).html('<input type="hidden" name="typeUser" value="PI">');
    });
  }
  //----------------------------------------------------------------
  //------------------- /END PAGE SEARCH OFERTS -------------------
  //----------------------------------------------------------------



  //----------------------------------------------------------------
  //------------------- START PAGE PROFILE COMPANY ----------------
  //----------------------------------------------------------------
  if ($('body').hasClass('page-profile')) {

    //activar tabs de crear oferta y lista de ofertas creadas
    $(".btn-pref .btn").click(function () {
        $(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
        $(this).removeClass("btn-default").addClass("btn-primary"); 
    });

    // VER OFERTA DESDE PERFIL COMPANY
    $('#table-ofert-created').on('click', '.show-ofert', function() {
      var idOfert   = $(this).attr('data-ofert'),
          tokenPage = $('#tokenPage').val();
      getFullDataOfertSelected(idOfert, tokenPage, $(this));
    });

    // ELIMINAR OFERTA
    $('#ofert-selected-modal').on('click', '#btn-remove-offer', function() {
      var confirmUnpublish = confirm("Te recordamos que una ves finalizada tu oferta no se podra volver hacer uso de ella \n ¿Vas a finalizar la oferta, estas seguro?");
      
      if(confirmUnpublish === true) {
        $('#form-remove-offer').submit();
      }
    });

    // // DESPUBLICAR OFERTA MODAL
    // $('#ofert-selected-modal').on('click', '.btn-unpublish', function() {
    //   var confirmUnpublish = confirm("Te recordamos que una ves finalizada tu oferta no se podra volver hacer uso de ella \n ¿Vas a finalizar la oferta, estas seguro?");
      
    //   if(confirmUnpublish === true) {
    //     var idOfertUn = $('#t0k3n_1d_0').val(),
    //         tokenPage = $('#tokenPage').val();
    //     unpublishOfertSelected(idOfertUn, tokenPage);
    //   }
    // });

    // // PUBLICAR OFERTA MODAL
    // $('#ofert-selected-modal').on('click', '.btn-publish', function() {
    //   var idOfertUn = $('#t0k3n_1d_0').val(),
    //       tokenPage = $('#tokenPage').val();
    //   publishOfertSelected(idOfertUn, tokenPage);
    // });

    // VER PERSONAS QUE APLICARON A LA OFERTA 
    $('#table-ofert-created').on('click', '.show-ofert-apply', function() {
      var idOfertApply   = $(this).attr('data-ofert'),
          nameOfertApply = $(this).attr('data-name-ofert'),
          tokenPage      = $('#tokenPage').val();
      getListApplyOfertSelected(idOfertApply, nameOfertApply, tokenPage);
    });

    // VER DATOS DE LA PERSONA 
    $('#modal-ofert-apply').on('click', '.show-person', function() {

      var idPerson     = $(this).attr('data-person'),
          personStatus = $(this).attr('data-person-status'),
          tokenPage    = $('#tokenPage').val(),
          idTab       = $(this).parents(".tab-pane").attr('id');

      getFullDataPerson(idPerson, tokenPage, idTab);
      
      if (personStatus == 'SELECTED') {
        setTimeout(function(){
          $( '#modal-person-selected .modal-content #person-selected #btn-selected-person' ).remove();
        }, 210);
      }else  if (personStatus == 'DISCARDED') {
        setTimeout(function(){
          $( '#modal-person-selected .modal-content #person-selected #btn-discard-person' ).remove();
        }, 210);
      }
    });

    // SELECCIONADO PERSONA 
    $('#modal-person-selected').on('click', '#btn-selected-person', function() {
      //$(this).attr("disabled", true);

      var confirmContact = confirm("Confirma la persona seleccionada");

      if(confirmContact === true) {
        var idOfert   = $('#id-ofert-selected').val(),
            idPerson  = $(this).attr('data-person-selected'),
            tokenPage = $('#tokenPage').val(),
            idTab     = $(this).siblings('.id-tab').val();

        selectedPersonForOfert(idOfert, idPerson, tokenPage, idTab, $(this));
      }else {
        //$(this).attr("disabled", false);
      }
    });

    // DESCARTAR PERSONA 
    $('#modal-person-selected').on('click', '#btn-discard-person', function() {
      //$(this).attr("disabled", true);
      var confirmContact = confirm("¿Quieres que lo descartemos?");

      if(confirmContact === true) {
        var idOfert   = $('#id-ofert-selected').val(),
            idPerson  = $(this).attr('data-person-discard'),
            tokenPage = $('#tokenPage').val(),
            idTab     = $(this).siblings('.id-tab').val();

        discardPersonForOfert(idOfert, idPerson, tokenPage, idTab, $(this));
      }else {
        //$(this).attr("disabled", false);
      }
    });

    // muestra el logo de la empresa al pasar el cursor por el nombre
    if ($('body').hasClass('page-edit-profile-company')) {
      $(".form-edit-profile .text-show-logo").hover(function(){
        $('.form-edit-profile .logo-company').fadeIn(500);
      }, function(){
        $('.form-edit-profile .logo-company').fadeOut(300);
      });
    }

    // agrega al buscador el nombre de la oferta en la tabla de ofertas creadas
    if ($('.hidden_name_offer').length) {
      var query_element = '.search';
      var varInterval = setInterval(function(){
        if(validateExistingElementInterval(query_element)) {
          $('input[type="text"][placeholder="Buscar"]')
            .val($('.hidden_name_offer').val())
            .trigger('keyup');
        }
      }, 250); // 10000ms = 10s
    }



  }
  //------------------------------------------------------------------
  //------------------ /END PAGE PROFILE COMPANY --------------------
  //------------------------------------------------------------------



  //-------------------------------------------------------------------------
  //------------------- START PAGE EDIT PROFILE COMPANY ---------------------
  //-------------------------------------------------------------------------
  if ($('body').hasClass('page-edit-profile-company')) {

    // muestra el logo de la empresa al pasar el cursor por el nombre
    if ($('body').hasClass('page-edit-profile-company')) {
      $(".form-edit-profile .text-show-logo").hover(function(){
        $('.form-edit-profile .logo-company').fadeIn(500);
      }, function(){
        $('.form-edit-profile .logo-company').fadeOut(300);
      });
    }
  }
  //----------------------------------------------------------------------
  //---------------- /END PAGE EDIT PROFILE COMPANY ----------------------
  //----------------------------------------------------------------------



  //---------------------------------------------------------------------
  //----------------- START PAGE PROFILE PERSON ------------------------
  //---------------------------------------------------------------------
  if ($('body').hasClass('page-profile-person')) {
    var idOfert;
    //  VER OFERTA DESDE PROFILE PERSON  
    $('#result-ofert-apply').on('click', '.show-ofert-apply', function() {
      idOfert = $(this).parent().find('#t0k3n_1d_0').val();
      var tokenPage = $('#tokenPage').val();
      getFullDataOfertSelectedPerson(idOfert, tokenPage, $(this));
    });

    // DESAPLICAR A OFERTA
    $('#ofert-selected-modal-person').on('click', '#ofert-disapply', function() {
      var tokenPage = $('#tokenPage').val();
      disapplyOfert(idOfert, tokenPage, $(this)); 
      setTimeout(function() {
        window.location.href = "/views/profile_person.php";
      },2800);
    });


  }
  //--------------------------------------------------------------------
  //------------------ /END PAGE PROFILE PERSON -----------------------
  //--------------------------------------------------------------------



  //--------------------------------------------------------------------
  //----------- PAGE PROFILE PERSON AND EDIT PROFILE COMPANY -----------
  //--------------------------------------------------------------------
  if ($('body').is('.page-edit-profile-company, .page-profile-person')) {
    $('.input-group-addon').on('click', '.delete-account', function() {
      $('#show-modal-confirm-delete-user').trigger("click");
    });

    $('#modal-confirm-delete-user .input-group-addon').on('click', '.submit-confirm-delete-user', function() {
      $(this).parent().find('.animation_image').show();
    });
    
  }
  //--------------------------------------------------------------------
  //----------- /END PROFILE PERSON AND EDIT PROFILE COMPANY -----------
  //--------------------------------------------------------------------




  //--------------------------------------------------------
  //----------------- START PAGE OFFER --------------------
  //--------------------------------------------------------
  if ($('body').hasClass('page-offer')) {
    // TOKEN LOGIN/RECOVER PERSON - Page Offer
    $('.wrap-view-offer').on('click', '#login', function() {
      $( "#tokenRol" ).html('<input type="hidden" name="loginToken" value="loginPerson">');
      $( "#tokenRolRecover" ).html('<input type="hidden" name="typeUser" value="PI">');
    });

    // APLICAR A OFERTA DESDE LA PAGINA OFERTA
    $('.page-offer').on('click', '#ofert-apply', function() {
      idOfert = $('#form-apply-operations').find('#t0k3n_1d_0').val();
      var tokenPage = $('#tokenPage').val();
      applyOfert(idOfert, tokenPage, $(this));
    });

    // DESAPLICAR A OFERTA DESDE LA PAGINA OFERTA
    $('.page-offer').on('click', '#ofert-disapply', function() {
      idOfert = $('#form-apply-operations').find('#t0k3n_1d_0').val();
      var tokenPage = $('#tokenPage').val();
      disapplyOfert(idOfert, tokenPage, $(this));
    });
  }
  //--------------------------------------------------------------
  //-------------------- /END PAGE OFFER ------------------------
  //--------------------------------------------------------------


});





//-----------------------------------------------------------------
//-----------------------------------------------------------------
// * * * * * * ** * * /END document.ready CARGA DE LA PAGINA * * * * * * * * * *
//-----------------------------------------------------------------
//-----------------------------------------------------------------







 function callbackChangeUrlAndRedirect(page, urlCurrent){
  $('#ofert-selected-modal-person').on('click', 'button.close', function() {  
    changeUrl(page, urlCurrent);
  });

  $('#ofert-selected-modal-person').on('click', 'button.btn-close-modal', function() {    
    changeUrl(page, urlCurrent);
  });

  // redirecciona cuando se inicia sesion en modal oferta home
  $('#loginModal').on('click', '#submitLgnRgt', function() {    
    $('.redirect').val(document.URL);
  });

  // redirecciona cuando se olvida la contrasenia en modal oferta home
  $('#loginModal').on('click', '#submitRecoverPassword', function() {    
    $('.redirect').val(document.URL);
  });

  // redirecciona cuando se registra una persona en modal oferta home
  $('#registerModal').on('click', '#submitRegisterPerson', function() {    
    $('.redirect').val(document.URL);
  });
}

// (((((((( NO ESTA EN USO ))))))))
// function addEventLoadTypeProfession() {
//   var tokenPage = $('#tokenPage').val(); 
     
//   $('.selTypeProfession .sel-type-profession ').selectpicker('refresh');
//   $('.selTypeProfession ul.dropdown-menu').children().addClass('item item-profession');
//   $('.selTypeProfession ul.dropdown-menu').on('click', '.item-profession', function() {
//     var professionSelected = parseInt($(this).attr( 'data-original-index' ));
//     //console.log('dato ' + professionSelected);
//     $.ajax({
//       url: '../../controls/control_utilities_callback_ajax.php',
//       type: 'POST',
//       data: {
//         id_request : 'LOAD_LIST_PROFESSION',
//         id_profession : professionSelected,
//         tag_html : 'select',
//         tokenPage : tokenPage
//       }, 
//       beforeSend: function(){
//         $('.animation_image').show();
//       },
//       success: function(data){
//         $(' .load-list-profession ').html(data);
//         $(' .sel-list-profession ').selectpicker('refresh');
//       },   
//       complete: function(){
//         $(".animation_image").hide();
//       },
//       error: function(XMLHttpRequest, textStatus, errorThrown) {
//         alert("Status: " + textStatus + "Error: " + errorThrown); 
//       }
//     });
//   });
// }


// *****************************************
// ---Valida ingreso de datos solo Numericos
function validateOnlyNumbers(e){
  if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
    return false;
  }
  return true;
}

// *****************************************
// --- CARGA LA OFERTA SELECCCIONADA EN EL MODAL
function getFullDataOfertSelected(idOfert, tokenPage, element) {
  $.ajax({
    url: '../../controls/control_load_oferts.php',
    type: 'POST',
    data: { 
      id_load : 'LOAD_OFERT_SELECTED_MODAL',
      id_ofert : idOfert,
      tokenPage : tokenPage
    }, 
    beforeSend: function(){
      element.parent().find('.animation_image').show();
    },
    success: function(data){
      $( "#ofert-selected-modal .modal-content" ).html(data);
      $(' .sel-list-city ').selectpicker('refresh');
      // Agrega  evento clic a lista de profesiones en el Modal
    },   
    complete: function(){
      element.parent().find('.animation_image').hide();
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      alert("Status: " + textStatus + "Error: " + errorThrown); 
    }
  });
}

// *****************************************
// --- DESPUBLICAR OFERTA SELECCCIONADA
// function unpublishOfertSelected(idOfertUn, tokenPage) {
//   $.ajax({
//     url: '../../controls/control_profile_company_operations.php',
//     type: 'POST',
//     data: { 
//       profileCompanyToken : 'unpublishOfert',
//       id_ofert : idOfertUn,
//       tokenPage : tokenPage
//     }, 
//     beforeSend: function(){
//       $('.form-operations .animation_image').show();
//     },
//     success: function(data){
//       $('.wrapper-messages-system').append(data).fadeIn(1500);
//       $('#ofert-selected-modal').modal('hide');
//       $('html, body').animate({
//         scrollTop: $('.wrapper-messages-system').position().top - 30
//       }, 450);
      
//       setTimeout(function() {
//         $(".wrapper-messages-system").fadeOut(4000);
//       }, 4000);

//       setTimeout(function() {
//         $(".wrapper-messages-system div").remove();
//       }, 4500);
//     },   
//     complete: function(){
//       $(".form-operations .animation_image").hide();
//     },
//     error: function(XMLHttpRequest, textStatus, errorThrown) {
//       alert("Status: " + textStatus + "Error: " + errorThrown); 
//     }
//   });
// }

// *****************************************
// --- PUBLICAR OFERTA SELECCCIONADA
// function publishOfertSelected(idOfertUn, tokenPage) {
//   $.ajax({
//     url: '../../controls/control_profile_company_operations.php',
//     type: 'POST',
//     data: { 
//       profileCompanyToken : 'publishOfert',
//       id_ofert : idOfertUn,
//       tokenPage : tokenPage
//     }, 
//     beforeSend: function(){
//       $('.form-operations .animation_image').show();
//     },
//     success: function(data){
//       $('.wrapper-messages-system').append(data).fadeIn(1000);
//       $('#ofert-selected-modal').modal('hide');
//       $('html, body').animate({
//         scrollTop: $('.wrapper-messages-system').position().top - 30
//       }, 450);
      
//       setTimeout(function() {
//         $(".wrapper-messages-system").fadeOut(4000);
//       }, 4000);

//       setTimeout(function() {
//         $(".wrapper-messages-system div").remove();
//       }, 4500);
//     },   
//     complete: function(){
//       $(".form-operations .animation_image").hide();
//      },
//      error: function(XMLHttpRequest, textStatus, errorThrown) {
//       alert("Status: " + textStatus + "Error: " + errorThrown); 
//     }
//   });
// }

// Trae y construye modal con la info de la oferta
function getFullDataOfertSelectedPerson(idOfert, tokenPage, element) {
  $.ajax({
    url: '../../controls/control_load_oferts.php',
    type: 'POST',
    data: { 
      id_load : 'LOAD_OFERT_SELECTED_MODAL_PERSON',
      id_ofert : idOfert,
      tokenPage : tokenPage
    }, 
    beforeSend: function(){
      element.parent().find('.animation_image').show();
    },
    success: function(data){
      $( "#ofert-selected-modal-person .modal-content" ).html(data);
    },   
    complete: function(){
      element.parent().find('.animation_image').hide();
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      alert("Status: " + textStatus + "Error: " + errorThrown); 
    }
  });
}

// Aplicar a Oferta
function applyOfert(idOfert, tokenPage, element) {
  $.ajax({
    url: '../../controls/control_ofert_operations.php',
    type: 'POST',
    data: { 
      id_operation : 'APPLY_OFERT',
      id_ofert : idOfert,
      tokenPage : tokenPage
    }, 
    beforeSend: function(){
      element.parent().find('.animation_image').show();
    },
    success: function(data){
      if (data.trim() == 'APPLIED_OK') {
        $( '#form-apply-operations #ofert-apply' ).remove();
        $( '#form-apply-operations .wrapper-btn-operation-ofert' ).html('<p class="message"><span class="glyphicon glyphicon-ok"></span> Aplicaste a la oferta, ahora solo tienes que esperar que la empresa te contacte</p>').show('slow');
      }else {
        $( '#form-apply-operations .wrapper-btn-operation-ofert' ).html('<p class="message"><span class="glyphicon glyphicon-remove"></span> Ha ocurrido un error al Aplicar a la oferta, por favor intentalo mas tarde.</p>').show('slow');
      }
    },   
    complete: function(){
      element.parent().find('.animation_image').hide();
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      alert("Status: " + textStatus + "Error: " + errorThrown); 
    }
  });
}

// Desaplicar a oferta
function disapplyOfert(idOfert, tokenPage, element) {
  $.ajax({
    url: '../../controls/control_ofert_operations.php',
    type: 'POST',
    data: { 
      id_operation : 'DISAPPLY_OFERT',
      id_ofert : idOfert,
      tokenPage : tokenPage
    }, 
    beforeSend: function(){
      element.parent().find('.animation_image').show();
    },
    success: function(data){
      if (data.trim() == 'DISAPPEARED_OK') {
        $( '#form-apply-operations #ofert-disapply' ).remove();
        $( '#form-apply-operations  .wrapper-btn-operation-ofert' ).html('<p class="message"><span class="glyphicon glyphicon-ok"></span> Has desaplicado; esperamos que encuentres otra oferta</p>').show('slow');
      }else {
        $( '#form-apply-operations  .wrapper-btn-operation-ofert' ).html('<p class="message"><span class="glyphicon glyphicon-remove"></span> Ha ocurrido un error al Desaplicar a la oferta, por favor intentalo mas tarde.</p>').show('slow');
      }
    },   
    complete: function(){
      element.parent().find('.animation_image').hide();
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      alert("Status: " + textStatus + "Error: " + errorThrown); 
    }
  });
}

// CARGA LISTA DE APLICADOS A LA OFERTA SELECCIONADA
function getListApplyOfertSelected(idOfertApply, nameOfertApply, tokenPage) {
  $.ajax({
    url: '../../controls/control_profile_company_operations.php',
    type: 'POST',
    data: { 
      profileCompanyToken : 'LOAD_LIST_APPLY_OFERT',
      id_ofert : idOfertApply,
      tokenPage : tokenPage
    }, 
    beforeSend: function(){
      $('.animation_image').show();
    },
    success: function(data){
      $( "#modal-ofert-apply .modal-content #list-apply-ofert" ).html(data);
      $( '#modal-ofert-apply .modal-title' ).text('Oferta: ' + nameOfertApply);
      $( '#modal-ofert-apply .modal-title' ).append('<input type="hidden" value=' + idOfertApply + ' id="id-ofert-selected" />');
    },   
    complete: function(){
      $(".animation_image").hide();
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      alert("Status: " + textStatus + "Error: " + errorThrown); 
    }
  });
}

// *****************************************
// --- CARGA LA INFORMACION DE LA PERSONA EN EL MODAL
function getFullDataPerson(idPerson, tokenPage, idTab) {
  $.ajax({
    url: '../../controls/control_profile_company_operations.php',
    type: 'POST',
    data: { 
      profileCompanyToken : 'LOAD_FULL_DATA_PERSON',
      id_person : idPerson,
      tokenPage : tokenPage,
      id_tab : idTab
    }, 
    beforeSend: function(){
      $('.animation_image').show();
    },
    success: function(data){
      $( "#modal-person-selected .modal-content #person-selected" ).html(data);
     },   
     complete: function(){
      $(".animation_image").hide();
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      alert("Status: " + textStatus + "Error: " + errorThrown); 
    }
  });
}

// *****************************************
// --- SELECCIONADO LA PERSONA QUE APLICO A LA OFERTA
function selectedPersonForOfert(idOfert, idPerson, tokenPage, idTab, element) {

  $.ajax({
    url: '../../controls/control_profile_company_operations.php',
    type: 'POST',
    data: { 
      profileCompanyToken : 'SELECTED_PERSON_FOR_OFFER',
      id_ofert : idOfert,
      id_person : idPerson,
      tokenPage : tokenPage
    }, 
    beforeSend: function(){
      element.parent().find('.animation_image').show();
    },
    success: function(data){
      var dataSelected = JSON.parse(data);

      if(dataSelected.status_mail == 'EMAIL_SEND') {

        switch(idTab) {
          case 'tab1-ofert-apply':
            var less = parseInt($('.badge-applied').text()) - 1;
            $('.badge-applied').text(less);
            var more = parseInt($('.badge-selected').text()) + 1;
            $('.badge-selected').text(more);
            break;

          case 'tab2-ofert-apply':
            var less = parseInt($('.badge-selected').text()) - 1;
            $('.badge-selected').text(less);
            var more = parseInt($('.badge-discarded').text()) + 1;
            $('.badge-discarded').text(more);
            break;

          case 'tab3-ofert-apply':
            var less = parseInt($('.badge-discarded').text()) - 1;
            $('.badge-discarded').text(less);
            var more = parseInt($('.badge-selected').text()) + 1;
            $('.badge-selected').text(more);
            break;
        }

        var rowPerson = $('.row-ofert-' + idOfert + '-person-' + idPerson + '')[0].outerHTML;
        $('.row-ofert-' + idOfert + '-person-' + idPerson + '').remove();

        $('#tab2-ofert-apply .wrapper-rows-person').append(rowPerson); 
        $('#tab2-ofert-apply .wrapper-rows-person .without-results').remove();
        $('.show-person-' + idPerson).attr('data-person-status', 'SELECTED');
      }

      $('#modal-ofert-apply .wrapper-messages-system-ajax').append(dataSelected.message_system_ajax).fadeIn(1000);
      $('#modal-person-selected').modal('hide');
      
      setTimeout(function() {
        $("#modal-ofert-apply .wrapper-messages-system-ajax").fadeOut(7000);
      }, 10000);

      setTimeout(function() {
        $("#modal-ofert-apply .wrapper-messages-system-ajax div").eq(0).remove();
      }, 11000);
    },   
    complete: function(){
      element.parent().find('.animation_image').show();
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      alert("Status: " + textStatus + "Error: " + errorThrown); 
    }
  });
}

// *****************************************
// DESCARTA A LA PERSONA QUE APLICO A LA OFERTA
function discardPersonForOfert(idOfert, idPerson, tokenPage, idTab, element) {
  $.ajax({
    url: '../../controls/control_profile_company_operations.php',
    type: 'POST',
    data: { 
      profileCompanyToken : 'DISCARDED_PERSON_FOR_OFERT',
      id_ofert : idOfert,
      id_person : idPerson,
      tokenPage : tokenPage
    }, 
    beforeSend: function(){
      element.parent().find('.animation_image').show();
    },
    success: function(data){
      var dataDiscarded = JSON.parse(data);

      if(dataDiscarded.status_mail == 'EMAIL_SEND') {
        $('#modal-person-selected').modal('hide');

         switch(idTab) {
          case 'tab1-ofert-apply':
            var less = parseInt($('.badge-applied').text()) - 1;
            $('.badge-applied').text(less);
            var more = parseInt($('.badge-selected').text()) + 1;
            $('.badge-selected').text(more);
            break;

          case 'tab2-ofert-apply':
            var less = parseInt($('.badge-selected').text()) - 1;
            $('.badge-selected').text(less);
            var more = parseInt($('.badge-discarded').text()) + 1;
            $('.badge-discarded').text(more);
            break;

          case 'tab3-ofert-apply':
            var less = parseInt($('.badge-discarded').text()) - 1;
            $('.badge-discarded').text(less);
            var more = parseInt($('.badge-selected').text()) + 1;
            $('.badge-selected').text(more);
            break;
        }

        var rowPerson = $('.row-ofert-' + idOfert + '-person-' + idPerson + '')[0].outerHTML;

        $('.row-ofert-' + idOfert + '-person-' + idPerson + '').remove();
        $('#tab3-ofert-apply .wrapper-rows-person').append(rowPerson); 
        $('#tab3-ofert-apply .wrapper-rows-person .without-results').remove();
        $('.show-person-' + idPerson).attr('data-person-status', 'DISCARDED');
      }

      $('#modal-ofert-apply .wrapper-messages-system-ajax').append(dataDiscarded.message_system_ajax).fadeIn(1000);
      $('#modal-person-selected').modal('hide');
      
      setTimeout(function() {
        $("#modal-ofert-apply .wrapper-messages-system-ajax").fadeOut(7000);
      }, 10000);

      setTimeout(function() {
        $("#modal-ofert-apply .wrapper-messages-system-ajax div").eq(0).remove();
      }, 11000);
    },   
    complete: function(){
      element.parent().find('.animation_image').show();
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      alert("Status: " + textStatus + "Error: " + errorThrown); 
    }
  });
}


//-------------------------------------------------------------------------------
//--------------- INICIO PAGE PROFILE COMPANY VALIDATE OFERT EDIT ---------------
//-------------------------------------------------------------------------------
if ($('body').hasClass('page-profile-company')) {

  // validando formulario editar oferta
  function editOfertValidate() {
    $('.error-edit-ofert, .error-edit-ofert-html').hide();
    var validateName     = false,
        validateDesc     = false,
        validateKowl     = false,
        validateCity     = false,
        validateTypeProf = false,
        validateValue    = false;

    // * * * *  * * * * * * * ** * * * 
    // * Validar Nombre de la Oferta *
    // * * * * * * * * * * * * * * * *
    // Valores para nombre de la oferta
    var nameOfert = {
          element: $('#form-remove-offer #edit-name-ofert'), 
          elementError: 'edit-name-ofert-error',
          min: 10,
          max: 100,
          msgEmpty: '(*) Ingresa un nombre para la oferta.',
          msgMin: 'Ingresa un nombre mas mayor a 10 caracteres.',
          msgMax: 'El nombre es demasiado largo, no puedes exceder los 100 caracteres.'
        };
    
    validateName = validate_input_empty(nameOfert["element"], nameOfert["elementError"], nameOfert["msgEmpty"]);

    // Validar minimo de caracteres
    if (validateName == true) {
      validateName = validate_input_length(nameOfert["element"], nameOfert["elementError"], nameOfert["msgMin"], nameOfert["min"], 'min');

      // Validar maximo de caracteres
      if (validateName == true) {
        validateName = validate_input_length(nameOfert["element"], nameOfert["elementError"], nameOfert["msgMax"], nameOfert["max"], 'max');
      }
    }

    // * * * *  * * * * * * * ** * * * * * *
    // * Validar Descripcion de la Oferta *
    // * * * * * * * * * * * * * * * * * * *
    // valores para nombre de la oferta
    var descOfert = {
          element: $('#form-remove-offer #edit-description-ofert'), 
          elementError: 'edit-descripcion-ofert-error',
          min: 15,
          max: 500,
          msgEmpty: '(*) Ingresa una descripcion para la oferta.',
          msgMin: 'Ingresa una descripcion mas larga.',
          msgMax: 'La descripcion es demasiada larga, no puedes exceder los 500 caracteres.'
        };

    validateDesc = validate_input_empty(descOfert["element"], descOfert["elementError"], descOfert["msgEmpty"]);

    // Validar minimo de caracteres
    if (validateDesc == true) {
      validateDesc = validate_input_length(descOfert["element"], descOfert["elementError"], descOfert["msgMin"], descOfert["min"], 'min');

      // Validar maximo de caracteres
      if (validateDesc == true) {
        validateDesc = validate_input_length(descOfert["element"], descOfert["elementError"], descOfert["msgMax"], descOfert["max"], 'max');
      }
    }


    // * * * *  * * * * * * * ** * * * * * *
    // * Validar Conocimientos de la Oferta *
    // * * * * * * * * * * * * * * * * * * *
    // valores para nombre de la oferta
    var kowlOfert = {
          element: $('#form-remove-offer #edit-kowledge-ofert'), 
          elementError: 'edit-kowledge-ofert-error',
          min: 15,
          max: 500,
          msgEmpty: '(*) Ingresa una descripcion para la oferta.',
          msgMin: 'Ingresa Conocimientos Requeridos mas largos.',
          msgMax: 'Los Conocimientos Requeridos es demasiado larga, no puedes exceder los 500 caracteres.'
        };

    validateKowl = validate_input_empty(kowlOfert["element"], kowlOfert["elementError"], kowlOfert["msgEmpty"]);

    // Validar minimo de caracteres
    if (validateKowl == true) {
      validateKowl = validate_input_length(kowlOfert["element"], kowlOfert["elementError"], kowlOfert["msgMin"], kowlOfert["min"], 'min');

      // Validar maximo de caracteres
      if (validateKowl == true) {
        validateKowl = validate_input_length(kowlOfert["element"], kowlOfert["elementError"], kowlOfert["msgMax"], kowlOfert["max"], 'max');
      }
    }


    // * * * *  * * * * * * * ** * * * * * *
    // * Validar Ciudad de la Oferta *
    // * * * * * * * * * * * * * * * * * * *
    // valores para nombre de la oferta
    var cityOfert = {
          element: $('#form-remove-offer .sel-list-city ul.dropdown-menu .selected'), 
          elementError: 'edit-city-ofert-error',
          msgRequired: '(*) Selecciona la ciudad a la que aplique la oferta.'
        };

    validateCity = validate_select_bootstrap(cityOfert["element"], cityOfert["elementError"], cityOfert["msgRequired"], false);
  

    // // (((((((( NO ESTA EN USO ))))))))
    // // * * * *  * * * * * * * ** * * * * * *
    // // * Validar Tipo de Profesion de la Oferta *
    // // * * * * * * * * * * * * * * * * * * *
    // // valores para nombre de la oferta
    // var typeProfOfert = {
    //       element: $('#form-remove-offer .sel-type-profession ul.dropdown-menu .selected'), 
    //       elementError: $('#form-remove-offer .selTypeProfession-modal div.sel-type-profession'),
    //       msgRequired: '(*) Selecciona el tipo de profesion a la que aplica la oferta.'
    //     };

    // validateTypeProf = validate_select_bootstrap(typeProfOfert["element"], typeProfOfert["elementError"], typeProfOfert["msgRequired"], true);
  

    // * * * *  * * * * * * * ** * * * 
    // * Validar Salario de la Oferta *
    // * * * * * * * * * * * * * * * *
    // valores para nombre de la oferta
    var valueOfert = {
          element: $('#form-remove-offer #edit-value-ofert'), 
          elementError: 'edit-value-ofert-error',
          msgEmpty: '(*) Ingrese el salario de la oferta, deja en cero (0) si es reservado.',
        };
    
    validateValue = validate_input_empty(valueOfert["element"], valueOfert["elementError"], valueOfert["msgEmpty"]);

    if(validateName === true && validateDesc === true && 
      validateKowl === true && validateCity === true && 
      validateTypeProf === true && validateValue === true ) {
      $('#form-remove-offer').submit();
    }  
  }
}
//-----------------------------------------------------------------------
//------------- /END PAGE PROFILE COMPANY VALIDATE OFERT EDIT ---------
//-----------------------------------------------------------------------


function validate_input_empty(element_input, element_error, message) {
  if(element_input.val().trim() === '') {
    element_input.focus();
    $('#' + element_error).text(message).show();
    return false;
  }
  return true;
}

function validate_input_length(element_input, element_error, message, length, control) {

  if (control == 'max') {
    if (element_input.val().length > length) {
      element_input.focus();
      $('#' + element_error).text(message).show();
      return false;
    } 
  }else {
    if (element_input.val().length < length) {
      element_input.focus();
      $('#' + element_error).text(message).show();
      return false;
    } 
  }
  
  return true;
}

function validate_select_bootstrap(element_input, element_error, message, html) {
  if (parseInt(element_input.attr('data-original-index')) == 0) {

    if (html == false) {
      element_input.focus();
      $('#' + element_error).text(message).show();
    }else {
      element_error.append('<label id="edit-type-preofession-ofert-error" class="error error-edit-ofert-html" for="selTypeProfession">' + message + '</label>');
    }
    return false;
  }
  return true;
}

// Trae y construye modal contenido normal
function getContentNormalModal(idContent, element) {

  var urlContent = '';
  switch(idContent) {
    case 'terms-conditions':
      urlContent = '/views/info/modal/terms_conditions.php'
      break;
    // case 'terms-conditions-company':
    //   urlContent = '/views/info/modal/terms_conditions_company.php'
    //   break; 
    // default:
      urlContent = 'NONE';
  }

  if (urlContent != 'NONE') {
    $.ajax({
      url: urlContent,
      beforeSend: function(){
        element.parent().find('.animation_image').show();
      },
      success: function(data){
        //console.log(data);
        $( "#modal-content-normal .modal-content" ).html(data);
      },   
      complete: function(){
        element.parent().find('.animation_image').hide();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus + "Error: " + errorThrown); 
      }
    });
  }
}

function changeUrl(page, urlViewOffer) {
  if (typeof (history.pushState) != "undefined") {
    var obj = { Page: page, Url: urlViewOffer };
    history.pushState(obj, obj.Page, obj.Url);
  } else {
    alert("Browser does not support HTML5.");
  }
}


// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com

  