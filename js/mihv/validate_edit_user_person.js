$(document).ready(function(){
  $.validator.addMethod("notEqualTo", function(value, element, param) {
  	return this.optional(element) || value != $(param).val();
  	}, "Please specify a different (non-default) value");

	$('#form-edit-user-person').validate({
		rules: {
			itName: {
				required: true,
				maxlength: 45
			},
			itLastName: {
				required: true,
				maxlength: 45
			},
			age: {
				required: true,
				min: 16,
				max: 70,
				number: true
			},
			itPhone: {
				maxlength: 45
			},
			itMovil: {
				required: true,
				minlength: 10,
				maxlength: 10,
				number: true
			},
			itExperience: {
				required: true,
				min: 0,
				max: 65,
				number: true,
				maxlength: 2
			},
			itaKnowledge: {
				required: true,
				maxlength: 800
			},
			itEmail: {
				required: true,
				email: true,
				maxlength: 45
			},
			itCurrentPassword: {
				minlength: 4,
				maxlength: 45
			},
			itNewPassword: {
				minlength: 4,
				maxlength: 45
			},
			selCity: {
				notEqualTo: '#selCityValidate'
			},
			itAddress: {
				required: true,
				maxlength: 100
			}
		},
		messages: {
			itName: {
				required: '(*) Ingresa tu nombre.',
				maxlength: 'No puedes superar los 45 caracteres.'			
			},
			itLastName: {
				required: '(*) Ingresa tus apellidos.',
				maxlength: 'No puedes superar los 45 caracteres.'	
			},
			age: {
				required: '(*) Ingresa tu edad',
				min: 'No puedes ingresar una edad menor a 16 años',
				max: 'No puedes ingresar una edad mayor a 70 años',
				number: 'Solo se permiten números'
			},
			itPhone: {
				maxlength: 'No puedes superar los 45 caracteres.'
			},
			itMovil: {
				required: '(*) Ingresa tu número de celular',
				minlength: 'Ingrese los 10 caracteres de una linea',
				maxlength: 'No puedes superar los 10 caracteres.',
				number: 'Solo se permiten números'
			},
			itExperience: {
				required: '(*) Ingresa la experiencia laboral, si no tienes deja en cero "0".',
				min: 'No puedes ingresar una experiencia menor a 0',
				max: 'No puedes ingresar una experiencia mayor a 50',
				number: true,
				maxlength: 'No puedes superar los 2 caracteres.'
			},
			itEmail: {
				required: '(*) Ingresa tu correo.',
				email: 'Correo incorrecto, por favor verifica.',
				maxlength: 'No puedes superar los 45 caracteres.'		
			},
			itCurrentPassword: {
				minlength: 'Ingresa una contraseña mayor a 4 caracteres',
				maxlength: 'No puedes superar los 45 caracteres.'
			},
			itNewPassword: {
				minlength: 'Ingresa una contraseña mayor a 4 caracteres',
				maxlength: 'No puedes superar los 45 caracteres.'
			},
			selExperiencie: {
				required: '(*) Ingresa la experiencia laboral, si no tienes deja en cero "0".',
				number: 'Debes ingresar solo numeros ej: 2'	
			},
			itaKnowledge: {
				required: '(*) Ingresa los conocimientos / habilidades / destrezas que posees.',
				number: 'No puedes superar los 800 caracteres.'	
			},
			selCity: {
				notEqualTo: '(*) Debes seleccionar una ciudad.'
			},
			itAddress: {
				required: '(*) Ingresa la dirección.',
				maxlength: 'No puedes superar los 100 caracteres.'
			}
		}
	});
});