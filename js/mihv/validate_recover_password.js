$(document).ready(function(){
	$('#form-recover-password').validate({
		rules: {
			newPassword: {
				required: true,
				minlength: 4
			},
			repeatNewPassword: {
				required: true,
				equalTo: "#newPassword"
			}
		},
		messages: {
			newPassword: {
				required: '(*) Ingresa una contraseña.',
				minlength: 'Ingresa una contraseña de 4 digitos o mayor.'		
			},
			repeatNewPassword: {
				required: '(*) Ingresa de nuevo la contraseña.',
				equalTo: 'Las contraseñas no son iguales, intenta de nuevo.'			
			}
		}
	});
});