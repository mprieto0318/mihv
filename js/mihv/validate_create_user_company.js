$(document).ready(function(){



	$('#form-register-company #selCity').selectpicker('refresh');
	$.validator.addMethod("notEqualTo", function(value, element, param) {
  	return this.optional(element) || value != $(param).val();
  }, "Please specify a different (non-default) value");

	$('#form-register-company').validate({
		rules: {
			itName: {
				required: true,
				maxlength: 45
			},
			itaDescription: {
				required: true,
				minlength: 25,
				maxlength: 800
			},
			itNit: {
				required: true,
				maxlength: 15
			},
			itEconomicActivity: {
				required: true,
				maxlength: 60
			},
			itPhone: {
				required: true,
				maxlength: 30
			},
			itMovil: {
				required: true,
				maxlength: 12
			},
			itEmail: {
				required: true,
				email: true,
				maxlength: 45
			},
			itPassword: {
				required: true,
				maxlength: 45
			},
			ckTerms:{ 
				required:true
			},
			selCity: {
				required: true,
				notEqualTo: '#selCityValidate'
			},
			itAddress: {
				required: true
			}
		},
		messages: {
			itName: {
				required: '(*) Ingresa el nombre de la empresa.',
				maxlength: 'No puedes superar los 45 caracteres.'		
			},
			itaDescription: {
				required: '(*) Ingresa la descripcion de la empresa.',
				minlength: 'Descripcion demasiado corta, ingresa mas de 25 caracteres.',
				maxlength: 'Descripcion demasiado larga, ingresa menos de 800 caracteres.'
			},
			itNit: {
				required: '(*) Ingresa el NIT de la empresa.',
				maxlength: 'No puedes superar los 15 caracteres.'
			},
			itEconomicActivity: {
				required: '(*) Ingresa la actividad economica de la empresa.',
				maxlength: 'No puedes superar los 60 caracteres.'			
			},
			itPhone: {
				required: '(*) Ingresa un telefono de la empresa.',
				maxlength: 'No puedes superar los 30 caracteres.'	
			},
			itMovil: {
				required: '(*) Ingresa el celular de contacto.',
				maxlength: 'No puedes superar los 12 caracteres.'	
			},
			itEmail: {
				required: '(*) Ingresa el email de la empresa.',
				email: 'Email incorrecto, por favor verificalo.',
				maxlength: 'No puedes superar los 45 caracteres.'	
			},
			itPassword: {
				required: '(*) Ingresa una contraseña.',
				maxlength: 'No puedes superar los 45 caracteres.'		
			},
			ckTerms: {
        required: '(*) Debes aceptar los terminos y condiciones.',
      },
			selCity: {
				notEqualTo: '(*) Debes seleccionar una ciudad.'	
			},
			itAddress: {
				required: '(*) Debes Ingresar la ciudad.'	
			}
		},
    errorPlacement: function(error, element) {
      if (element.is(":checkbox")) {
        error.appendTo( element.parents('.wrap-check-required') );
      }
      else {
        error.insertAfter( element );
      }
    }
	});

  $(".validate-nit").focusin(function(){
    $('.result-dv').val('');    
    $('.error-nit').hide();
  });

  $(".validate-nit").focusout(function(){
    CalcularDv();
  });
});


function CalcularDv()
{var vpri,x,y,z,i,nit1,dv1;nit1=document.form_register_company.itNit.value;if(isNaN(nit1))
{document.form_register_company.dv.value="X";document.form_register_company.dv_hidden.value="X";$('.error-nit').show();}else{vpri=new Array(16);x=0;y=0;z=nit1.length;vpri[1]=3;vpri[2]=7;vpri[3]=13;vpri[4]=17;vpri[5]=19;vpri[6]=23;vpri[7]=29;vpri[8]=37;vpri[9]=41;vpri[10]=43;vpri[11]=47;vpri[12]=53;vpri[13]=59;vpri[14]=67;vpri[15]=71;for(i=0;i<z;i++)
{y=(nit1.substr(i,1));x+=(y*vpri[z-i]);}
y=x%11
if(y>1)
{dv1=11-y;}else{dv1=y;}
document.form_register_company.dv.value=dv1;document.form_register_company.dv_hidden.value=dv1;}}
