$(document).ready(function(){
	$.validator.addMethod("notEqualTo", function(value, element, param) {
  	return this.optional(element) || value != $(param).val();
  	}, "Please specify a different (non-default) value");

	$('#form-edit-company').validate({
		rules: {
			itName: {
				required: true,
				maxlength: 45
			},
			itNit: {
				required: true,
				maxlength: 30
			},
			itEconomicActivity: {
				required: true,
				maxlength: 60
			},
			itPhone: {
				required: true,
				maxlength: 30
			},
			itMovil: {
				required: true,
				minlength: 10,
				maxlength: 12,
				number: true
			},
			selCity: {
				notEqualTo: '#selCityValidate'
			},
			itAddress: {
				required: true,
				maxlength: 100
			},
			itaDescription: {
				required: true,
				maxlength: 800
			},
			itEmail: {
				email: true,
				maxlength: 45
			},
			itCurrentPassword: {
				minlength: 4,
				maxlength: 45
			},
			itNewPassword: {
				minlength: 4,
				maxlength: 45
			}
		},
		messages: {
			itName: {
				required: '(*) Ingresa tu nombre.',
				maxlength: 'No puedes superar los 45 caracteres.'			
			},
			itNit: {
				required: '(*) Ingresa tus apellidos.',
				maxlength: 'No puedes superar los 30 caracteres.'	
			},
			itEconomicActivity: {
				required: '(*) Ingresa tu edad',
				maxlength: 'No puedes superar los 60 caracteres.'	
			},
			itPhone: {
				required: '(*) Ingresa tu número de telefono fijo',
				maxlength: 'No puedes superar los 45 caracteres.'
			},
			itMovil: {
				required: '(*) Ingresa tu número de celular',
				minlength: 'Ingrese los 10 caracteres de una linea',
				maxlength: 'No puedes superar los 12 caracteres.',
				number: 'Solo se permiten números'
			},
			selCity: {
				notEqualTo: '(*) Debes seleccionar una ciudad.'
			},
			itAddress: {
				required: '(*) Ingresa la dirección.',
				maxlength: 'No puedes superar los 100 caracteres.'
			},
			itaDescription: {
				required: '(*) Ingresa la descripción de la empresa',
				maxlength: 'No puedes superar los 45 caracteres.'		
			},
			itEmail: {
				email: 'Correo incorrecto, por favor verifica.',
				maxlength: 'No puedes superar los 45 caracteres.'		
			},
			itCurrentPassword: {
				minlength: 'Ingresa una contraseña mayor a 4 caracteres',
				maxlength: 'No puedes superar los 45 caracteres.'
			},
			itNewPassword: {
				minlength: 'Ingresa una contraseña mayor a 4 caracteres',
				maxlength: 'No puedes superar los 45 caracteres.'
			}
		}
	});
});