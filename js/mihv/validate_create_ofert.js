$(document).ready(function(){
  $('.wra-select-city .select-city').selectpicker('refresh');
	$.validator.addMethod("notEqualTo", function(value, element, param) {
  	return this.optional(element) || value != $(param).val();
  }, "Please specify a different (non-default) value");

	$('#form-create-ofert').validate({
		rules: {
			itName: {
				required: true,
				minlength: 10,
				maxlength: 100
			},
			itaDescription: {
				required: true,
				minlength: 15,
				maxlength: 500
			},
			itakKowledge: {
				required: true,
				minlength: 15,
				maxlength: 500
			},
			selCity: {
				required: true,
				notEqualTo: '#selCityValidate'
			},
			selTypeProfession: {
				required: true,
				notEqualTo: '#selTypeProfessionValidate'
			},
			selValue: {
				required: true,
				number: true
			}
		},
		messages: {
			itName: {
				required: '(*) Ingresa un nombre para la oferta.',
				minlength: 'Ingresa un nombre mas largo.',
				maxlength: 'El nombre es demasiado largo, no puedes exceder los 60 caracteres.'			
			},
			itaDescription: {
				required: '(*) Ingresa una descripcion para la oferta.',
				minlength: 'Ingresa una descripcion mas larga.',
				maxlength: 'La descripcion es demasiada larga, no puedes exceder los 500 caracteres.'			
			},
			itakKowledge: {
				required: '(*) Ingresa los conocimientos / habilidades / destresas que apliquen a la oferta.',
				minlength: 'Ingresa un poco mas de informacion.',
				maxlength: 'Este campo es demasiado largo, no puedes exceder los 500 caracteres.'			
			},
			selCity: {
				notEqualTo: '(*) Selecciona la ciudad a la que aplique la oferta.'	
			},
			selTypeProfession: {
				notEqualTo: '(*) Selecciona el tipo de profesion a la que aplica la oferta.'	
			},
			selValue: {
				required: '(*) Ingrese el salario de la oferta, deja en cero (0) si es reservado.',
				number: 'Debes ingresar solo numeros ej: 2000000'	
			}
		},
    submitHandler: function(form) {
    
      // COMENTANDO PAGO DE PSE Y SALDO DE LA CUENTA
      // if ($('#priority-create-ofert').val() == 'urgent') {
      //   $('#show-modal-create-ofert-priority').trigger("click");
      //   $('.modal-wrap-info-cost-offer').html($('#wrap-info-cost-offer').html());

      //   $('.modal-wrap-info-cost-offer p.price-na').hide();

      //   $('#modal-create-ofert-priority').on('click', '#btn-create-ofert-priority-pse', function() {
      //   	$('#payment_type').val('pse');
      //     form.submit();
      //   });
      //   $('#modal-create-ofert-priority').on('click', '#btn-create-ofert-priority-pse', function() {
      //   	$('#payment_type').val('balance');
      //     form.submit();
      //   });
      // }else {

      // 	$('#show-modal-create-ofert-normal').trigger("click");
      //   $('.modal-wrap-info-cost-offer').html($('#wrap-info-cost-offer').html());
        
      //   $('.modal-wrap-info-cost-offer p.price-urgent').hide();

      //   $('#modal-create-ofert-normal').on('click', '#btn-create-ofert-normal-pse', function() {
      //   	$('#payment_type').val('pse');
      //     form.submit();
      //   });

      //   $('#modal-create-ofert-normal').on('click', '#btn-create-ofert-normal-balance', function() {
      //   	$('#payment_type').val('balance');
      //     form.submit();
      //   });
      // }

      if ($('.showModal').val() == '0') {

        if ($('#priority-create-ofert').val() == 'urgent') {
          $('#show-modal-create-ofert-urgent').trigger("click");
          $('.modal-wrap-info-cost-offer').html($('#wrap-info-cost-offer').html());

          $('.modal-wrap-info-cost-offer .block-na').hide();
          $('.modal-wrap-info-cost-offer .block-urgent').removeClass('col-sm-6 col-md-6');
          $('.modal-wrap-info-cost-offer .block-urgent').addClass('col-sm-12 col-md-12');

          $('#modal-create-ofert-urgent').on('click', '#btn-create-ofert-urgent', function() {
            $(this).attr("disabled", true);
            form.submit();
          });
        }else {

          $('#show-modal-create-ofert-normal').trigger("click");
          $('.modal-wrap-info-cost-offer').html($('#wrap-info-cost-offer').html());
          
          $('.modal-wrap-info-cost-offer .block-urgent').hide();
          $('.modal-wrap-info-cost-offer .block-na').removeClass('col-sm-6 col-md-6');
          $('.modal-wrap-info-cost-offer .block-na').addClass('col-sm-12 col-md-12');

          $('#modal-create-ofert-normal').on('click', '#btn-create-ofert-normal', function() {
            $(this).attr("disabled", true);
            form.submit();
          });
        }
      }else {
        $('#btn-create-ofert-free').attr("disabled", true);
        form.submit();
      }

      
    }
	});

  // mostrando y oculatando mensaje de error en selects ciudad y tipo de profession
  $('.wrp-select ul.dropdown-menu').on('click', 'li', function() {
    var select = parseInt($(this).attr('data-original-index'));
    var elementError = $(this).parents(".wrp-select").find('.error');
    if (select != 0) {
        elementError.hide();
    }else {
      elementError.show();
    }
  });
});