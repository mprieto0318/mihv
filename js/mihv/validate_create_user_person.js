$(document).ready(function(){
	$('#form-register-person #selCity').selectpicker('refresh');

	$.validator.addMethod("notEqualTo", function(value, element, param) {
  	return this.optional(element) || value != $(param).val();
  	}, "Please specify a different (non-default) value");

	$('#form-register-person').validate({
		rules: {
			itName: {
				required: true,
				maxlength: 45
			},
			itLastName: {
				required: true,
				maxlength: 45
			},
			itEmail: {
				required: true,
				email: true,
				maxlength: 45
			},
			itPassword: {
				required: true,
				minlength: 4,
				maxlength: 45
			},
			selTypeProfession: {
				notEqualTo: '#selTypeProfessionValidate'
			},
			itaKnowledge: {
				required: true,
				maxlength: 800
			},
			ckTerms:{ 
				required:true
			},
			selCity: {
				required: true,
				notEqualTo: '#selCityValidate'
			},
			itAddress: {
				required: true
			},
			itPhone: {
				maxlength: 45
			},
			itMovil: {
				required: true,
				minlength: 10,
				maxlength: 10,
				number: true
			},
      age: {
        required: true,
        min: 16,
        max: 60,
        number: true
      },
      selExperiencie: {
        required: true,
        min: 0,
        max: 70,
        number: true,
        maxlength: 2
      }
		},
		messages: {
			itName: {
				required: '(*) Ingresa solo tu nombre(s).',
				maxlength: 'No puedes superar los 45 caracteres.'			
			},
			itLastName: {
				required: '(*) Ingresa solo tus apellidos.',
				maxlength: 'No puedes superar los 45 caracteres.'	
			},
			itEmail: {
				required: '(*) Ingresa tu correo.',
				email: 'Correo incorrecto, por favor verifica.',
				maxlength: 'No puedes superar los 45 caracteres.'		
			},
			itPassword: {
				required: '(*) Ingresa una contraseña.',
				minlength: 'Ingresa una contraseña mayor a 4 caracteres',
				maxlength: 'No puedes superar los 45 caracteres.'
			},
			selTypeProfession: {
				notEqualTo: '(*) Selecciona el tipo de profesion a la que aplica la oferta.'	
			},
			itaKnowledge: {
				required: '(*) Ingresa los conocimientos / habilidades / destrezas que posees.',
				number: 'No puedes superar los 800 caracteres.'	
			},
			ckTerms: {
		        required: '(*) Debes aceptar los terminos y condiciones.',
		    },
			selCity: {
				notEqualTo: '(*) Debes seleccionar una ciudad.'	
			},
			itAddress: {
				required: '(*) Debes Ingresar la ciudad.'	
			},
			itPhone: {
				maxlength: 'No puedes superar los 45 caracteres.'
			},
			itMovil: {
				required: '(*) Ingresa tu número de celular',
				minlength: 'Ingrese los 10 caracteres de una linea',
				maxlength: 'No puedes superar los 10 caracteres.',
				number: 'Solo se permiten números'
			},
      age: {
        required: '(*) Ingresa tu edad',
        min: 'No puedes ingresar una edad menor a 16 años',
        max: 'No puedes ingresar una edad mayor a 60 años',
        number: 'Solo se permiten números'
      },
      selExperiencie: {
        required: '(*) Ingresa la experiencia laboral, si no tienes deja en cero "0".',
        min: 'No puedes ingresar una experiencia menor a 0',
        max: 'No puedes ingresar una experiencia mayor a 70',
        number: true,
        maxlength: 'No puedes superar los 2 caracteres.'
      }
		},
    errorPlacement: function(error, element) {
      if (element.is(":checkbox")) {
        error.appendTo( element.parents('.wrap-check-required') );
      }else {
        error.insertAfter( element );
      }
    }
	});
});