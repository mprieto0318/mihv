<!DOCTYPE html>
<!--
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
-->
<html>
<head>
  <?php
  require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/head.php';
  ?>
  <title>Pagina no Encontrada</title>
</head>
<body class="page-404">

  <?php
    // Header
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/header.php';
  ?>


  <div class="container wrap-top">
    <div class="row">
      <div class="span12">
        <div class="hero-unit center">
            <h1>Pagina No Encontrada <small><font face="Tahoma" color="red">Error 404</font></small></h1>
            <br />
            <p>No se pudo encontrar la página solicitada, puedes ponerte en contacto con el administrador del sitio enviando un correo ha "soporte@mihv.com.co" o vuelve a intentarlo. Utiliza el botón <b>Atrás</b> de tu navegador para navegar a la página de la que provienes</p>
            <p><b>Para ir al inicio haz <b>click</b> en el siguiente boton:</b></p>
            <a href="/index.php" class="btn btn-large btn-info"><i class="glyphicon glyphicon-home"> </i> IR AL INICIO</a>
          </div>
          <br />
        <div class="thumbnail">
          <center><h4><b>Equipo MI HV</b></h4></center>
        </div>
          <br />
          <p></p>
          <!-- By ConnerT HTML & CSS Enthusiast -->  
      </div>
    </div>
  </div>



  <?php
    // Footer
    require $_SERVER['DOCUMENT_ROOT'] . '/views/sections/footer.php';
  ?>



</body>
</html>
