<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/functions_sql.cls.php");

class modelUser { 

  function model_user_login($tbl, $type, $id_user) {
		$functions_sql = new functions_sql();
		$sql = 'SELECT * FROM ' . $tbl . ' WHERE id_' . $type . ' = ' . $id_user .';';
		$result = $functions_sql->functions_sql_execute_query($sql);
		$user_data = $functions_sql->functions_sql_execute_get_dates($result);
		$this->model_user_update_date_login($tbl, $type, $id_user);
		$functions_sql->functions_sql_functions_sql_close_execute_query($result);
		
		return $user_data;
	}

	function model_user_get_data_user($tbl, $type, $field, $value) {

		switch ($field) {
			case 'EMAIL':
				$field_select = 'email_';
				$value_select = '"' .$value . '"';
				break;

      case 'NIT':
        $field_select = 'nit_';
        $value_select = '"' .$value . '"';
        break;
		}

		$functions_sql = new functions_sql();
		$sql = 'SELECT * FROM ' . $tbl . ' WHERE ' . $field_select . $type . ' = ' . $value_select .';';
		$result = $functions_sql->functions_sql_execute_query($sql);
		$user_data = $functions_sql->functions_sql_execute_get_dates($result);
		$functions_sql->functions_sql_functions_sql_close_execute_query($result);
		
		return $user_data;
	}

	function model_user_validate_status_user($tbl, $type, $email, $password) {
		$functions_sql = new functions_sql();
		$status_user = '';
		
		$sql = 'SELECT id_' . $type . ' FROM ' . $tbl . ' WHERE 
            email_' . $type . '="' . $email .'" AND 
            password_' . $type . '="' . $password . '" AND 
            active_' . $type . ' = 1 OR
            email_' . $type . '="' . $email .'" AND 
            password_' . $type . '="' . $password . '" AND 
            provisional_access_' . $type . ' = 1
            ;';
		$result = $functions_sql->functions_sql_execute_query($sql);
		
		if ($user_data = $functions_sql->functions_sql_execute_get_dates($result)) {
				$status_user = array(
				'status' => 'REGISTERED_USER',
				'user_data' => $user_data,
				);

		}else {

			$sql2 = 'SELECT id_' . $type . ', name_' . $type . ' FROM ' . $tbl . ' WHERE email_' . $type . '="' . $email .'" AND password_' . $type . '="' . $password . '" AND active_' . $type . ' = 0;';
			$result2 = $functions_sql->functions_sql_execute_query($sql2);
			
			if ($user_data = $functions_sql->functions_sql_execute_get_dates($result2)) {
				$status_user = array(
					'status' => 'UNREGISTERED_USER',
					'user_data' => $user_data,
				);
			}else {
				$status_user = array(
					'status' => 'BAD_CREDENTIALS',
					'user_data' => '',
				);
			}
		}

		return $status_user;
	}

  function model_user_validate_status_user_admin($email, $pass) {
    $functions_sql = new functions_sql();
    
    $sql = 'SELECT * FROM tbl_system_users 
            WHERE email_su = "' . $email . '" 
            AND password_su = "' . $pass . '"
            AND active_su = ' . 1 . ';';
    $result = $functions_sql->functions_sql_execute_query($sql);
    
    if ($data = $functions_sql->functions_sql_execute_get_dates($result)) {
      $this->model_user_update_date_login_admin($data['id_su']);
      return $data; 
    }

    return NULL;
  }

  function model_user_insert_person($itName, $itLastName, $itEmail, $itPassword, $itPhone, $itMovil, $itProfession, $selExperiencie, $itaKnowledge, $itaStudies, $age, $selCity, $itAddress, $token_active_user) {
		$functions_sql = new functions_sql();
		$sql = "INSERT INTO tbl_person_info(
			name_pi, 
			last_name_pi, 
			email_pi,
			password_pi,
			phone_pi,
			movil_pi,   
			experience_pi, 
			knowledge_pi,
			studies_pi,
			age_pi,
			profession_pi,
			city_pi,
			address_pi,
			token_active_account_pi
		)VALUES(
			'" . $itName . "',
			'" . $itLastName . "',
			'" . $itEmail . "',
			'" . $itPassword . "',
			'" . $itPhone . "',
			'" . $itMovil . "',
			'" . $selExperiencie . "',
			'" . $itaKnowledge . "',
      '" . $itaStudies . "',
			" . $age . ",
			'" . $itProfession . "',
			" . $selCity . ",
			'" . $itAddress . "',
			'" . $token_active_user . "'
		)";
		
		$result = $functions_sql->functions_sql_execute_query($sql);
		$functions_sql->functions_sql_close_connection();
		return $result;
	}

	function insert_company($itName, $itaDescription, $new_name_logo, $itNit, $itEconomicActivity, $itPhone, $itMovil, $itEmail, $itPassword, $selCity, $itAddress, $token_active_user) {
		$functions_sql = new functions_sql();
		$sql = "INSERT INTO tbl_company_info(
			name_ci, 
			description_ci,
			image_ci,
			nit_ci, 
			economic_activity_ci, 
			phone_ci, 
			movil_ci, 
			email_ci,
			password_ci,
			city_ci,
			address_ci,
			token_active_account_ci
		)VALUES(
			'" . $itName . "',
			'" . $itaDescription . "',
			'" . $new_name_logo . "',
			'" . $itNit . "',
			'" . $itEconomicActivity . "',
			'" . $itPhone . "',
			" . $itMovil . ",
			'" . $itEmail . "',
			'" . $itPassword . "',
			" . $selCity . ",
			'" . $itAddress . "',
			'" . $token_active_user . "'
		)";
		$result = $functions_sql->functions_sql_execute_query($sql);
		$functions_sql->functions_sql_close_connection();
		return $result;
	}   

	function model_user_update_date_login($tbl, $type, $id_user) {
		$functions_sql = new functions_sql();
		$sql = 'UPDATE ' . $tbl . ' SET login_date_' . $type . ' = "'. date('Y-m-d h:i:s') . '"
		 WHERE id_' . $type . ' = ' . $id_user . ';';
		$functions_sql->functions_sql_execute_query($sql);
		$functions_sql->functions_sql_close_connection();
	}

  function model_user_update_date_login_admin($id_user) {
    $functions_sql = new functions_sql();
    $sql = 'UPDATE tbl_system_users SET login_date_su = "'. date('Y-m-d h:i:s') . '"
     WHERE id_su= ' . $id_user . ';';
    $functions_sql->functions_sql_execute_query($sql);
    $functions_sql->functions_sql_close_connection();
  }

	function model_user_update_token_recover_password($tbl_user, $type, $itEmailRecover, $token_new_password) {
		$functions_sql = new functions_sql();
		$sql = 'UPDATE ' . $tbl_user . ' SET token_recover_password_' . $type . ' = "'. $token_new_password . '"
		 WHERE email_' . $type . ' = "' . $itEmailRecover . '";';
		$result = $functions_sql->functions_sql_execute_query($sql);
		$functions_sql->functions_sql_close_connection();
		
		return $result;
	}


	function validate_exits_user($tbl, $type, $email, $nit_dv = NULL){
		$functions_sql = new functions_sql();

    $val_nit = ($nit_dv == NULL) ? '' : ' OR nit_ci = "' . $nit_dv . '"';

		$sql = 'SELECT * FROM ' . $tbl . ' WHERE email_' . $type . '="' . $email .'"' . $val_nit . ';';
		$result = $functions_sql->functions_sql_execute_query($sql);

		if($info_user = $functions_sql->functions_sql_execute_get_dates($result)) {
			return TRUE;
		}else {
			return FALSE;
		}
	}

	function model_user_update_token_active_user($tbl, $prefix, $id_user, $token_active_user) {
		$functions_sql = new functions_sql();
		$sql = 'UPDATE tbl_' . $tbl . '_info SET token_active_account_' . $prefix . ' = "'.  $token_active_user . '"
		 WHERE id_' . $prefix . ' = ' . $id_user . ';';
		$response = $functions_sql->functions_sql_execute_query($sql);
		$functions_sql->functions_sql_close_connection();

		return $response;
	}


	function model_user_get_user_inactive($tbl, $prefix, $fields, $time) {

		$functions_sql = new functions_sql();
		$sql = 'SELECT ' . $fields . ' FROM    tbl_' . $tbl . '_info
						WHERE active_' . $prefix . ' = 0 AND 
						created_' . $prefix . ' <= NOW() - INTERVAL ' . $time . ';';

		$result = $functions_sql->functions_sql_execute_query($sql);
    if (!empty($result)) {
      $rows = array();
      while($item = $functions_sql->functions_sql_execute_get_dates($result)) {
        $rows[] = $item;
      }
      $functions_sql->functions_sql_close_query_and_connection($result);
      return $rows;
    }
    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
	}

	function model_user_delete_registry_oferts_apply($column, $id) {
		$functions_sql = new functions_sql();
		$sql = 'DELETE FROM tbl_oferts_apply WHERE ' . $column . '=' . $id .';';
		$response = $functions_sql->functions_sql_execute_query($sql);
		$functions_sql->functions_sql_close_connection();

		return $response;
	}
	
	function model_user_delete_user($tbl, $prefix, $id_user) {
		$functions_sql = new functions_sql();
		$sql = 'DELETE FROM tbl_' . $tbl . '_info WHERE id_' . $prefix . '=' . $id_user .';';
		$response = $functions_sql->functions_sql_execute_query($sql);
		$functions_sql->functions_sql_close_connection();

		return $response;
	}

	function model_user_get_offers($id_user) {
		$functions_sql = new functions_sql();
		$sql = 'SELECT  id_o FROM tbl_oferts
						WHERE tbl_company_info_id_ci = ' . $id_user . ' ;';

		$result = $functions_sql->functions_sql_execute_query($sql);
    if (!empty($result)) {
      $rows = array();
      while($item = $functions_sql->functions_sql_execute_get_dates($result)) {
        $rows[] = $item;
      }
      $functions_sql->functions_sql_close_query_and_connection($result);
      return $rows;
    }
    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
	}

	function model_user_delete_offer_appy($id_offer) {
		$functions_sql = new functions_sql();
		$sql = 'DELETE FROM tbl_oferts_apply WHERE tbl_oferts_id_o = ' . $id_offer .';';
		$response = $functions_sql->functions_sql_execute_query($sql);
		$functions_sql->functions_sql_close_connection();

		return $response;
	}

	function model_user_delete_offer($id_user, $id_o = NULL) {
		$functions_sql = new functions_sql();

		$and_offer = ($id_o != NULL) ? ' AND id_o = ' . $id_o : '';

		$sql = 'DELETE FROM tbl_oferts WHERE tbl_company_info_id_ci = ' . $id_user . $and_offer . ';';
		$response = $functions_sql->functions_sql_execute_query($sql);
		$functions_sql->functions_sql_close_connection();

		return $response;
	}

  function model_user_provisional_access($tbl, $prefix, $token_active_user, $provisional_access) {
    $functions_sql = new functions_sql();
    $sql = 'UPDATE tbl_' . $tbl . '_info SET 
            provisional_access_' . $prefix . ' = ' . $provisional_access . ' WHERE 
            token_active_account_' . $prefix . ' =  "' . $token_active_user . '" ;';
    
    $provisional_access = $functions_sql->functions_sql_execute_query($sql);
    $functions_sql->functions_sql_close_connection();
    
    return $provisional_access;
  }
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
}