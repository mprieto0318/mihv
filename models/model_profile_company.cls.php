<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/functions_sql.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/models/models_utilities.cls.php");

class modelProfileCompany { 

  // retorna saldo disponible y saldo gastado de la empresa
  function model_profile_company_get_balances() {
    $functions_sql = new functions_sql();
    $sql = 'SELECT available_balance_ci, balance_spent_ci FROM tbl_company_info WHERE id_ci = ' . $_SESSION['user']['id_ci'] . ';';

    $result = $functions_sql->functions_sql_execute_query($sql);
    if (!empty($result)) {
      $balances = array();
      
      $item = $functions_sql->functions_sql_execute_get_dates($result);
      $balances['available'] = $item['available_balance_ci'];
      $balances['spent'] = $item['balance_spent_ci'];
      
      $functions_sql->functions_sql_close_query_and_connection($result);
      return $balances;
    }
    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }

  function model_profile_company_get_value_offer($priority = NULL, $payment_type = NULL) {
    $functions_sql = new functions_sql();

    if ($priority == NULL) {
      $sql = 'SELECT * FROM tbl_system_value_offer';
      $result = $functions_sql->functions_sql_execute_query($sql);

      if (!empty($result)) {
        $value_offer = array();

        while($item = $functions_sql->functions_sql_execute_get_dates($result)) {
          $value_offer[$item['id_svo']] = $item['value_svo'];
        }

        $functions_sql->functions_sql_close_query_and_connection($result);
        return $value_offer;
      }
    }else {
      $sql = 'SELECT value_svo FROM tbl_system_value_offer WHERE id_svo = "' . $priority . '_' . $payment_type . '";';
      $result = $functions_sql->functions_sql_execute_query($sql);

      if (!empty($result)) {
        $value_offer = 0;
        
        $item = $functions_sql->functions_sql_execute_get_dates($result);
        $value_offer = $item['value_svo'];
        
        $functions_sql->functions_sql_close_query_and_connection($result);
        return $value_offer;
      }
    }

    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }

  function model_profile_company_get_publish_free($id_ci) {
    $functions_sql = new functions_sql();
    $sql = 'SELECT publish_free_ci FROM tbl_company_info WHERE id_ci = ' . $id_ci . ';';

    $result = $functions_sql->functions_sql_execute_query($sql);
    if (!empty($result)) {
      $data = $functions_sql->functions_sql_execute_get_dates($result);
      $functions_sql->functions_sql_close_query_and_connection($result);
      return $data;
    }
    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }

  function model_profile_company_update_publish_free($id_ci) {
    $functions_sql = new functions_sql();

    $sql = 'UPDATE tbl_company_info SET 
          publish_free_ci = publish_free_ci - 1   
          WHERE id_ci = ' . $id_ci . ';';

    $result = $functions_sql->functions_sql_execute_query($sql);
    $functions_sql->functions_sql_close_connection();
  
    return $result;
  }

  function model_profile_company_update_balances($id_ci, $new_available_balance, $new_balance_spent) {
    $functions_sql = new functions_sql();
    
    $sql = 'UPDATE tbl_company_info SET 
          available_balance_ci = ' . $new_available_balance . ', 
          balance_spent_ci = ' . $new_balance_spent . ' 
          WHERE id_ci = ' . $id_ci . ';';

    $result = $functions_sql->functions_sql_execute_query($sql);
    $functions_sql->functions_sql_close_connection();
  
    return $result;
  }

  function model_profile_company_get_all_oferts_by_user() {
    $functions_sql = new functions_sql();
    $sql = 'SELECT id_o, name_o, created_o, priority_o FROM tbl_oferts WHERE tbl_company_info_id_ci = ' . $_SESSION['user']['id_ci'] . ' AND status_o = "OPEN" ORDER BY id_o DESC;';
    $result = $functions_sql->functions_sql_execute_query($sql);

    if (!empty($result)) {
      $rows = array();
      $num_row = 0;

      while($item = $functions_sql->functions_sql_execute_get_dates($result)) {
        $rows[$num_row]['id_o']                      = $item['id_o'];
        $rows[$num_row]['name_o']                    = $item['name_o'];
        $rows[$num_row]['priority_o']                = $item['priority_o'];
        $rows[$num_row]['created_o']                 = $item['created_o'];
        $rows[$num_row]['options']                   = 'options';
        $num_row++;
      }
      
      $functions_sql->functions_sql_close_query_and_connection($result);
      return $rows;
    }
    
    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }

  function model_profile_company_get_all_billing_company() {
    $functions_sql = new functions_sql();
    $sql = 'SELECT * FROM tbl_system_billing WHERE id_ci = ' . $_SESSION['user']['id_ci'] . ' ORDER BY id_sb DESC;';
    $result = $functions_sql->functions_sql_execute_query($sql);
    
    $rows = array();
    $num_row = 0;

    while($item = $functions_sql->functions_sql_execute_get_dates($result)) {
      $rows[$num_row]['id_sb']   = $item['id_sb'];
      $rows[$num_row]['cost_o']  = $item['cost_o'];
      $rows[$num_row]['date_sb'] = $item['date_sb'];
      $rows[$num_row]['url_bill_pdf']     = 'factura-de-venta_' . $item['id_sb'] . '.pdf';
      $num_row++;
    }

    $functions_sql->functions_sql_close_query_and_connection($result);
    return $rows;
  }


  function model_profile_company_get_recharge_history_by_id($id_ci) {
    $functions_sql = new functions_sql();
    $sql = 'SELECT id_sbr, id_transaction_sbr, value_sbr, created_sbr FROM tbl_system_balance_recharge WHERE id_ci = ' .$id_ci . ';';
    $result = $functions_sql->functions_sql_execute_query($sql);
    
    $rows = array();
    $num_row = 0;

    while($item = $functions_sql->functions_sql_execute_get_dates($result)) {
      $rows[$num_row]['id_sbr']             = $item['id_sbr'];
      $rows[$num_row]['id_transaction_sbr'] = $item['id_transaction_sbr'];
      $rows[$num_row]['value_sbr']          = $item['value_sbr'];
      $rows[$num_row]['created_sbr']          = $item['created_sbr'];
      $rows[$num_row]['url_recharge_pdf']        = 'comprobante-recarga-saldo_' . $item['id_sbr'] . '.pdf';
      $num_row++;
    }

    $functions_sql->functions_sql_close_query_and_connection($result);
    return $rows;
  }


 //// (((((((( NO ESTA EN USO ))))))))
 //  // Funcion omitida para crear la oferta por sentencia preparada
 //  function model_profile_company_insert_ofert_company($itName, $itaDescription, $itakKowledge, $selGenre,  $itProfession, $selValue, $selTypeContract,  $selPriority, $selVacancies, $selCity) {
 //    $publish_free_ci = ($_SESSION['user']['publish_free_ci'] == 0) ? 0 : 1;
	// 	$functions_sql = new functions_sql();
	// 	$sql = "INSERT INTO tbl_oferts(
	// 		name_o, 
	// 		description_o, 
	// 		knowledge_o, 
	// 		genre_o, 
	// 		profession_o,  
	// 		value_o,
	// 		type_contract_o,
	// 		priority_o,
	// 		vacancies_o,
 //      publish_free_o,
 //      tbl_city_id_c,
	// 		tbl_company_info_id_ci
 //    )VALUES(
	// 		'" . $itName . "',
	// 		'" . $itaDescription . "',
	// 		'" . $itakKowledge . "',
	// 		'" . $selGenre . "',
	// 		'" . $itProfession . "',
	// 		" . $selValue . ",
	// 		'" . $selTypeContract . "',
	// 		'" . $selPriority . "',
	// 		" . $selVacancies . ",
 //      " . $publish_free_ci . ",
 //      " . $selCity . ",
	// 		" . $_SESSION['user']['id_ci'] . "
	// 	)";
	// 	$result = $functions_sql->functions_sql_execute_query($sql);
	// 	$functions_sql->functions_sql_close_connection();
	// 	return $result;
	// }

  function model_profile_company_insert_ofert_company_prepare($itName, $itaDescription, $itakKowledge, $selGenre,  $itProfession, $selValue, $selTypeContract,  $selPriority, $selVacancies, $selCity, $publish_free) {
    $publish_free_o = ($publish_free == 0) ? 0 : 1;
    $functions_sql = new functions_sql();

    $sql = "INSERT INTO tbl_oferts(
      name_o, 
      description_o, 
      knowledge_o, 
      genre_o, 
      profession_o,  
      value_o,
      type_contract_o,
      priority_o,
      vacancies_o,
      publish_free_o,
      tbl_city_id_c,
      tbl_company_info_id_ci
    )VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    $sentence = $functions_sql->functions_sql_prepare($sql);

    if (!$sentence->bind_param('ssssssssssss', $itName, $itaDescription, $itakKowledge, $selGenre, $itProfession, $selValue, $selTypeContract, $selPriority, $selVacancies, $publish_free_o, $selCity,  $_SESSION['user']['id_ci'])) {
      echo "Falló la vinculación de parámetros: (" . $sentence->errno . ") " . $sentence->error;
    }
    
    $sentence = $functions_sql->functions_sql_execute_query_prepare($sentence);
    $functions_sql->functions_sql_close_connection();
    
    return $sentence->insert_id;
  }

  function model_profile_company_insert_billing($id_o, $cost_o, $iva_offer, $pay_method, $base_value_o) {
    $functions_sql = new functions_sql();

    $sql = "INSERT INTO tbl_system_billing(
      id_ci, 
      id_o, 
      nit_ci, 
      email_ci, 
      name_ci,  
      city_ci,
      address_ci,
      cost_o,
      iva_o,
      base_value_o,
      pay_method_o
    )VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    $sentence = $functions_sql->functions_sql_prepare($sql);

    if (!$sentence->bind_param('sssssssssss', $_SESSION['user']['id_ci'], $id_o, $_SESSION['user']['nit_ci'], $_SESSION['user']['email_ci'], $_SESSION['user']['name_ci'], $_SESSION['user']['city_ci'], $_SESSION['user']['address_ci'], $cost_o, $iva_offer, $base_value_o, $pay_method)) {
      echo "Falló la vinculación de parámetros: (" . $sentence->errno . ") " . $sentence->error;
    }
    
    $sentence = $functions_sql->functions_sql_execute_query_prepare($sentence);
    $functions_sql->functions_sql_close_connection();

    return $sentence->insert_id;
  }


	function model_profile_company_update_ofert_company($itName, $itaDescription, $itakKowledge, $selGenre, $itProfession,  $selValue, $selTypeContract, $selPriority, $selVacancies, $selCity, $id_o) {
		$functions_sql = new functions_sql();
		
    $sql = "UPDATE tbl_oferts SET 
			name_o = '" . $itName . "', 
			description_o = '" . $itaDescription . "', 
			knowledge_o = '" . $itakKowledge . "', 
			genre_o = '" . $selGenre . "', 
			profession_o = '" . $itProfession . "', 
			value_o = '" . $selValue . "', 
			type_contract_o = '" . $selTypeContract . "', 
			priority_o = '" . $selPriority . "', 
			vacancies_o = " . $selVacancies . ", 
			tbl_city_id_c = " . $selCity . "
			WHERE tbl_company_info_id_ci = " . $_SESSION['user']['id_ci'] . " 
			AND id_o = " . $id_o . " ;";

    $modelUtilities = new modelUtilities();
    $modelUtilities->model_utilities_insert_query_register('UPDATE_OFERT', 'SUCCESS', $sql);
		$result = $functions_sql->functions_sql_execute_query($sql);
		$functions_sql->functions_sql_close_connection();
		
		return $result;
	}

  function model_profile_company_validate_offer($id_o) {
    $functions_sql = new functions_sql();
    
    $sql = 'SELECT id_o FROM tbl_oferts 
            WHERE tbl_company_info_id_ci = ' . $_SESSION['user']['id_ci'] . ' 
            AND id_o = ' . $id_o . ';';

    $result = $functions_sql->functions_sql_execute_query($sql);
    
    if ($data = $functions_sql->functions_sql_execute_get_dates($result)) {
          
      $functions_sql->functions_sql_close_query_and_connection($result);
      return TRUE;
    }
    
    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }


  function model_profile_company_remove_offer($id_o) {
    $functions_sql = new functions_sql();

    $sql = 'DELETE FROM tbl_oferts_apply WHERE tbl_oferts_id_o = ' . $id_o . ';';
    $result = $functions_sql->functions_sql_execute_query($sql);

    $sql = "DELETE FROM tbl_oferts WHERE id_o = " . $id_o . " ;";
    $result = $functions_sql->functions_sql_execute_query($sql);

    $functions_sql->functions_sql_close_connection();
    
    return $result;
  }

	// function model_profile_company_unpublish_ofert_company($id_o) {
	// 	$functions_sql = new functions_sql();

	// 	$sql = "UPDATE tbl_oferts SET 
	// 		status_o = 'CLOSE' 
	// 		WHERE tbl_company_info_id_ci = " . $_SESSION['user']['id_ci'] . " 
	// 		AND id_o = " . $id_o . " ;";

 //    $modelUtilities = new modelUtilities();
 //    $modelUtilities->model_utilities_insert_query_register('UNPUBLISH_OFERT', 'PRUEBA', $sql);
	// 	$result = $functions_sql->functions_sql_execute_query($sql);
	// 	$functions_sql->functions_sql_close_connection();
		
	// 	return $result;
	// }

	// function model_profile_company_publish_ofert_company($id_o) {
	// 	$functions_sql = new functions_sql();
		
 //    $sql = "UPDATE tbl_oferts SET 
	// 		status_o = 'OPEN' 
	// 		WHERE tbl_company_info_id_ci = " . $_SESSION['user']['id_ci'] . " 
	// 		AND id_o = " . $id_o . " ;";

 //    $modelUtilities = new modelUtilities();
 //    $modelUtilities->model_utilities_insert_query_register('UNPUBLISH_OFERT', 'PRUEBA', $sql);
	// 	$result = $functions_sql->functions_sql_execute_query($sql);
	// 	$functions_sql->functions_sql_close_connection();
		
	// 	return $result;
	// }

	function model_profile_company_load_apply_ofert_selected($id_ofert, $status = NULL) {
    $functions_sql = new functions_sql();
    
    $sql = 'SELECT id_pi, name_pi, last_name_pi, email_pi, movil_pi, date_applied_oa, status_oa FROM 
    				tbl_oferts_apply INNER JOIN tbl_person_info ON  
						tbl_oferts_apply.tbl_person_info_id_pi = tbl_person_info.id_pi WHERE
						tbl_oferts_id_o = ' . $id_ofert . ' AND status_oa = "' . $status . '" ;';
    
    $result = $functions_sql->functions_sql_execute_query($sql);
    
    if (!empty($result)) {
      $rows = array();
      $num_row = 0;
      
      while($item = $functions_sql->functions_sql_execute_get_dates($result)) {
        $rows[$num_row]['id_pi']           = $item['id_pi'];
        $rows[$num_row]['full_name']       = $item['name_pi'] . ' ' . $item['last_name_pi'];
        $rows[$num_row]['email_pi']        = $item['email_pi'];
        $rows[$num_row]['movil_pi']        = $item['movil_pi'];
        $rows[$num_row]['date_applied_oa'] = $item['date_applied_oa'];
        $num_row++;
      }
      
      $functions_sql->functions_sql_close_query_and_connection($result);
      return $rows;
    }
    
    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }

  function model_profile_company_load_full_data_person($id_person) {
    $functions_sql = new functions_sql();
    
    $sql = 'SELECT  id_pi, 
    								name_pi, 
    								last_name_pi, 
    								email_pi, 
    								phone_pi, 
    								movil_pi, 
    								profession_pi,
    								experience_pi,
                    age_pi,
                    city_pi,
                    address_pi,
    								knowledge_pi,
                    studies_pi
    								FROM 
    				        tbl_person_info	INNER JOIN tbl_city ON tbl_person_info.city_pi = tbl_city.id_c
                    WHERE id_pi = ' . $id_person . ';';

    $result = $functions_sql->functions_sql_execute_query($sql);
    
    if (!empty($result)) {

      $rows = $functions_sql->functions_sql_execute_get_dates($result);     
      $functions_sql->functions_sql_close_query_and_connection($result);
      return $rows;
    }

    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }

  function model_profile_company_update_status_person_for_ofert($status, $id_ofert, $id_person) {
    $functions_sql = new functions_sql();
    
    $sql = 'UPDATE tbl_oferts_apply SET status_oa = "' . $status . '"  WHERE 
    		tbl_person_info_id_pi = ' . $id_person . ' AND tbl_oferts_id_o = ' . $id_ofert . ';';
    
		$result = $functions_sql->functions_sql_execute_query($sql);
		$functions_sql->functions_sql_close_connection();
	
		return $result;
  }


  function model_profile_company_get_info_for_contact_person($id_ofert, $id_person) {
    $functions_sql = new functions_sql();
    $sql = 'SELECT name_pi, 
								   last_name_pi, 
								   email_pi, 
								   name_o, 
								   name_ci, 
								   phone_ci, 
								   movil_ci, 
								   email_ci FROM tbl_oferts_apply 
								   INNER JOIN tbl_oferts ON tbl_oferts_apply.tbl_oferts_id_o = tbl_oferts.id_o 
								   INNER JOIN tbl_person_info ON tbl_oferts_apply.tbl_person_info_id_pi = tbl_person_info.id_pi
								   INNER JOIN tbl_company_info ON tbl_oferts.tbl_company_info_id_ci = tbl_company_info.id_ci WHERE
							     tbl_oferts_apply.tbl_person_info_id_pi = ' . $id_person . ' AND tbl_oferts_apply.tbl_oferts_id_o = ' . $id_ofert . ';';
    $result = $functions_sql->functions_sql_execute_query($sql);
    if (!empty($result)) {
      $rows = array();

      $item = $functions_sql->functions_sql_execute_get_dates($result);
      $rows['name_pi']      = $item['name_pi'];
      $rows['last_name_pi'] = $item['last_name_pi'];
      $rows['email_pi']     = $item['email_pi'];
      $rows['name_o']       = $item['name_o'];
      $rows['name_ci']      = $item['name_ci'];
      $rows['phone_ci']     = $item['phone_ci'];
      $rows['movil_ci']     = $item['movil_ci'];
      $rows['email_ci']     = $item['email_ci'];
        
      $functions_sql->functions_sql_close_query_and_connection($result);
      return $rows;
    }

    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }

  function model_profile_company_get_full_data_company() {
    $functions_sql = new functions_sql();
    
    $sql = 'SELECT * FROM tbl_company_info WHERE id_ci = ' . $_SESSION['user']['id_ci'] . ';';
    $result = $functions_sql->functions_sql_execute_query($sql);
    
    if (!empty($result)) {
      $rows = array();
      
      $item = $functions_sql->functions_sql_execute_get_dates($result);
      $rows['name_ci']       = utf8_encode($item['name_ci']); 
      $rows['description_ci']  = utf8_encode($item['description_ci']);
      $rows['image_ci']      = utf8_encode($item['image_ci']);
      $rows['nit_ci']   = $item['nit_ci'];
      $rows['economic_activity_ci']      = utf8_encode($item['economic_activity_ci']);
      $rows['phone_ci']      = $item['phone_ci'];
      $rows['movil_ci'] = $item['movil_ci'];
      $rows['email_ci']  = $item['email_ci'];
      $rows['password_ci'] = $item['password_ci'];
      $rows['city_ci'] = $item['city_ci'];
      $rows['address_ci'] = $item['address_ci'];
      
      $functions_sql->functions_sql_close_query_and_connection($result);
      return $rows;
    }
    
    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }

  function model_profile_company_update_data_company($itName, $logo, $itEconomicActivity, $itPhone, $itMovil, $itaDescription, $itPassword = NULL, $selCity, $itAddress, $itEmail) { 
    $functions_sql = new functions_sql();

    if ($itPassword == NULL) {
      $sql = "UPDATE tbl_company_info SET 
              name_ci = '" . $itName . "', 
              image_ci = '" . $logo . "', 
              economic_activity_ci = '" . $itEconomicActivity . "', 
              phone_ci = '" . $itPhone . "', 
              movil_ci = '" . $itMovil . "', 
              city_ci = " . $selCity . ", 
              address_ci = '" . $itAddress . "', 
              description_ci = '" . $itaDescription . "',
              email_ci = '" . $itEmail . "'
            WHERE id_ci = " . $_SESSION['user']['id_ci'] . " ;";
    }else {
      $sql = "UPDATE tbl_company_info SET 
              name_ci = '" . $itName . "', 
              image_ci = '" . $logo . "', 
              economic_activity_ci = '" . $itEconomicActivity . "', 
              phone_ci = '" . $itPhone . "', 
              movil_ci = '" . $itMovil . "', 
              city_ci = " . $selCity . ", 
              address_ci = '" . $itAddress . "', 
              description_ci = '" . $itaDescription . "',
              password_ci = '" . $itPassword . "',
              email_ci = '" . $itEmail . "'
            WHERE id_ci = " . $_SESSION['user']['id_ci'] . " ;";
    }

    $result = $functions_sql->functions_sql_execute_query($sql);
    $functions_sql->functions_sql_close_connection();

    return $result;
  }

  /*
   * RECARGAR SALDO
   */
  function model_profile_company_recharge_balance($operation, $id_ci, $nit_ci, $value_recharge) {
    $functions_sql = new functions_sql();
    
    if ($operation == 'SUM') {
      $sql = 'UPDATE tbl_company_info SET 
          available_balance_ci = available_balance_ci + ' . $value_recharge . '
          WHERE id_ci =  ' . $id_ci . ' AND nit_ci =  "' . $nit_ci . '";';

    }else if ($operation == 'SUBTRACT') { 
      $sql = 'UPDATE tbl_company_info SET 
          available_balance_ci = available_balance_ci - ' . $value_recharge . '
          WHERE id_ci =  ' . $id_ci . ' AND nit_ci =  "' . $nit_ci . '";';
    }else {
      return FALSE;
    }
      
    $response = $functions_sql->functions_sql_execute_query($sql);
      
    return $response;
  }

  /*
   * Guardar registro de la Recarga
   */
  function model_profile_company_save_system_recharge_balance($id_transaction, $id_ci, $nit_ci, $name_ci, $email_ci, $value_recharge) {
    $functions_sql = new functions_sql();

    $sql = "INSERT INTO tbl_system_balance_recharge(
      id_transaction_sbr,
      id_ci, 
      nit_ci, 
      name_ci, 
      email_ci, 
      id_su,  
      email_su,
      document_su,
      value_sbr
    )VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";

    $sentence = $functions_sql->functions_sql_prepare($sql);

    if (!$sentence->bind_param('sssssssss', $id_transaction, $id_ci, $nit_ci, $name_ci, $email_ci, $_SESSION['user']['id_su'], $_SESSION['user']['email_su'], $_SESSION['user']['document_su'], $value_recharge)) {
      echo "Falló la vinculación de parámetros: (" . $sentence->errno . ") " . $sentence->error;
    }
    
    $sentence = $functions_sql->functions_sql_execute_query_prepare($sentence);
    $functions_sql->functions_sql_close_connection();
    
    return $sentence->insert_id;
  }

  function model_profile_company_delete_recharge_balance($id_recharge) {
    $functions_sql = new functions_sql();

    $auto_increment = $id_recharge - 1;

    $sql = 'DELETE FROM tbl_system_balance_recharge WHERE id_sbr = ' . $id_recharge . ';';
    $result = $functions_sql->functions_sql_execute_query($sql);

    $sql = 'ALTER TABLE tbl_system_balance_recharge AUTO_INCREMENT = ' . $auto_increment . ';';
    $result = $functions_sql->functions_sql_execute_query($sql);

    $functions_sql->functions_sql_close_connection();
    
    return $result;

  }
  
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
}// EndClass