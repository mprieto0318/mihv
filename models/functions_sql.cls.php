<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/conec.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/controls/control_utilities.cls.php");

class functions_sql {
	private $mysqli_children;
	function __construct() {
		$conec = new conection(); 
		$mysql = $conec->conection();
		$this->mysqli_children = $mysql;
	} 

  function __destruct() {
		#se destruye la clase al momento de no utilizarla solo con tener la funcion
	} 

  function functions_sql_functions_sql_close_execute_query($result){
		 $result->free();
	}

  function functions_sql_execute_get_dates($result) {
		$dates = $result->fetch_assoc();
		return $dates;
	} 

  function functions_sql_execute_query($sql) {
		 $result = $this->mysqli_children->query($sql);
		 return $result;
	}

  function functions_sql_prepare($sql) {
    if (!($sentence = $this->mysqli_children->prepare($sql))) {
       echo "Falló la preparación: (" . $mysqli->errno . ") " . $mysqli->error;
    }
    return $sentence;
  }

  function functions_sql_execute_query_prepare($sentence) {
    if (!$sentence->execute()) {
      echo "Falló la ejecución: (" . $sentence->errno . ") " . $sentence->error;
    }
    return $sentence;
  }

  function functions_sql_close_connection() {
		$this->mysqli_children->close();
	}

	function functions_sql_clear_dates($dates) {
		$values = array();
		foreach ($dates as $key => $value) {
			if (!is_array($value)) {
				// 1. validar sql injection -> real_escape_string
				// 2. checkear que sea texto plano -> control_utilities_check_plain
				// 3. codificar datos -> utf8_decode
				
				$controlUtilities = new controlUtilities();
				$values[$key] = utf8_decode($controlUtilities->control_utilities_check_plain($this->mysqli_children->real_escape_string($dates[$key])));
			}else{
				$values[$key] = $value;
			}	
		}
		return $values;
	}

  function functions_sql_close_query_and_connection($result){
    $this->functions_sql_functions_sql_close_execute_query($result);
    $this->functions_sql_close_connection();
  }

// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
}