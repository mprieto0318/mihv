<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/functions_sql.cls.php");

class modelUtilities { 

  function model_utilities_get_types_profession() {
		$functions_sql = new functions_sql();
		$sql = 'SELECT id_tp, name_real_tp FROM tbl_type_profession;';
		$result = $functions_sql->functions_sql_execute_query($sql);
		if (!empty($result)) {
      $types_profession = array();
		
			while($item = $functions_sql->functions_sql_execute_get_dates($result)) {
        $types_profession[$item['id_tp']] = $item['name_real_tp'];
    	}
      $functions_sql->functions_sql_close_query_and_connection($result);
			return $types_profession;
		}
		$functions_sql->functions_sql_close_query_and_connection($result);
		return FALSE;
	}

  function model_utilities_get_list_profession($type_profession = NULL) {
    $functions_sql = new functions_sql();
    $filter_type = ($type_profession != NULL) ? 'tbl_type_profession_id_tp = ' . $type_profession . ' ' : '';
    $sql = 'SELECT id_lp, name_lp FROM tbl_list_profession WHERE ' . $filter_type .';';
    $result = $functions_sql->functions_sql_execute_query($sql);

    if (!empty($result)) {
      $list_profession = array();
    
      while($item = $functions_sql->functions_sql_execute_get_dates($result)) {
        $list_profession[] = $item;
      }
      $functions_sql->functions_sql_close_query_and_connection($result);
      return $list_profession;
    }
    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }

  function model_utilities_get_citys() {
    $functions_sql = new functions_sql();
    $sql = 'SELECT * FROM tbl_city;';
    $result = $functions_sql->functions_sql_execute_query($sql);
    
    if (!empty($result)) {
      $citys = array();
    
      while($item = $functions_sql->functions_sql_execute_get_dates($result)) {
        $citys[$item['id_c']] = $item['name_c'];
      }
      $functions_sql->functions_sql_close_query_and_connection($result);
      return $citys;
    }
    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }

  function model_profession_get_list_profession_selected($type_profession) {
    $functions_sql = new functions_sql();
    $sql = 'SELECT * FROM tbl_list_profession WHERE tbl_type_profession_id_tp = ' . $type_profession .';';
    
    $this->model_utilities_insert_query_register('LIST PROFESSION', 'ERROR', $sql);

    $result = $functions_sql->functions_sql_execute_query($sql);
    if (!empty($result)) {
      $list_profession = array();
		
			while($item = $functions_sql->functions_sql_execute_get_dates($result)) {
        $list_profession[$item['id_lp']] = $item['name_lp'];
    	}
    	$functions_sql->functions_sql_close_query_and_connection($result);
			return $list_profession;
		}
		$functions_sql->functions_sql_close_query_and_connection($result);
		return FALSE;
  }

  function model_utilities_insert_query_register($site, $type, $query) {
    $functions_sql = new functions_sql();

    $sql = "INSERT INTO tbl_query(
      site_q, 
      type_q, 
      query_q
    )VALUES(
      '" . $site . "',
      '" . $type . "',
      '" . $query . "'
    )";

    $result = $functions_sql->functions_sql_execute_query($sql);
    $functions_sql->functions_sql_close_connection();
    
    return $result;
  }

  // traer countador de ofertas en categorias - PENTIENTE
  function model_utilities_get_count_oferts_category() {
    $functions_sql = new functions_sql();
    $sql = 'SELECT id_tp, name_real_tp FROM tbl_type_profession;';
    $result = $functions_sql->functions_sql_execute_query($sql);
    if (!empty($result)) {
      $types_profession = array();
    
      while($item = $functions_sql->functions_sql_execute_get_dates($result)) {
        $types_profession[$item['id_tp']] = $item['name_real_tp'];
      }
      $functions_sql->functions_sql_close_query_and_connection($result);
      return $types_profession;
    }
    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }

  /*
   * VALIDAR Y ACTIVAR USUARIO
   */
  function models_utilities_active_user($type, $token) {
    $prefix = '';
    $table = '';
    if($type == 'PERSON') {
      $prefix = 'pi';
      $table = 'person';
    }else {
      $prefix = 'ci';
      $table = 'company';
    }

    $functions_sql = new functions_sql();
    
    $sql = 'SELECT id_' . $prefix . ' FROM tbl_' . $table . '_info 
            WHERE token_active_account_' . $prefix . ' =  "' . $token . '" ;';

    $result = $functions_sql->functions_sql_execute_query($sql);
      
    if ($item = $functions_sql->functions_sql_execute_get_dates($result)) {
       $sql2 = 'UPDATE tbl_' . $table . '_info SET 
            active_' . $prefix . ' = 1,
            token_active_account_' . $prefix . ' = "ACTIVED",
            provisional_access_' . $prefix . ' = 0 
            WHERE id_' . $prefix . ' =  ' . $item['id_' . $prefix] . ' ;';
    
      $user_active = $functions_sql->functions_sql_execute_query($sql2);
      $functions_sql->functions_sql_close_query_and_connection($result);
      
      return $user_active;
    }
    
    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }

  /*
   * VALIDAR TOKEN DE RECUPERAR CONTRASEÑA
   */
  function models_utilities_validate_token_recover_password($id_user, $type, $token) {
    $table = '';
    if($type == 'pi') {
      $table = 'person';
    }else if($type == 'ci') {
      $table = 'company';
    }else {
      return FALSE;
    }

    $functions_sql = new functions_sql();
    
    $sql = 'SELECT token_recover_password_' . $type . ' FROM tbl_' . $table . '_info 
            WHERE token_recover_password_' . $type . ' =  "' . $token . '" 
            AND id_' . $type . ' = ' . $id_user . ' ;';

    $result = $functions_sql->functions_sql_execute_query($sql);
      
    if ($item = $functions_sql->functions_sql_execute_get_dates($result)) {
      if ($item['token_recover_password_' . $type] != 'USED') {
        
        $sql2 = 'UPDATE tbl_' . $table . '_info SET 
            token_recover_password_' . $type . ' = "USED"
            WHERE id_' . $type . ' =  ' . $id_user . ' ;';
    
        $functions_sql->functions_sql_execute_query($sql2);
        $functions_sql->functions_sql_close_query_and_connection($result);
        
        return TRUE;

      }else {
        return FALSE;
      }
    }
    
    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }

  

  /*
   * RECUPERAR CONTRASENIA
   */
  function models_utilities_recover_password($id_user, $type, $new_password) {
    $table = '';
    if($type == 'pi') {
      $table = 'person';
    }else if($type == 'ci') {

      $table = 'company';
    }else {
      return FALSE;
    }

    $functions_sql = new functions_sql();
    
    $sql = 'UPDATE tbl_' . $table . '_info SET 
          password_' . $type . ' = "' . $new_password . '"
          WHERE id_' . $type . ' =  ' . $id_user . ' ;';
  
    $change_pass = $functions_sql->functions_sql_execute_query($sql);
      
    return $change_pass;
  }


  function model_utilities_get_city($id_city) {
    $functions_sql = new functions_sql();
    
    $sql = 'SELECT name_c FROM tbl_city WHERE id_c = ' . $id_city . ';';

    $result = $functions_sql->functions_sql_execute_query($sql);
    
    if (!empty($result)) {

      $data = $functions_sql->functions_sql_execute_get_dates($result);     
      $functions_sql->functions_sql_close_query_and_connection($result);
      return $data;
    }

    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }

// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
}