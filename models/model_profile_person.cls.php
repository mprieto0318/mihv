<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/functions_sql.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/models/models_utilities.cls.php");

class modelProfilePerson { 

  function model_profile_person_get_all_oferts_apply_by_user() {
    $functions_sql = new functions_sql();
    
    $sql = 'SELECT tbl_oferts_id_o, name_ci, name_o, date_applied_oa, status_oa FROM 
    tbl_oferts_apply INNER JOIN tbl_oferts ON 
  	tbl_oferts_apply.tbl_oferts_id_o = tbl_oferts.id_o 
  	INNER JOIN tbl_company_info ON tbl_company_info.id_ci = tbl_oferts.tbl_company_info_id_ci  WHERE 
  	status_o = "OPEN" AND tbl_person_info_id_pi = ' . $_SESSION['user']['id_pi'] . ';';

    $result = $functions_sql->functions_sql_execute_query($sql);
    
    if (!empty($result)) {
      $rows = array();
      $num_row = 0;
      
      while($item = $functions_sql->functions_sql_execute_get_dates($result)) {
        $rows[$num_row]['id_o']            = $item['tbl_oferts_id_o'];
        $rows[$num_row]['name_o']          = $item['name_o'];
        $rows[$num_row]['name_ci']         = $item['name_ci'];
        $rows[$num_row]['date_applied_oa'] = $item['date_applied_oa'];
        $rows[$num_row]['status_oa']       = $item['status_oa'];
        $num_row++;
      }

      $functions_sql->functions_sql_close_query_and_connection($result);
      return $rows;
    }

    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }

	function model_profile_person_get_full_data_person() {
    $functions_sql = new functions_sql();
    
    $sql = 'SELECT * FROM tbl_person_info WHERE id_pi = ' . $_SESSION['user']['id_pi'] . ';';
    $result = $functions_sql->functions_sql_execute_query($sql);
    
    if (!empty($result)) {
      $item = $functions_sql->functions_sql_execute_get_dates($result);
      $functions_sql->functions_sql_close_query_and_connection($result);
      
      return $item;
    }

    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }

  function model_profile_person_update_data_person($itName, $itLastName, $itPhone, $itMovil, $itExperience, $itEmail,  $itPassword, $itaKnowledge, $itaStudies, $age, $itProfession, $selCity, $itAddress) {
		$functions_sql = new functions_sql();

		if ($itPassword == NULL) {
			$sql = "UPDATE tbl_person_info SET 
							name_pi = '" . $itName . "', 
							last_name_pi = '" . $itLastName . "', 
							email_pi = '" . $itEmail . "',
							phone_pi = '" . $itPhone . "', 
							movil_pi = '" . $itMovil . "', 
							experience_pi = '" . $itExperience . "', 
							knowledge_pi = '" . $itaKnowledge . "',
              studies_pi = '" . $itaStudies . "',
              age_pi = " . $age . ",
              city_pi = " . $selCity . ",
              address_pi = '" . $itAddress . "',
							profession_pi = '" . $itProfession . "'
						WHERE id_pi = " . $_SESSION['user']['id_pi'] . " ;";
		}else {
			$sql = "UPDATE tbl_person_info SET 
							name_pi = '" . $itName . "', 
							last_name_pi = '" . $itLastName . "', 
							email_pi = '" . $itEmail . "', 
							password_pi = '" . $itPassword . "', 
							phone_pi = '" . $itPhone . "', 
							movil_pi = '" . $itMovil . "', 
							experience_pi = '" . $itExperience . "', 
							knowledge_pi = '" . $itaKnowledge . "',
              studies_pi = '" . $itaStudies . "',
              age_pi = " . $age . ",
              city_pi = " . $selCity . ",
              address_pi = '" . $itAddress . "',
              profession_pi = '" . $itProfession . "' 
						WHERE id_pi = " . $_SESSION['user']['id_pi'] . " ;";
		}

		$result = $functions_sql->functions_sql_execute_query($sql);
		$functions_sql->functions_sql_close_connection();

		return $result;
	}
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
}// EndClass