<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/functions_sql.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/models/models_utilities.cls.php");

class modelOfertOperations { 

  function model_ofert_operations_apply_ofert($id_ofert) {
		$functions_sql = new functions_sql();
		$sql = "INSERT INTO tbl_oferts_apply(
			tbl_person_info_id_pi,
			tbl_oferts_id_o
    )VALUES(
			" . $_SESSION['user']['id_pi'] . ",
			" . $id_ofert . "
		)";

		$result = $functions_sql->functions_sql_execute_query($sql);
		$functions_sql->functions_sql_close_connection();
		
		return $result;
	}

	function model_ofert_operations_disapply_ofert($id_o) {
		$functions_sql = new functions_sql();
		
		$sql = 'DELETE FROM tbl_oferts_apply WHERE 
						tbl_person_info_id_pi = ' . $_SESSION['user']['id_pi'] . ' AND tbl_oferts_id_o = ' . $id_o . ';';

    // $modelUtilities = new modelUtilities();
    // $modelUtilities->model_utilities_insert_query_register('DISAPPLY_OFERT', 'ID: ' . $id_o . ' - USER: ' . $_SESSION['user']['id_pi'], $sql);
		$result = $functions_sql->functions_sql_execute_query($sql);
		$functions_sql->functions_sql_close_connection();
		
		return $result;
	}

	function model_profile_company_update_offer_life_cycle($id_o, $life_cycle) {
		$functions_sql = new functions_sql();
		$sql = "UPDATE tbl_oferts SET 
			offer_life_cycle_o = " . $life_cycle . " 
			WHERE id_o = " . $id_o . " ;";

    // $modelUtilities = new modelUtilities();
    // $modelUtilities->model_utilities_insert_query_register('UNPUBLISH_OFERT', 'PRUEBA', $sql);
		$result = $functions_sql->functions_sql_execute_query($sql);
		$functions_sql->functions_sql_close_connection();
		
		return $result;
	}


 //  // COMENTANDO DESPUBLICAR OFERTA
	// function model_profile_company_unpublish_ofert_company($id_o) {
	// 	$functions_sql = new functions_sql();
	// 	$sql = "UPDATE tbl_oferts SET 
	// 		status_o = 'CLOSE' 
	// 		WHERE tbl_company_info_id_ci = " . $_SESSION['user']['id_ci'] . " 
	// 		AND id_o = " . $id_o . " ;";

 //    $modelUtilities = new modelUtilities();
 //    $modelUtilities->model_utilities_insert_query_register('UNPUBLISH_OFERT', 'PRUEBA', $sql);
	// 	$result = $functions_sql->functions_sql_execute_query($sql);
	// 	$functions_sql->functions_sql_close_connection();
		
	// 	return $result;
	// }

	// function model_profile_company_publish_ofert_company($id_o) {
	// 	$functions_sql = new functions_sql();
	// 	$sql = "UPDATE tbl_oferts SET 
	// 		status_o = 'OPEN' 
	// 		WHERE tbl_company_info_id_ci = " . $_SESSION['user']['id_ci'] . " 
	// 		AND id_o = " . $id_o . " ;";

 //    $modelUtilities = new modelUtilities();
 //    $modelUtilities->model_utilities_insert_query_register('UNPUBLISH_OFERT', 'PRUEBA', $sql);
	// 	$result = $functions_sql->functions_sql_execute_query($sql);
	// 	$functions_sql->functions_sql_close_connection();
		
	// 	return $result;
	// }
// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com
}// EndClass
   
