<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once($_SERVER['DOCUMENT_ROOT'] . "/models/functions_sql.cls.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/models/models_utilities.cls.php");

class modelLoadOferts { 

	function model_load_oferts_count_all_oferts(){
    $functions_sql = new functions_sql();
    $sql = 'SELECT COUNT(*) FROM tbl_oferts;';

    $execute = $functions_sql->functions_sql_execute_query($sql);
    $result = $functions_sql->functions_sql_execute_get_dates($execute);
    if (!empty($result) && isset($result)) {
      return $result;
    }
    return null;
  }

  function model_load_oferts_count_search_oferts($itSearch, $filters = 'NO', $selCity = NULL, $selDate = NULL, $selGenre = NULL, $selSalary = NULL) {
    $functions_sql = new functions_sql();

    if ($filters == 'NO') {
      $sql = 'SELECT COUNT(*) FROM tbl_oferts WHERE 
      status_o = "OPEN" AND 
      name_o LIKE "%' . $itSearch . '%" AND tbl_city_id_c > 0;';
    }else {
      $query_filters = '';
      
      if ($selCity == 'all') {
        $query_filters .= 'AND tbl_city_id_c > 0 ';
      }else {
        $query_filters .= 'AND tbl_city_id_c = ' . $selCity . ' ';
      }
      if ($selDate != 'all') {
        $query_filters .= 'AND created_o <= "' . $selDate . '" ';
      }
      if ($selGenre != 'all') {
        $query_filters .= 'AND genre_o = "' . $selGenre . '" ';
      }
      if ($selSalary != 'all') {
        $query_filters .= 'AND value_o >= ' . $selSalary . ' ';
      }

      $sql = 'SELECT COUNT(*) FROM tbl_oferts WHERE 
      status_o = "OPEN" AND 
      name_o LIKE "%' . $itSearch . '%" ' . $query_filters . ';';
    }

    $modelUtilities = new modelUtilities();
    $modelUtilities->model_utilities_insert_query_register('COUNT OFERTS', 'SUCCESS', $sql);

    $execute = $functions_sql->functions_sql_execute_query($sql);
    $result = $functions_sql->functions_sql_execute_get_dates($execute);
    if (!empty($result) && isset($result)) {
      return $result;
    }
    return null;
  }

  function model_load_oferts_search_oferts($position, $item_per_page, $itSearch = NULL, $filters = 'NO', $selCity = NULL, $selDate = NULL, $selGenre = NULL, $selSalary = NULL) {

    $functions_sql = new functions_sql();

    if ($filters == 'NO') {
      $sql = 'SELECT id_o, name_o, name_ci, created_o, image_ci, description_o FROM tbl_oferts INNER JOIN tbl_company_info ON 
      tbl_oferts.tbl_company_info_id_ci=tbl_company_info.id_ci WHERE 
      status_o = "OPEN" AND 
      name_o LIKE "%' . $itSearch . '%" AND tbl_city_id_c > 0 
      AND priority_o = "na" 
      ORDER BY id_o DESC LIMIT  ' . $position . ',' . $item_per_page . ';';

    }else {
      $query_filters = '';
      
      if ($selCity == 'all') {
        $query_filters .= 'AND tbl_city_id_c > 0 ';
      }else {
        $query_filters .= 'AND tbl_city_id_c = ' . $selCity . ' ';
      }
      if ($selDate != 'all') {
        $query_filters .= 'AND created_o <= "' . $selDate . '" ';
      }
      if ($selGenre != 'all') {
        $query_filters .= 'AND genre_o = "' . $selGenre . '" ';
      }
      if ($selSalary != 'all') {
        $query_filters .= 'AND value_o >= ' . $selSalary . ' ';
      }

      $sql = 'SELECT * FROM tbl_oferts INNER JOIN tbl_company_info ON 
      tbl_oferts.tbl_company_info_id_ci=tbl_company_info.id_ci WHERE 
      status_o = "OPEN" AND 
      name_o LIKE "%' . $itSearch . '%" ' . $query_filters . ' 
      AND priority_o = "na" ORDER BY id_o DESC LIMIT  ' . $position . ',' . $item_per_page . ';';
    }
    
    // $modelUtilities = new modelUtilities();
    // $modelUtilities->model_utilities_insert_query_register('GET OFERTS', 'ERROR', $sql);

    $result = $functions_sql->functions_sql_execute_query($sql);
    if (!empty($result)) {
      $rows = array();
      while($item = $functions_sql->functions_sql_execute_get_dates($result)) {
        $rows[] = $item;
      }
      $functions_sql->functions_sql_close_query_and_connection($result);
      return $rows;
    }
    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }

  function model_load_oferts_search_oferts_priority($itSearch = NULL, $filters = 'NO', $selCity = NULL, $selDate = NULL, $selGenre = NULL, $selSalary = NULL) {
    $functions_sql = new functions_sql();

    if ($filters == 'NO') {
      $sql = 'SELECT id_o, name_o, name_ci, created_o, image_ci, description_o FROM tbl_oferts INNER JOIN tbl_company_info ON 
      tbl_oferts.tbl_company_info_id_ci=tbl_company_info.id_ci WHERE 
      status_o = "OPEN" AND 
      name_o LIKE "%' . $itSearch . '%" AND tbl_city_id_c > 0 
      AND priority_o = "urgent"  ORDER BY id_o DESC;';

    }else {
      $query_filters = '';
      
      if ($selCity == 'all') {
        $query_filters .= 'AND tbl_city_id_c > 0 ';
      }else {
        $query_filters .= 'AND tbl_city_id_c = ' . $selCity . ' ';
      }
      if ($selDate != 'all') {
        $query_filters .= 'AND created_o <= "' . $selDate . '" ';
      }
      if ($selGenre != 'all') {
        $query_filters .= 'AND genre_o = "' . $selGenre . '" ';
      }
      if ($selSalary != 'all') {
        $query_filters .= 'AND value_o >= ' . $selSalary . ' ';
      }

      $sql = 'SELECT * FROM tbl_oferts INNER JOIN tbl_company_info ON 
      tbl_oferts.tbl_company_info_id_ci=tbl_company_info.id_ci WHERE 
      status_o = "OPEN" AND 
      name_o LIKE "%' . $itSearch . '%" ' . $query_filters . ' 
      AND priority_o = "urgent"  ORDER BY id_o DESC;';
    }

    $modelUtilities = new modelUtilities();
    $modelUtilities->model_utilities_insert_query_register('GET OFERTS', 'ERROR', $sql);

    $result = $functions_sql->functions_sql_execute_query($sql);
    if (!empty($result)) {
      $rows = array();
      while($item = $functions_sql->functions_sql_execute_get_dates($result)) {
        $rows[] = $item;
      }
      $functions_sql->functions_sql_close_query_and_connection($result);
      return $rows;
    }
    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }


  function model_load_oferts_get_full_data_ofert_seleted($id_ofert) {
    $functions_sql = new functions_sql();

    $sql = 'SELECT * FROM tbl_oferts INNER JOIN tbl_company_info ON 
      tbl_oferts.tbl_company_info_id_ci=tbl_company_info.id_ci
      INNER JOIN tbl_city ON tbl_oferts.tbl_city_id_c = tbl_city.id_c
      WHERE id_o = ' . $id_ofert. ';';

    $result = $functions_sql->functions_sql_execute_query($sql);

    if ($item = $functions_sql->functions_sql_execute_get_dates($result)) {
      $rows = array();
      
      // Informacion de la oferta
      $rows['id_o']                 = $item['id_o'];
      $rows['name_o']               = $item['name_o'];
      $rows['description_o']        = $item['description_o'];
      $rows['knowledge_o']          = $item['knowledge_o'];
      $rows['genre_o']              = $item['genre_o'];
      $rows['profession_o']         = $item['profession_o'];
      $rows['value_o']              = $item['value_o'];
      $rows['type_contract_o']      = $item['type_contract_o'];
      $rows['priority_o']           = $item['priority_o'];
      $rows['vacancies_o']          = $item['vacancies_o'];
      $rows['created_o']       = $item['created_o'];
      $rows['status_o']             = $item['status_o'];

      // Informacion de ciudad
      $rows['id_c']                 = $item['id_c'];
      $rows['name_c']               = $item['name_c'];
      
      // Informacion de la empresa
      $rows['id_ci']                = $item['id_ci'];
      $rows['name_ci']              = $item['name_ci'];
      $rows['description_ci']       = $item['description_ci'];
      $rows['image_ci']             = $item['image_ci'];
      $rows['economic_activity_ci'] = $item['economic_activity_ci'];
      $rows['phone_ci']             = $item['phone_ci'];
      $rows['movil_ci']             = $item['movil_ci'];
    
      $functions_sql->functions_sql_close_query_and_connection($result);
      return $rows;
    }
    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }

  function model_load_oferts_validate_apply_ofert_seleted($id_ofert) {
    $functions_sql = new functions_sql();

    $sql = 'SELECT COUNT(id_oa) FROM tbl_oferts_apply INNER JOIN tbl_oferts ON 
            tbl_oferts_apply.tbl_oferts_id_o = tbl_oferts.id_o WHERE 
            tbl_person_info_id_pi = ' . $_SESSION['user']["id_pi"] . ' AND tbl_oferts_id_o = ' . $id_ofert . ' AND status_o = "OPEN";';

    $result = $functions_sql->functions_sql_execute_query($sql);
    $exists = $functions_sql->functions_sql_execute_get_dates($result);
    $functions_sql->functions_sql_close_query_and_connection($result);

    return $exists;
  }

  function model_load_oferts_priority_blocks() {
    $functions_sql = new functions_sql();

    $sql = 'SELECT id_o, name_o, description_o, name_ci, image_ci, name_c FROM tbl_oferts INNER JOIN tbl_company_info ON 
      tbl_oferts.tbl_company_info_id_ci=tbl_company_info.id_ci
      INNER JOIN tbl_city ON tbl_oferts.tbl_city_id_c = tbl_city.id_c
      WHERE status_o = "OPEN" AND priority_o = "urgent" ORDER BY id_o DESC LIMIT 4 ;';

    $result = $functions_sql->functions_sql_execute_query($sql);
    if (!empty($result)) {
      $rows = array();

      while($item = $functions_sql->functions_sql_execute_get_dates($result)) {
        $rows[] = $item;
      }
    
      $functions_sql->functions_sql_close_query_and_connection($result);
      return $rows;
    }
    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }

  function model_load_oferts_priority_blocks_complete($limit) {
    $functions_sql = new functions_sql();

    $sql = 'SELECT id_o, name_o, description_o, name_ci, image_ci, name_c FROM tbl_oferts INNER JOIN tbl_company_info ON 
      tbl_oferts.tbl_company_info_id_ci=tbl_company_info.id_ci
      INNER JOIN tbl_city ON tbl_oferts.tbl_city_id_c = tbl_city.id_c
      WHERE status_o = "OPEN" AND  priority_o = "na" ORDER BY id_o DESC LIMIT ' . $limit . ' ;';

    $result = $functions_sql->functions_sql_execute_query($sql);
    if (!empty($result)) {
      $rows = array();

      while($item = $functions_sql->functions_sql_execute_get_dates($result)) {
        $rows[] = $item;
      }
    
      $functions_sql->functions_sql_close_query_and_connection($result);
      return $rows;
    }

    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }


  function model_load_offers_life_cycle($life_cycle, $time){
    $functions_sql = new functions_sql();
    
    $sql = 'SELECT id_o, name_o, id_ci, email_ci, name_ci FROM tbl_oferts INNER JOIN tbl_company_info ON 
      tbl_oferts.tbl_company_info_id_ci=tbl_company_info.id_ci
      WHERE status_o = "OPEN" AND offer_life_cycle_o = ' . $life_cycle . ' 
      AND created_o <= NOW() - INTERVAL ' . $time . ';';

    $result = $functions_sql->functions_sql_execute_query($sql);
    if (!empty($result)) {
      $rows = array();
      while($item = $functions_sql->functions_sql_execute_get_dates($result)) {
        $rows[] = $item;
      }
      $functions_sql->functions_sql_close_query_and_connection($result);
      return $rows;
    }
    $functions_sql->functions_sql_close_query_and_connection($result);
    return FALSE;
  }

// Desarrollado por Miguel Angel Prieto Mendez
// Email: prieto.miguel0318@gmail.com  
}